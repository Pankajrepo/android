package com.brilliance.dualiit.repositories

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import com.brilliance.dualiit.data.InterestPagingSource
import com.brilliance.dualiit.data.addressbook.AddressBookPagingSource
import com.brilliance.dualiit.data.conversation.RecentChatsPagingSource
import com.brilliance.dualiit.data.invitation.InvitationPagingSource
import com.brilliance.dualiit.data.meeting.MeetingPagingSource
import com.brilliance.dualiit.data.notifications.NotificationPagingSource
import com.brilliance.dualiit.model.BaseResponse
import com.brilliance.dualiit.model.SearchData
import com.brilliance.dualiit.model.UserModel
import com.brilliance.dualiit.ui.chat.MessagesPagingSource
import com.brilliance.dualiit.ui.chat.models.ConversationModel
import com.brilliance.dualiit.ui.speeddating.models.SessionResponse
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.networkRequest.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class HomeRepository(
    private val api: ApiService,
    private val prefs: AppPreferencesHelper
) {


    fun getInterests(query: String) =
        Pager(
            config = PagingConfig(
                pageSize = 10,
                maxSize = 200,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { InterestPagingSource(api, prefs, query) }
        ).liveData

    fun getInvitations() =
        Pager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { InvitationPagingSource(api, prefs) }
        ).liveData

    fun getNotificationsList() =
        Pager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { NotificationPagingSource(api, prefs) }
        ).liveData

    fun getUserMessages(key: String) =
        Pager(
            config = PagingConfig(
                pageSize = 50,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { MessagesPagingSource(api, prefs, key) }
        ).liveData

    fun getRecentChatsList() =
        Pager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { RecentChatsPagingSource(api, prefs) }
        ).liveData

    fun getFriendsList(query: String) =
        Pager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { AddressBookPagingSource(api, prefs, 1, query) }
        ).liveData

    fun getBlockedList(query: String) =
        Pager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { AddressBookPagingSource(api, prefs, 4, query) }
        ).liveData

    fun getMeetingsList() =
        Pager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { MeetingPagingSource(api, prefs) }
        ).liveData

//    fun searchUsers(query: String) =
//        Pager(
//            config = PagingConfig(
//                pageSize = 10,
//                maxSize = 200,
//                enablePlaceholders = false
//            ),
//            pagingSourceFactory = { SearchUsersPagingSource(api, prefs, query) }
//        ).liveData

//    suspend fun getSuggestedUsers(): BaseResponse<List<UserModel>> {
//        val map = HashMap<String, String>()
//        map["Authorization"] = prefs.authToken
//        return api.getSuggestedUsers(map)
//    }

    fun getSuggestedUsers(): Flow<BaseResponse<List<UserModel>>> = flow {
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        emit(api.getSuggestedUsers(map))
    }.flowOn(Dispatchers.IO)

    fun updateNotificationStatus(userModel: UserModel): Flow<BaseResponse<UserModel>> =
        flow {
            val map = HashMap<String, String>()
            map["Authorization"] = prefs.authToken
            emit(api.updateNotification(map, userModel))
        }.flowOn(Dispatchers.IO)

    suspend fun deleteConversationHistory(key: String): Flow<BaseResponse<Any>> =
        flow {
            val map = HashMap<String, String>()
            map["Authorization"] = prefs.authToken
            emit(api.deleteConversationHistory(map, key))
        }.flowOn(Dispatchers.IO)

    suspend fun deleteChat(key: String): Flow<BaseResponse<Any>> =
        flow {
            val map = HashMap<String, String>()
            map["Authorization"] = prefs.authToken
            emit(api.deleteChat(map, key))
        }.flowOn(Dispatchers.IO)

    suspend fun getSearchedUsersList(query: String): BaseResponse<SearchData> {
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        return api.searchUsers(headers = map, query = query)
    }

//    suspend fun getRecentChatsList(): BaseResponse<RecentChatsModel> {
//        val map = HashMap<String, String>()
//        map["Authorization"] = prefs.authToken
//        return api.getRecentChats(map)
//    }

//    suspend fun getFriendsList(): BaseResponse<AddressBookModel> {
//        val map = HashMap<String, String>()
//        map["Authorization"] = prefs.authToken
//        return api.getFriendsList(map)
//    }

    suspend fun getOtherUserProfile(id: String): BaseResponse<UserModel> {
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        return api.getOtherUserProfile(map, id)
    }

    suspend fun updateProfile(hashMap: HashMap<String, Any>): BaseResponse<UserModel> {
        val map = HashMap<String, String>()
        map["Authorization"] = "Bearer ${prefs.userModel?.accessToken}"
        return api.updateProfile(map, hashMap)
    }

    suspend fun getUserChats(key: String, last: Int): BaseResponse<ConversationModel> {
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        return api.getUserChats(headers = map, key = key, last = last)
    }

    suspend fun getPaginatedUserChats(
        key: String,
        page: Int,
        last: Int
    ): BaseResponse<ConversationModel> {
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        return api.getUserChats(headers = map, key = key, page = page, last = last)
    }

//    suspend fun getInvitationsList(): BaseResponse<InvitationResponse> {
//        val map = HashMap<String, String>()
//        map["Authorization"] = prefs.authToken
//        return api.getInvitationsList(headers = map)
//    }

    suspend fun acceptOrRejectInvitation(
        inviteId: String,
        hashMap: HashMap<String, Any>
    ): BaseResponse<Any> {
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        return api.acceptOrRejectInvitation(headers = map, inviteId = inviteId, fields = hashMap)
    }

    suspend fun acceptOrRejectMeeting(
        meetingId: String,
        status: Int
    ): BaseResponse<Any> {
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        return api.acceptOrRejectMeeting(headers = map, meetingId = meetingId, status = status)
    }

    suspend fun updateMeeting(
        meetingId: String,
        fields: HashMap<String, Any>
    ): BaseResponse<Any> {
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        return api.updateMeeting(headers = map, meetingId = meetingId, fields = fields)
    }

    suspend fun unInviteUser(friendId: String): Flow<BaseResponse<Any>> = flow {
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        emit(api.unInvite(map, friendId))
    }.flowOn(Dispatchers.IO)

    suspend fun deleteMeeting(meetingId: String): Flow<BaseResponse<Any>> = flow {
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        emit(api.deleteMeeting(map, meetingId))
    }.flowOn(Dispatchers.IO)

    suspend fun sendInvitation(friendId: String): BaseResponse<Any> {
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        return api.sendInvitation(headers = map, friendId = friendId)
    }

//    suspend fun getBlockedList(): BaseResponse<AddressBookModel> {
//        val map = HashMap<String, String>()
//        map["Authorization"] = prefs.authToken
//        return api.getBlockedList(map)
//    }

    suspend fun blockOrUnblockFriend(status: Int, friendId: String): BaseResponse<Any> {
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        return api.blockOrUnblockFriend(map, status, friendId)
    }

    suspend fun createMeeting(fields: HashMap<String, Any>): BaseResponse<Any> {
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        return api.createMeeting(map, fields)
    }

    suspend fun createSessionToken(): BaseResponse<SessionResponse> {
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        return api.createSessionToken(map)
    }

    suspend fun generateTokenWithSession(sessionId: String): BaseResponse<SessionResponse> {
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        return api.generateTokenWithSession(map, sessionId)
    }
}