package com.brilliance.dualiit.interfaces

import android.location.Location

interface AddressCallback {
    fun onGetCountry(country:String,state:String,location:Location?)
}