package com.brilliance.dualiit.interfaces;

public interface OnOtpCompletionListener {
  void onOtpCompleted(String otp);

}