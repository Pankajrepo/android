package com.brilliance.dualiit.interfaces

interface OnItemClickListener {
    fun onButtonMainClick( activity_name:String)
    fun onDataClick(text:String,pos:Int,key_text:String)
}