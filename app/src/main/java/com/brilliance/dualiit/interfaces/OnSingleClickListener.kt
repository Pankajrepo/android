package com.brilliance.dualiit.interfaces

import android.view.View
import android.os.SystemClock

abstract class OnSingleClickListener : View.OnClickListener {
    private val GAP = 700L
    private var prevClickTime = 0L
    override fun onClick(v: View) {
        doClick(v)
    }
    @Synchronized
    private fun doClick(v: View) {
        val current = SystemClock.elapsedRealtime()
        if (current - prevClickTime > GAP) {
            prevClickTime = current
            onSingleClick(v)
        }
    }
    abstract fun onSingleClick(v: View)
}