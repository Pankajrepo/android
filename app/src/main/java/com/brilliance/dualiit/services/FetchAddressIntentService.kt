package com.brilliance.dualiit.services

import android.app.IntentService
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.ResultReceiver
import com.brilliance.dualiit.utils.AppConstant
import timber.log.Timber
import java.util.*

class FetchAddressIntentService : IntentService("FetchAddressIntentService") {

    private var resultReceiver: ResultReceiver? = null

    override fun onHandleIntent(intent: Intent?) {
        if (intent != null) {
            var errorMessage = ""
            resultReceiver = intent.getParcelableExtra(AppConstant.RECEIVER)
            val location = intent.getParcelableExtra<Location>(AppConstant.LOCATION_DATA_EXTRA)
            if (location == null) {
                errorMessage = "no_location_data_provided"
                Timber.d(errorMessage)
                return
            }
            val geoCoder = Geocoder(this, Locale.getDefault())
            var addresses: List<Address> = emptyList()
            try {
                addresses = geoCoder.getFromLocation(location.latitude, location.longitude, 1)
            } catch (e: Exception) {
                errorMessage = e.message.toString()
                Timber.d(errorMessage)
            }

            if (addresses.isEmpty()) {
                deliverResultToReceiver(AppConstant.FAILURE_RESULT, errorMessage)
            } else {
                val address = addresses[0]
                val addressToSend =
                    "${address.locality}, ${address.countryName}, ${address.countryCode}"
                val addressFragments = with(address) {
                    (0..maxAddressLineIndex).map { getAddressLine(it) }
                }
                deliverResultToReceiver(
                    AppConstant.SUCCESS_RESULT,
                    addressToSend
//                    addressFragments.joinToString(separator = "\n")
                )
            }

        }
    }

    private fun deliverResultToReceiver(resultCode: Int, message: String) {
        val bundle = Bundle().apply { putString(AppConstant.RESULT_DATA_KEY, message) }
        resultReceiver?.send(resultCode, bundle)
    }

}