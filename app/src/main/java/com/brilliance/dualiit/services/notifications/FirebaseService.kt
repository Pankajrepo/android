package com.brilliance.dualiit.services.notifications

import android.content.Intent
import android.util.Log
import com.brilliance.dualiit.ui.chat.ChatActivity
import com.google.firebase.messaging.FirebaseMessagingService

class FirebaseService : FirebaseMessagingService() {

    override fun handleIntent(intent: Intent?) {
        super.handleIntent(intent)

        Log.d("FirebaseMessaging", "MSG ${intent?.extras}")

        when (intent?.extras?.get("type")) {
            "message" -> {
            }
        }

    }
}