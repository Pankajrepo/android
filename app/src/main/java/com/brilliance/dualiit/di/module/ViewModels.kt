package com.brilliance.dualiit.di.module

import com.brilliance.dualiit.repositories.HomeRepository
import com.brilliance.dualiit.ui.auth.AuthRepository
import com.brilliance.dualiit.ui.auth.viewmodel.AuthViewModel
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModels = module {

    single {
        HomeRepository(api = get(), prefs = get())
    }

    single {
        AuthRepository(apiService = get(), prefs = get())
    }

    viewModel {
        AuthViewModel(api = get(), prefs = get(), repository = get())
    }

    viewModel {
        HomeViewModel(prefs = get(), repository = get())
    }

}