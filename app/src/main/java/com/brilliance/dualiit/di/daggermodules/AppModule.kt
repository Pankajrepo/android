package com.brilliance.dualiit.di.daggermodules

import android.content.Context
import androidx.multidex.BuildConfig
import com.brilliance.dualiit.utils.AppPreferencesHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.engineio.client.transports.WebSocket
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideSharedPreferences(
        @ApplicationContext context: Context
    ): AppPreferencesHelper {
        val prefs = context.getSharedPreferences(
            "${BuildConfig.APPLICATION_ID}_store",
            Context.MODE_PRIVATE
        )
        return AppPreferencesHelper(prefs)
    }

    @Singleton
    @Provides
    fun provideSocketClient(
        prefs: AppPreferencesHelper
    ): Socket {
        val authToken = if (prefs.authToken.isNotEmpty()) prefs.authToken.substring(7) else ""
        val options = IO.Options().apply {
            query = "token=${authToken}"
            transports = arrayOf(WebSocket.NAME)
            reconnection = true
            timeout = -1
            reconnectionDelay = 1000
        }
        return IO.socket("http://13.235.99.21:5000", options)
    }
}