package com.brilliance.dualiit.di.module

import android.content.Context
import androidx.multidex.BuildConfig
import com.brilliance.dualiit.utils.AppPreferencesHelper
import io.socket.client.IO
import io.socket.engineio.client.transports.WebSocket
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val appModule = module {
    single {
        androidContext().getSharedPreferences(
            "${BuildConfig.APPLICATION_ID}_store",
            Context.MODE_PRIVATE
        )
    }
    single {
        AppPreferencesHelper(get())
    }

//    single {
//        val prefs: AppPreferencesHelper = get()
//        val authToken = if (prefs.authToken.isNotEmpty()) prefs.authToken.substring(7) else ""
//        val options = IO.Options().apply {
//            query = "token=${authToken}"
//            transports = arrayOf(WebSocket.NAME)
//            reconnection = true
//            timeout = -1
//            reconnectionDelay = 1000
//        }
//        IO.socket("http://13.235.99.21:5000", options)
//    }
}