package com.brilliance.dualiit.di.module

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import com.brilliance.dualiit.BuildConfig
import com.brilliance.dualiit.ui.auth.activities.ChooseLoginSignup
import com.google.gson.GsonBuilder
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.networkRequest.ApiService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import timber.log.Timber
import java.io.IOException
import java.util.concurrent.TimeUnit

val networkModule = module {
    single {
        val fac = GsonConverterFactory.create(GsonBuilder().setLenient().create())
        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_API_URL)
            .client(get())
            .addConverterFactory(fac)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .build()
    }

    single {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        OkHttpClient.Builder().addInterceptor(logging)
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(ConnectivityInterceptor(androidContext().applicationContext))
            .addInterceptor { chain ->
                if (BuildConfig.DEBUG)
                    Timber.tag("AUTH_TOKEN").d(AppPreferencesHelper(get()).authToken)
                val request = chain.request().newBuilder()
                    .addHeader("Authorization", AppPreferencesHelper(get()).authToken)
                    .addHeader("platform", "android")
                    .build()

                val response = chain.proceed(request)
                if (response.code == 401) {
                    Log.d("UN-AUTH", "401 Unauthorized error")
                    Log.d("UN-AUTH", "AUTH_TOKEN: ${(AppPreferencesHelper(get()).authToken)}")
                }

                response
            }.build()
    }

    single {
        val ret: Retrofit = get()
        ret.create(ApiService::class.java)
    }
}

class ConnectivityInterceptor(private val context: Context) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!isOnline(context)) {
            throw NoConnectivityException()
        }
        val builder: Request.Builder = chain.request().newBuilder()
        return chain.proceed(builder.build())
    }

    private fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val networkCapabilities =
                connectivityManager.getNetworkCapabilities(network) ?: return false
            return when {
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                //for other device how are able to connect with Ethernet
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                //for check internet over Bluetooth
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) -> true
                else -> false
            }
        } else {
            val nwInfo = connectivityManager.activeNetworkInfo ?: return false
            return nwInfo.isConnected
        }
    }
}

class NoConnectivityException : IOException() {
    override val message: String?
        get() = "No network available, please check your WiFi or Data connection"
}