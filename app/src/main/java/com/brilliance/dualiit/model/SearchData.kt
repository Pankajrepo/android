package com.brilliance.dualiit.model

import androidx.annotation.Keep

@Keep
data class SearchData(
    val docs: List<SearchDoc>? = null,
    val limit: Int? = 0,
    val nextPage: Int? = 0,
    val page: Int? = 0,
    val prevPage: Any? = null,
    val totalDocs: Int? = 0,
    val totalPages: Int? = 0
)