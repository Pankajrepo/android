package com.brilliance.dualiit.model

import androidx.annotation.Keep

@Keep
data class Contact(
    val name: String? = "",
    val phone: String? = "",
    val userPhoto: String? = ""
)