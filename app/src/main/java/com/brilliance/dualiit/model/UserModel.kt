package com.brilliance.dualiit.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class UserModel(
    @SerializedName("_id")
    var id: String? = "",
    var isVerifiedEmail: Boolean? = false,
    var interest: List<String>? = null,
    var orientation: String? = "",
    var bio: String? = "",
    var relationshipStatus: String? = "",
    var locale: String? = "",
    var accountType: String? = "",
    var status: Int? = null,
    var phone: String? = "",
    var email: String? = "",
    var deviceToken: String? = "",
    var appVersion: String? = "",
    var deviceType: String? = "",
    var createdAt: String? = "",
    var updatedAt: String? = "",
    var regionCode: String? = "",
    var lastLogin: String? = "",
    var age: Int? = 0,
    var area: String? = "",
    var avatar: String? = "",
    var birthDay: String? = "",
    var country: String? = "",
    var facebook: Facebook? = null,
    val notification: Notification? = null,
    val privacy: Privacy? = null,
    var facebookId: String? = "",
    var fullName: String? = "",
    var gender: String? = "",
    var interested: String? = "",
    var invitation: String? = "",
    var location: Location? = null,
    var region: String? = "",
    var upcoming: String? = "",
    var accessToken: String? = "",
    var refreshToken: String? = "",
    var icebreaker: String? = "",
    var weekendplan: String? = "",
    var lastOnlineTime: String? = "",
    var onlineStatus: Boolean? = false,
    var maxAge: Int? = 0,
    var minAge: Int? = 0,
    var friendStatus: Int? = 1
)

@Keep
data class Facebook(
    var id: String? = "",
    var name: String? = "",
    var email: String? = ""
)

@Keep
data class Location(
    var type: String? = "",
    var coordinates: List<Double>? = null
)

@Keep
data class Notification(
    val enabled: Int? = 0,
    val sound: Int? = 0,
    val vibration: Int? = 0,
    val invitation: Int? = 0,
    val meetingReminder: Int? = 0,
    val previewOnScreen: Int? = 0
)

@Keep
data class Privacy(
    val phone: Int? = 1,
    val bio: Int? = 1,
    val weekendplan: Int? = 1
)

