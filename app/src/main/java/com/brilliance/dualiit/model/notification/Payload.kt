package com.brilliance.dualiit.model.notification


import androidx.annotation.Keep

@Keep
data class Payload(
    val friendId: String? = "",
    val userId: String? = ""
)