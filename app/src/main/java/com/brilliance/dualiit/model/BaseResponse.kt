package com.brilliance.dualiit.model

import androidx.annotation.Keep
import com.brilliance.dualiit.utils.networkRequest.ApiService

@Keep
data class BaseResponse<T>(
    val success:Boolean = false,
    val serverCode:Int = 0,
    val message:String = "",
    val data:T ?= null
)

@Keep
data class ErrorData(
    val message:String ?= "",
    val statusCode:Int = 0,
    val priority:Int = ApiService.PRIORITY_DEFAULT
)