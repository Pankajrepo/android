package com.brilliance.dualiit.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class InterestModel(

    @SerializedName("data")
    val interestData: InterestData? = null,

    val message: String? = "",
    val serverCode: Int? = 0,
    val success: Boolean? = false
)