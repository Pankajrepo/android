package com.brilliance.dualiit.model.notification


import androidx.annotation.Keep

@Keep
data class NotificationModel(
    val _id: String? = "",
    val action: String? = "",
    val createdAt: String? = "",
    val from: From? = null,
    val message: String? = "",
    val payload: Payload? = null,
    val read: Int? = 0,
    val referShema: String? = "",
    val status: Int? = 0,
    val title: String? = "",
    val to: To? = null,
    val updatedAt: String? = ""
)