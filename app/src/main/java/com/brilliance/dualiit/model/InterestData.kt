package com.brilliance.dualiit.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class InterestData(

    @SerializedName("docs")
    val interestDocs: List<InterestDoc>? = null,

    val limit: Int? = 0,
    val nextPage: Int? = 0,
    val page: Int? = 0,
    val prevPage: Any? = null,
    val totalDocs: Int? = 0,
    val totalPages: Int? = 0
)