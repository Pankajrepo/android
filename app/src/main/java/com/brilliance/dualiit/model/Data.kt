package com.brilliance.dualiit.model

import androidx.annotation.Keep

@Keep
data class Data(
    val _id: String,
    val accountType: String,
    val age: Int,
    val appVersion: String,
    val area: String,
    val avatar: String,
    val createdAt: String,
    val deviceToken: String,
    val deviceType: String,
    val email: String,
    val facebookId: String,
    val fullName: String,
    val gender: String,
    val interest: List<Any>,
    val invitation: String,
    val isVerifiedEmail: Boolean,
    val lastLogin: String,
    val lastOnlineTime: String,
    val locale: String,
    val onlineStatus: Boolean,
    val orientation: String,
    val phone: String,
    val region: String,
    val regionCode: String,
    val status: Int,
    val upcoming: String,
    val updatedAt: String
)