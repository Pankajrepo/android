package com.brilliance.dualiit.model

import androidx.annotation.Keep

@Keep
data class CommonResponseModel(
    val message: String,
    val status: Boolean,
    val statusCode: Int
)

@Keep
data class DialogSuccessModel(
    val mClass: Class<*>? = null,
    val arguments: HashMap<String, Any?>? = null,
    val isTopFinish: Boolean = false,
    val flagIsTopClearTask: Boolean = false,
    val title: String? = null,
    val message: String? = null
)

@Keep
data class RedirectModel(
    val mClass: Class<*>,
    val arguments: HashMap<String, Any?>? = null,
    val isTopFinish: Boolean = false,
    val flagIsTopClearTask: Boolean = false
)