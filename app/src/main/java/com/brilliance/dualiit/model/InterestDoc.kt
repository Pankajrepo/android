package com.brilliance.dualiit.model

import androidx.annotation.Keep

@Keep
data class InterestDoc(
    val _id: String,
    val createdAt: String,
    val name: String
) {
    var isSelected: Boolean = false
}