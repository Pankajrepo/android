package com.brilliance.dualiit.model

import androidx.annotation.Keep

@Keep
data class UserAvatar(
    val _id: String? = "",
    val avatar: String? = "",
    val createdAt: String? = "",
    val type: String? = "",
    val updatedAt: String? = ""
)