package com.brilliance.dualiit.model.notification


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class NotificationResponse(

    @SerializedName("docs")
    val notificationDocs: List<NotificationModel>? = null,
    val limit: Int? = 0,
    val nextPage: Any? = null,
    val page: Int? = 0,
    val prevPage: Any? = null,
    val totalDocs: Int? = 0,
    val totalPages: Int? = 0
)