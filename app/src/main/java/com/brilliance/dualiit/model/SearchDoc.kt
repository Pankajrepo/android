package com.brilliance.dualiit.model

import androidx.annotation.Keep

@Keep
data class SearchDoc(
    val _id: String? = "",
    val avatar: String? = "",
    val birthDay: String? = "",
    val country: String? = "",
    val email: String? = "",
    val fullName: String? = "",
    val interest: List<String>? = null,
    val lastOnlineTime: String? = "",
    val locale: String? = "",
    val onlineStatus: Boolean? = false,
    val phone: String? = "",
    val region: String? = "",
    val regionCode: String? = ""
)