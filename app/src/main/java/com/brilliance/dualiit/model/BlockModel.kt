package com.brilliance.dualiit.model

import androidx.annotation.Keep

@Keep
data class BlockModel(
    var userName: String,
    var userProfile: String,
    var lastTime: String,
    var isBlocked: Boolean = false
)
