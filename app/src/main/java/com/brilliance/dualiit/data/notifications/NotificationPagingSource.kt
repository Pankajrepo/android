package com.brilliance.dualiit.data.notifications

import androidx.paging.PagingSource
import com.brilliance.dualiit.model.notification.NotificationModel
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.networkRequest.ApiService
import retrofit2.HttpException
import java.io.IOException

private const val STARTING_PAGE_INDEX = 1

class NotificationPagingSource(
    private val apiService: ApiService,
    private val prefs: AppPreferencesHelper
) : PagingSource<Int, NotificationModel>() {

    private var notificationsList: List<NotificationModel>? = ArrayList()

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, NotificationModel> {
        val position = params.key ?: STARTING_PAGE_INDEX
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        return try {

            val response = apiService.getNotificationsList(
                headers = map,
                page = position,
                limit = params.loadSize
            )

            notificationsList = response.data?.notificationDocs

            LoadResult.Page(
                data = notificationsList!!,
                prevKey = if (position == STARTING_PAGE_INDEX) null else position - 1,
                nextKey = if (notificationsList!!.isEmpty()) null else position + 1
            )
        } catch (e: IOException) {
            LoadResult.Error(e)
        } catch (e: HttpException) {
            LoadResult.Error(e)
        }
    }

}