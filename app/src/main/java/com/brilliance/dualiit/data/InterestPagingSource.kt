package com.brilliance.dualiit.data

import androidx.paging.PagingSource
import com.brilliance.dualiit.model.InterestDoc
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.networkRequest.ApiService
import retrofit2.HttpException
import java.io.IOException

private const val INTEREST_STARTING_PAGE_INDEX = 1

class InterestPagingSource(
    private val apiService: ApiService,
    private val prefs: AppPreferencesHelper,
    private val query: String
) : PagingSource<Int, InterestDoc>() {

    private var interests: List<InterestDoc>? = ArrayList()

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, InterestDoc> {
        val position = params.key ?: INTEREST_STARTING_PAGE_INDEX
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        return try {

            val response = apiService.getInterests(
                headers = map,
                page = position,
                perPage = params.loadSize,
                query = query
            )

            interests = response.interestData?.interestDocs

            LoadResult.Page(
                data = interests!!,
                prevKey = if (position == INTEREST_STARTING_PAGE_INDEX) null else position - 1,
                nextKey = if (interests!!.isEmpty()) null else position + 1
            )
        } catch (e: IOException) {
            LoadResult.Error(e)
        } catch (e: HttpException) {
            LoadResult.Error(e)
        }
    }

}