package com.brilliance.dualiit.data.conversation

import androidx.paging.PagingSource
import com.brilliance.dualiit.ui.chat.RecentChat
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.networkRequest.ApiService
import retrofit2.HttpException
import java.io.IOException

private const val STARTING_PAGE_INDEX = 1

class RecentChatsPagingSource(
    private val apiService: ApiService,
    private val prefs: AppPreferencesHelper
) : PagingSource<Int, RecentChat>() {

    private var recentChatsList: List<RecentChat>? = ArrayList()

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, RecentChat> {
        val position = params.key ?: STARTING_PAGE_INDEX
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        return try {

            val response = apiService.getRecentChats(
                headers = map,
                page = position,
                limit = params.loadSize
            )

            recentChatsList = response.data?.recentChats

            LoadResult.Page(
                data = recentChatsList!!,
                prevKey = if (position == STARTING_PAGE_INDEX) null else position - 1,
                nextKey = if (recentChatsList!!.isEmpty()) null else position + 1
            )
        } catch (e: IOException) {
            LoadResult.Error(e)
        } catch (e: HttpException) {
            LoadResult.Error(e)
        }
    }

}