package com.brilliance.dualiit.data.meeting

import androidx.paging.PagingSource
import com.brilliance.dualiit.ui.meeting.models.Meeting
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.networkRequest.ApiService
import retrofit2.HttpException
import java.io.IOException

private const val STARTING_PAGE_INDEX = 1

class MeetingPagingSource(
    private val apiService: ApiService,
    private val prefs: AppPreferencesHelper
) : PagingSource<Int, Meeting>() {

    private var meetingsList: List<Meeting>? = ArrayList()

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Meeting> {
        val position = params.key ?: STARTING_PAGE_INDEX
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        return try {

            val response = apiService.getMeetingsList(
                headers = map,
                page = position,
                limit = params.loadSize
            )

            meetingsList = response.data?.docs

            LoadResult.Page(
                data = meetingsList!!,
                prevKey = if (position == STARTING_PAGE_INDEX) null else position - 1,
                nextKey = if (meetingsList!!.isEmpty()) null else position + 1
            )
        } catch (e: IOException) {
            LoadResult.Error(e)
        } catch (e: HttpException) {
            LoadResult.Error(e)
        }
    }

}