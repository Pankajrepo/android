package com.brilliance.dualiit.data.addressbook

import androidx.paging.PagingSource
import com.brilliance.dualiit.ui.chat.AddressBook
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.networkRequest.ApiService
import retrofit2.HttpException
import java.io.IOException

private const val STARTING_PAGE_INDEX = 1

class AddressBookPagingSource(
    private val apiService: ApiService,
    private val prefs: AppPreferencesHelper,
    private val status: Int,
    private val query: String
) : PagingSource<Int, AddressBook>() {

    private var friendsList: List<AddressBook>? = ArrayList()

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, AddressBook> {
        val position = params.key ?: STARTING_PAGE_INDEX
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        return try {

            val response = apiService.getFriendsList(
                headers = map,
                page = position,
                limit = params.loadSize,
                status = status,
                query = query
            )

            friendsList = response.data?.addressBookList

            LoadResult.Page(
                data = friendsList!!,
                prevKey = if (position == STARTING_PAGE_INDEX) null else position - 1,
                nextKey = if (friendsList!!.isEmpty()) null else position + 1
            )
        } catch (e: IOException) {
            LoadResult.Error(e)
        } catch (e: HttpException) {
            LoadResult.Error(e)
        }
    }

}