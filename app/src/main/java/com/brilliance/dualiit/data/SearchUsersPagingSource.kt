package com.brilliance.dualiit.data
//
//import androidx.paging.PagingSource
//import com.vforl.model.SearchDoc
//import com.vforl.utils.AppPreferencesHelper
//import com.vforl.utils.networkRequest.ApiService
//import retrofit2.HttpException
//import java.io.IOException
//
//private const val SEARCH_STARTING_PAGE_INDEX = 1
//
//class SearchUsersPagingSource(
//    private val apiService: ApiService,
//    private val prefs: AppPreferencesHelper,
//    private val query: String
//) : PagingSource<Int, SearchDoc>() {
//
//    private var usersList: List<SearchDoc>? = ArrayList()
//
//    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, SearchDoc> {
//        val position = params.key ?: SEARCH_STARTING_PAGE_INDEX
//        val map = HashMap<String, String>()
//        map["Authorization"] = "Bearer ${prefs.userModel?.accessToken}"
//
//        return try {
//            val response = apiService.searchUsers(
//                headers = map,
//                page = position,
//                perPage = params.loadSize,
//                query = query
//            )
//
//            usersList = response.data?.docs
//
//            LoadResult.Page(
//                data = usersList!!,
//                prevKey = if (position == SEARCH_STARTING_PAGE_INDEX) null else position - 1,
//                nextKey = if (usersList!!.isEmpty()) null else position + 1
//            )
//
//        } catch (e: IOException) {
//            LoadResult.Error(e)
//        } catch (e: HttpException) {
//            LoadResult.Error(e)
//        }
//    }
//}