package com.brilliance.dualiit.data.invitation

import androidx.paging.PagingSource
import com.brilliance.dualiit.ui.invitation.models.InvitationModel
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.networkRequest.ApiService
import retrofit2.HttpException
import java.io.IOException

private const val INVITATION_STARTING_PAGE_INDEX = 1

class InvitationPagingSource(
    private val apiService: ApiService,
    private val prefs: AppPreferencesHelper
) : PagingSource<Int, InvitationModel>() {

    private var invitations: List<InvitationModel>? = ArrayList()

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, InvitationModel> {
        val position = params.key ?: INVITATION_STARTING_PAGE_INDEX
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        return try {

            val response = apiService.getInvitationsList(
                headers = map,
                page = position,
                limit = params.loadSize
            )

            invitations = response.data?.invitationsList

            LoadResult.Page(
                data = invitations!!,
                prevKey = if (position == INVITATION_STARTING_PAGE_INDEX) null else position - 1,
                nextKey = if (invitations!!.isEmpty()) null else position + 1
            )
        } catch (e: IOException) {
            LoadResult.Error(e)
        } catch (e: HttpException) {
            LoadResult.Error(e)
        }
    }

}