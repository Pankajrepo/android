package com.brilliance.dualiit.utils.game

enum class Result {
    WON, LOST, TIE
}
