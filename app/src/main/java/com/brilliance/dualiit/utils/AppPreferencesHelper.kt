package com.brilliance.dualiit.utils

import android.content.SharedPreferences
import com.google.gson.Gson
import com.brilliance.dualiit.model.UserModel

class AppPreferencesHelper
internal constructor(val mPrefs: SharedPreferences) {
    /*-------FOR LOGIN USER DATA --------------------*/
    var authToken: String
        get() = mPrefs.getString("USER-AUTH-TOKEN", "").toString()
        set(value) = mPrefs.edit().putString("USER-AUTH-TOKEN", "Bearer $value").apply()

  var userModel: UserModel?
        get() = getPrefUserDetail()
        set(value) = mPrefs.edit().putString("USER-MODEL", Gson().toJson(value).toString()).apply()


    fun getPrefUserDetail(): UserModel? {
        val rawUserData =
            mPrefs.getString("USER-MODEL", "")
        return Gson().fromJson(rawUserData, UserModel::class.java)
    }


    fun isUserLogin(): Boolean {
        return !mPrefs.getString("USER-AUTH-TOKEN", "").equals("", true)
    }

    fun logout() {
        mPrefs.edit().clear().apply()
    }

    var latitude: Float
        get() = mPrefs.getFloat("latitude", 0.0F)
        set(value) = mPrefs.edit().putFloat("latitude", value).apply()
    var longitude: Float
        get() = mPrefs.getFloat("longitude", 0.0F)
        set(value) = mPrefs.edit().putFloat("longitude", value).apply()

    var isThemeNight: Int
        get() = mPrefs.getInt("ThemeNight", 0)
        set(value) = mPrefs.edit().putInt("ThemeNight", value).apply()

    var selectedTab: Int
        get() = mPrefs.getInt("selectedTab", 1)
        set(value) = mPrefs.edit().putInt("selectedTab", value).apply()

    var liveImage: String
        get() = mPrefs.getString("liveImage", "NoImage").toString()
        set(value) = mPrefs.edit().putString("liveImage", value).apply()

    var selectedTabInMatch: Int
        get() = mPrefs.getInt("selectedTabInMatch", 1)
        set(value) = mPrefs.edit().putInt("selectedTabInMatch", value).apply()

    var isOnBoardShow: Boolean
        get() = mPrefs.getBoolean("isOnBoardShow", false)
        set(value) = mPrefs.edit().putBoolean("isOnBoardShow", value).apply()

    var isAuthLogin: Boolean
        get() = mPrefs.getBoolean("isAuthLogin", false)
        set(value) = mPrefs.edit().putBoolean("isAuthLogin", value).apply()


    var showPsl: Boolean
        get() = mPrefs.getBoolean("showPsl", false)
        set(value) = mPrefs.edit().putBoolean("showPsl", value).apply()

    var isApiWorking: Boolean
        get() = mPrefs.getBoolean("isApiWorking", false)
        set(value) = mPrefs.edit().putBoolean("isApiWorking", value).apply()

    var newsNotification: Boolean
        get() = mPrefs.getBoolean("newsNotification", true)
        set(value) = mPrefs.edit().putBoolean("newsNotification", value).apply()

    //TODO:newWork11
    var videoAutoPlay: Boolean
        get() = mPrefs.getBoolean("videoAutoPlay", false)
        set(value) = mPrefs.edit().putBoolean("videoAutoPlay", value).apply()

    var listeningSpeed: Float
        get() = mPrefs.getFloat("listeningSpeed", 0.8F)
        set(value) = mPrefs.edit().putFloat("listeningSpeed", value).apply()


    var fireBaseToken: String
        get() = mPrefs.getString("fireBaseToken", "").toString()
        set(value) = mPrefs.edit().putString("fireBaseToken", value).apply()

    var deviceId: String
        get() = mPrefs.getString("deviceId", "").toString()
        set(value) = mPrefs.edit().putString("deviceId", value).apply()

    var homeCategoryResponse: String
        get() = mPrefs.getString("homeCategoryResponse", "{}")!!
        set(value) = mPrefs.edit().putString("homeCategoryResponse", value).apply()

    var matchNotificationTime: String
        get() = mPrefs.getString("NotificationTime", "{}")!!
        set(value) = mPrefs.edit().putString("NotificationTime", value).apply()

    var predictionDetailBanner: String
        get() = mPrefs.getString("predictionDetailBanner", "").toString()
        set(value) = mPrefs.edit().putString("predictionDetailBanner", value).apply()

    var predictionBanner: String
        get() = mPrefs.getString("predictionBanner", "").toString()
        set(value) = mPrefs.edit().putString("predictionBanner", value).apply()

    var matchHomeBanner: String
        get() = mPrefs.getString("matchHomeBanner", "").toString()
        set(value) = mPrefs.edit().putString("matchHomeBanner", value).apply()

    var matchLeagueBanner: String
        get() = mPrefs.getString("matchLeagueBanner", "").toString()
        set(value) = mPrefs.edit().putString("matchLeagueBanner", value).apply()

    var matchDetailBanner: String
        get() = mPrefs.getString("matchDetailBanner", "NoData").toString()
        set(value) = mPrefs.edit().putString("matchDetailBanner", value).apply()

    var pslBanner: String
        get() = mPrefs.getString("pslBanner", "").toString()
        set(value) = mPrefs.edit().putString("pslBanner", value).apply()

    var country: String
        get() = mPrefs.getString("country", "").toString()
        set(value) = mPrefs.edit().putString("country", value).apply()

    var state: String
        get() = mPrefs.getString("state", "").toString()
        set(value) = mPrefs.edit().putString("state", value).apply()


    var isLocationGot: Boolean
        get() = mPrefs.getBoolean("location", false)
        set(value) = mPrefs.edit().putBoolean("location", value).apply()

    fun setUpdatePermission(key: String, value: Boolean) {
        mPrefs.edit().putBoolean(key, value).apply()
    }
}