package com.brilliance.dualiit.utils.networkRequest;

import android.util.Log;

import com.google.gson.Gson;
import com.brilliance.dualiit.di.module.NoConnectivityException;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketException;

import retrofit2.HttpException;
import rx.Subscriber;

/**
 * Will handel all api response and error
 */
abstract public class CallbackWrapper<T> extends Subscriber<T> {
    private static final String TAG = CallbackWrapper.class.getSimpleName();

    public abstract void onSuccess(T t);

    public abstract void onError(String message, Integer statusCode);

    @Override
    public void onCompleted() {
    }

    @Override
    public void onError(Throwable e) {
//        Log.e("callbackresponseOnError", e.getMessage());
        if (e instanceof NoConnectivityException || e instanceof SocketException) {
            onError("No network available, please check your WiFi or Data connection", 404);
        } else {
            try {
                String error = ((HttpException) e).response().errorBody().string();
                Log.e("error", error);

                ErrorCheckModel errorCheckModel = new Gson().fromJson(error, ErrorCheckModel.class);
                if (errorCheckModel.message == null || errorCheckModel.serverCode == null) {
                    onError("Api Exception", 0);
                } else {
                    onError(errorCheckModel.message, errorCheckModel.serverCode);
                }

            } catch (Exception ex) {
                onError("Api Exception", 0);
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onNext(T t) {
        ErrorCheckModel errorCheckModel = checkError(t);
        Log.e("callbackresponse", new Gson().toJson(t));
        if (errorCheckModel.status) {
            onSuccess(t);
        } else {
            onError(errorCheckModel.message, errorCheckModel.serverCode);
        }
    }

    private ErrorCheckModel checkError(Object data) {
        ErrorCheckModel errorCheckModel = new ErrorCheckModel();
        try {
            JSONObject object = new JSONObject(new Gson().toJson(data));
            errorCheckModel.status = object.getBoolean("success");
            errorCheckModel.message = object.getString("message");
            errorCheckModel.serverCode = object.getInt("serverCode");
        } catch (JSONException e) {
        }
        return errorCheckModel;
    }

    private class ErrorCheckModel {
        private boolean status = false;
        private Integer serverCode = 0;
        private String message = "";
    }


}
