package com.brilliance.dualiit.utils


import android.graphics.Typeface
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.tabs.TabLayout
import com.brilliance.dualiit.R

class TabSelectedListener(
    private var tabLayout: TabLayout
) : TabLayout.OnTabSelectedListener {

    private var mSelectFont:Typeface?= null
    private var mUnSelectFont:Typeface ?= null

    init {
        mSelectFont = ResourcesCompat.getFont(tabLayout.context, R.font.futura_medium)
        mUnSelectFont = ResourcesCompat.getFont(tabLayout.context, R.font.futura_medium)
    }
    override fun onTabSelected(selectedTab: TabLayout.Tab) {
        val tabLayout1 =
            (tabLayout.getChildAt(0) as ViewGroup).getChildAt(selectedTab.position)
                    as LinearLayout

        val tabTextView = tabLayout1.getChildAt(1) as TextView
        tabTextView.typeface = mSelectFont
    }

    override fun onTabUnselected(unselectedTab: TabLayout.Tab) {
        val tabLayout1 =
            (tabLayout.getChildAt(0) as ViewGroup).getChildAt(unselectedTab.position)
        as LinearLayout

        val tabTextView = tabLayout1.getChildAt(1) as TextView
        tabTextView.typeface = mUnSelectFont

    }

    override fun onTabReselected(tab: TabLayout.Tab) {
    }


}