package com.brilliance.dualiit.utils.extensions

import android.os.Build
import android.text.Html
import android.widget.TextView

fun TextView.setHtml(text: String?) {
    text?.let {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            this.text = Html.fromHtml(it, Html.FROM_HTML_MODE_LEGACY)
        } else {
            @Suppress("DEPRECATION")
            this.text = Html.fromHtml(it)
        }
    }
}