package com.brilliance.dualiit.utils.networkRequest

import com.brilliance.dualiit.model.*
import com.brilliance.dualiit.model.notification.NotificationResponse
import com.brilliance.dualiit.ui.auth.request.LoginRequest
import com.brilliance.dualiit.ui.auth.request.ResetRequest
import com.brilliance.dualiit.ui.auth.request.SignupRequest
import com.brilliance.dualiit.ui.chat.AddressBookModel
import com.brilliance.dualiit.ui.chat.RecentChatsModel
import com.brilliance.dualiit.ui.chat.models.ConversationModel
import com.brilliance.dualiit.ui.invitation.models.InvitationResponse
import com.brilliance.dualiit.ui.meeting.models.MeetingModel
import com.brilliance.dualiit.ui.speeddating.models.SessionResponse
import okhttp3.MultipartBody
import retrofit2.http.*
import rx.Observable

interface ApiService {

    companion object {
        const val PRIORITY_DEFAULT = 0
        const val PRIORITY_HIGH = 1
        const val PRIORITY_MEDIUM = 2
        const val PRIORITY_LOW = 3
    }


    @FormUrlEncoded
    @POST("v1/auth/checkExist")
    suspend fun checkExist(
        @Field("phone") phone: String, @Field("email") email: String
    ): BaseResponse<Any>


    @FormUrlEncoded
    @POST("v1/auth/sendEmailVerify")
    fun sendEmailVerifyCode(
        @Field("email") email: String
    ): Observable<BaseResponse<Any>>

    @FormUrlEncoded
    @POST("v1/auth/verifyEmail")
    fun verifyEmail(
        @Field("code") code: String, @Field("email") email: String
    ): Observable<BaseResponse<Any>>

    @POST("v1/auth/signUp")
    suspend fun signUp(
        @Body signupRequest: SignupRequest
    ): BaseResponse<UserModel>

    @POST("v1/auth/login")
    suspend fun login(
        @Body loginRequest: LoginRequest
    ): BaseResponse<UserModel>

    @POST("v1/auth/resetPwd")
    suspend fun resetPwd(
        @Body resetRequest: ResetRequest
    ): BaseResponse<UserModel>

    @FormUrlEncoded
    @PATCH("v1/user/changePwd")
    suspend fun changePassword(
        @HeaderMap headers: HashMap<String, String>,
        @Field("oldPassword") oldPassword: String,
        @Field("password") newPassword: String
    ): BaseResponse<Any>


    @FormUrlEncoded
    @POST("v1/auth/sendSMSCode")
    suspend fun sendSMSCode(
        @Field("phone") phone: String
    ): BaseResponse<Any>


    @GET("v1/user")
    fun getProfile(
        @HeaderMap headers: HashMap<String, String>
    ): Observable<BaseResponse<UserModel>>


    @POST("v1/media/avatar")
    @Multipart
    fun uploadUserAvatar(@Part filePart: MultipartBody.Part): Observable<BaseResponse<UserAvatar>>


    @FormUrlEncoded
    @PATCH("v1/user")
    suspend fun updateProfile(
        @HeaderMap headers: HashMap<String, String>,
        @FieldMap fields: HashMap<String, Any>
    ): BaseResponse<UserModel>

    @PATCH("v1/user")
    suspend fun updateNotification(
        @HeaderMap headers: HashMap<String, String>,
        @Body userModel: UserModel
    ): BaseResponse<UserModel>

    @PATCH("v1/user")
    suspend fun updateProfileWithLocation(
        @HeaderMap headers: HashMap<String, String>,
        @Body body: HashMap<String, Any>
    ): BaseResponse<UserModel>


    @GET("v1/user/homeSuggest")
    suspend fun getSuggestedUsers(
        @HeaderMap headers: HashMap<String, String>
    ): BaseResponse<List<UserModel>>

    @GET("v1/search")
    suspend fun searchUsers(
        @HeaderMap headers: HashMap<String, String>,
//        @Query("pagination") pagination: Boolean = true,
//        @Query("page") page: Int,
//        @Query("limit") perPage: Int,
        @Query("sortBy") sortBy: String = "createdAt",
        @Query("sortType") sortType: Int = -1,
        @Query("search") query: String
    ): BaseResponse<SearchData>


    @GET("v1/user/{id}")
    suspend fun getOtherUserProfile(
        @HeaderMap headers: HashMap<String, String>,
        @Path("id") id: String
    ): BaseResponse<UserModel>

    @GET("v1/conversation/message")
    suspend fun getUserChats(
        @HeaderMap headers: HashMap<String, String>,
        @Query("page") page: Int = 1,
        @Query("conversationKey") key: String,
        @Query("last") last: Int = 1
    ): BaseResponse<ConversationModel>


    @GET("v1/interest")
    suspend fun getInterests(
        @HeaderMap headers: HashMap<String, String>,
        @Query("pagination") pagination: Boolean = true,
        @Query("page") page: Int,
        @Query("limit") perPage: Int,
        @Query("sortBy") sortBy: String = "createdAt",
        @Query("sortType") sortType: Int = -1,
        @Query("name") query: String
    ): InterestModel

    @GET("v1/invite")
    suspend fun getInvitationsList(
        @HeaderMap headers: HashMap<String, String>,
        @Query("pagination") pagination: Boolean = true,
        @Query("page") page: Int,
        @Query("limit") limit: Int,
        @Query("sortBy") sortBy: String = "createdAt",
        @Query("sortType") sortType: Int = -1
    ): BaseResponse<InvitationResponse>

    @GET("v1/notification")
    suspend fun getNotificationsList(
        @HeaderMap headers: HashMap<String, String>,
        @Query("pagination") pagination: Boolean = true,
        @Query("page") page: Int,
        @Query("limit") limit: Int,
        @Query("sortBy") sortBy: String = "createdAt",
        @Query("sortType") sortType: Int = -1
    ): BaseResponse<NotificationResponse>

    @GET("v1/conversation")
    suspend fun getRecentChats(
        @HeaderMap headers: HashMap<String, String>,
        @Query("pagination") pagination: Boolean = true,
        @Query("page") page: Int,
        @Query("limit") limit: Int,
        @Query("sortBy") sortBy: String = "createdAt",
        @Query("sortType") sortType: Int = 1
    ): BaseResponse<RecentChatsModel>

    @GET("v1/friend")
    suspend fun getFriendsList(
        @HeaderMap headers: HashMap<String, String>,
        @Query("pagination") pagination: Boolean = true,
        @Query("page") page: Int,
        @Query("limit") limit: Int,
        @Query("sortBy") sortBy: String = "createdAt",
        @Query("sortType") sortType: Int = -1,
        @Query("status") status: Int,
        @Query("search") query: String,
    ): BaseResponse<AddressBookModel>

    @GET("v1/friend")
    suspend fun getBlockedList(
        @HeaderMap headers: HashMap<String, String>,
        @Query("pagination") pagination: Boolean = true,
        @Query("page") page: Int,
        @Query("limit") limit: Int,
        @Query("sortBy") sortBy: String = "createdAt",
        @Query("sortType") sortType: Int = -1,
        @Query("status") status: Int
    ): BaseResponse<AddressBookModel>

    @FormUrlEncoded
    @PATCH("v1/invite/{inviteId}")
    suspend fun acceptOrRejectInvitation(
        @HeaderMap headers: HashMap<String, String>,
        @Path("inviteId") inviteId: String,
        @FieldMap fields: HashMap<String, Any>
    ): BaseResponse<Any>


    @GET("v1/stream/{sessionId}")
    suspend fun generateTokenWithSession(
        @HeaderMap headers: HashMap<String, String>,
        @Path("sessionId") sessionId: String
    ): BaseResponse<SessionResponse>


    @FormUrlEncoded
    @POST("v1/invite")
    suspend fun sendInvitation(
        @HeaderMap headers: HashMap<String, String>,
        @Field("friendId") friendId: String,
        @Field("type") type: String = "user"
    ): BaseResponse<Any>

    @FormUrlEncoded
    @PATCH("v1/invite/uninvite")
    suspend fun unInvite(
        @HeaderMap headers: HashMap<String, String>,
        @Field("friendId") friendId: String
    ): BaseResponse<Any>

    @POST("v1/stream")
    suspend fun createSessionToken(
        @HeaderMap headers: HashMap<String, String>
    ): BaseResponse<SessionResponse>


    @FormUrlEncoded
    @PATCH("v1/friend")
    suspend fun blockOrUnblockFriend(
        @HeaderMap headers: HashMap<String, String>,
        @Field("status") status: Int,
        @Field("friendId") friendId: String
    ): BaseResponse<Any>


//    Meeting Schedule EndPoints

    @FormUrlEncoded
    @POST("v1/meeting-schedule")
    suspend fun createMeeting(
        @HeaderMap headers: HashMap<String, String>,
        @FieldMap fields: HashMap<String, Any>
    ): BaseResponse<Any>


    @GET("v1/meeting-schedule")
    suspend fun getMeetingsList(
        @HeaderMap headers: HashMap<String, String>,
        @Query("pagination") pagination: Boolean = true,
        @Query("page") page: Int,
        @Query("limit") limit: Int,
        @Query("sortBy") sortBy: String = "createdAt",
        @Query("sortType") sortType: Int = -1
    ): BaseResponse<MeetingModel>

    @FormUrlEncoded
    @PATCH("v1/meeting-schedule/{id}")
    suspend fun acceptOrRejectMeeting(
        @HeaderMap headers: HashMap<String, String>,
        @Path("id") meetingId: String,
        @Field("status") status: Int
    ): BaseResponse<Any>

    @FormUrlEncoded
    @PATCH("v1/meeting-schedule/{id}")
    suspend fun updateMeeting(
        @HeaderMap headers: HashMap<String, String>,
        @Path("id") meetingId: String,
        @FieldMap fields: HashMap<String, Any>
    ): BaseResponse<Any>

    @DELETE("v1/conversation/{key}")
    suspend fun deleteConversationHistory(
        @HeaderMap headers: HashMap<String, String>,
        @Path("key") key: String,
    ): BaseResponse<Any>

    @DELETE("v1/conversation/{key}/clearchat")
    suspend fun deleteChat(
        @HeaderMap headers: HashMap<String, String>,
        @Path("key") key: String,
    ): BaseResponse<Any>

    @POST("v1/auth/deleteMyAccount")
    suspend fun deleteMyAccount(
        @HeaderMap headers: HashMap<String, String>
    ): BaseResponse<Any>

    @DELETE("v1/meeting-schedule/{id}")
    suspend fun deleteMeeting(
        @HeaderMap headers: HashMap<String, String>,
        @Path("id") meetingId: String,
    ): BaseResponse<Any>
}