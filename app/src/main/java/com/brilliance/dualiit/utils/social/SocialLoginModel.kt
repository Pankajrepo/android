package com.brilliance.dualiit.utils.social
import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.Keep

@Keep
data class SocialLoginModel(
    val id: String,
    val name: String,
    val email: String? = "",
    val imgUrl: String?,
    val loginType: String
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString()!!,
        source.readString()!!,
        source.readString()!!,
        source.readString()!!,
        source.readString()!!
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeString(name)
        writeString(email)
        writeString(imgUrl)
        writeString(loginType)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<SocialLoginModel> =
            object : Parcelable.Creator<SocialLoginModel> {
                override fun createFromParcel(source: Parcel): SocialLoginModel =
                    SocialLoginModel(source)
                override fun newArray(size: Int): Array<SocialLoginModel?> = arrayOfNulls(size)
            }
    }
}