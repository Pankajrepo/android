package com.brilliance.dualiit.utils.customView;

import android.content.Context;
import android.graphics.Typeface;
import androidx.core.content.res.ResourcesCompat;
import com.brilliance.dualiit.R;

class TypeFactory {
    final Typeface bold;
    final Typeface medium;
    final Typeface regular;
    final Typeface light;
    final Typeface semibold;
    public TypeFactory(Context context) {
        bold = ResourcesCompat.getFont(context, R.font.futur);
        medium = ResourcesCompat.getFont(context, R.font.futur);
        regular = ResourcesCompat.getFont(context, R.font.futur);
        light = ResourcesCompat.getFont(context, R.font.futur);
        semibold = ResourcesCompat.getFont(context, R.font.futur);
    }
}
