package com.brilliance.dualiit.utils.networkRequest

import rx.Observable
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


class APIDataPresenter<T>(
    private val resultInterface: ResultInterface<Any>,
    private val map: Observable<T>
) {

    fun doInBackgroundWork(): Subscription? {
        return map
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .unsubscribeOn(Schedulers.io())
            .subscribe(object : CallbackWrapper<T>() {
                override fun onSuccess(t: T) {
                    resultInterface.onSuccessBase(t)
                }

                override fun onError(message: String?,statusCode:Int) {
                    resultInterface.onFailed(message,statusCode)
                }
            })
    }

    fun doInBackgroundWork(success: (m: T) -> Unit): Subscription? {
        return map
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .unsubscribeOn(Schedulers.io())
            .subscribe(object : CallbackWrapper<T>() {
                override fun onSuccess(t: T) {
                    success(t)
                }

                override fun onError(message: String?,statusCode:Int) {
                    resultInterface.onFailed(message,statusCode)
                }
            })
    }
}