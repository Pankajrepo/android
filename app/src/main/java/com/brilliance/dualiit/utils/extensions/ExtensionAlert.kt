package com.brilliance.dualiit.utils.extensions

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.DialogConfirmationBinding
import com.brilliance.dualiit.databinding.DialogSuccessBinding
import com.brilliance.dualiit.interfaces.OnSingleClickListener
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout


fun Context.showConfirmDialog(
    title: String = "Confirm",
    message: String = "",
    cancelable: Boolean = false,
    isShowCancel: Boolean = true,
    onCancel: View.OnClickListener? = null,
    onDone: View.OnClickListener? = null
) {
    val builder = AlertDialog.Builder(this)
    val bindView = DialogConfirmationBinding.inflate(LayoutInflater.from(this))
    bindView.actionCancel.visibility = if (isShowCancel) View.VISIBLE else View.GONE
    bindView.title.text = title
    bindView.message.text = message
    builder.setCancelable(cancelable)
    builder.setView(bindView.root)
    val dialog = builder.create()
    bindView.actionCancel.setOnClickListener(object : OnSingleClickListener() {
        override fun onSingleClick(v: View) {
            dialog.dismiss()
            onCancel?.let {
                it.onClick(v)
            }
        }
    })
    bindView.actionDone.setOnClickListener(object : OnSingleClickListener() {
        override fun onSingleClick(v: View) {
            dialog.dismiss()
            onDone?.let { it.onClick(v) }
        }
    })
    dialog.show()
}

fun Context.showDialog(
    title: String? = this.getString(R.string.app_name),
    message: String? = "",
    cancelable: Boolean = false,
    onPressOk: View.OnClickListener? = null
) {
    val builder = AlertDialog.Builder(this)
    val bindView = DialogSuccessBinding.inflate(LayoutInflater.from(this))
    bindView.title.text = title ?: ""
    bindView.message.text = message ?: ""
    builder.setCancelable(cancelable)
    builder.setView(bindView.root)
    val dialog = builder.create()
    bindView.actionDone.setOnClickListener(object : OnSingleClickListener() {
        override fun onSingleClick(v: View) {
            dialog.dismiss()
            onPressOk?.let {
                it.onClick(v)
            }
        }
    })
    dialog.show()
}

fun Context.showToast(string: String?) {
    string?.let {
        Toast.makeText(this, string, Toast.LENGTH_SHORT).show()
    }
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.makeInvisible() {
    this.visibility = View.INVISIBLE
}

fun Activity.showSnakeBar(messageText: String? = null, isError: Boolean = false) {
    val mSnackBar = Snackbar.make(
        window.decorView.findViewById(android.R.id.content),
        messageText.toString(),
        Snackbar.LENGTH_LONG
    )
    val view = mSnackBar.view
    val params = view.layoutParams as FrameLayout.LayoutParams
    params.gravity = Gravity.TOP
    view.layoutParams = params
    if (isError) {
        view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
    } else {
        view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorRedShade))
    }
    mSnackBar.view.visibility = View.INVISIBLE
    mSnackBar.addCallback(object : Snackbar.Callback() {
        override fun onShown(snackbar: Snackbar?) {
            super.onShown(snackbar)
            snackbar!!.view.visibility = View.VISIBLE
            Handler().postDelayed({
                snackbar.view.visibility = View.INVISIBLE
            }, 1000)
        }
    })
    val mainTextView = view.findViewById<View>(R.id.snackbar_text) as TextView
    mainTextView.setTextColor(ContextCompat.getColor(this, R.color.colorTextBlack))
    mSnackBar.show()
}

fun Activity.launchActivity(
    aClass: Class<*>,
    arguments: HashMap<String, Any?>? = null,
    isTopFinish: Boolean = false,
    flagIsTopClearTask: Boolean = false
) {
    this.startActivity(Intent(this, aClass).apply {
        if (flagIsTopClearTask) {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        arguments?.let { map ->
            map.keys.forEach {
                when (map[it]) {
                    is Int -> putExtra(it, map[it] as Int)
                    is Byte -> putExtra(it, map[it] as Byte)
                    is Char -> putExtra(it, map[it] as Char)
                    is Long -> putExtra(it, map[it] as Long)
                    is Float -> putExtra(it, map[it] as Float)
                    is Short -> putExtra(it, map[it] as Short)
                    is Double -> putExtra(it, map[it] as Double)
                    is Boolean -> putExtra(it, map[it] as Boolean)
                    is String -> putExtra(it, map[it] as String)
                    is Bundle -> putExtra(it, map[it] as Bundle)
                }
            }
        }
    })
    if (isTopFinish)
        this.finish()
}

fun Fragment.launch(
    aClass: Class<*>,
    arguments: HashMap<String, Any?>? = null,
    isTopFinish: Boolean = false,
    flagIsTopClearTask: Boolean = false
) {
    this.startActivity(Intent(requireContext(), aClass).apply {
        if (flagIsTopClearTask) {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        arguments?.let { map ->
            map.keys.forEach {
                when (map[it]) {
                    is Int -> putExtra(it, map[it] as Int)
                    is Byte -> putExtra(it, map[it] as Byte)
                    is Char -> putExtra(it, map[it] as Char)
                    is Long -> putExtra(it, map[it] as Long)
                    is Float -> putExtra(it, map[it] as Float)
                    is Short -> putExtra(it, map[it] as Short)
                    is Double -> putExtra(it, map[it] as Double)
                    is Boolean -> putExtra(it, map[it] as Boolean)
                    is String -> putExtra(it, map[it] as String)
                    is Bundle -> putExtra(it, map[it] as Bundle)
                }
            }
        }
    })
    if (isTopFinish) {
        requireActivity().finish()
    }
}

fun TextInputLayout.clearError() {
    this.isErrorEnabled = false
}