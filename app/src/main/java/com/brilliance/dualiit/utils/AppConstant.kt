package com.brilliance.dualiit.utils

object AppConstant {

    const val SPALSH_DELAY: Long = 2000

    const val ISCOMMINGFROM = "iscommingfrom"

    const val GROCERY = "grocery"
    const val SEARCH_PRODUCT = "search_product"
    const val SWEETSHOP = "sweetshop"
    const val THADIWALA = "thadiwala"
    const val RESTAURANT = "restaurant"
    const val TIMESLOT = "timeslot"
    const val CANCELORDER = "cancelorder"
    const val CUSTOMIZATION = "customization"
    const val SHOPPRODUCTS = "shopproducts"
    const val RECENTORDERSPRODUCTS = "recentordersproducts"


    //click constants
    const val CUSTOMIZECLICK = "customizeclick"
    const val NAVIGATIONITEMCLICK = "navigationitemclick"

    //Request Constants
    const val REQ_MOBILE = "request_mobile"
    const val REQ_EXTRA = "request_extra"
    const val REQ_RESET = "request_reset"
    const val REQ_VERIFY_EMAIL = "request_verify_email"
    const val MAP_URL = "https://maps.googleapis.com/"

    //Master Category IDs
    const val CAT_GROCERY_ID = "1"
    const val CAT_RESTAURANT = "2"
    const val CAT_SWEET_SHOP = "3"
    const val CAT_THADIWALA = "4"

    val REQUEST_LOCATION = 1111

    const val SUCCESS_RESULT = 1
    const val FAILURE_RESULT = 0
    const val PACKAGE_NAME = "com.vforl"
    const val RECEIVER = "$PACKAGE_NAME.RECEIVER"
    const val RESULT_DATA_KEY = "$PACKAGE_NAME.RESULT_DATA_KEY"
    const val LOCATION_DATA_EXTRA = "$PACKAGE_NAME.LOCATION_DATA_EXTRA"


    const val TERMS_OF_USE_URL = "https://brilliance.co.in/dualiit-terms-and-conditions/"
    const val PRIVACY_POLICY_URL = "https://brilliance.co.in/dualiit-policy-privacy/"

}

