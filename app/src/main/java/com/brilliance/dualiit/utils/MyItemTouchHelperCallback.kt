package com.brilliance.dualiit.utils

import android.graphics.Canvas
import android.graphics.Color
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.brilliance.dualiit.ui.meeting.MeetingsAdapter

class MyItemTouchHelperCallback(
    private val callbackItemTouch: CallbackItemTouch
) : ItemTouchHelper.Callback() {


    override fun isItemViewSwipeEnabled(): Boolean {
        return true
    }

    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int {
        val swipeFlags = ItemTouchHelper.START or ItemTouchHelper.END
        return makeMovementFlags(0, swipeFlags)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        callbackItemTouch.onSwiped(viewHolder, viewHolder.bindingAdapterPosition)
    }

    override fun onChildDrawOver(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder?,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        val foregroundView = (viewHolder as MeetingsAdapter.SenderMeetingViewHolder).fgView
        getDefaultUIUtil().onDraw(
            c,
            recyclerView,
            foregroundView,
            dX,
            dY,
            actionState,
            isCurrentlyActive
        )

        super.onChildDrawOver(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {

        val foregroundView = (viewHolder as MeetingsAdapter.SenderMeetingViewHolder).bgView
        getDefaultUIUtil().onDrawOver(
            c,
            recyclerView,
            foregroundView,
            dX,
            dY,
            actionState,
            isCurrentlyActive
        )

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }

    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {

        val foregroundView = (viewHolder as MeetingsAdapter.SenderMeetingViewHolder).fgView
        foregroundView.setBackgroundColor(
            ContextCompat.getColor(
                viewHolder.fgView.context,
                android.R.color.white
            )
        )
        getDefaultUIUtil().clearView(foregroundView)

        super.clearView(recyclerView, viewHolder)
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {

        if (viewHolder != null) {
            val foregroundView = (viewHolder as MeetingsAdapter.SenderMeetingViewHolder).fgView
            if (actionState == ItemTouchHelper.ACTION_STATE_DRAG) {
                foregroundView.setBackgroundColor(Color.LTGRAY)
            }
            getDefaultUIUtil().onSelected(foregroundView)
        }

        super.onSelectedChanged(viewHolder, actionState)
    }

    override fun convertToAbsoluteDirection(flags: Int, layoutDirection: Int): Int {
        return super.convertToAbsoluteDirection(flags, layoutDirection)
    }
}











