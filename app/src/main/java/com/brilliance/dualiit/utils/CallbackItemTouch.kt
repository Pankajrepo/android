package com.brilliance.dualiit.utils

import androidx.recyclerview.widget.RecyclerView

interface CallbackItemTouch {

    fun onSwiped(viewHolder: RecyclerView.ViewHolder, position: Int)
}