import android.content.Context
import android.graphics.Point
import android.net.ConnectivityManager
import android.os.Build
import android.util.DisplayMetrics
import android.util.Log
import android.view.WindowManager
import com.brilliance.dualiit.R
import java.util.*

fun isConnected(context: Context?): Boolean {
    val manager: ConnectivityManager? =
        context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
    val info = manager?.activeNetworkInfo
    return !(info == null || !info.isConnected)
}

fun toLowerCase(str: String?): String {
    var rs = "" + str
    str?.let {
        rs = it.toLowerCase(Locale.getDefault())
    }
    return rs
}

fun toUpperCase(str: String?): String {
    var rs = "" + str
    str?.let {
        rs = it.toUpperCase(Locale.getDefault())
    }
    return rs
}


fun getRealWightHeight(windowManager: WindowManager): Pair<Int, Int> {
    val display = windowManager.defaultDisplay
    val outPoint = Point()
    if (Build.VERSION.SDK_INT >= 20) {
        display.getRealSize(outPoint)
    } else {
        display.getSize(outPoint)
    }
    var mRealSizeHeight = 0
    var mRealSizeWidth = 0
    if (outPoint.y > outPoint.x) {
        mRealSizeHeight = outPoint.y
        mRealSizeWidth = outPoint.x
    } else {
        mRealSizeHeight = outPoint.x
        mRealSizeWidth = outPoint.y
    }
    return Pair(mRealSizeWidth, mRealSizeHeight)
}

fun getNewsFontSize(context: Context): Float {
    val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val displayMetrics = DisplayMetrics()
    windowManager.defaultDisplay?.getMetrics(displayMetrics)

    val dimension = getRealWightHeight(windowManager)
    val rw = dimension.first
    val rh = dimension.second

    var textSize = context.resources.getDimension(R.dimen.news_text_font_size)
    val densityDpi = displayMetrics.densityDpi
    if (rw < 1000) {
        if (rh in 1000..1199) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_11dp)
        }

        if (rh in 1200..1299) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_11_5dp)
        }

        if (rh in 1300..1399) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_13_5dp)
        }
        if (rh in 1400..1499) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_13dp)
        }
    }
    if (rw in 1080..1199) {
        if (rh in 1900..1999) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_13_5dp)
        }
        if (rh in 2000..2099) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_14dp)
        }
        if (rh in 2100..2199) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_14_5dp)
        }
        if (rh in 2200..2299) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_15dp)
        }
        if (rh in 2300..2399) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_15_5dp)
        }
        if (rh in 2400..2499) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_16dp)
        }
        if (rh in 2500..2599) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_17dp)
        }
        if (rh in 2600..2699) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_17_5dp)
        }
        if (rh in 2700..2799) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_18dp)
        }
        if (rh in 2900..2999) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_18_5dp)
        }
        if (rh in 3000..3099) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_18dp)
        }
    }

    if (rw in 1200..1399) {
        if (rh in 1500..1699) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_15dp)
        }
        if (rh in 1700..1899) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_16dp)
        }
        if (rh in 1900..1999) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_17_5dp)
        }
        if (rh >= 2000) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_18dp)
        }
    }

    if (rw >= 1440) {
        if (rh in 2400..2499) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_12_5dp)
        }

        if (rh in 2500..2599) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_13_5dp)
        }
        if (rh in 2600..2699) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_14dp)
        }
        if (rh in 2700..2799) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_14_5dp)
        }
        if (rh in 2800..2999) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_15dp)
        }
        if (rh >= 3000) {
            textSize = context.resources.getDimension(R.dimen.news_text_font_size_16dp)
        }
    }
    textSize /= displayMetrics.density
    if (rw in 1080..1399) {
        textSize = getSizeAccordingDensity(densityDpi, textSize)
    } else {
        if (rw >= 1400) {
            textSize = getSizeAccordingDensity2(densityDpi, textSize)
        }
    }
    Log.d("DensityDpi", " density $densityDpi $rw*$rh")
    return textSize
}

fun getSizeAccordingDensity(densityDpi: Int, size: Float): Float {
    var textSize = size
    when (densityDpi) {

        360 -> {
            textSize = size + 2.7F
        }

        380 -> {
            textSize = size + 2.5F
        }

        420 -> {
            textSize = size + 0.8f
        }

        440 -> {
            //textSize = textSize
        }

        480 -> {
            textSize = size - 2f
        }

        560 -> {
            textSize = size - 4.5f
        }

        640 -> {
            textSize = size - 4.8f
        }
    }
    return textSize
}

fun getSizeAccordingDensity2(densityDpi: Int, size: Float): Float {
    var textSize = size
    when (densityDpi) {
        560 -> {
            textSize = size
        }
        640 -> {
            textSize = size - 2f
        }
    }
    return textSize
}
