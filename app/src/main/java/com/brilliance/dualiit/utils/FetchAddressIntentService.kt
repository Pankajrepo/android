package com.brilliance.dualiit.utils

import android.app.IntentService
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.os.ResultReceiver
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import java.io.IOException
import java.util.Locale

class FetchAddressIntentService : IntentService("FetchAddress") {

    private val TAG = "FetchAddressIntent"

    private var receiver: ResultReceiver? = null

    override fun onHandleIntent(intent: Intent?) {
        var errorMessage = ""
        Log.d("Sanjay", "onHandleIntent")
        receiver = intent?.getParcelableExtra(AppConstant.RECEIVER)

        if (intent == null || receiver == null) {
            Log.wtf(TAG, "No receiver received. There is nowhere to send the results.")
            return
        }
        val location = intent.getParcelableExtra<LatLng>(AppConstant.LOCATION_DATA_EXTRA)
        if (location == null) {
            errorMessage = "no_location_data_provided"
            Log.wtf(TAG, errorMessage)
            deliverResultToReceiver(AppConstant.FAILURE_RESULT, errorMessage)
            return
        }

        val geocoder = Geocoder(this, Locale.getDefault())
        var addresses: List<Address> = emptyList()

        try {
            addresses = geocoder.getFromLocation(
                location.latitude,
                location.longitude,
                1
            )
        } catch (ioException: IOException) {
            errorMessage = "Service not available"
            Log.e(TAG, errorMessage, ioException)
        } catch (illegalArgumentException: IllegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = "Invalid LatLng"
            Log.e(
                TAG, "$errorMessage. Latitude = $location.latitude , " +
                        "Longitude = $location.longitude", illegalArgumentException
            )
        }

        // Handle case where no address was found.
        if (addresses.isEmpty()) {
            if (errorMessage.isEmpty()) {
                errorMessage = "No Address found"
                Log.e("TAG", errorMessage)
            }
            deliverResultToReceiver(AppConstant.FAILURE_RESULT, errorMessage)
        } else {
            val address = addresses[0]

            val addressFragments = with(address) {
                (0..maxAddressLineIndex).map { getAddressLine(it) }
            }
            deliverResultToReceiver(
                AppConstant.SUCCESS_RESULT,
                addressFragments.joinToString(separator = "\n")
            )
        }
    }

    private fun deliverResultToReceiver(resultCode: Int, message: String) {
        val bundle = Bundle().apply { putString(AppConstant.RESULT_DATA_KEY, message) }
        receiver?.send(resultCode, bundle)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e(TAG, "onDestroy service")
    }
}
