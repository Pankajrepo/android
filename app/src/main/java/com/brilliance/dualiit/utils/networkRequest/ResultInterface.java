package com.brilliance.dualiit.utils.networkRequest;

public interface ResultInterface<T> {

    void onSuccessBase(T t);

    void onFailed(String msg, int statusCode);
    void  noInternet();
}