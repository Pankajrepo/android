package com.brilliance.dualiit.utils

val <T> T.exhaustive: T
    get() = this