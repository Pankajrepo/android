package com.brilliance.dualiit.utils.social;

public interface AuthenticationInterface {
     void okLogin(SocialLoginModel user);
     void failedLogin(String st);
}

