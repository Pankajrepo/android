package com.brilliance.dualiit

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.brilliance.dualiit.di.module.appModule
import com.brilliance.dualiit.di.module.networkModule
import com.brilliance.dualiit.di.module.viewModels
import com.brilliance.dualiit.ui.home.activities.MainActivity
import com.brilliance.dualiit.ui.meeting.dialogs.ReceiveMeetingDialog
import com.brilliance.dualiit.ui.meeting.models.callevents.CallingResponse
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.google.gson.Gson
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.FormatStrategy
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import dagger.hilt.android.HiltAndroidApp
import io.socket.client.Socket
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber
import javax.inject.Inject

@HiltAndroidApp
class MyApp : MultiDexApplication(), Application.ActivityLifecycleCallbacks {

    val prefs: AppPreferencesHelper by inject()
    private val gson: Gson = Gson()

    @Inject
    lateinit var socket: Socket

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@MyApp)
//            modules(appModule, networkModule, viewModels)
            koin.loadModules(listOf(appModule, networkModule, viewModels))
//            koin.createRootScope()
        }

//        val formatStrategy: FormatStrategy = PrettyFormatStrategy.newBuilder()
//            .showThreadInfo(true) // Set methodOffset to 5 in order to hide internal method calls
//            .tag("") // To replace the default PRETTY_LOGGER tag with a dash (-).
//            .build()
//
//        Logger.addLogAdapter(AndroidLogAdapter(formatStrategy))


//        Timber.plant(object : Timber.DebugTree() {
//            override fun log(
//                priority: Int, tag: String?, message: String, t: Throwable?
//            ) {
//                Logger.log(priority, "-$tag", message, t)
//            }
//        })
//        // Usage
//        Timber.d("onCreate: Inside Application!")

        registerActivityLifecycleCallbacks(this)

        initiateSocketConnection()
    }


    @SuppressLint("LogNotTimber")
    private fun initiateSocketConnection() {
        socket.on(Socket.EVENT_CONNECT) {
            Timber.d("Initial:: Success: Connection successful")
        }

        socket.on(Socket.EVENT_CONNECT_ERROR) {
            Timber.d("Initial:: CONNECTION ERROR: ${it[0]}")
        }

        socket.on(Socket.EVENT_DISCONNECT) {
            Timber.d("DISCONNECT: ${it[0]}")
        }

        socket.on("error") {
            Timber.d("Error: ${it[0]}")
        }

        socket.on("connect_timeout") {
            Timber.d("Connect timeout: ${it[0]}")
        }

        socket.on("reconnecting") {
            Timber.d("Reconnecting: ${it[0]}")
        }

        socket.on("reconnect_error") {
            Timber.d("Reconnect error: ${it[0]}")
        }

        socket.on("reconnect") {
            Timber.d("Reconnect: ${it[0]}")
        }

        socket.on("scheduledMeeting") { args ->
            Timber.d("Receiving Event: ${args[0]}")

            val callingResponse = gson.fromJson(
                args[0].toString(),
                CallingResponse::class.java
            )

            if (callingResponse.from?._id != prefs.userModel?.id) {

                if (callingResponse.status == "calling") {
                    Handler(Looper.getMainLooper()).post {
                        val callingIntent = Intent(this, ReceiveMeetingDialog::class.java).setFlags(
                            Intent.FLAG_ACTIVITY_NEW_TASK
                        )

                        callingIntent.apply {
                            putExtra("USER_NAME", callingResponse.from?.fullName)
                            putExtra("USER_ID", callingResponse.from?._id)
                            putExtra("USER_PHOTO", callingResponse.from?.avatar)
                            putExtra("MEETING_ID", callingResponse.meetingId)
                        }

                        startActivity(callingIntent)

                    }
                }

            }

        }
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
//        Timber.d("onActivityCreated: ${activity.componentName.className}")
    }

    override fun onActivityStarted(activity: Activity) {
//        Timber.d("onActivityStarted: ${activity.componentName.className}")
    }

    override fun onActivityResumed(activity: Activity) {
        Timber.d("onActivityResumed: ${activity.componentName.className}")
    }

    override fun onActivityPaused(activity: Activity) {
        Timber.d("onActivityPaused: ${activity.componentName.className}")
    }

    override fun onActivityStopped(activity: Activity) {
//        Timber.d("onActivityStopped: ${activity.componentName.className}")
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
//        Timber.d("onActivitySaveInstance: ${activity.componentName.className}")
    }

    override fun onActivityDestroyed(activity: Activity) {
        Timber.d("onActivityDestroyed: ${activity.componentName.className}")
        if (activity is MainActivity) {
            activity.sendOfflineEvent()
            socket.disconnect()
            Timber.d("onActivityDestroyed: Socket Closed")
        }
    }
}