package com.brilliance.dualiit.ui.fragment

import android.content.Intent
import android.graphics.drawable.InsetDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.FragmentChatBinding
import com.brilliance.dualiit.ui.base.fragment.BaseFragment
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.ui.chat.AddressBookActivity
import com.brilliance.dualiit.ui.chat.ChatActivity
import com.brilliance.dualiit.ui.chat.ChatsAdapter
import com.brilliance.dualiit.ui.home.adapters.InterestsLoadStateAdapter
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class ChatsFragment : BaseFragment() {
    private val viewModel: HomeViewModel by viewModel()
    private lateinit var binding: FragmentChatBinding
    override fun baseViewModel(): BaseViewModel = viewModel
    private lateinit var chatsAdapter: ChatsAdapter
//    private var popupMenu: PopupMenu? = null

    override fun createDataBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentChatBinding.inflate(inflater, container, false)
//        setObserver()
        setupRecyclerView()

//        viewModel.recentChatsList.observe(viewLifecycleOwner) {
//            if (it.isEmpty()) {
////                activity?.showToast("No data available")
//                binding.progressBar.isVisible = false
//                binding.tvEmpty.isVisible = true
//            } else {
//                binding.progressBar.isVisible = false
//                binding.tvEmpty.isVisible = false
//                binding.rvRecentChats.isVisible = true
//                chatsAdapter.submitList(it)
//            }
//        }

        viewModel.recentChats.observe(viewLifecycleOwner) {
            chatsAdapter.submitData(viewLifecycleOwner.lifecycle, it)
        }

        chatsAdapter.setOnItemClickListener {
            Log.d("ChatsFragment", it.key.toString())
            Log.d("ChatsFragment", "User ID: ${it.user?._id}")
            val intent = Intent(activity, ChatActivity::class.java)
            intent.putExtra("SENDER_ID", it.user?._id)
            intent.putExtra("KEY", it.key)
            startActivity(intent)
        }

        binding.btnAddressBook.setOnClickListener {
            startActivity(Intent(activity, AddressBookActivity::class.java))
        }

        chatsAdapter.addLoadStateListener { loadState ->
            binding.apply {
                progressBar.isVisible = loadState.source.refresh is LoadState.Loading
                rvRecentChats.isVisible = loadState.source.refresh is LoadState.NotLoading
                buttonRetry.isVisible = loadState.source.refresh is LoadState.Error
                textViewError.isVisible = loadState.source.refresh is LoadState.Error

                if (loadState.source.refresh is LoadState.NotLoading &&
                    loadState.append.endOfPaginationReached &&
                    chatsAdapter.itemCount < 1
                ) {
                    rvRecentChats.isVisible = false
                    tvEmpty.isVisible = true
                } else {
                    tvEmpty.isVisible = false
                }
            }
        }

//        binding.ivMoreOptions.setOnClickListener {
//            popupMenu = PopupMenu(requireContext(), it)
//            popupMenu?.inflate(R.menu.menu_conversation_screen)
//            popupMenu?.setOnMenuItemClickListener { menuItem ->
//                when (menuItem.itemId) {
////                    R.id.action_new_msg -> {
////                        activity?.showToast("New Message")
////                        true
////                    }
////                    R.id.action_incoming_request -> {
////                        activity?.showToast("Incoming requests")
////                        true
////                    }
////                    R.id.action_meeting_schedule -> {
////                        activity?.showToast("Meeting schedule")
////                        true
////                    }
//                    R.id.action_clear_all_conversation -> {
//                        activity?.showToast("Clear all chats")
//                        true
//                    }
//                    else -> true
//                }
//            }
//            try {
//                val popup = PopupMenu::class.java.getDeclaredField("mPopup")
//                popup.isAccessible = true
//                val menu = popup.get(popupMenu)
//                menu.javaClass
//                    .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
//                    .invoke(menu, true)
//            } catch (e: Exception) {
//                e.printStackTrace()
//            } finally {
//                popupMenu?.show()
//            }
//        }

        binding.buttonRetry.setOnClickListener {
            chatsAdapter.retry()
        }


        return binding.root
    }

    private fun setupRecyclerView() {
        chatsAdapter = ChatsAdapter()
        binding.rvRecentChats.apply {
            layoutManager = LinearLayoutManager(requireContext())
            itemAnimator = null
            val attrs = intArrayOf(android.R.attr.listDivider)
            val a = context.obtainStyledAttributes(attrs)
            val divider = a.getDrawable(0)
            val insetLeft = resources.getDimensionPixelSize(R.dimen._40sdp)
            val insetRight = resources.getDimensionPixelSize(R.dimen._16sdp)
            val insetDivider = InsetDrawable(divider, insetLeft, 0, insetRight, 0)
            a.recycle()
            val itemDecoration =
                DividerItemDecoration(
                    this@ChatsFragment.requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            itemDecoration.setDrawable(insetDivider)
            addItemDecoration(itemDecoration)

            adapter = chatsAdapter.withLoadStateFooter(
                footer = InterestsLoadStateAdapter {
                    chatsAdapter.retry()
                }
            )
            setHasFixedSize(true)
        }
    }

    override fun setObserver() {
//        viewModel.getRecentChats()
        super.setBaseObserver()
    }

    override fun onResume() {
        super.onResume()
        chatsAdapter.refresh()
        setObserver()
    }

}