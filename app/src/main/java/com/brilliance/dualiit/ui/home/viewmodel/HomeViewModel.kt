package com.brilliance.dualiit.ui.home.viewmodel

import android.util.Log
import androidx.annotation.Keep
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.brilliance.dualiit.model.InterestDoc
import com.brilliance.dualiit.model.SearchDoc
import com.brilliance.dualiit.model.UserModel
import com.brilliance.dualiit.repositories.HomeRepository
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.ui.chat.AddressBook
import com.brilliance.dualiit.ui.chat.RecentChat
import com.brilliance.dualiit.ui.chat.models.Message
import com.brilliance.dualiit.ui.invitation.models.InvitationModel
import com.brilliance.dualiit.ui.speeddating.models.SessionResponse
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.Resource
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import retrofit2.HttpException
import timber.log.Timber

class HomeViewModel(
    private val repository: HomeRepository,
    private val prefs: AppPreferencesHelper
) : BaseViewModel(prefs) {

    var currentPage = MutableLiveData<Int>()


    private val currentInterestQuery = MutableLiveData(CURRENT_QUERY)
    private val currentUsersSearchQuery = MutableLiveData(CURRENT_USERS_QUERY)

    val interestsList = currentInterestQuery.switchMap { queryString ->
        repository.getInterests(queryString).cachedIn(viewModelScope)
    }

    private var userMessagesList: LiveData<PagingData<Message>> = MutableLiveData()

    private var _chatsList = MutableLiveData<List<Message>>()
    var chatsList: LiveData<List<Message>> = _chatsList

    val invitations = repository.getInvitations()

    val notificationsList = repository.getNotificationsList()

    val recentChats = repository.getRecentChatsList()

    private val currentFriendSearchQuery = MutableLiveData("")
    private val currentBlockSearchQuery = MutableLiveData("")

    val addressBookList = currentFriendSearchQuery.switchMap { queryString ->
        repository.getFriendsList(queryString).cachedIn(viewModelScope)
    }

    val blockedList = currentBlockSearchQuery.switchMap { queryString ->
        repository.getBlockedList(queryString).cachedIn(viewModelScope)
    }

    fun searchFriendsList(query: String) {
        currentFriendSearchQuery.value = query
    }

    fun searchBlockedList(query: String) {
        currentBlockSearchQuery.value = query
    }

    val meetingsList = repository.getMeetingsList()

    fun getUserMessagesList(key: String) = viewModelScope.launch {
        val response = repository.getUserMessages(key)
        userMessagesList = response
    }


//    val searchedUsersList = currentUsersSearchQuery.switchMap { queryString ->
//        repository.searchUsers(queryString).cachedIn(viewModelScope)
//    }

    private var _usersListStateFlow: MutableStateFlow<Resource<List<UserModel>>> =
        MutableStateFlow(Resource.Empty())

    val usersListState: StateFlow<Resource<List<UserModel>>> = _usersListStateFlow

    private var _deleteMessagesEventChannel = Channel<DeleteConversationEvent>()
    val deleteMessageEvent = _deleteMessagesEventChannel.receiveAsFlow()

    private var _deleteChatEventChannel = Channel<DeleteChatEvent>()
    val deleteChatEvent = _deleteChatEventChannel.receiveAsFlow()

    private var _unInviteEventChannel = Channel<UnInviteEvent>()
    val unInviteEvent = _unInviteEventChannel.receiveAsFlow()

    private var _deleteMeetingEventChannel = Channel<DeleteMeetingEvent>()
    val deleteMeetingEvent = _deleteMeetingEventChannel.receiveAsFlow()

    private var _interestList = MutableLiveData<List<InterestDoc>>()
    val selectedInterestList: LiveData<List<InterestDoc>> = _interestList

    private var _usersList = MutableLiveData<List<UserModel>>()
    val usersList: LiveData<List<UserModel>> = _usersList

    private var _searchedUsersList = MutableLiveData<List<SearchDoc>>()
    val usersSearchList: LiveData<List<SearchDoc>> = _searchedUsersList

    private var _recentChatsList = MutableLiveData<List<RecentChat>>()
    val recentChatsList: LiveData<List<RecentChat>> = _recentChatsList

    private var _friendsList = MutableLiveData<List<AddressBook>>()
    val friendsList: LiveData<List<AddressBook>> = _friendsList

    private var _sessionResponse = MutableLiveData<SessionResponse>()
    val sessionResponse: LiveData<SessionResponse> = _sessionResponse

    private var _otherUserProfile = MutableLiveData<UserModel>()
    val otherUserProfile: LiveData<UserModel> = _otherUserProfile

    private var _userChatsList = MutableLiveData<List<Message>>()
    val userChatsList: LiveData<List<Message>> = _userChatsList

    private var _invitationsList = MutableLiveData<List<InvitationModel>>()
    val invitationsList: LiveData<List<InvitationModel>> = _invitationsList

    private val profileEventsChannel = Channel<ProfileEvent>()
    val profileEvent = profileEventsChannel.receiveAsFlow()

    private val sessionEventsChannel = Channel<SessionEvent>()
    val sessionEvent = sessionEventsChannel.receiveAsFlow()

    private val meetingsEventChannel = Channel<MeetingsEvent>()
    val meetingsEvent = meetingsEventChannel.receiveAsFlow()

    fun submitSelectedInterestList(interestsList: List<InterestDoc>) = viewModelScope.launch {
        profileEventsChannel.send(ProfileEvent.SelectedInterests(interestsList))
    }

    private val invitationEventChannel = Channel<InvitationEvent>()
    val invitationEvent = invitationEventChannel.receiveAsFlow()

    private fun showToastForAcceptInvitation(message: String) = viewModelScope.launch {
        invitationEventChannel.send(InvitationEvent.InvitationAccepted(message))
    }

    private fun showToastForRejectInvitation(message: String) = viewModelScope.launch {
        invitationEventChannel.send(InvitationEvent.InvitationRejected(message))
    }

    private fun sendDeleteMessageEvent(event: DeleteConversationEvent) = viewModelScope.launch {
        _deleteMessagesEventChannel.send(event)
    }

    fun searchInterests(query: String) {
        currentInterestQuery.value = query
    }

    fun getSuggestedUsersList() {
        viewModelScope.launch {
            repository.getSuggestedUsers()
                .onStart {
                    _usersListStateFlow.value = Resource.Loading()
                }
                .catch { e ->
                    if (e is HttpException) {
                        if (e.code() == 401) {
                            _usersListStateFlow.value =
                                Resource.Error("Please Login to Continue", 401)
                            prefs.logout()
                        }
                    }
                }
                .collect { data ->
                    if (data.success) {
                        _usersListStateFlow.value = Resource.Success(data.data!!)
                    } else {
                        _usersListStateFlow.value =
                            Resource.Error("Error in getting suggested users")
                    }
                }
        }
    }

    fun updateNotificationStatus(userModel: UserModel) {
        viewModelScope.launch {
            repository.updateNotificationStatus(userModel)
                .catch { e ->
                    Log.d("HomeViewModel", "Notification Server Error: ${e.message}")
                }
                .collect { data ->
                    if (data.success) {
                        prefs.userModel = data.data
                        Log.d("HomeViewModel", "Notification Success: ${data.data}")
                    } else {
                        Log.d("HomeViewModel", "Notification Error: ${data.message}")
                    }
                }
        }
    }

    fun deleteConversationHistory(key: String) {
        viewModelScope.launch {
            repository.deleteConversationHistory(key)
                .onStart {
                    sendDeleteMessageEvent(DeleteConversationEvent.Loading)
                }
                .catch { e ->
                    Log.d("HomeViewModel", "Delete Message Error: ${e.message}")
                    sendDeleteMessageEvent(DeleteConversationEvent.Error(e.message.toString()))
                }
                .collect { data ->
                    if (data.success) {
                        Log.d("HomeViewModel", "Delete Message Success: $data")
                        sendDeleteMessageEvent(DeleteConversationEvent.Success)
                    } else {
                        sendDeleteMessageEvent(DeleteConversationEvent.Error(data.message))
                    }
                }
        }
    }

    fun deleteChat(key: String) {
        viewModelScope.launch {
            repository.deleteChat(key)
                .onStart {
                    _deleteChatEventChannel.send(DeleteChatEvent.Loading)
                }
                .catch { e ->
                    Log.d("HomeViewModel", "Delete Chat Error: ${e.message}")
                    _deleteChatEventChannel.send(DeleteChatEvent.Error(e.message.toString()))
                }
                .collect { data ->
                    if (data.success) {
                        Log.d("HomeViewModel", "Delete Chat Success: $data")
                        _deleteChatEventChannel.send(DeleteChatEvent.Success)
                    } else {
                        _deleteChatEventChannel.send(DeleteChatEvent.Error(data.message))
                    }
                }
        }
    }

    fun unInviteUser(friendId: String) = viewModelScope.launch {
        repository.unInviteUser(friendId)
            .catch { e ->
                Log.d("HomeViewModel", "UnInvite Error: ${e.message}")
                _unInviteEventChannel.send(UnInviteEvent.Error(e.message.toString()))
            }
            .collect { data ->
                if (data.success) {
                    _unInviteEventChannel.send(UnInviteEvent.Success)
                } else {
                    _unInviteEventChannel.send(UnInviteEvent.Error(data.message))
                    Log.d("HomeViewModel", "UnInvite Error: ${data.message}")
                }
            }
    }

    fun deleteMeeting(meetingId: String) = viewModelScope.launch {
        repository.deleteMeeting(meetingId)
            .catch { e ->
                Log.d("HomeViewModel", "Delete Meeting Error: ${e.message}")
                _deleteMeetingEventChannel.send(DeleteMeetingEvent.Error(e.message.toString()))
            }
            .collect { data ->
                if (data.success) {
                    _deleteMeetingEventChannel.send(DeleteMeetingEvent.Success)
                } else {
                    _deleteMeetingEventChannel.send(DeleteMeetingEvent.Error(data.message))
                    Log.d("HomeViewModel", "Delete Meeting Error: ${data.message}")
                }
            }
    }

//    fun getSuggestedUsers() = viewModelScope.launch {
//        if (prefs.authToken.length < 10) {
//            prefs.logout()
//            redirect.postValue(
//                RedirectModel(
//                    mClass = ChooseLoginSignup::class.java,
//                    isTopFinish = true,
//                    flagIsTopClearTask = true
//                )
//            )
//        } else {
//            val response = repository.getSuggestedUsers()
//            if (response.serverCode == 401) {
//                prefs.logout()
//            }
//            if (response.success) {
//                _usersList.value = response.data!!
//            }
//        }
//
//    }

    fun acceptInvitation(inviteId: String, hashMap: HashMap<String, Any>) =
        viewModelScope.launch {
            val response = repository.acceptOrRejectInvitation(inviteId, hashMap)
            if (response.serverCode == 401) {
                prefs.logout()
            }
            if (response.success) {
                showToastForAcceptInvitation("Invitation Accepted")
            }
        }

    fun rejectInvitation(inviteId: String, hashMap: HashMap<String, Any>) =
        viewModelScope.launch {
            val response = repository.acceptOrRejectInvitation(inviteId, hashMap)
            if (response.serverCode == 401) {
                prefs.logout()
            }
            if (response.success) {
                showToastForRejectInvitation("Meeting Rejected")
            }
        }

    fun acceptMeeting(meetingId: String, status: Int) =
        viewModelScope.launch {
            val response = repository.acceptOrRejectMeeting(meetingId, status)
            if (response.serverCode == 401) {
                prefs.logout()
            }
            if (response.success) {
                showToastForAcceptInvitation("Meeting Accepted")
            }
        }

    fun rejectMeeting(meetingId: String, status: Int) =
        viewModelScope.launch {
            val response = repository.acceptOrRejectMeeting(meetingId, status)
            if (response.serverCode == 401) {
                prefs.logout()
            }
            if (response.success) {
                showToastForRejectInvitation("Invitation Rejected")
            }
        }

    fun updateMeeting(meetingId: String, fields: HashMap<String, Any>) =
        viewModelScope.launch {
            val response = repository.updateMeeting(meetingId, fields)
            if (response.serverCode == 401) {
                prefs.logout()
            }
            if (response.success) {
                showToastForAcceptInvitation("Meeting Rescheduled")
            }
        }


//    private fun logOutUser() {
//        prefs.logout()
//        startActivity(
//            Intent(
//                this,
//                ChooseLoginSignup::class.java
//            ).setFlags(
//                Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
//            )
//        )
//        finish()
//    }

//    fun getRecentChats() = viewModelScope.launch {
//        val response = repository.getRecentChatsList()
//        if (response.serverCode == 401) {
//            prefs.logout()
//        }
//        if (response.success) {
//            _recentChatsList.value = response.data?.recentChats
//        }
//    }

//    fun getFriendsList() = viewModelScope.launch {
//        val response = repository.getFriendsList()
//        if (response.serverCode == 401) {
//            prefs.logout()
//        }
//        if (response.success) {
//            _friendsList.value = response.data?.addressBookList
//        }
//    }

//    fun getBlockedList() = viewModelScope.launch {
//        val response = repository.getBlockedList()
//        if (response.serverCode == 401) {
//            prefs.logout()
//        }
//        if (response.success) {
//            _friendsList.value = response.data?.addressBookList
//        }
//    }

    fun getOtherUserProfile(id: String) = viewModelScope.launch {
        val response = repository.getOtherUserProfile(id)
        if (response.serverCode == 401) {
            prefs.logout()
        }
        if (response.success) {
            Log.d("HomeViewModel", "Other User: ${response.data}")
            _otherUserProfile.value = response.data!!
        }
    }

    fun getSearchedUsers(query: String) = viewModelScope.launch {
        val response = repository.getSearchedUsersList(query)
        if (response.serverCode == 401) {
            prefs.logout()
        }
        if (response.success) {
            Log.d("HomeViewModel", "Searched Users: ${response.data?.docs}")
            _searchedUsersList.value = response.data?.docs!!
        }
    }

    fun clearSearchList() {
        _searchedUsersList.value = emptyList()
    }

    fun getUserMessages(key: String) = viewModelScope.launch {
        val response = repository.getUserChats(key, 1)
        if (response.serverCode == 401) {
            prefs.logout()
        }
        if (response.success) {
            Log.d("HomeViewModel", "Chats List: ${response.data}")
            val messagesList = response.data?.messages
            currentPage.value = response.data?.page ?: 1
            if (messagesList != null) {
                messagesList.let {
                    _userChatsList.value = it
                }
            } else {
                _userChatsList.value = ArrayList()
            }
        }
    }

    fun getPaginatedUserMessages(key: String) = viewModelScope.launch {
        if (currentPage.value == 0) {
            Log.d("HomeViewModel", "Last Page")
        } else {
            val response = repository.getPaginatedUserChats(key, currentPage.value!!, 0)

            if (response.serverCode == 401) {
                prefs.logout()
            }
            if (response.success) {
                Log.d("HomeViewModel", "Chats List: ${response.data}")
                val messagesList = response.data?.messages
                if (messagesList != null) {
                    if (_userChatsList.value == null) {
                        _userChatsList.value = messagesList!!
                    } else {
//                        _userChatsList.value = _userChatsList.value!!.plus(messagesList.asReversed()).asReversed()
                        _chatsList.value = messagesList!!
                    }
                } else {
                    _userChatsList.value = ArrayList()
                }
            }
        }

    }


//    fun getInvitationsList() = viewModelScope.launch {
//        val response = repository.getInvitationsList()
//        if (response.serverCode == 401) {
//            prefs.logout()
//        }
//        if (response.success) {
//            Log.d("HomeViewModel", "Invitations List: ${response.data?.invitationsList}")
//            _invitationsList.value = response.data?.invitationsList
//        }
//    }

    fun sendInvitation(friendId: String) = viewModelScope.launch {
        val response = repository.sendInvitation(friendId)
        if (response.serverCode == 401) {
            prefs.logout()
        }
        if (response.success) {
            showToastForInvitationStatus("Invite Request Sent")
        }
    }

    private fun showToastForInvitationStatus(message: String) = viewModelScope.launch {
        invitationEventChannel.send(InvitationEvent.InvitationStatus(message))
    }

    private fun sendCreateSessionResponse(sessionResponse: SessionResponse) =
        viewModelScope.launch {
            sessionEventsChannel.send(SessionEvent.OnSessionResponse(sessionResponse))
        }

    private fun sendGenerateTokenResponse(sessionResponse: SessionResponse) =
        viewModelScope.launch {
            sessionEventsChannel.send(SessionEvent.OnGenerateTokenResponse(sessionResponse))
        }

    fun blockOrUnblockFriend(status: Int, friendId: String) = viewModelScope.launch {
        val response = repository.blockOrUnblockFriend(status, friendId)
        if (response.serverCode == 401) {
            prefs.logout()
        }
        if (response.success) {
            showToastForInvitationStatus("Unblocked")
        }
    }

    private fun sendCreateMeetingResponse(message: String) =
        viewModelScope.launch {
            meetingsEventChannel.send(MeetingsEvent.OnCreateMeeting(message))
        }

    fun createMeeting(fields: HashMap<String, Any>) = viewModelScope.launch {
        val response = repository.createMeeting(fields);
        if (response.serverCode == 401) {
            prefs.logout()
        }
        if (response.success) {
            sendCreateMeetingResponse("Meeting Scheduled Successfully!")
        }
    }

    fun createSessionToken() = viewModelScope.launch {
        val response = repository.createSessionToken()
        if (response.serverCode == 401) {
            prefs.logout()
        }
//        _sessionResponse.value = response.data
        response.data?.let { sendCreateSessionResponse(it) }
    }

    fun generateTokenWithSession(sessionId: String) = viewModelScope.launch {
        val response = repository.generateTokenWithSession(sessionId)
        if (response.serverCode == 401) {
            prefs.logout()
        }
//        _sessionResponse.value = response.data
        response.data?.let { sendGenerateTokenResponse(it) }
    }

    fun updateProfile(hashMap: HashMap<String, Any>) = viewModelScope.launch {
        val response = repository.updateProfile(hashMap)
        if (response.serverCode == 401) {
            prefs.logout()
        }
//        prefs.userModel = response.data
        prefs.userModel?.accessToken = response.data?.accessToken
        Timber.d("UPDATE: $response")
        Timber.d("Successfully updated profile")
    }


    companion object {
        private const val CURRENT_QUERY = ""
        private const val CURRENT_USERS_QUERY = ""
    }


    sealed class ProfileEvent {
        @Keep
        data class SelectedInterests(val interestsList: List<InterestDoc>) : ProfileEvent()
    }

    sealed class InvitationEvent {
        @Keep
        data class InvitationStatus(val message: String) : InvitationEvent()

        @Keep
        data class InvitationAccepted(val message: String) : InvitationEvent()

        @Keep
        data class InvitationRejected(val message: String) : InvitationEvent()
    }

    sealed class SessionEvent {
        @Keep
        data class OnSessionResponse(val sessionResponse: SessionResponse) : SessionEvent()

        @Keep
        data class OnGenerateTokenResponse(val sessionResponse: SessionResponse) : SessionEvent()
    }

    sealed class MeetingsEvent {
        @Keep
        data class OnCreateMeeting(val message: String) : MeetingsEvent()
    }

    sealed class DeleteConversationEvent {
        object Loading : DeleteConversationEvent()
        object Success : DeleteConversationEvent()
        class Error(val message: String) : DeleteConversationEvent()
    }

    sealed class DeleteChatEvent {
        object Loading : DeleteChatEvent()
        object Success : DeleteChatEvent()
        class Error(val message: String) : DeleteChatEvent()
    }

    sealed class DeleteMeetingEvent {
        object Success : DeleteMeetingEvent()
        class Error(val message: String) : DeleteMeetingEvent()
    }

    sealed class UnInviteEvent {
        object Success : UnInviteEvent()
        class Error(val message: String) : UnInviteEvent()
    }
}

