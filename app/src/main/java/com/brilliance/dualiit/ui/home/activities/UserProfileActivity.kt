package com.brilliance.dualiit.ui.home.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ActivityUserProfileBinding
import com.brilliance.dualiit.model.UserModel
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.brilliance.dualiit.ui.meeting.activities.CreateMeetingActivity
import com.brilliance.dualiit.utils.extensions.showToast
import com.bumptech.glide.Glide
import com.google.android.material.chip.Chip
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class UserProfileActivity : AppCompatActivity() {

    private lateinit var binding: ActivityUserProfileBinding
    private val viewModel: HomeViewModel by viewModel()
    private var userId: String? = null
    private var currentFriendStatus = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        userId = intent?.getStringExtra("USER_ID")

        userId?.let {
            viewModel.getOtherUserProfile(it)
        }

        viewModel.otherUserProfile.observe(this) {
            binding.progressBar.isVisible = false
            loadUserData(it)
        }


        binding.ivNavigationBack.setOnClickListener {
            finish()
        }

        lifecycleScope.launchWhenStarted {
            viewModel.invitationEvent.collect { event ->
                when (event) {
                    is HomeViewModel.InvitationEvent.InvitationStatus -> {
                        showToast(event.message)
                        binding.btnInviteFriend.text = "UnInvite"
                        currentFriendStatus = 2
                    }
                    else -> Unit
                }
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.unInviteEvent.collect { event ->
                when (event) {
                    is HomeViewModel.UnInviteEvent.Error -> {
                        showToast(event.message)
                    }
                    is HomeViewModel.UnInviteEvent.Success -> {
                        showToast("UnInvited!")
                        binding.btnInviteFriend.text = "Invite"
                        currentFriendStatus = 1
                    }
                }
            }
        }

    }

    @SuppressLint("SetTextI18n")
    private fun loadUserData(userModel: UserModel) {
        Log.d("UserProfileActivity", "User: $userModel")
        binding.apply {
            if (userModel.avatar.isNullOrEmpty()) {
                ivUserProfile.setBackgroundResource(R.drawable.user_placeholder)
            } else {
                Glide.with(this@UserProfileActivity)
                    .load(userModel.avatar)
                    .into(ivUserProfile)
            }

            tvUserName.text = userModel.fullName

            if (userModel.area.isNullOrEmpty()) {
                tvUserLocation.text = "Unknown"
            } else {
                tvUserLocation.text =
                    "${userModel.area}, ${userModel.country}, ${userModel.region}"
            }

            if (userModel.birthDay.isNullOrEmpty()) {
                tvBirthday.text = "Not Added"
            } else {
                tvBirthday.text = userModel.birthDay
            }

            if (userModel.birthDay.isNullOrEmpty()) {
                tvBirthday.text = "Not Added"
            } else {
                tvBirthday.text = userModel.birthDay
            }

            if (userModel.icebreaker.isNullOrEmpty()) {
                tvIcebreaker.text = "Not Added"
            } else {
                tvIcebreaker.text = userModel.icebreaker
            }

            if (userModel.bio.isNullOrEmpty() || userModel.privacy?.bio == 0) {
                tvBio.text = "Bio info not Added"
            } else {
                tvBio.text = userModel.bio
            }

            if (userModel.relationshipStatus.isNullOrEmpty()) {
                tvRelationStatus.text = "Not Added"
            } else {
                tvRelationStatus.text = userModel.relationshipStatus
            }

            if (userModel.icebreaker.isNullOrEmpty()) {
                tvIcebreaker.text = "Not Added"
            } else {
                tvIcebreaker.text = userModel.icebreaker
            }

//            if (userModel.weekendplan.isNullOrEmpty() || userModel.privacy?.weekendplan == 0) {
//                tvWeekendPlan.text = "Not Added"
//            } else {
//                tvWeekendPlan.text = userModel.weekendplan
//            }

            if (userModel.orientation.isNullOrEmpty()) {
                tvOrientation.text = "Not Added"
            } else {
                tvOrientation.text = userModel.orientation
            }

            when (userModel.friendStatus) {
                1 -> {
                    btnInviteFriend.text = "Invite"
                    currentFriendStatus = 1
                }
                2 -> {
                    btnInviteFriend.text = "UnInvite"
                    currentFriendStatus = 2
                }
                3 -> {
                    btnInviteFriend.text = "Schedule Meeting"
                    currentFriendStatus = 3
//                    btnInviteFriend.setOnClickListener {
//                        val createMeetingIntent =
//                            Intent(this@UserProfileActivity, CreateMeetingActivity::class.java)
//                        createMeetingIntent.putExtra("USER_ID", userId)
//                        createMeetingIntent.putExtra("USER_NAME", userModel.fullName)
//                        createMeetingIntent.putExtra("USER_PHOTO", userModel.avatar)
//                        startActivity(createMeetingIntent)
//                    }
                }
            }

            btnInviteFriend.setOnClickListener {
                when (currentFriendStatus) {
                    1 -> {
                        viewModel.sendInvitation(userId!!)
                    }
                    2 -> {
                        viewModel.unInviteUser(userId!!)
                    }
                    3 -> {
                        val createMeetingIntent =
                            Intent(this@UserProfileActivity, CreateMeetingActivity::class.java)
                        createMeetingIntent.putExtra("USER_ID", userId)
                        createMeetingIntent.putExtra("USER_NAME", userModel.fullName)
                        createMeetingIntent.putExtra("USER_PHOTO", userModel.avatar)
                        startActivity(createMeetingIntent)
                    }
                }
            }

            if (userModel.interest.isNullOrEmpty()) {
                tvInterests.text = "Not Added"
            } else {
                tvInterests.isVisible = false
                for (i in userModel.interest!!.indices) {
                    val interestName = userModel.interest!![i]
//                    (interestsList as ArrayList).add(interestName)
                    val chip = Chip(this@UserProfileActivity)
                    val paddingDp = TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, 10F,
                        resources.displayMetrics
                    ).toInt()
                    chip.setPadding(paddingDp, paddingDp, paddingDp, paddingDp)
                    chip.chipBackgroundColor = ColorStateList.valueOf(
                        ContextCompat.getColor(
                            this@UserProfileActivity,
                            R.color.com_facebook_button_background_color_disabled
                        )
                    )
                    chip.text = interestName
                    chip.isCloseIconVisible = false
                    binding.chipGroupInterests.addView(chip)
                }
            }

        }
    }
}