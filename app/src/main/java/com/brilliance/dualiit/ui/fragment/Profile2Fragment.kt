package com.brilliance.dualiit.ui.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.FragmentProfile2Binding
import com.brilliance.dualiit.ui.base.fragment.BaseFragment
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.brilliance.dualiit.utils.extensions.hide
import com.brilliance.dualiit.utils.extensions.show
import com.bumptech.glide.Glide
import com.google.android.material.chip.Chip
import org.koin.androidx.viewmodel.ext.android.viewModel

class Profile2Fragment : BaseFragment() {

    private var _binding: FragmentProfile2Binding? = null
    private val binding get() = _binding!!

    private val homeViewModel: HomeViewModel by viewModel()

    override fun baseViewModel(): BaseViewModel = homeViewModel

    override fun createDataBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        _binding = FragmentProfile2Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadUserDetails()
    }

    @SuppressLint("SetTextI18n", "ClickableViewAccessibility")
    private fun loadUserDetails() {
        binding.apply {

            sliderAgePreference.setOnTouchListener { _, _ ->
                true
            }

            prefs.userModel?.let { user ->

                if (user.avatar.isNullOrEmpty()) {
                    ivUserProfile.setBackgroundResource(R.drawable.ic_user_avatar)
                } else {
                    Glide.with(this@Profile2Fragment)
                        .load(user.avatar)
                        .into(ivUserProfile)
                }

                tvName.text = user.fullName

                tvLocation.text = if (user.area.isNullOrEmpty()) {
                    "Location not added"
                } else {
                    "${user.area}, ${user.country}, ${user.region}"
                }

                tvEmail.text = if (user.email.isNullOrEmpty()) "Email not added" else user.email

                tvPhone.text = if (user.phone.isNullOrEmpty()) "Phone not added" else user.phone

                tvGender.text = if (user.gender.isNullOrEmpty()) "Not added" else user.gender

                tvBirthday.text = if (user.birthDay.isNullOrEmpty()) "Not added" else user.birthDay

                tvGenderPreference.text =
                    if (user.interested.isNullOrEmpty()) "Not added" else user.interested

                tvIcebreaker.text =
                    if (user.icebreaker.isNullOrEmpty()) "Not added" else user.icebreaker

//                tvWeekendPlan.text =
//                    if (user.weekendplan.isNullOrEmpty()) "Not added" else user.weekendplan

                tvRelationStatus.text =
                    if (user.relationshipStatus.isNullOrEmpty()) "Not added" else user.relationshipStatus

                tvOrientation.text =
                    if (user.orientation.isNullOrEmpty()) "Not added" else user.orientation

                tvBio.text = if (user.bio.isNullOrEmpty()) "Not added" else user.bio

                if (user.interest.isNullOrEmpty()) {
                    tvAddIntersts.show()
                    chipGroupInterests.hide()
                } else {
                    tvAddIntersts.hide()
                    for (i in user.interest!!.indices) {
                        val interestName = user.interest!![i]
                        val chip = Chip(requireContext())
                        val paddingDp = TypedValue.applyDimension(
                            TypedValue.COMPLEX_UNIT_DIP, 10F,
                            resources.displayMetrics
                        ).toInt()
                        chip.setPadding(paddingDp, paddingDp, paddingDp, paddingDp)
                        chip.chipBackgroundColor = ColorStateList.valueOf(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.com_facebook_button_background_color_disabled
                            )
                        )
                        chip.text = interestName
                        chip.isCloseIconVisible = false
                        chip.id = ViewCompat.generateViewId();
                        chipGroupInterests.addView(chip)

                    }
                }

                if (user.minAge == null || user.maxAge == null ||
                    user.minAge == 0 || user.maxAge == 0
                ) {
                    sliderAgePreference.setValues(18f, 60f)
                    tvMinMaxAge.text = "18-60"
                } else {
                    val minAge = user.minAge?.toFloat()
                    val maxAge = user.maxAge?.toFloat()
                    sliderAgePreference.setValues(minAge, maxAge)
                    tvMinMaxAge.text = "${user.minAge}-${user.maxAge}"
                }


            }

            binding.btnSettings.setOnClickListener {
                startActivity(Intent(requireActivity(), SettingsActivity::class.java))
            }

            binding.btnEditProfile.setOnClickListener {
                startActivity(Intent(requireActivity(), EditProfileActivity::class.java))
            }
        }
    }

    override fun setObserver() {
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}