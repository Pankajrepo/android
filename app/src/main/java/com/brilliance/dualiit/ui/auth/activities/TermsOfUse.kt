package com.brilliance.dualiit.ui.auth.activities

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import com.brilliance.dualiit.databinding.ActivityTermsOfUseBinding
import com.brilliance.dualiit.ui.auth.viewmodel.AuthViewModel
import com.brilliance.dualiit.ui.base.activity.BaseActivity
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class TermsOfUse : AppCompatActivity() {


    private lateinit var binding: ActivityTermsOfUseBinding

    private var url: String =
        "https://brilliance.co.in/dualiit-terms-and-conditions/"


    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTermsOfUseBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.privacyWebview.loadUrl(url)

        binding.privacyWebview.settings.javaScriptEnabled = true

        binding.privacyWebview.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                binding.privacyProgress.visibility = View.VISIBLE
                binding.privacyProgress.progress = newProgress

                if (newProgress == 100) {
                    binding.privacyProgress.visibility = View.GONE
                }

                super.onProgressChanged(view, newProgress)
            }
        }

        binding.ivNavigationBack.setOnClickListener {
            finish()
        }

    }
}