package com.brilliance.dualiit.ui.chat

import androidx.paging.PagingSource
import com.brilliance.dualiit.ui.chat.models.Message
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.networkRequest.ApiService
import retrofit2.HttpException
import java.io.IOException

private const val MESSAGES_STARTING_PAGE_INDEX = 1


class MessagesPagingSource(
    private val apiService: ApiService,
    private val prefs: AppPreferencesHelper,
    private val key: String
) : PagingSource<Int, Message>() {

    private var messages: List<Message>? = ArrayList()


    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Message> {
        val position = params.key ?: MESSAGES_STARTING_PAGE_INDEX
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken

        return try {

            val response = apiService.getUserChats(
                headers = map,
                page = position,
                key = key,
                last = 1
            )

            messages = response.data?.messages

            LoadResult.Page(
                data = messages!!,
                prevKey = if (position == MESSAGES_STARTING_PAGE_INDEX) null else position - 1,
                nextKey = if (messages!!.isEmpty()) null else position + 1
            )

        } catch (e: IOException) {
            LoadResult.Error(e)
        } catch (e: HttpException) {
            LoadResult.Error(e)
        }
    }


}