package com.brilliance.dualiit.ui.base.activity

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.brilliance.dualiit.BuildConfig
import com.brilliance.dualiit.R
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.extensions.launchActivity
import com.brilliance.dualiit.utils.extensions.showDialog
import com.brilliance.dualiit.utils.networkRequest.ApiService
import com.theartofdev.edmodo.cropper.CropImage
import dagger.hilt.android.AndroidEntryPoint
import org.koin.android.ext.android.inject
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

@AndroidEntryPoint
abstract class BaseActivity : AppCompatActivity() {
    var isGranted = false
    abstract fun baseViewModel(): BaseViewModel?
    abstract fun setObserver()
    val prefs: AppPreferencesHelper by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setStatusBar(this)
        observeError()
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    fun setStatusBar(activity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = activity.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        }
    }

    fun setBaseObserver() {
        baseViewModel()?.let { viewModel ->

            viewModel.actionBarOnBackPressListener.observe(this, Observer {
                if (it) {
                    onBackPressed()
                    finish()
                }
            })
            viewModel.redirect.observe(this, Observer {
                it?.let {
                    launchActivity(
                        it.mClass,
                        arguments = it.arguments,
                        isTopFinish = it.isTopFinish,
                        flagIsTopClearTask = it.flagIsTopClearTask
                    )
                }
            })
            viewModel.showDialog.observe(this, Observer {
                it?.let {
                    this.showDialog(
                        title = it.title,
                        message = it.message,
                        onPressOk = View.OnClickListener { v ->
                            it.mClass?.let { mClass ->
                                launchActivity(
                                    mClass,
                                    arguments = it.arguments,
                                    isTopFinish = it.isTopFinish,
                                    flagIsTopClearTask = it.flagIsTopClearTask
                                )
                            }
                        }
                    )
                }
            })
        }
    }

    private fun observeError() {
        baseViewModel()?.let { viewModel ->
            viewModel.mErrorMessage.observe(this, Observer {
                Log.d("observeError", "observeError " + it.statusCode)
                when (it.statusCode) {
                    0 -> {
                        showToast(it.message)
                    }
                    601 -> {
                        viewModel.clearCart.postValue(it.message)
                    }
                    200 -> {
                        if (it.priority == ApiService.PRIORITY_HIGH) {
                            viewModel.errorMessage.set(it.message)
                        } else {
                            showToast(it.message)
                        }
                    }
                    else -> {
                        if (it.priority == ApiService.PRIORITY_HIGH) {
                            viewModel.errorMessage.set(it.message)
                        } else {
                            showToast(it.message)
                        }
                    }
                }
            })
        }
    }

    fun showToast(msg: String? = "") {
        Toast.makeText(this, "" + msg, Toast.LENGTH_SHORT).show()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        setVersionOnScreen()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CAPTURE) {
            if (resultCode == Activity.RESULT_OK) {
                if (captureMediaFile != null) {
                    cropImage(captureMediaFile)
                } else if (data != null && data.extras != null) {
                    var out: FileOutputStream? = null
                    try {
                        val extras = data.extras
                        val imageBitmap = extras!!["data"] as Bitmap?
                        val mediaPath = File.createTempFile(
                            "image",
                            ".png",
                            File(
                                getTempMediaDirectory(
                                    this
                                )
                            )
                        ).absolutePath
                        out = FileOutputStream(mediaPath)
                        imageBitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, out)
                        cropImage(Uri.fromFile(File(mediaPath)))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    } finally {
                        try {
                            out?.close()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                } else {
                    Log.e("Invalid", "Invalid data")
                }
            }
        } else if (requestCode == REQUEST_GALLERY && data != null) {
            cropImage(data.data)
        } else if (requestCode == REQUEST_SAVE_IMAGE_IN_GALLERY && data != null) {
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                val resultUri = result.uri
                getCroppedImage(resultUri)
            }
        } else {


            //    show_alert();
        }
    }

    fun getCroppedImage(resultUri: Uri?) {
        baseViewModel()?.let {
            it.croppedImage.postValue(resultUri)
        }
    }

    //Set Version on Screen
    fun setVersionOnScreen() {
        val tvSample = ImageView(this)
        tvSample.setPadding(4, 4, 4, 4)
        val viewGroup = RelativeLayout.LayoutParams(
            150,
            150
        )
        viewGroup.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
        viewGroup.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
        viewGroup.setMargins(10, 10, 10, 150)
        val relativeLayout = RelativeLayout(this)
        val relative = RelativeLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        relativeLayout.layoutParams = relative
        relativeLayout.addView(tvSample)
        tvSample.layoutParams = viewGroup
        (findViewById<View>(android.R.id.content) as ViewGroup).addView(relativeLayout)
    }

    fun askListOfPermissions(
        permissions: Array<String?>,
        requestCode: Int
    ): Boolean {
        var isOk = true
        val perNeed = StringBuilder()
        for (per in permissions) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    per!!
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (isOk) isOk = false
                perNeed.append(per)
                perNeed.append(",")
            }
        }
        if (isOk) {
            return true
        }
        val reqPermissions =
            if (perNeed.length > 1) perNeed.substring(0, perNeed.length - 1).toString() else ""
        if (!reqPermissions.isEmpty()) {
            val arrPer =
                reqPermissions.split(",".toRegex()).toTypedArray()
            ActivityCompat.requestPermissions(this, permissions, requestCode)
        }
        return false
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        isGranted = true
        for (grantResult in grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                isGranted = false

                // System.out.println("prefered location===" + getApplicationClass().getPreferedLocation());
                break
            }
        }
        if (isGranted) {
            if (requestCode == REQUEST_CAPTURE) {
                pickImageFromCamera()
            } else if (requestCode == REQUEST_GALLERY) {
                pickImageFromGallery()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    // ------------------------------ Current Location Code End --------------------------------//
    var captureMediaFile: Uri? = null

    //Capture Image
    fun dialogImagePicker() { //TO pick image from gallery and camera
        val imagePicker =
            AlertDialog.Builder(this, R.style.AppTheme_AlertDialog)
        imagePicker.setItems(
            arrayOf(
                getString(R.string.gallery),
                getString(R.string.camera)
            )
        ) { _, which ->
            val intent = Intent()
            when (which) {
                0 -> pickImageFromGallery()
                1 -> pickImageFromCamera()
                else -> {
                }
            }
        }.setCancelable(true).setTitle(getString(R.string.select_image)).show()
    }

    //Pick Image From Gallery
    fun pickImageFromGallery() {
        if (!askListOfPermissions(
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ),
                REQUEST_GALLERY
            )
        ) return
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        //        intent.setType("image*//**//*");
        intent.type = "image/*"
        startActivityForResult(
            Intent.createChooser(intent, getString(R.string.select_image)),
            REQUEST_GALLERY
        )
    }

    fun pickImageFromCamera() {
        pickImageFromCamera(
            getTempMediaDirectory(
                this
            )
        )
    }

    var tempFile: File? = null

    //Pick Image From Camera
    fun pickImageFromCamera(dir: String?): File? {
        if (!askListOfPermissions(
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ),
                REQUEST_CAPTURE
            )
        ) return null
        val intent = Intent()
        intent.action = MediaStore.ACTION_IMAGE_CAPTURE
        try {
            tempFile = File.createTempFile("image", ".png", File(dir))
            captureMediaFile = FileProvider.getUriForFile(
                this,
                BuildConfig.APPLICATION_ID + ".provider",
                tempFile!!
            )
            intent.putExtra(MediaStore.EXTRA_OUTPUT, captureMediaFile)
            intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        } catch (e: IOException) {
            e.printStackTrace()
        }
        startActivityForResult(
            intent,
            REQUEST_CAPTURE
        )
        return tempFile
    }

    //Crop Image
    fun cropImage(sourceFileUri: Uri?) {
        CropImage.activity(sourceFileUri).setOutputCompressQuality(30)
            .setInitialCropWindowPaddingRatio(0f)
            .setAspectRatio(1, 1)
            .start(this)
    }

    fun replaceFragment(
        @IdRes container: Int,
        fragment: Fragment,
        arguments: Bundle?
    ) {
        if (arguments != null) {
            fragment.arguments = arguments
        }
        Handler().postDelayed({
            supportFragmentManager.beginTransaction().replace(container, fragment).commit()
        }, 300)
    }

    companion object {
        const val REQUEST_CAPTURE = 112
        const val REQUEST_GALLERY = 113
        const val REQUEST_SAVE_IMAGE_IN_GALLERY = 115
        fun getTempMediaDirectory(context: Context): String? {
            val state = Environment.getExternalStorageState()
            var dir: File? = null
            dir = if (Environment.MEDIA_MOUNTED == state) {
                context.externalCacheDir
            } else {
                context.cacheDir
            }
            if (dir != null && !dir.exists()) {
                dir.mkdirs()
            }
            return if (dir!!.exists() && dir.isDirectory) {
                dir.absolutePath
            } else null
        }
    }
}