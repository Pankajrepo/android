package com.brilliance.dualiit.ui.speeddating.models

import androidx.annotation.Keep

@Keep
data class SignalMessage(
    val screenShare: String? = "",
    val remote: Boolean? = false
)