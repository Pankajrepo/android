package com.brilliance.dualiit.ui.base.viewModel

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import androidx.annotation.DrawableRes
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.brilliance.dualiit.model.DialogSuccessModel
import com.brilliance.dualiit.model.ErrorData
import com.brilliance.dualiit.model.RedirectModel
import com.brilliance.dualiit.model.UserModel
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.SingleLiveEvent
import com.brilliance.dualiit.utils.networkRequest.ApiService
import com.brilliance.dualiit.utils.networkRequest.CallbackWrapper
import rx.Observable
import rx.Subscriber
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

abstract class BaseViewModel(
    private val prefs: AppPreferencesHelper
) : ViewModel() {
    val isLogin = SingleLiveEvent<Boolean>()
    val isRefresh = ObservableBoolean(false)
    val clearCart =  MutableLiveData<String>()
    val errorMessage = ObservableField<String>("")
    val progress = ObservableBoolean(false)
    val redirect = SingleLiveEvent<RedirectModel>()
    val showDialog = SingleLiveEvent<DialogSuccessModel>()
    val showSnakBar = SingleLiveEvent<String>()
    val setResult = SingleLiveEvent<Intent>()
    val actionLogin = SingleLiveEvent<Boolean>()
    val croppedImage = SingleLiveEvent<Uri>()

    /*---------START FOR ACTION BAR-----------*/
    val actionBarTitle = SingleLiveEvent<String>()
    val actionBarOnBackPressIcon = SingleLiveEvent<@DrawableRes Int>()
    val actionBarOnBackPressListener = SingleLiveEvent<Boolean>()
    val actionBarBackVisible = SingleLiveEvent<Boolean>()
    val actionBarDoneVisible = SingleLiveEvent<Boolean>()
    val actionBarOnClickDoneListener = SingleLiveEvent<Boolean>()
    val actionBarDoneText = SingleLiveEvent<String>()
    val actionBarSearchView = SingleLiveEvent<Boolean>()
    private val _mErrorMessage = MutableLiveData<ErrorData>()
    val mErrorMessage: LiveData<ErrorData>
        get() = _mErrorMessage
    /*---------START FOR ACTION BAR-----------*/

    fun actionBarOnBackPress(view: View) {
        actionBarOnBackPressListener.postValue(true)
    }

    fun actionBarOnClickDone(view: View) {
        actionBarOnClickDoneListener.postValue(true)
    }

    /*------------USER INFORMATION ---------------*/
    val userModel = SingleLiveEvent<UserModel>()

    init {
        isLogin.postValue(prefs.isUserLogin())
        actionBarBackVisible.postValue(true)
        actionBarDoneVisible.postValue(true)
        actionBarTitle.postValue("V4L")
        actionBarDoneText.postValue("")
        actionBarSearchView.postValue(false)
        userModel.postValue(prefs.getPrefUserDetail())
    }

    fun mainLogin(view: View) {
        actionLogin.postValue(true)
    }

    /*---------------API CALLING---------------------*/
    private var subscriber: Subscription? = null

    fun <T> requestData(
        api: Observable<T>,
        success: (m: T) -> Unit,
        failed: (message: String?, statusCode: Int) -> Unit = onFailed,
        onServerNetworkFailed: (m: String?, statusCode: Int) -> Unit = onFailed,
        priority:Int = ApiService.PRIORITY_DEFAULT
    ) {
        progress.set(true)
        api.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .unsubscribeOn(Schedulers.io())
            .subscribe(object : CallbackWrapper<T>() {
                override fun onSuccess(t: T) {
                    isRefresh.set(false)
                    progress.set(false)
                    errorMessage.set("")
                    success(t)
                }

                override fun onError(message: String?, statusCode: Int) {
                    isRefresh.set(false)
                    progress.set(false)
                    when (statusCode) {
                        401 -> onServerNetworkFailed(message, statusCode)
                        404 -> onServerNetworkFailed(message, statusCode)
                        500 -> failed("Internal Server error", statusCode)
                        else ->      _mErrorMessage.postValue(ErrorData(message,statusCode,priority))
                    }
                }
            })
    }

    fun <T> requestPlaceData(
        api: Observable<T>,
        success: (m: T) -> Unit,
        failed: (m: String?, statusCode: Int) -> Unit = onFailed
    ) {
        progress.set(true)
        subscriber = api.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .unsubscribeOn(Schedulers.io())
            .subscribe(object : Subscriber<T>() {
                override fun onError(e: Throwable?) {
                    isRefresh.set(false)
                    progress.set(false)
                    failed(e?.message, 0)
                }

                override fun onNext(t: T) {
                    isRefresh.set(false)
                    progress.set(false)
                    success(t)
                }

                override fun onCompleted() {
                }
            })
    }

    private val onFailed: (m: String?, statusCode: Int) -> Unit = { msg, code ->
        Log.d("onFailed", " BASE VIEW ")
        when (code) {
            401 -> {
                // Here Auth Failed! exception
                msg?.let {
                    Log.d("onFailed", "$it")
                }
            }
            404 -> {
                // NO Internet Available
                msg?.let {
                    showDialog.postValue(
                        DialogSuccessModel(
                            title = "No Internet Available",
                            message = it
                        )
                    )
                }
            }
            else -> {
                msg?.let {
                     errorMessage.set(it)
                }
            }
        }
    }
}