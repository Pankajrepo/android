package com.brilliance.dualiit.ui.meeting.models.callevents

data class To(
    val _id: String? = "",
    val name: String? = ""
)