package com.brilliance.dualiit.ui.meeting.models.callevents

data class CallingResponse(
    val from: From? = null,
    val meetingId: String? = "",
    val status: String? = "",
    val to: To? = null,
    val type: String? = ""
)