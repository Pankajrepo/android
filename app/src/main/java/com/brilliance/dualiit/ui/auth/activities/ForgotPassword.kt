package com.brilliance.dualiit.ui.auth.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.lifecycleScope
import com.brilliance.dualiit.BuildConfig
import com.brilliance.dualiit.databinding.ActivityForgotPasswordBinding
import com.brilliance.dualiit.ui.auth.helpers.OTPEvent
import com.brilliance.dualiit.ui.auth.request.ResetRequest
import com.brilliance.dualiit.ui.auth.request.SignupRequest
import com.brilliance.dualiit.ui.auth.viewmodel.AuthViewModel
import com.brilliance.dualiit.ui.base.activity.BaseActivity
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.utils.AppConstant
import com.brilliance.dualiit.utils.extensions.showToast
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class ForgotPassword : BaseActivity() {
    private lateinit var binding: ActivityForgotPasswordBinding
    private val viewModel: AuthViewModel by viewModel()
    override fun baseViewModel(): BaseViewModel = viewModel
    private var deviceToken: String? = ""
    override fun setObserver() {
        super.setBaseObserver()

        lifecycleScope.launchWhenStarted {
            viewModel.otpEvent.collect { event ->
                when (event) {
                    is OTPEvent.NumberAlreadyExist -> {
                    }
                    is OTPEvent.OTPFailed -> {
                        showToast(event.message)
                    }
                    is OTPEvent.OTPSuccess -> {
                        showToast("OTP Sent Successfully")
                        val phoneNumber =
                            "${binding.countryCodePicker.selectedCountryCodeWithPlus}${
                                binding.etPhone.text.toString().trim()
                            }"
                        val resetRequest = ResetRequest()
                        resetRequest.appVersion = BuildConfig.VERSION_NAME
                        resetRequest.deviceType = BuildConfig.DEVICE_TYPE
                        resetRequest.deviceToken = deviceToken
                        resetRequest.phone = phoneNumber
                        startActivity(
                            Intent(this@ForgotPassword, ValidateOTP::class.java).putExtra(
                                AppConstant.REQ_RESET,
                                true
                            ).putExtra(AppConstant.REQ_EXTRA, resetRequest)
                        )
                    }
                }
            }
        }

//        viewModel.receiveOTP.observe(this, {
//            val resetRequest = ResetRequest()
//            resetRequest.appVersion = BuildConfig.VERSION_NAME
//            resetRequest.deviceType = BuildConfig.DEVICE_TYPE
//            resetRequest.deviceToken = "DUMMY"
//            resetRequest.phone = binding.etPhone.text.toString()
//            resetRequest.password = ""
//            startActivity(
//                Intent(this, ValidateOTP::class.java).putExtra(
//                    AppConstant.REQ_RESET,
//                    true
//                ).putExtra(AppConstant.REQ_EXTRA, resetRequest)
//            )
//
//        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBar(this)
        binding = ActivityForgotPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setObserver()

        FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.d("FIREBASE_TOKEN", "Fetching FCM registration token failed", task.exception)
                return@addOnCompleteListener
            }

            deviceToken = task.result
            Log.d("FIREBASE_TOKEN", "TOKEN $deviceToken")
        }

        binding.ivNavigationBack.setOnClickListener {
            finish()
        }

        binding.btnSendCode.setOnClickListener {

            if (!validatePhoneNumber()) {
                return@setOnClickListener
            }
            val phoneNumber =
                "${binding.countryCodePicker.selectedCountryCodeWithPlus}${
                    binding.etPhone.text.toString().trim()
                }"

            viewModel.phone.value = phoneNumber
            viewModel.sendSMSCode()

        }

    }

    private fun validatePhoneNumber(): Boolean {
        val phoneNumber = binding.tiPhone.editText?.text.toString().trim()
        return when {
            phoneNumber.isEmpty() -> {
                binding.tiPhone.error = "Phone Number can not be empty"
//                binding.tiPhone.requestFocus()
                false
            }
            phoneNumber.length < 10 -> {
                binding.tiPhone.error = "Invalid phone number"
//                binding.tiPhone.requestFocus()
                false
            }
            else -> {
                binding.tiPhone.error = null
                binding.tiPhone.isErrorEnabled = false
                true
            }
        }
    }
}