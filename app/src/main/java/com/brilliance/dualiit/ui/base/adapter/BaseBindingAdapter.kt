package com.brilliance.dualiit.ui.base.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import com.brilliance.dualiit.ui.base.model.BaseViewHolder

abstract class BaseBindingAdapter(var list: ArrayList<*>) : RecyclerView.Adapter<BaseViewHolder>() {
    var context: Context? = null
    var mColorInt = 0
    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }
}