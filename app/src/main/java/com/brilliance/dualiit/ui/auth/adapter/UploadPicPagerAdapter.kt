package com.brilliance.dualiit.ui.auth.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.viewpager.widget.PagerAdapter
import com.brilliance.dualiit.R

class UploadPicPagerAdapter internal constructor(var context: Context) :
        PagerAdapter() {
    private val GalImages = intArrayOf(
            R.drawable.dummy_1,
            R.drawable.dummy_2,
            R.drawable.dummy_1
    )
    var mLayoutInflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    override fun getCount(): Int {
        return GalImages.size
    }

    override fun isViewFromObject(
            view: View,
            `object`: Any
    ): Boolean {
        return view === `object` as LinearLayout
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView =
                mLayoutInflater.inflate(R.layout.upload_pic_pager_item, container, false)
        val imageView =
                itemView.findViewById<View>(R.id.imageView) as ImageView
        imageView.setImageResource(GalImages[position])
        container.addView(itemView)
        return itemView
    }

    override fun destroyItem(
            container: ViewGroup,
            position: Int,
            `object`: Any
    ) {
        container.removeView(`object` as LinearLayout)
    }

}