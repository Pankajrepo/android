package com.brilliance.dualiit.ui.invitation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ItemInvitationBinding
import com.brilliance.dualiit.ui.invitation.models.InvitationModel
import com.bumptech.glide.Glide
import org.ocpsoft.prettytime.PrettyTime
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class InvitationAdapter :
    PagingDataAdapter<InvitationModel, InvitationAdapter.InvitationViewHolder>(DiffCallback()) {

    inner class InvitationViewHolder(private val binding: ItemInvitationBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.btnAcceptInvitation.setOnClickListener {
                onAcceptClickListener?.let { click ->
                    val invitation = getItem(bindingAdapterPosition)
                    if (invitation != null) {
                        click(invitation)
                    }
                }
            }

            binding.btnRejectInvitation.setOnClickListener {
                onAcceptClickListener?.let { click ->
                    val invitation = getItem(bindingAdapterPosition)
                    if (invitation != null) {
                        click(invitation)
                    }
                }
            }
            binding.ivProfile.setOnClickListener {
                onProfileClickListener?.let { click ->
                    val userId = getItem(bindingAdapterPosition)?.friendId?._id
                    if (userId != null) {
                        click(userId)
                    }
                }
            }
        }

        fun bind(invitation: InvitationModel) {
            binding.apply {
                if (invitation.friendId?.avatar == "") {
                    ivProfile.setBackgroundResource(R.drawable.ic_user_avatar)
                } else {
                    Glide.with(itemView)
                        .load(invitation.friendId?.avatar)
                        .into(ivProfile)
                }
                tvUserName.text = invitation.friendId?.fullName
                tvLastMsgTime.text = formattedDate(invitation.updatedAt.toString())
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InvitationViewHolder {
        val binding =
            ItemInvitationBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return InvitationViewHolder(binding)
    }

    override fun onBindViewHolder(holder: InvitationViewHolder, position: Int) {
        val invitation = getItem(position)
        if (invitation != null) {
            holder.bind(invitation)
        }

    }

    class DiffCallback : DiffUtil.ItemCallback<InvitationModel>() {
        override fun areItemsTheSame(oldItem: InvitationModel, newItem: InvitationModel): Boolean {
            return oldItem.friendId?._id == newItem.friendId?._id
        }

        override fun areContentsTheSame(
            oldItem: InvitationModel,
            newItem: InvitationModel
        ): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private var onAcceptClickListener: ((InvitationModel) -> Unit)? = null

    fun setOnAcceptClickListener(listener: (InvitationModel) -> Unit) {
        onAcceptClickListener = listener
    }

    private var onProfileClickListener: ((String) -> Unit)? = null

    fun setOnProfileClickListener(listener: (String) -> Unit) {
        onProfileClickListener = listener
    }

    private var onRejectClickListener: ((InvitationModel) -> Unit)? = null

    fun setOnRejectClickListener(listener: (InvitationModel) -> Unit) {
        onRejectClickListener = listener
    }

    private fun formattedDate(t: String): String {
        val prettyTime = PrettyTime(Locale(getCountry()))
        var time: String? = null
        try {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:", Locale.ENGLISH)
            val date = simpleDateFormat.parse(t)
            val simpleDateFormat2 = SimpleDateFormat("MMM dd yyyy HH:mm aa", Locale.ENGLISH)
//            time = prettyTime.format(date)
            time = simpleDateFormat2.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return time!!
    }

    private fun getCountry(): String {
        val locale = Locale.getDefault()
        val country = locale.country
        return country.toLowerCase()
    }
}