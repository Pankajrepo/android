package com.brilliance.dualiit.ui.auth.activities

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.telephony.TelephonyManager
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.DatePicker
import androidx.core.view.isVisible
import com.brilliance.dualiit.databinding.ActivityCompleteProfileBinding
import com.brilliance.dualiit.ui.auth.viewmodel.AuthViewModel
import com.brilliance.dualiit.ui.base.activity.BaseActivity
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.ui.home.activities.MainActivity
import com.google.android.material.chip.Chip
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File
import java.util.*


class CompleteProfile : BaseActivity(), DatePickerDialog.OnDateSetListener {

    private lateinit var binding: ActivityCompleteProfileBinding
    private val viewModel: AuthViewModel by viewModel()
    override fun baseViewModel(): BaseViewModel = viewModel
    private var uri: Uri? = null
    private var userAvatar: String? = null
    private var userGender: String? = ""
    private var userPreference: String? = ""
    private var userStatus: String? = ""
    override fun setObserver() {

        viewModel.userAvatar.observe(this) {
            userAvatar = it
        }

        viewModel.uploadProgress.observe(this) {
            binding.progressBar.isVisible = it == true
        }


        viewModel.croppedImage.observe(this) {
            uri = it
//            binding.userImg.visibility = View.GONE
            binding.selectImg.visibility = View.VISIBLE
            binding.selectImg.setImageURI(uri)
            if (uri != null) {
                val file = File(uri!!.path!!)

                val filePart = MultipartBody.Part.createFormData(
                    "images",
                    file.name,
                    file.asRequestBody("image/*".toMediaTypeOrNull())
                )
                viewModel.uploadAvatar(filePart)
            }

        }

        viewModel.completeProfileProgress.observe(this) {
            if (it == true) {
                binding.updateProgressBar.isVisible = true
                binding.btnUpdateProfile.isVisible = false
                showToast("Profile Updated")
                startActivity(
                    Intent(this, MainActivity::class.java)
                        .setFlags(
                            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        )
                )
                finish()
            } else {
                binding.updateProgressBar.isVisible = false
                binding.btnUpdateProfile.isVisible = true

            }
        }

        super.setBaseObserver()
    }

    private var currentDay = 0
    private var currentMonth = 0
    private var currentYear = 0
    private var savedDay = 0
    private var savedMonth = 0
    private var savedYear = 0
    private var dateOfBirth: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCompleteProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setObserver()

        binding.etBirthdayPicker.inputType = InputType.TYPE_NULL


        binding.etBirthdayPicker.setOnClickListener {
            pickBirthdayDate()
        }

        binding.userImg.setOnClickListener {
            dialogImagePicker()
        }

        binding.chipGroupUserGender.setOnCheckedChangeListener { group, checkedId ->
            val selectedChip = group.findViewById<Chip>(checkedId)
            if (selectedChip != null) {
                userGender = selectedChip.text.toString().lowercase(Locale.ROOT)
            }
        }

        binding.chipGroupStatus.setOnCheckedChangeListener { group, checkedId ->
            val selectedChip = group.findViewById<Chip>(checkedId)
            if (selectedChip != null) {
                userStatus = selectedChip.text.toString().lowercase(Locale.ROOT)
            }
        }

        binding.chipGroupPreference.setOnCheckedChangeListener { group, checkedId ->
            val selectedChip = group.findViewById<Chip>(checkedId)

            if (selectedChip != null) {
                userPreference = if (selectedChip.text.toString() == "Both") {
                    "other"
                } else {
                    selectedChip.text.toString().lowercase(Locale.ROOT)
                }
            }
        }

        binding.btnUpdateProfile.setOnClickListener {
            if (!validateBio() || !validateBirthday() || !validateGender() || !validateStatus()
                || !validatePreference() || !validateImageUri()
            ) {
                return@setOnClickListener
            }
            val userMap = HashMap<String, Any>()
            val bio = binding.tiBio.editText?.text.toString().trim()

            if (userAvatar != null) {
                userMap["avatar"] = userAvatar!!
            }

            if (bio.isNotEmpty()) {
                userMap["bio"] = bio
            }

            val country = getCountry()

            Log.d("CompleteProfile", "Country: $country")

            if (country.isNotEmpty()) {
                userMap["country"] = country
            }

            if (!dateOfBirth.isNullOrEmpty()) {
                userMap["birthDay"] = dateOfBirth!!
            }

            if (userPreference != null) {
                userMap["interested"] = userPreference!!
            }

            if (userGender != null) {
                userMap["gender"] = userGender!!
            }

            if (userStatus != null) {
                userMap["relationshipStatus"] = userStatus!!
            }

            viewModel.updateCompleteProfile(userMap)
        }
    }

    private fun getDateTimeCalender() {
        val cal = Calendar.getInstance()
        currentDay = cal.get(Calendar.DAY_OF_MONTH)
        currentMonth = cal.get(Calendar.MONTH)
        currentYear = cal.get(Calendar.YEAR)
    }

    private fun pickBirthdayDate() {
        getDateTimeCalender()
        val datePickerDialog =
            DatePickerDialog(this, this, currentYear - 18, currentMonth, currentDay)
        val calender = Calendar.getInstance().apply {
            set(currentYear - 18, currentMonth, currentDay)
        }
        datePickerDialog.datePicker.maxDate = calender.timeInMillis
        datePickerDialog.show()
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        savedDay = dayOfMonth
        savedMonth = month + 1
        savedYear = year

        var monthString = savedMonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        }

        dateOfBirth = "${savedDay}-${monthString}-${savedYear}"
        binding.etBirthdayPicker.setText(dateOfBirth)
    }

    private fun validateImageUri(): Boolean {
        return if (uri == null) {
            showToast("Please Select User Profile!")
            false
        } else {
            true
        }
    }

    private fun validateGender(): Boolean {
        return if (binding.chipGroupUserGender.checkedChipId == -1) {
            showToast("Please select Gender!")
            false
        } else {
            true
        }
    }

    private fun validateStatus(): Boolean {
        return if (binding.chipGroupStatus.checkedChipId == -1) {
            showToast("Please select Your Relationship status!")
            false
        } else {
            true
        }
    }

    private fun validatePreference(): Boolean {
        return if (binding.chipGroupPreference.checkedChipId == -1) {
            showToast("Please select Preference!")
            false
        } else {
            true
        }
    }

    private fun validateBio(): Boolean {
        val bio = binding.tiBio.editText?.text.toString().trim()
        return if (bio.isEmpty()) {
            binding.tiBio.error = "Please enter your Bio here"
            false
        } else {
            binding.tiBio.error = null
            binding.tiBio.isErrorEnabled = false
            true
        }
    }

    private fun validateBirthday(): Boolean {
        return if (dateOfBirth?.isEmpty()!!) {
            binding.etBirthdayPicker.error = "Please enter your Date of Birth"
            false
        } else {
            binding.etBirthdayPicker.error = null
            true
        }
    }

    private fun getCountry(): String {
        val telephoneManager =
            this.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        return telephoneManager.networkCountryIso.uppercase()
    }
}