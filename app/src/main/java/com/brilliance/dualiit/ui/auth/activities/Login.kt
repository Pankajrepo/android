package com.brilliance.dualiit.ui.auth.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.lifecycleScope
import com.brilliance.dualiit.BuildConfig
import com.brilliance.dualiit.databinding.ActivityLoginBinding
import com.brilliance.dualiit.ui.auth.request.LoginRequest
import com.brilliance.dualiit.ui.auth.viewmodel.AuthViewModel
import com.brilliance.dualiit.ui.base.activity.BaseActivity
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.ui.home.activities.MainActivity
import com.brilliance.dualiit.utils.AppConstant
import com.brilliance.dualiit.utils.AppConstant.TERMS_OF_USE_URL
import com.brilliance.dualiit.utils.Resource
import com.brilliance.dualiit.utils.extensions.hide
import com.brilliance.dualiit.utils.extensions.launchActivity
import com.brilliance.dualiit.utils.extensions.show
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class Login : BaseActivity() {
    private lateinit var binding: ActivityLoginBinding
    private val viewModel: AuthViewModel by viewModel()
    override fun baseViewModel(): BaseViewModel = viewModel
    private var deviceToken: String? = ""
    override fun setObserver() {

        lifecycleScope.launchWhenStarted {
            viewModel.loginState.collect {
                when (it) {
                    is Resource.Empty -> {
                    }
                    is Resource.Error -> {
                        binding.progressBar.hide()
                        showToast(it.message)
                    }
                    is Resource.Loading -> {
                        binding.progressBar.show()

                    }
                    is Resource.Success -> {
                        binding.progressBar.hide()
                        showToast("Logged in successfully")
                        launchActivity(
                            aClass = MainActivity::class.java,
                            isTopFinish = true,
                            flagIsTopClearTask = true
                        )
                    }
                }
            }
        }

        super.setBaseObserver()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBar(this)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setObserver()

        FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.d("FIREBASE_TOKEN", "Fetching FCM registration token failed", task.exception)
                return@addOnCompleteListener
            }

            deviceToken = task.result
            Log.d("FIREBASE_TOKEN", "TOKEN $deviceToken")
        }

        binding.forgotPassword.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    ForgotPassword::class.java
                )
            )

        }

        binding.btnLogin.setOnClickListener {
            if (!validatePhoneNumber() || !validatePassword()) {
                return@setOnClickListener
            }
            val phoneNumber =
                "${binding.countryCodePicker.selectedCountryCodeWithPlus}${
                    binding.etPhone.text.toString().trim()
                }"
            val loginRequest = LoginRequest()
            loginRequest.appVersion = BuildConfig.VERSION_NAME
            loginRequest.deviceType = BuildConfig.DEVICE_TYPE
            loginRequest.deviceToken = deviceToken
            loginRequest.phone = phoneNumber
            loginRequest.password = binding.etPassword.text.toString()

            Log.d("LoginRequest", "$loginRequest")
            viewModel.loginUser(loginRequest)
        }

        binding.termOfUse.setOnClickListener {
//            startActivity(Intent(this, TermsOfUse::class.java))
            openInBrowser()
        }

        binding.ivNavigationBack.setOnClickListener {
            onBackPressed()
        }

    }

    private fun openInBrowser() {
        val openURL = Intent(Intent.ACTION_VIEW)
        openURL.data = Uri.parse(TERMS_OF_USE_URL)
        startActivity(openURL)
    }

    private fun validatePhoneNumber(): Boolean {
        val phoneNumber = binding.tiPhone.editText?.text.toString().trim()
        return when {
            phoneNumber.isEmpty() -> {
                binding.tiPhone.error = "Phone Number can not be empty"
//                binding.tiPhone.requestFocus()
                false
            }
            phoneNumber.length < 10 -> {
                binding.tiPhone.error = "Invalid phone number"
//                binding.tiPhone.requestFocus()
                false
            }
//            !phoneNumber.contains("+") -> {
//                binding.tiPhone.error = "Please enter phone number with country code"
////                binding.tiPhone.requestFocus()
//                false
//            }
            else -> {
                binding.tiPhone.error = null
                binding.tiPhone.isErrorEnabled = false
                true
            }
        }
    }

    private fun validatePassword(): Boolean {
        val password = binding.tiPassword.editText?.text.toString().trim()
        return when {
            password.isEmpty() -> {
                binding.tiPassword.error = "Password can not be empty"
//                binding.tiPassword.requestFocus()
                false
            }
            password.length < 6 -> {
                binding.tiPassword.error = "Password must be 6 characters long"
//                binding.tiPassword.requestFocus()
                false
            }
            else -> {
                binding.tiPassword.error = null
                binding.tiPassword.isErrorEnabled = false
                true
            }
        }
    }

}