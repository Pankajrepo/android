package com.brilliance.dualiit.ui.home.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ItemNearmeBinding
import com.brilliance.dualiit.model.UserModel
import com.bumptech.glide.Glide
import java.text.SimpleDateFormat
import java.util.*

class NearMeSuggestAdapter : RecyclerView.Adapter<NearMeSuggestAdapter.NearMeViewHolder>() {


    inner class NearMeViewHolder(private val binding: ItemNearmeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                onItemClickListener?.let { click ->
                    val user = nearMeList[bindingAdapterPosition]
                    click(user)
                }
            }
        }

        fun bind(userModel: UserModel) {
            binding.apply {

                if (userModel.avatar == "") {
                    ivProfile.setBackgroundResource(R.drawable.ic_user_avatar)
                } else {
                    Glide.with(itemView)
                        .load(userModel.avatar)
                        .into(ivProfile)
                }

                if (userModel.onlineStatus == true) {
                    statusOnline.isVisible = true
                    statusOnline.setBackgroundResource(R.drawable.button_green_circle)
                } else {
                    statusOnline.isVisible = false
                }

                tvName.text = userModel.fullName

                userModel.birthDay?.let {
                    if (it.isNotEmpty()) {
                        tvPhone.text = "${getAge(userModel.birthDay.toString())} years old"
                    } else {
                        tvPhone.text = ""
                    }
                }

            }
        }
    }

    private val differCallback = object : DiffUtil.ItemCallback<UserModel>() {
        override fun areItemsTheSame(oldItem: UserModel, newItem: UserModel): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: UserModel, newItem: UserModel): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val differ = AsyncListDiffer(this, differCallback)

    var nearMeList: List<UserModel>
        get() = differ.currentList
        set(value) = differ.submitList(value)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NearMeViewHolder {
        val binding = ItemNearmeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NearMeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: NearMeViewHolder, position: Int) {
        val user = nearMeList[position]
        holder.bind(user)
    }

    override fun getItemCount(): Int {
        return nearMeList.size
    }

    private fun getAge(birthDay: String): String {
        val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        val birthdate = simpleDateFormat.parse(birthDay)
        val dob = Calendar.getInstance()
        dob.time = birthdate

        val today: Calendar = Calendar.getInstance()
        var age: Int = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--
        }
        val ageInt = age
        return ageInt.toString()
    }

    private var onItemClickListener: ((UserModel) -> Unit)? = null

    fun setOnItemClickListener(listener: (UserModel) -> Unit) {
        onItemClickListener = listener
    }
}