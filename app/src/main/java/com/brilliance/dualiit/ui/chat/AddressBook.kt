package com.brilliance.dualiit.ui.chat

import androidx.annotation.Keep

@Keep
data class AddressBook(
    val _id: String? = "",
    val createdAt: String? = "",
    val friendId: FriendId? = null,
    val status: Int? = 0,
    val updatedAt: String? = "",
    val userId: UserId? = null
)