package com.brilliance.dualiit.ui.chat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ItemAddressBookBinding
import com.bumptech.glide.Glide

class AddressBookAdapter :
    PagingDataAdapter<AddressBook, AddressBookAdapter.AddressBookViewHolder>(DiffCallback()) {

    inner class AddressBookViewHolder(private val binding: ItemAddressBookBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                onItemClickListener?.let { click ->
                    val chat = getItem(bindingAdapterPosition)
                    if (chat != null) {
                        click(chat)
                    }
                }
            }
        }

        fun bind(addressBook: AddressBook) {
            binding.apply {
                if (addressBook.friendId?.avatar == "") {
                    ivProfile.setBackgroundResource(R.drawable.ic_user_avatar)
                } else {
                    Glide.with(itemView)
                        .load(addressBook.friendId?.avatar)
                        .into(ivProfile)
                }

                if (addressBook.friendId?.onlineStatus == true) {
                    statusOnline.setBackgroundResource(R.drawable.button_green_circle)
                } else {
                    statusOnline.setBackgroundResource(R.drawable.button_red_circle)
                }

                tvName.text = addressBook.friendId?.fullName
            }
        }
    }


    class DiffCallback : DiffUtil.ItemCallback<AddressBook>() {
        override fun areItemsTheSame(oldItem: AddressBook, newItem: AddressBook): Boolean {
            return oldItem._id == newItem._id
        }

        override fun areContentsTheSame(oldItem: AddressBook, newItem: AddressBook): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressBookViewHolder {
        val binding =
            ItemAddressBookBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AddressBookViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AddressBookViewHolder, position: Int) {
        val chatItem = getItem(position)
        if (chatItem != null) {
            holder.bind(chatItem)
        }
    }

    private var onItemClickListener: ((AddressBook) -> Unit)? = null

    fun setOnItemClickListener(listener: (AddressBook) -> Unit) {
        onItemClickListener = listener
    }
}