package com.brilliance.dualiit.ui.speeddating.models

import androidx.annotation.Keep

@Keep
data class SpeedDatingResponse(
    val _id: String? = "",
    val createdAt: String? = "",
    val speedDating: Int? = 0,
    val status: Int? = 0,
    val time: String? = "",
    val type: String? = "",
    val updatedAt: String? = "",
    val user: User? = null
)