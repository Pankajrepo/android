package com.brilliance.dualiit.ui.auth.activities

import android.content.Intent
import android.os.Bundle
import com.brilliance.dualiit.databinding.ActivityUploadpicWelcomeBinding
import com.brilliance.dualiit.ui.auth.adapter.UploadPicPagerAdapter
import com.brilliance.dualiit.ui.auth.viewmodel.AuthViewModel
import com.brilliance.dualiit.ui.base.activity.BaseActivity
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.ui.home.activities.MainActivity
import org.koin.androidx.viewmodel.ext.android.viewModel


class UploadPicWelcome : BaseActivity() {
    private lateinit var binding: ActivityUploadpicWelcomeBinding
    private val viewModel: AuthViewModel by viewModel()
    override fun baseViewModel(): BaseViewModel = viewModel

    override fun setObserver() {
        super.setBaseObserver()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBar(this)
        binding = ActivityUploadpicWelcomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setObserver()


        val adapter = UploadPicPagerAdapter(this)
        binding.viewpager.adapter = adapter
        binding.viewpager.currentItem = adapter.count - 1


        binding.btnUploadPic.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    MainActivity::class.java
                ).setFlags(
                    Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
                )
            )
            finish()
        }

        binding.tvSkip.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    MainActivity::class.java
                ).setFlags(
                    Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
                )
            )
            finish()
        }

        binding.uploadPicToolbar.toolbar.setNavigationOnClickListener { onBackPressed() }

    }


}