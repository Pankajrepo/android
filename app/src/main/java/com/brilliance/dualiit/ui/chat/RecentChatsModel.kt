package com.brilliance.dualiit.ui.chat

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class RecentChatsModel(
    @SerializedName("docs")
    val recentChats: List<RecentChat>? = null,
    val `false`: Boolean? = false,
    val limit: Int? = 0,
    val nextPage: Any? = null,
    val page: Int? = 0,
    val prevPage: Any? = null,
    val totalDocs: Int? = 0,
    val totalPages: Int? = 0
)