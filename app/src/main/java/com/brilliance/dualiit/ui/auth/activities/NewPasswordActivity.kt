package com.brilliance.dualiit.ui.auth.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.brilliance.dualiit.databinding.ActivityNewPasswordBinding
import com.brilliance.dualiit.ui.auth.request.ResetRequest
import com.brilliance.dualiit.ui.auth.viewmodel.AuthViewModel
import com.brilliance.dualiit.utils.AppConstant
import com.brilliance.dualiit.utils.Resource
import com.brilliance.dualiit.utils.extensions.hide
import com.brilliance.dualiit.utils.extensions.launchActivity
import com.brilliance.dualiit.utils.extensions.show
import com.brilliance.dualiit.utils.extensions.showToast
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewPasswordActivity : AppCompatActivity() {

    private lateinit var binding: ActivityNewPasswordBinding
    private val viewModel: AuthViewModel by viewModel()
    private var resetRequest: ResetRequest? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNewPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setObserver()

        intent.let {
            if (intent.hasExtra(AppConstant.REQ_RESET)) {
                resetRequest =
                    intent.getParcelableExtra<ResetRequest>(AppConstant.REQ_EXTRA) as ResetRequest
            }
        }

        binding.ivNavigationBack.setOnClickListener {
            finish()
        }


        binding.btnResetPassword.setOnClickListener {
            if (!validatePassword()) {
                return@setOnClickListener
            }
            resetRequest?.let {
                it.password = binding.tilNewPassword.editText?.text.toString()
                viewModel.resetPassword(resetRequest!!)
            }
        }

    }

    private fun setObserver() {
        lifecycleScope.launchWhenStarted {
            viewModel.resetPasswordState.collect {
                when (it) {
                    is Resource.Empty -> {
                    }
                    is Resource.Error -> {
                        binding.progressBar.hide()
                        showToast(it.message)
                    }
                    is Resource.Loading -> {
                        binding.progressBar.show()
                    }
                    is Resource.Success -> {
                        binding.progressBar.hide()
                        showToast("Password reset successfully")
                        launchActivity(
                            aClass = Login::class.java,
                            isTopFinish = true,
                            flagIsTopClearTask = true
                        )
                    }
                }
            }
        }
    }

    private fun validatePassword(): Boolean {
        val password = binding.tilNewPassword.editText?.text.toString().trim()
        return when {
            password.isEmpty() -> {
                binding.tilNewPassword.error = "Password can not be empty"
                false
            }
            password.length < 6 -> {
                binding.tilNewPassword.error = "Password must be 6 characters long"
                false
            }
            else -> {
                binding.tilNewPassword.error = null
                binding.tilNewPassword.isErrorEnabled = false
                true
            }
        }
    }


}