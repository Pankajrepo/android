package com.brilliance.dualiit.ui.chat

import androidx.annotation.Keep


@Keep
data class RecentChat(
    val key: String? = "",
    val lastMessage: LastMessage? = null,
    val type: String? = "",
    val user: User? = null,
    val unread: Int? = 0
)