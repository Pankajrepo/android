package com.brilliance.dualiit.ui.base.adapter;

import android.content.Context;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.brilliance.dualiit.ui.base.fragment.BaseFragment;

import java.util.List;


public class ViewPagerFragmentArrayAdapter extends FragmentStatePagerAdapter {

    private final FragmentManager fm;
    List<? extends BaseFragment> fragments;
    Context context;

    public ViewPagerFragmentArrayAdapter(Context context, FragmentManager fm, List<? extends BaseFragment> fragments) {
        super(fm);
        this.fm = fm;
        this.context = context;
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        if (fragments != null) {
            return fragments.get(position);
        }
        return null;
    }

    @Override
    public int getCount() {
        return fragments != null ? fragments.size() : 0;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }
}
