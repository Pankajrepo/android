package com.brilliance.dualiit.ui.invitation.models

import androidx.annotation.Keep

@Keep
data class FriendId(
    val _id: String? = "",
    val email: String? = "",
    val fullName: String? = "",
    val phone: String? = "",
    val avatar: String? = ""
)