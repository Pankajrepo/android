package com.brilliance.dualiit.ui.auth.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.lifecycle.lifecycleScope
import com.brilliance.dualiit.databinding.ActivitySplashBinding
import com.brilliance.dualiit.ui.auth.viewmodel.AuthViewModel
import com.brilliance.dualiit.ui.base.activity.BaseActivity
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.ui.home.activities.MainActivity
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.simpleframework.xml.core.Complete

class SplashActivity : BaseActivity() {
    private lateinit var binding: ActivitySplashBinding
    private val viewModel: AuthViewModel by viewModel()
    override fun baseViewModel(): BaseViewModel = viewModel
    override fun setObserver() {
        super.setBaseObserver()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBar(this)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setObserver()

        lifecycleScope.launch {
            delay(800)
            if (prefs.isUserLogin()) {
                startActivity(
                    Intent(
                        this@SplashActivity,
                        MainActivity::class.java
                    ).setFlags(
                        Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    )
                )
                finish()
            } else {
                startActivity(
                    Intent(
                        this@SplashActivity,
//                        TODO: Change Entry Point for different types
                        ChooseLoginSignup::class.java
                    ).setFlags(
                        Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    )
                )
                finish()
            }
        }

    }
}