package com.brilliance.dualiit.ui.chat.models

import androidx.annotation.Keep

@Keep
data class Message(
    var _id: String? = "",
    val body: String? = "",
    val dataOfType: String? = "",
    val dateTime: String? = "",
    var deleted: Int? = 0,
    val from: From? = null,
    var read: Int? = 0,
    val to: To? = null,
    val type: String? = ""
)