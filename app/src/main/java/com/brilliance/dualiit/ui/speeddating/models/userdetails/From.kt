package com.brilliance.dualiit.ui.speeddating.models.userdetails

import androidx.annotation.Keep

@Keep
data class From(
    val _id: String? = "",
    val accountType: String? = "",
    val age: Int? = 0,
    val appVersion: String? = "",
    val area: String? = "",
    val avatar: String? = "",
    val birthDay: String? = "",
    val country: String? = "",
    val createdAt: String? = "",
    val deviceToken: String? = "",
    val deviceType: String? = "",
    val email: String? = "",
    val facebookId: String? = "",
    val fullName: String? = "",
    val gender: String? = "",
    val icebreaker: String? = "",
    val interest: List<String>? = null,
    val interested: String? = "",
    val invitation: String? = "",
    val isVerifiedEmail: Boolean? = false,
    val lastLogin: String? = "",
    val lastOnlineTime: String? = "",
    val locale: String? = "",
    val location: Location? = null,
    val maxAge: Int? = 0,
    val minAge: Int? = 0,
    val onlineStatus: Boolean? = false,
    val orientation: String? = "",
    val phone: String? = "",
    val region: String? = "",
    val regionCode: String? = "",
    val status: Int? = 0,
    val upcoming: String? = "",
    val updatedAt: String? = "",
    val weekendplan: String? = ""
)