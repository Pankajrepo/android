package com.brilliance.dualiit.ui.speeddating

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebViewClient
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ActivityCallingBinding
import com.brilliance.dualiit.ui.home.activities.MainActivity
import com.brilliance.dualiit.ui.speeddating.models.SignalMessage
import com.brilliance.dualiit.ui.speeddating.models.userdetails.UserDetailsResponse
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.extensions.showToast
import com.brilliance.dualiit.utils.screensharing.ScreenSharingCapturer
import com.google.gson.Gson
import com.opentok.android.*
import com.opentok.android.PublisherKit.PublisherListener
import com.opentok.android.Session.SessionListener
import com.opentok.android.Session.SignalListener
import com.opentok.android.SubscriberKit.SubscriberListener
import dagger.hilt.android.AndroidEntryPoint
import io.socket.client.Socket
import kotlinx.coroutines.delay
import org.json.JSONException
import org.json.JSONObject
import org.koin.android.ext.android.inject
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import timber.log.Timber
import java.text.DecimalFormat
import javax.inject.Inject

@AndroidEntryPoint
class CallingActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks {

    private lateinit var binding: ActivityCallingBinding
    val prefs: AppPreferencesHelper by inject()

    @Inject
    lateinit var socket: Socket

    private var session: Session? = null
    private var publisher: Publisher? = null
    private var publisherScreen: Publisher? = null
    private var subscriber: Subscriber? = null
    private var subscriberScreen: Subscriber? = null
    private var apiKey: String = ""
    private var sessionId: String = ""
    private var token: String = ""
    private var userId: String = ""
    private var userName: String = ""
    private var meetingType: String = ""
    private var meetingTime: Int? = 0
    val gson: Gson = Gson()
    private var isAudioOn: Boolean = true
    private var isVideoOn: Boolean = true
    private var isScreenShareMode = false
    private var timer: CountDownTimer? = null


    companion object {
        private const val PERMISSIONS_REQUEST_CODE = 124
        private const val TAG = "CallingActivity"
        private const val SIGNAL_TYPE = "chat"
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCallingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        apiKey = intent?.getStringExtra("API_KEY").toString()
        sessionId = intent?.getStringExtra("SESSION_ID").toString()
        token = intent?.getStringExtra("TOKEN").toString()
        userId = intent?.getStringExtra("ID").toString()
        userName = intent?.getStringExtra("NAME").toString()
        meetingType = intent?.getStringExtra("MEETING_TYPE").toString()
        meetingTime = intent?.getIntExtra("MEETING_TIME", 2)

        requestPermissions()

        initializeSocket()

        binding.fabEndCall.setOnClickListener {
            sendEndCallEvent()
        }

        lifecycleScope.launchWhenStarted {
            delay(8000)
            meetingTime?.let { time ->
                startCallingTimer((time * 60000).toLong())
            }
        }

        binding.webViewContainer.webViewClient = WebViewClient()
        val webSettings: WebSettings = binding.webViewContainer.settings
        webSettings.javaScriptEnabled = true
        binding.webViewContainer.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        binding.webViewContainer.loadUrl("https://www.google.com")

        binding.fabFlipCamera.setOnClickListener {
            if (publisher == null) {
                return@setOnClickListener
            }
            publisher?.cycleCamera()
        }

        binding.fabMicOnOff.setOnClickListener {
            if (isAudioOn) {
                binding.fabMicOnOff.setImageResource(R.drawable.ic_microphone_off)
                binding.fabMicOnOff.setColorFilter(
                    ContextCompat.getColor(
                        this,
                        R.color.colorTextDarkGrey
                    )
                )
                publisher?.publishAudio = false
                isAudioOn = false
            } else {
                binding.fabMicOnOff.setImageResource(R.drawable.ic_microphone)
                binding.fabMicOnOff.setColorFilter(
                    ContextCompat.getColor(
                        this,
                        R.color.colorTextDarkGrey
                    )
                )
                publisher?.publishAudio = true
                isAudioOn = true
            }
        }

        binding.fabVideoOnOff.setOnClickListener {
            if (isVideoOn) {
                binding.fabVideoOnOff.setImageResource(R.drawable.ic_video_off)
                binding.fabVideoOnOff.setColorFilter(
                    ContextCompat.getColor(
                        this,
                        R.color.colorTextDarkGrey
                    )
                )
                publisher?.publishVideo = false
                isVideoOn = false
            } else {
                binding.fabVideoOnOff.setImageResource(R.drawable.ic_video_call)
                binding.fabVideoOnOff.setColorFilter(
                    ContextCompat.getColor(
                        this,
                        R.color.colorTextDarkGrey
                    )
                )
                publisher?.publishVideo = true
                isVideoOn = true
            }
        }

        binding.switchScreenShare.setOnCheckedChangeListener { _, _ ->
            isScreenShareMode = true
        }
    }

    private fun startCallingTimer(meetingTime: Long) {
        timer = object : CountDownTimer(meetingTime, 1000) {
            @SuppressLint("SetTextI18n")
            override fun onTick(millisUntilFinished: Long) {
                val formatter = DecimalFormat("00");

                val minute = (millisUntilFinished / 60000) % 60
                val second = (millisUntilFinished / 1000) % 60

                binding.tvTimeLeft.text =
                    "${formatter.format(minute)}:${formatter.format(second)}"
            }

            override fun onFinish() {
                sendEndCallEvent()
            }

        }
        timer?.start()
    }

    private fun initializeSocket() {
        socket.on(Socket.EVENT_CONNECT) {
            Timber.d("Success: Connection successful")
        }

        socket.on(Socket.EVENT_CONNECT_ERROR) {
            Timber.d("CONNECTION ERROR: ${it[0]}")
        }

        socket.on(Socket.EVENT_DISCONNECT) {
            Timber.d("DISCONNECT: ${it[0]}")
        }

        socket.on("error") {
            Timber.d("Error: ${it[0]}")
        }

        socket.on("connect_timeout") {
            Timber.d("Connect timeout: ${it[0]}")
        }

        socket.on("reconnecting") {
            Timber.d("Reconnecting: ${it[0]}")
        }

        socket.on("reconnect_error") {
            Timber.d("Reconnect error: ${it[0]}")
        }

        socket.on("reconnect") {
            Timber.d("Reconnect: ${it[0]}")
        }

        socket.on("callStatus") { args ->
            Timber.d("Call Status Event: ${args[0]}")
            val userDetailsResponse = gson.fromJson<UserDetailsResponse>(
                args[0].toString(),
                UserDetailsResponse::class.java
            )

            if (userDetailsResponse.status == "ended") {
                session?.disconnect()
                startActivity(
                    Intent(
                        this,
                        MainActivity::class.java
                    ).setFlags(
                        Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    )
                )
                finish()
            }
        }

    }

    private fun disConnectStream() {
        timer?.cancel()
        if (publisher != null) {
            binding.publisherContainer.removeView(publisher!!.view)
            session?.unpublish(publisher)
            publisher = null
        }
        session?.disconnect()
    }

    private fun sendEndCallEvent() {
        val fromUserObject = JSONObject()
        try {
            fromUserObject.put("_id", userId)
            fromUserObject.put("name", userName)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val toUserObject = JSONObject()
        try {
            toUserObject.put("_id", prefs.userModel?.id)
            toUserObject.put("name", prefs.userModel?.fullName)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val jsonObject = JSONObject()
        try {
            jsonObject.put("from", fromUserObject)
            jsonObject.put("to", toUserObject)
            jsonObject.put("status", "ended")
            jsonObject.put("type", meetingType)
            jsonObject.put("shareScreen", false)
            jsonObject.put("speedDating", meetingTime)
            jsonObject.put("sessionId", sessionId)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Timber.d("Sending Call Status Event: $jsonObject")

        socket.emit("callStatus", jsonObject)

        disConnectStream()

        startActivity(
            Intent(
                this,
                MainActivity::class.java
            ).setFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            )
        )
        finish()
    }

    @AfterPermissionGranted(PERMISSIONS_REQUEST_CODE)
    private fun requestPermissions() {
        val perms = arrayOf(
            Manifest.permission.INTERNET,
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO
        )
        if (EasyPermissions.hasPermissions(this, *perms)) {

            if (apiKey.isEmpty() || sessionId.isEmpty() || token.isEmpty()) {
                showToast("Credentials are empty")
                return
            }
            initializeSession(apiKey, sessionId, token)
        } else {
            EasyPermissions.requestPermissions(
                this,
                getString(R.string.rationale_video_app),
                PERMISSIONS_REQUEST_CODE,
                *perms
            )
        }
    }

    override fun onPause() {
        super.onPause()
        if (session == null) {
            return
        }

        session?.onPause()

        if (isFinishing) {
            disConnectStream()
        }
    }

    override fun onDestroy() {
        disConnectStream()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        if (session == null) {
            return
        }

        session?.onResume()
    }

    override fun onBackPressed() {
        showConfirmationDialog()
    }

    private fun showConfirmationDialog() {
        val builder = AlertDialog.Builder(this)
        with(builder)
        {
            setTitle("Cancel Speed dating")
            setMessage("Do you really want to cancel the call now!")
            setPositiveButton("YES") { dialog, _ ->
                sendEndCallEvent()
                dialog.dismiss()
            }
            setNegativeButton("NO") { dialog, _ ->
                dialog.dismiss()
            }
            show()
        }.getButton(AlertDialog.BUTTON_POSITIVE)
            .setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String?>) {
        Timber.d("onPermissionsGranted: $requestCode:${perms.size}}")
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String?>) {
        showToast("onPermissionsDenied: " + requestCode + ":" + perms.size)
    }

    private fun initializeSession(apiKey: String, sessionId: String, token: String) {
        Timber.d("apiKey: $apiKey")
        Timber.d("sessionId: $sessionId")
        Timber.d("token: $token")
        /*
        The context used depends on the specific use case, but usually, it is desired for the session to
        live outside of the Activity e.g: live between activities. For a production applications,
        it's convenient to use Application context instead of Activity context.
         */session = Session.Builder(this, apiKey, sessionId).build()
        session?.setSessionListener(sessionListener)
        session?.setSignalListener(signalListener)
        session?.connect(token)
    }

    private val signalListener =
        SignalListener { session, type, data, connection ->
            val remote = connection != session.connection
            if (type != null && type == SIGNAL_TYPE) {
                if (remote) {
                    if (data.equals("true")) {
                        binding.switchScreenShare.isClickable = false
                        binding.webViewContainer.isVisible = false
                        binding.streamContainer.isVisible = true
                    } else if (data.equals("false")) {
                        binding.switchScreenShare.isClickable = true
                        binding.streamContainer.isVisible = false
                        binding.webViewContainer.isVisible = true
                    }
                }
            }
        }

    private val sessionListener: SessionListener = object : SessionListener {
        override fun onConnected(session: Session) {
            Timber.d("onConnected: Connected to session: ${session.sessionId}")

            publisher = Publisher.Builder(this@CallingActivity)
                .build()
            publisher?.setPublisherListener(publisherListener)
            publisher?.renderer
                ?.setStyle(
                    BaseVideoRenderer.STYLE_VIDEO_SCALE,
                    BaseVideoRenderer.STYLE_VIDEO_FILL
                )
            binding.publisherContainer.addView(publisher?.view)
            session.publish(publisher)

            binding.switchScreenShare.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    val signalMessage = SignalMessage(screenShare = "true")
                    session.sendSignal(SIGNAL_TYPE, signalMessage.screenShare)

                    val screenSharingCapturer =
                        ScreenSharingCapturer(this@CallingActivity, binding.webViewContainer)

                    publisherScreen = Publisher.Builder(this@CallingActivity)
                        .capturer(screenSharingCapturer)
                        .build()

                    publisherScreen?.setPublisherListener(publisherListener)
                    publisherScreen?.publisherVideoType =
                        PublisherKit.PublisherKitVideoType.PublisherKitVideoTypeScreen
                    publisherScreen?.audioFallbackEnabled = false

//                binding.webViewContainer.webViewClient = WebViewClient()
//                val webSettings: WebSettings = binding.webViewContainer.settings
//                webSettings.javaScriptEnabled = true
//                binding.webViewContainer.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
//                binding.webViewContainer.loadUrl("https://www.youtube.com")

                    publisherScreen?.setStyle(
                        BaseVideoRenderer.STYLE_VIDEO_SCALE,
                        BaseVideoRenderer.STYLE_VIDEO_FILL
                    )
//                binding.publisherContainer.addView(publisher?.view)

                    session.publish(publisherScreen)
                } else {
                    session.unpublish(publisherScreen)
                    publisherScreen = null
                    val signalMessage = SignalMessage(screenShare = "false")
                    session.sendSignal(SIGNAL_TYPE, signalMessage.screenShare)
                }
            }

//            if (isScreenShareMode) {
//
//
//            } else {

//            }


        }

        override fun onDisconnected(session: Session) {
            Timber.d(
                "onDisconnected: Disconnected from session: ${session.sessionId}"
            )
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onStreamReceived(session: Session, stream: Stream) {
            Timber.d(
                "onStreamReceived: New Stream Received ${stream.streamId} in session: ${session.sessionId}"
            )

            if (subscriber == null) {
                subscriber = Subscriber.Builder(this@CallingActivity, stream).build()
                subscriber?.renderer?.setStyle(
                    BaseVideoRenderer.STYLE_VIDEO_SCALE,
                    BaseVideoRenderer.STYLE_VIDEO_FILL
                )
                subscriber?.setSubscriberListener(subscriberListener)
                session.subscribe(subscriber)
                binding.subscriberContainer.addView(subscriber?.view)
            }

            if (subscriberScreen == null) {
                if (stream.streamVideoType == Stream.StreamVideoType.StreamVideoTypeScreen) {
                    subscriberScreen = Subscriber.Builder(this@CallingActivity, stream).build()
                    subscriberScreen?.renderer?.setStyle(
                        BaseVideoRenderer.STYLE_VIDEO_SCALE,
                        BaseVideoRenderer.STYLE_VIDEO_FILL
                    )
                    subscriberScreen?.setSubscriberListener(subscriberShareListener)
                    session.subscribe(subscriberScreen)
                    binding.webViewContainer.isVisible = false
                    binding.webViewContainer.translationZ = 0f
                    binding.streamContainer.isVisible = true
                    binding.streamContainer.translationZ = 4f
                    binding.streamContainer.addView(subscriberScreen?.view)
                }
            }


        }

        override fun onStreamDropped(session: Session, stream: Stream) {
            Timber.d(
                "onStreamDropped: Stream Dropped: ${stream.streamId} in session: ${session.sessionId}"
            )

            if (stream.streamVideoType == Stream.StreamVideoType.StreamVideoTypeScreen) {
                if (subscriberScreen != null) {
                    subscriberScreen = null
                    binding.streamContainer.removeAllViews()
                }
            } else if (stream.streamVideoType == Stream.StreamVideoType.StreamVideoTypeCamera) {
                if (subscriber != null) {
                    subscriber = null
                    binding.subscriberContainer.removeAllViews()
                }
            }

        }

        override fun onError(session: Session, opentokError: OpentokError) {
//            FIXME: Fix Error while ending call
//            showToast("Session error: " + opentokError.message)
        }
    }

    var subscriberListener: SubscriberListener = object : SubscriberListener {
        override fun onConnected(subscriberKit: SubscriberKit) {
            Timber.d(
                "onConnected: Subscriber connected. Stream: ${subscriberKit.stream.streamId}"
            )

        }

        override fun onDisconnected(subscriberKit: SubscriberKit) {
            Timber.d(
                "onDisconnected: Subscriber disconnected. Stream: ${subscriberKit.stream.streamId}"
            )
            showToast("Subscriber Disconnected")
        }

        override fun onError(subscriberKit: SubscriberKit, opentokError: OpentokError) {
            showToast("SubscriberKit onError: " + opentokError.message)
        }
    }

    var subscriberShareListener: SubscriberListener = object : SubscriberListener {
        override fun onConnected(subscriberKit: SubscriberKit) {
            Timber.d(
                "onConnected: Subscriber connected. Stream: ${subscriberKit.stream.streamId}"
            )

        }

        override fun onDisconnected(subscriberKit: SubscriberKit) {
            Timber.d(
                "onDisconnected: Subscriber disconnected. Stream: ${subscriberKit.stream.streamId}"
            )
            showToast("Subscriber Disconnected")
        }

        override fun onError(subscriberKit: SubscriberKit, opentokError: OpentokError) {
            Timber.d("SubscriberKit onError: ${opentokError.message}")
            showToast("SubscriberKit onError: " + opentokError.message)
        }
    }


    private val publisherListener: PublisherListener = object : PublisherListener {
        override fun onStreamCreated(publisherKit: PublisherKit, stream: Stream) {
            Timber.d(
                "onStreamCreated: Publisher Stream Created. Own stream ${stream.streamId}"
            )
        }

        override fun onStreamDestroyed(publisherKit: PublisherKit, stream: Stream) {
            Timber.d(
                "onStreamDestroyed: Publisher Stream Destroyed. Own stream ${stream.streamId}"
            )
        }

        override fun onError(publisherKit: PublisherKit, opentokError: OpentokError) {
            Timber.d("PublisherKit onError: ${opentokError.message}")
            showToast("PublisherKit onError: " + opentokError.message)
        }
    }
}