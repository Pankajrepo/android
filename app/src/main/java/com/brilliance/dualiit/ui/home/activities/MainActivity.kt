package com.brilliance.dualiit.ui.home.activities

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ActivityMainBinding
import com.brilliance.dualiit.ui.auth.activities.CompleteProfile
import com.brilliance.dualiit.ui.base.activity.BaseActivity
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.ui.fragment.*
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationBarView
import dagger.hilt.android.AndroidEntryPoint
import io.socket.client.IO
import io.socket.client.Socket
import org.json.JSONException
import org.json.JSONObject
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import javax.inject.Inject


@AndroidEntryPoint
class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: HomeViewModel by viewModel()
    override fun baseViewModel(): BaseViewModel = viewModel

    private var isProfile: Boolean? = false

    @Inject
    lateinit var socket: Socket


    override fun setObserver() {
        super.setBaseObserver()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBar(this)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setObserver()

        isProfile = intent?.getBooleanExtra("IS_PROFILE", false)

        val notifyData = intent?.extras
        Timber.d("Notification Data: $notifyData")

        when (intent?.extras?.get("type")) {
            "invitation" -> {
                loadFragment(NotificationFragment())
            }
            "message" -> {
                loadFragment((ChatsFragment()))
            }
            else -> loadFragment(NearMeFragment())
        }

        if (isProfile == true) {
            loadFragment(Profile2Fragment())
        }

        setBottomNavigation()

//        sendOnlineEvent()
    }

    private fun setBottomNavigation() {
        val navigation =
            findViewById<View>(R.id.navigation) as BottomNavigationView
        when (intent?.extras?.get("type")) {
            "invitation" -> {
                navigation.menu.findItem(R.id.navigation_notification).isChecked = true
            }
            "message" -> {
                navigation.menu.findItem(R.id.navigation_chat).isChecked = true
            }
            else -> navigation.menu.findItem(R.id.navigation_nearme).isChecked = true
        }
        if (isProfile == true) {
            navigation.menu.findItem(R.id.navigation_home).isChecked = true
        }
        navigation.setOnItemSelectedListener(mOnNavigationItemSelectedListener)
    }


    private val mOnNavigationItemSelectedListener: NavigationBarView.OnItemSelectedListener =
        object : NavigationBarView.OnItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                val fragment: Fragment
                when (item.itemId) {
                    R.id.navigation_chat -> {
                        fragment = ChatsFragment()
                        loadFragment(fragment)
                        return true
                    }
                    R.id.navigation_home -> {
                        fragment = Profile2Fragment()
                        loadFragment(fragment)
                        return true
                    }
                    R.id.navigation_meetiing -> {
                        fragment = MeetingFragment()
                        loadFragment(fragment)
                        return true
                    }
                    R.id.navigation_nearme -> {
                        fragment = NearMeFragment()
                        loadFragment(fragment)
                        return true
                    }
                    R.id.navigation_notification -> {
                        fragment = NotificationFragment()
                        loadFragment(fragment)
                        return true
                    }
                }
                return false
            }
        }

    private fun loadFragment(fragment: Fragment) {
        // load fragment
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.main_container, fragment)
        transaction.commit()
    }

    private fun initiateSocketConnection() {
        socket.on(Socket.EVENT_CONNECT) {
            Timber.d("Success: Connection successful")
            sendOnlineEvent()
        }

        socket.on(Socket.EVENT_CONNECT_ERROR) {
            Timber.d("CONNECTION ERROR: ${it[0]}")
        }

        socket.on(Socket.EVENT_DISCONNECT) {
            Timber.d("DISCONNECT: ${it[0]}")
        }

        socket.on("error") {
            Timber.d("Error: ${it[0]}")
        }


        socket.on("connect_timeout") {
            Timber.d("Connect timeout: ${it[0]}")
        }

        socket.on("reconnecting") {
            Timber.d("Reconnecting: ${it[0]}")
        }

        socket.on("reconnect_error") {
            Timber.d("Reconnect error: ${it[0]}")
        }

        socket.on("reconnect") {
            Timber.d("Reconnect: ${it[0]}")
        }
        socket.connect()
    }

    private fun sendOnlineEvent() {
        val jsonObject = JSONObject()
        try {
            jsonObject.put("_id", prefs.userModel?.id)
            jsonObject.put("fullName", prefs.userModel?.fullName)
            jsonObject.put("avatar", prefs.userModel?.avatar)
            jsonObject.put("phone", prefs.userModel?.phone)
            jsonObject.put("email", prefs.userModel?.email)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Timber.d("Sending Online Event: $jsonObject")

        socket.emit("active", jsonObject)

    }

    fun sendOfflineEvent() {
        val jsonObject = JSONObject()
        try {
            jsonObject.put("_id", prefs.userModel?.id)
            jsonObject.put("fullName", prefs.userModel?.fullName)
            jsonObject.put("avatar", prefs.userModel?.avatar)
            jsonObject.put("phone", prefs.userModel?.phone)
            jsonObject.put("email", prefs.userModel?.email)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Timber.d("Sending Offline Event: $jsonObject")

        socket.emit("idle", jsonObject)

    }

    override fun onResume() {
        super.onResume()
        initiateSocketConnection()
    }

    override fun onPause() {
        super.onPause()
        if (isFinishing) {
            sendOfflineEvent()
        }
    }

    override fun onDestroy() {
        sendOfflineEvent()
        socket.disconnect()
        super.onDestroy()

    }

    override fun onStart() {
        super.onStart()
        if (prefs.userModel?.avatar.isNullOrEmpty()) {
            showToast("Complete Profile to Proceed")
            startActivity(
                Intent(
                    this,
                    CompleteProfile::class.java
                ).setFlags(
                    Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                )
            )
            finish()
        }


    }

}