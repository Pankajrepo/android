package com.brilliance.dualiit.ui

import android.content.Intent
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.FragmentInvitationBinding
import com.brilliance.dualiit.ui.home.activities.UserProfileActivity
import com.brilliance.dualiit.ui.home.adapters.InterestsLoadStateAdapter
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.brilliance.dualiit.ui.invitation.InvitationAdapter
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class InvitationFragment : Fragment(R.layout.fragment_invitation) {

    private var _binding: FragmentInvitationBinding? = null
    private val binding get() = _binding!!

    private lateinit var invitationAdapter: InvitationAdapter
    private val viewModel: HomeViewModel by viewModel()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentInvitationBinding.bind(view)


        setupRecyclerView()

//        binding.progressBar.isVisible = false
//        binding.tvEmpty.isVisible = true

        viewModel.invitations.observe(viewLifecycleOwner) {
//            binding.progressBar.isVisible = false
//            binding.rvInvitations.isVisible = true
//            binding.tvEmpty.isVisible = false
            invitationAdapter.submitData(viewLifecycleOwner.lifecycle, it)
        }

        invitationAdapter.setOnProfileClickListener { userId ->
            val userDetailsIntent = Intent(requireActivity(), UserProfileActivity::class.java)
            userDetailsIntent.putExtra("USER_ID", userId)
            startActivity(userDetailsIntent)
        }

        invitationAdapter.setOnAcceptClickListener { invitation ->
            val hashMap = HashMap<String, Any>()
            hashMap["status"] = 2
            viewModel.acceptInvitation(invitation._id.toString(), hashMap)

//            Handler().postDelayed({
//                val fragment =
//                    activity?.supportFragmentManager?.findFragmentById(R.id.main_container)
//                val fm = activity?.supportFragmentManager?.beginTransaction()
//                fm?.detach(fragment!!)
//                fm?.attach(fragment!!)
//                fm?.commit()
//            }, 500)
        }

        invitationAdapter.setOnRejectClickListener { invitation ->
            val hashMap = HashMap<String, Any>()
            hashMap["status"] = 3
            viewModel.rejectInvitation(invitation._id.toString(), hashMap)

//            Handler().postDelayed({
//                val fragment =
//                    activity?.supportFragmentManager?.findFragmentById(R.id.main_container)
//                val fm = activity?.supportFragmentManager?.beginTransaction()
//                fm?.detach(fragment!!)
//                fm?.attach(fragment!!)
//                fm?.commit()
//            }, 500)
        }

        lifecycleScope.launchWhenStarted {
            viewModel.invitationEvent.collect { event ->
                when (event) {
                    is HomeViewModel.InvitationEvent.InvitationAccepted -> {
//                        activity?.showToast(event.message)
                        invitationAdapter.refresh()
//                        invitationAdapter.submitData(emptyList())
//                        viewModel.getInvitationsList()
                    }

                    is HomeViewModel.InvitationEvent.InvitationRejected -> {
//                        activity?.showToast(event.message)
                        invitationAdapter.refresh()
//                        invitationAdapter.submitList(emptyList())
//                        viewModel.getInvitationsList()
                    }
                }
            }
        }

        invitationAdapter.addLoadStateListener { loadState ->
            binding.apply {
                progressBar.isVisible = loadState.source.refresh is LoadState.Loading
                rvInvitations.isVisible = loadState.source.refresh is LoadState.NotLoading
                buttonRetry.isVisible = loadState.source.refresh is LoadState.Error
                textViewError.isVisible = loadState.source.refresh is LoadState.Error

                if (loadState.source.refresh is LoadState.NotLoading &&
                    loadState.append.endOfPaginationReached &&
                    invitationAdapter.itemCount < 1
                ) {
                    rvInvitations.isVisible = false
                    tvEmpty.isVisible = true
                } else {
                    tvEmpty.isVisible = false
                }
            }
        }

        binding.buttonRetry.setOnClickListener {
            invitationAdapter.retry()
        }

    }

    private fun setupRecyclerView() {
        invitationAdapter = InvitationAdapter()
        binding.rvInvitations.apply {
            layoutManager = LinearLayoutManager(requireContext())
            itemAnimator = null
            val attrs = intArrayOf(android.R.attr.listDivider)
            val a = context.obtainStyledAttributes(attrs)
            val divider = a.getDrawable(0)
            val inset = resources.getDimensionPixelSize(R.dimen._16sdp)
            val insetDivider = InsetDrawable(divider, inset, 0, inset, 0)
            a.recycle()
            val itemDecoration =
                DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
            itemDecoration.setDrawable(insetDivider)
            addItemDecoration(itemDecoration)

            adapter = invitationAdapter.withLoadStateFooter(
                footer = InterestsLoadStateAdapter {
                    invitationAdapter.retry()
                }
            )
        }
    }

    override fun onResume() {
        super.onResume()
//        viewModel.getInvitationsList()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}