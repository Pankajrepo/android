package com.brilliance.dualiit.ui.invitation.models

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class InvitationResponse(
    @SerializedName("docs")
    val invitationsList: List<InvitationModel>? = null,
    val limit: Int? = 0,
    val nextPage: Any? = null,
    val page: Int? = 0,
    val prevPage: Any? = null,
    val totalDocs: Int? = 0,
    val totalPages: Int? = 0
)