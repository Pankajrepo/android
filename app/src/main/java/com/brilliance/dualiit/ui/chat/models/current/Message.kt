package com.brilliance.dualiit.ui.chat.models.current

import androidx.annotation.Keep

@Keep
data class Message(
    val body: String? = "",
    val dataOfType: String? = "",
    val type: String? = ""
)