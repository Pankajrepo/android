package com.brilliance.dualiit.ui.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.location.Location
import android.net.Uri
import android.os.*
import android.text.InputType
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.brilliance.dualiit.databinding.FragmentProfileBinding
import com.brilliance.dualiit.model.InterestDoc
import com.brilliance.dualiit.services.FetchAddressIntentService
import com.brilliance.dualiit.ui.auth.activities.ValidateOTP
import com.brilliance.dualiit.ui.auth.viewmodel.AuthViewModel
import com.brilliance.dualiit.ui.base.fragment.BaseFragment
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.brilliance.dualiit.utils.AppConstant
import com.brilliance.dualiit.utils.extensions.showToast
import com.bumptech.glide.Glide
import com.facebook.*
import com.facebook.login.LoginBehavior
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.google.android.material.chip.Chip
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.collect
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.io.File
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import com.brilliance.dualiit.R as DualiitR


class ProfileFragment : BaseFragment(), DatePickerDialog.OnDateSetListener {

    lateinit var binding: FragmentProfileBinding
    private val viewModel: AuthViewModel by viewModel()

    //    private lateinit var viewModel: AuthViewModel
    private val homeViewModel: HomeViewModel by viewModel()
    override fun baseViewModel(): BaseViewModel = viewModel
    private var isEditMode: Boolean = false
    private var imageUri: Uri? = null
    private var userAvatar: String? = null
    private var ageMin: Int? = null
    private var ageMax: Int? = null
    private var genderPrefs: String? = "male"
    private var userGender: String? = "male"
    private var orientation: String? = "straight"
    private var interestsList: List<String> = ArrayList()
    private var resultReceiver: ResultReceiver? = null
    private var callbackManager: CallbackManager? = null
    private var currentDay = 0
    private var currentMonth = 0
    private var currentYear = 0
    private var savedDay = 0
    private var savedMonth = 0
    private var savedYear = 0
    private var dateOfBirth: String? = ""
    private var area: String? = null
    private var country: String? = null
    private var region: String? = null
    private var userLocation: com.brilliance.dualiit.model.Location? = null
    private var isEmailVerified: Boolean = false
    private var addInterestBtn: Chip? = null
    private var locationSettingsBuilder: LocationSettingsRequest.Builder? = null
    private var jsonObject: JsonObject? = null
    private var locationPermissions = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )
    private var isVerified: Boolean? = null
    private var userId: String? = null

    @RequiresApi(Build.VERSION_CODES.Q)
    private var locationPermissionsBackground = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        FacebookSdk.addLoggingBehavior(LoggingBehavior.REQUESTS);

        callbackManager = CallbackManager.Factory.create()

        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult?> {
                override fun onSuccess(result: LoginResult?) {

//                    GraphRequest.newMeRequest(
//                        result?.accessToken
//                    ) { `object`, response ->
//                        Timber.d(response.toString())
//                        // Application code
//                        val userID: String = `object`.getString("id")
//                        val userEmail: String = `object`.getString("email")
//                        val isVerified: String = `object`.getString("verified")
//                        val userName: String =
//                            `object`.getString("name")
//                        Log.d("ProfileFragment", "Facebook UserId: $userID\n Email: $userEmail\n Name: $userName")
//                        Log.d("ProfileFragment", "Verified: $isVerified")
//                    }

                    val request = GraphRequest.newMeRequest(
                        result?.accessToken
                    ) { `object`, response ->
                        // Application code
                        try {
                            if (`object`.has("id")) {
                                val id = `object`.getString("id")
                                val isVerified = `object`.getString("is_verified")
                                Log.d("ProfileFragment", "UserID: $id")
                                Log.d("ProfileFragment", "Verified: $isVerified")
                            }
                        } catch (e: Exception) {
                            Log.d("ProfileFragment", "Exception: ${e.message}")

                        }
                    }
                    val parameters = Bundle()
                    parameters.putString("fields", "id,name,link,is_verified")
                    request.parameters = parameters
                    request.executeAsync()


//                    userId = "UserID: ${result?.accessToken?.userId}"
//                    val isVerified = "Verification status: ${result?.accessToken?.userId}"
//                    Log.d("ProfileFragment", "FB Login Success: $userId")
//                    Log.d("ProfileFragment", "Verification: ${isVerified}")
                    activity?.showToast("Logged in with Facebook")
                }

                override fun onCancel() {}

                override fun onError(error: FacebookException?) {
                    Log.d("Error here: ", error?.message.toString())
                }
            })
    }


    @SuppressLint("ClickableViewAccessibility")
    override fun createDataBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentProfileBinding.inflate(inflater, container, false)


        setObserver()
        resultReceiver = AddressResultReceiver(Handler())
        binding.fbLoginBtn.fragment = this
        binding.fbLoginBtn.setPermissions(listOf("public_profile"))
        binding.fbLoginBtn.isClickable = true
        binding.fbLoginBtn.isEnabled = true
        binding.fbLoginBtn.loginBehavior = LoginBehavior.WEB_ONLY


//        homeViewModel.selectedInterestList.observe(viewLifecycleOwner, Observer {
//            if (it.isEmpty()) {
//                binding.tvAddIntersts.isVisible = true
//            } else {
//                binding.tvAddIntersts.isVisible = false
//                setInterestChips(it)
//            }
//        })

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            homeViewModel.profileEvent.collect { event ->
                when (event) {
                    is HomeViewModel.ProfileEvent.SelectedInterests -> {
                        if (event.interestsList.isEmpty()) {
                            binding.tvAddIntersts.isVisible = true
                        } else {
                            binding.tvAddIntersts.isVisible = false
                            val myInterestsList = event.interestsList.toSet().toList()
                            binding.chipGroupInterests.removeAllViews()
                            setInterestChips(myInterestsList)

                            (interestsList as ArrayList).clear()

                            for (i in myInterestsList.indices) {
                                val interestName = myInterestsList[i].name
                                (interestsList as ArrayList).add(interestName)
                            }
                        }
                    }
                }
            }
        }

        Timber.d("accessToken: ${prefs.userModel?.refreshToken}")
        Timber.d("refreshToken: ${prefs.userModel?.accessToken}")
        Timber.d("authToken: ${prefs.authToken}")

        binding.btnSettings.setOnClickListener {
            startActivity(Intent(activity, SettingsActivity::class.java))
        }

        binding.ageSlider.setOnTouchListener { _, _ ->
            !isEditMode
        }

        binding.chipUserMale.setOnTouchListener { _, _ ->
            !isEditMode
        }
        binding.chipUserFemale.setOnTouchListener { _, _ ->
            !isEditMode
        }

        binding.chipMale.setOnTouchListener { _, _ ->
            !isEditMode
        }

        binding.chipFemale.setOnTouchListener { _, _ ->
            !isEditMode
        }

        binding.chipBoth.setOnTouchListener { _, _ ->
            !isEditMode
        }

        binding.chipStraight.setOnTouchListener { _, _ ->
            !isEditMode
        }

        binding.chipBisexual.setOnTouchListener { _, _ ->
            !isEditMode
        }

        binding.chipHeterosexual.setOnTouchListener { _, _ ->
            !isEditMode
        }

        binding.chipHomosexual.setOnTouchListener { _, _ ->
            !isEditMode
        }


        binding.apply {

            binding.etName.setText(prefs.userModel?.fullName)
            binding.etEmail.setText(prefs.userModel?.email)
            binding.etPhone.setText(prefs.userModel?.phone)
            binding.etIceBreaker.setText(prefs.userModel?.icebreaker)
//            binding.etInvitationNote.setText(prefs.userModel?.invitation)
            binding.etWeekendPlan.setText(prefs.userModel?.weekendplan)

            if (prefs.userModel?.avatar == "" || prefs.userModel?.avatar == null) {
                ivProfile.setImageResource(DualiitR.drawable.ic_user_avatar)
            } else {
                Glide.with(this@ProfileFragment)
                    .load(prefs.userModel?.avatar)
                    .into(ivProfile)
            }

            when (prefs.userModel?.gender) {
                "male" -> binding.chipUserMale.isChecked = true
                "female" -> binding.chipUserFemale.isChecked = true
//                else -> binding.chipUserMale.isSelected = true
            }

            when (prefs.userModel?.interested) {
                "male" -> binding.chipMale.isChecked = true
                "female" -> binding.chipFemale.isChecked = true
                "other" -> binding.chipBoth.isChecked = true
//                else -> binding.chipMale.isSelected = true
            }

            when (prefs.userModel?.orientation) {
                "straight" -> binding.chipStraight.isChecked = true
                "bisexual" -> binding.chipBisexual.isChecked = true
                "heterosexual" -> binding.chipHeterosexual.isChecked = true
                "homosexual" -> binding.chipHomosexual.isChecked = true
//                else -> binding.chipStraight.isSelected = true
            }


            if (prefs.userModel?.minAge == null || prefs.userModel?.maxAge == null ||
                prefs.userModel?.minAge == 0 || prefs.userModel?.maxAge == 0
            ) {
                ageSlider.setValues(18f, 60f)
                tvMinMaxAge.text = "18-60"
            } else {
                val minAge = prefs.userModel?.minAge?.toFloat()
                val maxAge = prefs.userModel?.maxAge?.toFloat()
                ageSlider.setValues(minAge, maxAge)
                tvMinMaxAge.text = "${prefs.userModel?.minAge}-${prefs.userModel?.maxAge}"
            }

            tvName.text = prefs.userModel?.fullName
            if (prefs.userModel?.country.isNullOrEmpty()) {
                tvLocation.text = "Add Location"
            } else {
                tvLocation.text =
                    "${prefs.userModel?.area}, ${prefs.userModel?.country}, ${prefs.userModel?.region}"
            }
            if (prefs.userModel?.email.isNullOrEmpty()) {
                tvEmail.text = "Add Email Address"
            } else {
                tvEmail.text = prefs.userModel?.email
            }


            tvPhone.text = prefs.userModel?.phone
            if (prefs.userModel?.icebreaker.isNullOrEmpty()) {
                tvIcebreaker.text = "Add Icebreaker"
            } else {
                tvIcebreaker.text = prefs.userModel?.icebreaker
            }

//            if (prefs.userModel?.invitation.isNullOrEmpty()) {
//                tvInvitation.text = "Add Invitation Note"
//            } else {
//                tvInvitation.text = prefs.userModel?.invitation
//            }

            if (prefs.userModel?.weekendplan.isNullOrEmpty()) {
                tvWeekendPlan.text = "Add Weekend Plan"
            } else {
                tvWeekendPlan.text = prefs.userModel?.weekendplan
            }

            if (prefs.userModel?.birthDay.isNullOrEmpty()) {
                tvBirthday.text = "Add Birthday"
            } else {
                tvBirthday.text = prefs.userModel?.birthDay
            }

            if (prefs.userModel?.interest?.isNullOrEmpty()!!) {
                tvAddIntersts.text = "Add Interests"
            } else {
                tvAddIntersts.isVisible = false
                for (i in prefs.userModel?.interest!!.indices) {
                    val interestName = prefs.userModel?.interest!![i]
//                    (interestsList as ArrayList).add(interestName)
                    val chip = Chip(requireContext())
                    val paddingDp = TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, 10F,
                        resources.displayMetrics
                    ).toInt()
                    chip.setPadding(paddingDp, paddingDp, paddingDp, paddingDp)
                    chip.chipBackgroundColor = ColorStateList.valueOf(
                        ContextCompat.getColor(
                            requireContext(),
                            DualiitR.color.com_facebook_button_background_color_disabled
                        )
                    )
                    chip.text = interestName
                    chip.isCloseIconVisible = false
                    binding.chipGroupInterests.addView(chip)
                }
                binding.chipGroupInterests.addView(createInterestBtn())
            }
        }

        binding.editToggle.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.apply {
                    isEditMode = true
                    etName.isVisible = true
                    etPhone.isVisible = true
                    etIceBreaker.isVisible = true
//                    etInvitationNote.isVisible = true
                    etWeekendPlan.isVisible = true
                    btnEditProfileDone.isVisible = true
                    tvName.isVisible = false
                    tvPhone.isVisible = false
                    tvIcebreaker.isVisible = false
//                    tvInvitation.isVisible = false
                    tvWeekendPlan.isVisible = false
                    btnSettings.isVisible = false
                    addInterestBtn?.isClickable = true
                    addInterestBtn?.isEnabled = true

                    if (!prefs.userModel?.isVerifiedEmail!!) {
                        btnVerifyEmail.isVisible = true
                        etEmail.isVisible = true
                        tvEmail.isVisible = false
                        btnVerifyEmail.setOnClickListener {
                            viewModel.email.value = binding.etEmail.text.toString()
                            viewModel.sendEmailVerifyCode()
                        }
                    } else {
                        btnVerifyEmail.isVisible = true
                        btnVerifyEmail.text = "Verified"
                        btnVerifyEmail.isClickable = false
                        etEmail.isVisible = false
                        tvEmail.isVisible = true
                    }



                    ivProfile.setOnClickListener {
                        dialogImagePicker()
                    }
                    tvLocation.setOnClickListener {
                        requestLocationPermissions()
                    }

                    if (prefs.userModel?.interest.isNullOrEmpty()) {
                        tvAddIntersts.setOnClickListener {
                            val interestBottomSheet = InterestBottomSheetFragment()
                            interestBottomSheet.show(childFragmentManager, "Interests")
                        }
                    } else {
                        addInterestBtn?.setOnClickListener {
                            if (interestsList.size >= 5) {
                                requireActivity().showToast("You can only add up to 5 interests")
                            } else {
                                val interestBottomSheet = InterestBottomSheetFragment()
                                interestBottomSheet.show(childFragmentManager, "Interests")
                            }
                        }
                    }

                    tvBirthday.setOnClickListener {
                        pickBirthdayDate()
                    }
                }
            } else {
                binding.apply {
                    isEditMode = false
                    etName.isVisible = false
                    etEmail.isVisible = false
                    etPhone.isVisible = false
                    etIceBreaker.isVisible = false
//                    etInvitationNote.isVisible = false
                    etWeekendPlan.isVisible = false
                    btnEditProfileDone.isVisible = false
                    tvName.isVisible = true
                    tvEmail.isVisible = true
                    tvPhone.isVisible = true
                    tvIcebreaker.isVisible = true
//                    tvInvitation.isVisible = true
                    tvWeekendPlan.isVisible = true
                    btnSettings.isVisible = true
                    ivProfile.isClickable = false
                    tvLocation.isClickable = false
                    tvAddIntersts.isClickable = false
                    tvBirthday.isClickable = false
                    btnVerifyEmail.isVisible = false
                    addInterestBtn?.isClickable = false
                    addInterestBtn?.isEnabled = false
                }
            }
        }

        binding.ageSlider.addOnChangeListener { slider, value, fromUser ->
            val currentValues = slider.values
            val minAge = currentValues[0].toInt()
            val maxAge = currentValues[1].toInt()
            ageMin = minAge
            ageMax = maxAge
            binding.tvMinMaxAge.text = "$minAge-$maxAge"
        }



        binding.btnEditProfileDone.setOnClickListener {
            val userMap = HashMap<String, Any>()

            if (userAvatar != null) {
                userMap["avatar"] = userAvatar!!
            }

            if (binding.etIceBreaker.text.toString().isNotEmpty()) {
                userMap["icebreaker"] = binding.etIceBreaker.text.toString()
            }

            if (binding.etName.text.toString().isNotEmpty()) {
                userMap["fullName"] = binding.etName.text.toString()
            }

//            if (binding.etInvitationNote.text.toString().isNotEmpty()) {
//                userMap["invitation"] = binding.etInvitationNote.text.toString()
//            }

            if (binding.etWeekendPlan.text.toString().isNotEmpty()) {
                userMap["weekendplan"] = binding.etWeekendPlan.text.toString()
            }

            if (!dateOfBirth.isNullOrEmpty()) {
                userMap["birthDay"] = dateOfBirth!!
            }

            if (interestsList.isNotEmpty()) {
                val interestToJson = Gson().toJsonTree(interestsList)
                Timber.d("$interestToJson")
                userMap["interest"] = interestToJson
            }

            if (genderPrefs != null) {
                userMap["interested"] = genderPrefs!!
            }

            if (userGender != null) {
                userMap["gender"] = userGender!!
            }

            if (orientation != null) {
                userMap["orientation"] = orientation!!
            }

            if (ageMin != null && ageMax != null) {
                userMap["minAge"] = ageMin!!
                userMap["maxAge"] = ageMax!!
            }

            if (area != null) {
                userMap["area"] = area!!
            }
            if (country != null) {
                userMap["country"] = country!!
            }
            if (region != null) {
                userMap["region"] = region!!
            }



            if (isEmailVerified) {
                userMap["isVerifiedEmail"] = true
            }


            Timber.d("User Entries: ${userMap.entries}")

//            if (userLocation != null) {
//                val location = Gson().toJson(userLocation)
//                Timber.d(userLocation?.toString())
//
//
//                val isCompleted = viewModel.updateProfileWithLocation(map).isCompleted
//                if (isCompleted) {
//                    viewModel.updateProfile(userMap)
//                }
//
//            } else {
            viewModel.updateProfile(userMap)
//            }


            Handler().postDelayed(Runnable {
                val fragment =
                    activity?.supportFragmentManager?.findFragmentById(DualiitR.id.main_container)
                val fm = activity?.supportFragmentManager?.beginTransaction()
                fm?.detach(fragment!!)
                fm?.attach(fragment!!)
                fm?.commit()
            }, 1000)

        }

        binding.chipGroupGenderPrefs.setOnCheckedChangeListener { group, checkedId ->
            val selectedChip = group.findViewById<Chip>(checkedId)

            if (selectedChip != null) {
                genderPrefs = if (selectedChip.text.toString() == "Both") {
                    "other"
                } else {
                    selectedChip.text.toString().toLowerCase()
                }
                activity?.showToast("${selectedChip.text} is selected")
            }
        }

        binding.chipGroupOrientation.setOnCheckedChangeListener { group, checkedId ->
            val selectedChip = group.findViewById<Chip>(checkedId)

            if (selectedChip != null) {
                orientation = selectedChip.text.toString().toLowerCase()
                activity?.showToast("${selectedChip.text} is selected")
            }
        }

        binding.chipGroupUserGender.setOnCheckedChangeListener { group, checkedId ->
            val selectedChip = group.findViewById<Chip>(checkedId)

            if (selectedChip != null) {
                userGender = selectedChip.text.toString().toLowerCase()
                activity?.showToast("${selectedChip.text} is selected")
            }
        }

        return binding.root
    }

    private fun hasPermissions(vararg permissions: String): Boolean = permissions.all {
        ActivityCompat.checkSelfPermission(
            requireContext(),
            it
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestLocationPermissions() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            if (!hasPermissions(*locationPermissions)) {
                requestPermissions(locationPermissions, LOCATION_REQUEST_CODE)
            } else {
                getCurrentLocation()
            }
        } else {
            if (!hasPermissions(*locationPermissionsBackground)) {
                requestPermissions(locationPermissionsBackground, LOCATION_REQUEST_CODE)
            } else {
                getCurrentLocation()
            }
        }
//        if (!PermissionsUtility.hasLocationPermissions(requireContext())) {
//            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
//                EasyPermissions.requestPermissions(
//                    this,
//                    "You need to accept location permissions to use this app.",
//                    LOCATION_REQUEST_CODE,
//                    Manifest.permission.ACCESS_FINE_LOCATION,
//                    Manifest.permission.ACCESS_COARSE_LOCATION
//                )
//            } else {
//                EasyPermissions.requestPermissions(
//                    this,
//                    "You need to accept location permissions to use this app.",
//                    LOCATION_REQUEST_CODE,
//                    Manifest.permission.ACCESS_FINE_LOCATION,
//                    Manifest.permission.ACCESS_COARSE_LOCATION,
//                    Manifest.permission.ACCESS_BACKGROUND_LOCATION
//                )
//            }
//        } else {
//            getCurrentLocation()
//        }


    }

    private fun pickBirthdayDate() {
        getDateTimeCalender()
        val datePickerDialog =
            DatePickerDialog(requireContext(), this, currentYear, currentMonth, currentDay)
        datePickerDialog.datePicker.maxDate = Date().time
        datePickerDialog.show()
    }


    private fun getDateTimeCalender() {
        val cal = Calendar.getInstance()
        currentDay = cal.get(Calendar.DAY_OF_MONTH)
        currentMonth = cal.get(Calendar.MONTH)
        currentYear = cal.get(Calendar.YEAR)
    }

    @SuppressLint("MissingPermission")
    private fun getCurrentLocation() {
        val locationRequest = LocationRequest().apply {
            interval = 10000
            fastestInterval = 3000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        locationSettingsBuilder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)

        val result: Task<LocationSettingsResponse>? =
            LocationServices.getSettingsClient(requireActivity())
                .checkLocationSettings(locationSettingsBuilder?.build())
        result?.addOnCompleteListener {
            try {
                it.getResult(ApiException::class.java)
            } catch (e: ApiException) {
                when (e.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                        try {
                            val resolveApiException = e as ResolvableApiException
                            resolveApiException.startResolutionForResult(
                                requireActivity(),
                                REQUEST_CHECK_CODE
                            )
                        } catch (ex: IntentSender.SendIntentException) {
                            ex.printStackTrace()
                        } catch (ex: ClassCastException) {
                            ex.printStackTrace()
                        }
                    }

                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                        TODO()
                    }
                }
            }
        }

        LocationServices.getFusedLocationProviderClient(requireContext())
            .requestLocationUpdates(locationRequest, object : LocationCallback() {

                override fun onLocationResult(locationResult: LocationResult?) {
                    super.onLocationResult(locationResult)
                    LocationServices.getFusedLocationProviderClient(requireContext())
                        .removeLocationUpdates(this)
                    if (locationResult != null && locationResult.locations.size > 0) {
                        val latestLocationIndex = locationResult.locations.size - 1
                        val latitude = locationResult.locations[latestLocationIndex].latitude
                        val longitude = locationResult.locations[latestLocationIndex].longitude
//                        binding.tvLocation.text = String.format(
//                            "Lat: %s Long: %s",
//                            latitude,
//                            longitude
//                        )
                        val location = Location("providerNA")
                        location.latitude = latitude
                        location.longitude = longitude
                        val coordinates = listOf(latitude, longitude)
                        jsonObject = JsonObject()
                        val toJson = Gson().toJsonTree(coordinates)
                        jsonObject?.addProperty("type", "Point")
                        jsonObject?.add("coordinates", toJson)
                        Timber.d(jsonObject.toString())
                        userLocation = com.brilliance.dualiit.model.Location("Point", coordinates)
                        fetchLocationFromLatLong(location)
                        val map = HashMap<String, Any>()
                        map["location"] = userLocation!!
                        viewModel.updateProfileWithLocation(map)
                    }
                }
            }, Looper.getMainLooper())
    }

    private fun fetchLocationFromLatLong(location: Location) {
        val intent = Intent(requireActivity(), FetchAddressIntentService::class.java)
        intent.putExtra(AppConstant.RECEIVER, resultReceiver)
        intent.putExtra(AppConstant.LOCATION_DATA_EXTRA, location)
        activity?.startService(intent)
    }

    private fun setInterestChips(list: List<InterestDoc>) {
        for (i in list.indices) {
            val interestName = list[i].name
//            (interestsList as ArrayList).add(interestName)
            val chip = Chip(requireContext())
            val paddingDp = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 10F,
                resources.displayMetrics
            ).toInt()
            chip.setPadding(paddingDp, paddingDp, paddingDp, paddingDp)
            chip.chipBackgroundColor = ColorStateList.valueOf(
                ContextCompat.getColor(
                    requireContext(),
                    DualiitR.color.com_facebook_button_background_color_disabled
                )
            )
            chip.text = interestName
            chip.isCloseIconVisible = false
            binding.chipGroupInterests.addView(chip)
        }
        if (addInterestBtn != null) {
            binding.chipGroupInterests.removeView(addInterestBtn)
        }
        binding.chipGroupInterests.addView(createInterestBtn())
        addInterestBtn?.setOnClickListener {
            if (interestsList.size >= 5) {
                requireActivity().showToast("You can only add up to 5 interests")
            } else {
                val interestBottomSheet = InterestBottomSheetFragment()
                interestBottomSheet.show(childFragmentManager, "Interests")
            }
        }
//        binding.chipGroupInterests.invalidate()
    }


    private fun createInterestBtn(): Chip {
        val paddingDp = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 10F,
            resources.displayMetrics
        ).toInt()
        addInterestBtn = Chip(requireContext()).apply {
            setPadding(paddingDp, paddingDp, paddingDp, paddingDp)
            chipBackgroundColor = ColorStateList.valueOf(
                ContextCompat.getColor(
                    requireContext(),
                    DualiitR.color.com_facebook_button_background_color_disabled
                )
            )
            text = "+ Add"
            isCloseIconVisible = false
        }
        return addInterestBtn!!
    }

//    override fun onResume() {
//        super.onResume()
//        activity?.findViewById<RelativeLayout>(R.id.toolbar_layout)?.visibility = View.GONE
//    }
//
//
//    override fun onStop() {
//        super.onStop()
//        activity?.findViewById<RelativeLayout>(R.id.toolbar_layout)?.visibility = View.VISIBLE
//    }


    override fun setObserver() {
        viewModel.croppedImage.observe(viewLifecycleOwner, Observer {
            imageUri = it
            binding.ivProfile.setImageURI(imageUri)
            if (imageUri != null) {
                val file = File(imageUri!!.path!!)

                val filePart = MultipartBody.Part.createFormData(
                    "images",
                    file.name,
                    file.asRequestBody("image/*".toMediaTypeOrNull())
                )
                viewModel.uploadAvatar(filePart)
            }
        })

        viewModel.userAvatar.observe(viewLifecycleOwner, Observer {
            userAvatar = it
        })

        viewModel.uploadProgress.observe(viewLifecycleOwner, Observer {
            binding.imageProgress.isVisible = it == true
        })

        viewModel.updateProgress.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                binding.progressBar.isVisible = true
                binding.editToggle.isChecked = false
                binding.btnEditProfileDone.isVisible = false
                binding.btnSettings.isVisible = true
                addInterestBtn?.isClickable = false
                addInterestBtn?.isEnabled = false
                activity?.showToast("Profile Updated")
            } else {
                binding.progressBar.isVisible = false
            }

        })

        viewModel.receiveEmailOTP.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                startActivity(
                    Intent(requireActivity(), ValidateOTP::class.java).putExtra(
                        AppConstant.REQ_VERIFY_EMAIL,
                        true
                    ).putExtra("EMAIL", binding.etEmail.text.toString())
                )
            }
        })

        viewModel.isEmailValid.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                isEmailVerified = true
                activity?.showToast("Email Verified Successfully")
                Timber.d("Email Verified Successfully")
                binding.btnVerifyEmail.text = "Verified"
                binding.btnVerifyEmail.isClickable = false
                binding.etEmail.inputType = InputType.TYPE_NULL
            } else {
                isEmailVerified = false
            }
        })

        super.setBaseObserver()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager?.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
//        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
        when (requestCode) {
            LOCATION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getCurrentLocation()
                } else {
                    activity?.showToast("Storage Permission is required to Select Picture")
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

//    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
//        getCurrentLocation()
//    }
//
//    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
//        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
//            AppSettingsDialog.Builder(this).build().show()
//        } else {
//            requestLocationPermissions()
//        }
//    }


    companion object {
        private const val LOCATION_REQUEST_CODE = 120
        private const val REQUEST_CHECK_CODE = 125
    }


    inner class AddressResultReceiver(handler: Handler) : ResultReceiver(handler) {

        override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
            super.onReceiveResult(resultCode, resultData)
            if (resultCode == AppConstant.SUCCESS_RESULT) {
                binding.tvLocation.text = resultData?.getString(AppConstant.RESULT_DATA_KEY)
                val address =
                    resultData?.getString(AppConstant.RESULT_DATA_KEY)?.split(",")?.toTypedArray()
                area = address?.get(0) ?: ""
                country = address?.get(1)
                region = address?.get(2)
                Timber.d("$area, $country, $region")
            } else {
                activity?.showToast(resultData?.getString(AppConstant.RESULT_DATA_KEY))
            }
        }

    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        savedDay = dayOfMonth
        savedMonth = month + 1
        savedYear = year

        var monthString = savedMonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        }

        dateOfBirth = "${savedDay}-${monthString}-${savedYear}"
        binding.tvBirthday.text = dateOfBirth
    }
}