package com.brilliance.dualiit.ui.meeting.activities

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ActivityCreateMeetingBinding
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.brilliance.dualiit.utils.extensions.showToast
import com.bumptech.glide.Glide
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class CreateMeetingActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener,
    TimePickerDialog.OnTimeSetListener {

    private lateinit var binding: ActivityCreateMeetingBinding
    private val viewModel: HomeViewModel by viewModel()
    private var userId: String? = null
    private var userName: String? = null
    private var userPhoto: String? = null
    private var currentDay = 0
    private var currentMonth = 0
    private var currentYear = 0
    private var savedDay = 0
    private var savedMonth = 0
    private var savedYear = 0
    private var selectedDate: String? = ""
    private var selectedTime: String? = ""
    private val timeCalendar = Calendar.getInstance()
    private var currentHour = 0
    private var currentMinute = 0
    private var meetingId: String? = null
    private var meetingTime: String? = null
    private var meetingDate: String? = null
    private var TAG = "CreateMeetingActivity"
    private var dateTime: String? = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCreateMeetingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        meetingId = intent?.getStringExtra("MEETING_ID")

        if (meetingId != null) {
            userName = intent?.getStringExtra("USER_NAME")
            userPhoto = intent?.getStringExtra("USER_PHOTO")
            meetingTime = intent?.getStringExtra("MEETING_TIME")
            meetingDate = intent?.getStringExtra("MEETING_DATE")
            binding.tvTitle.text = "Reschedule Meeting"
            val oldMeetingTime = "$meetingDate $meetingTime"
            binding.btnPickDate.text = formattedDate(oldMeetingTime)
            binding.btnCreateMeeting.text = "Reschedule"
        } else {
            userId = intent?.getStringExtra("USER_ID")
            userName = intent?.getStringExtra("USER_NAME")
            userPhoto = intent?.getStringExtra("USER_PHOTO")
            binding.tvTitle.text = "Create Meeting"

        }

        binding.tvUserName.text = userName

        if (userPhoto == "") {
            binding.ivUserProfile.setBackgroundResource(R.drawable.ic_user_avatar)
        } else {
            Glide.with(this)
                .load(userPhoto)
                .into(binding.ivUserProfile)
        }

        lifecycleScope.launchWhenStarted {
            viewModel.meetingsEvent.collect { event ->
                when (event) {
                    is HomeViewModel.MeetingsEvent.OnCreateMeeting -> {
                        showToast(event.message)
                        finish()
                    }
                }
            }
        }

        binding.btnPickDate.setOnClickListener {
            showDatePickerDialog()
        }

        binding.createMeetingToolbar.toolbar.setNavigationOnClickListener { onBackPressed() }

        binding.btnCreateMeeting.setOnClickListener {
            if (selectedDate.isNullOrEmpty() || selectedTime.isNullOrEmpty()) {
                showToast("Please select meeting date and time to continue")
            } else {
                createNewMeeting()
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.invitationEvent.collect { event ->
                when (event) {
                    is HomeViewModel.InvitationEvent.InvitationAccepted -> {
                        showToast("Meeting Rescheduled")
                        finish()
                    }
                }
            }
        }

    }

    private fun createNewMeeting() {
        if (meetingId != null) {
            val meetingMap = HashMap<String, Any>()
            meetingMap["date"] = selectedDate.toString()
            meetingMap["time"] = selectedTime.toString()
            meetingMap["dateTime"] = dateTime.toString()
            viewModel.updateMeeting(meetingId!!, meetingMap)

        } else {
            val meetingMap = HashMap<String, Any>()
            meetingMap["userId"] = userId.toString()
            meetingMap["date"] = selectedDate.toString()
            meetingMap["time"] = selectedTime.toString()
            meetingMap["dateTime"] = dateTime.toString()

            viewModel.createMeeting(meetingMap)
        }
    }

    private fun pickTime() {
        val timePickerDialog =
            TimePickerDialog(
                this,
                this,
                currentHour,
                currentMinute,
                DateFormat.is24HourFormat(this)
            )
        timePickerDialog.show()
    }


    private fun getDateTimeCalender() {
        val cal = Calendar.getInstance()
        currentDay = cal.get(Calendar.DAY_OF_MONTH)
        currentMonth = cal.get(Calendar.MONTH)
        currentYear = cal.get(Calendar.YEAR)
        currentHour = cal.get(Calendar.HOUR_OF_DAY)
        currentMinute = cal.get(Calendar.MINUTE)
    }

    private fun showDatePickerDialog() {
        getDateTimeCalender()
        val datePickerDialog =
            DatePickerDialog(this, this, currentYear, currentMonth, currentDay)
//        datePickerDialog.datePicker.maxDate = Date().time
        datePickerDialog.datePicker.minDate = Date().time
        datePickerDialog.show()
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        savedDay = dayOfMonth
        savedMonth = month + 1
        savedYear = year

        var monthString = savedMonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        }

        selectedDate = "${savedDay}-${monthString}-${savedYear}"
//        binding.btnPickDate.text = selectedDate
        pickTime()
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        timeCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
        timeCalendar.set(Calendar.MINUTE, minute)
//        currentHour = hourOfDay
//        currentMinute = minute
        Timber.d("Selected hour: $hourOfDay Selected Minute: $minute")
        val timeFormat = DateFormat.format("hh:mm a", timeCalendar)
        selectedTime = DateFormat.format("HH:mm", timeCalendar).toString()
        Log.d(TAG, "Time: $selectedTime")
//        binding.btnPickTime.text = timeFormat
        val timeToShow = "$selectedDate $selectedTime"
        binding.btnPickDate.text = formattedDate(timeToShow)
        dateTime = formatDateToTimeStamp(timeToShow)
        Log.d("CreateMeetingActivity", "DateTime: ${formatDateToTimeStamp(timeToShow)}")
    }

    private fun formattedDate(t: String): String {
        var time: String? = null
        try {
            val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.ENGLISH)
            val date = simpleDateFormat.parse(t)
            val simpleDateFormat2 = SimpleDateFormat("EEE, dd/MM/yyyy, hh:mm aa", Locale.ENGLISH)
            time = simpleDateFormat2.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return time!!
    }

    private fun formatDateToTimeStamp(t: String): String {
        var time: String? = null
        try {
            val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.ENGLISH)
            val date = simpleDateFormat.parse(t)
            val simpleDateFormat2 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ENGLISH)
            time = simpleDateFormat2.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return time!!
    }
}