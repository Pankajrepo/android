package com.brilliance.dualiit.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.brilliance.dualiit.databinding.ActivityNotificationSettingsBinding
import com.brilliance.dualiit.model.Notification
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.brilliance.dualiit.utils.AppPreferencesHelper
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class NotificationSettingsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityNotificationSettingsBinding
    private val viewModel: HomeViewModel by viewModel()
    val prefs: AppPreferencesHelper by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNotificationSettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setNotificationValues()

        binding.switchMsgNotification.setOnCheckedChangeListener { _, isChecked ->
            val notification = prefs.userModel?.notification?.copy(
                enabled = if (isChecked) 1 else 0
            )
            notification?.let {
                changeNotificationToggle(notification = it)
            }
        }

        binding.switchInvitations.setOnCheckedChangeListener { _, isChecked ->
            val notification = prefs.userModel?.notification?.copy(
                invitation = if (isChecked) 1 else 0
            )
            notification?.let {
                changeNotificationToggle(notification = it)
            }
        }

        binding.switchMeetingReminder.setOnCheckedChangeListener { _, isChecked ->
            val notification = prefs.userModel?.notification?.copy(
                meetingReminder = if (isChecked) 1 else 0
            )
            notification?.let {
                changeNotificationToggle(notification = it)
            }
        }

        binding.switchShowPreview.setOnCheckedChangeListener { _, isChecked ->
            val notification = prefs.userModel?.notification?.copy(
                previewOnScreen = if (isChecked) 1 else 0
            )
            notification?.let {
                changeNotificationToggle(notification = it)
            }
        }

        binding.switchSoundNotification.setOnCheckedChangeListener { _, isChecked ->
            val notification = prefs.userModel?.notification?.copy(
                sound = if (isChecked) 1 else 0
            )
            notification?.let {
                changeNotificationToggle(notification = it)
            }
        }

        binding.switchVibration.setOnCheckedChangeListener { _, isChecked ->
            val notification = prefs.userModel?.notification?.copy(
                vibration = if (isChecked) 1 else 0
            )
            notification?.let {
                changeNotificationToggle(notification = it)
            }
        }

        binding.notificationToolbar.toolbar.setNavigationOnClickListener { onBackPressed() }

    }

    private fun changeNotificationToggle(notification: Notification) {
        val userModel = prefs.userModel?.copy(
            notification = notification,
            accessToken = null,
            refreshToken = null
        )
        userModel?.let {
            viewModel.updateNotificationStatus(it)
        }
    }

    private fun setNotificationValues() {
        binding.switchMsgNotification.isChecked = prefs.userModel?.notification?.enabled == 1
        binding.switchInvitations.isChecked = prefs.userModel?.notification?.invitation == 1
        binding.switchMeetingReminder.isChecked =
            prefs.userModel?.notification?.meetingReminder == 1
        binding.switchSoundNotification.isChecked = prefs.userModel?.notification?.sound == 1
        binding.switchVibration.isChecked = prefs.userModel?.notification?.vibration == 1
        binding.switchShowPreview.isChecked = prefs.userModel?.notification?.previewOnScreen == 1
    }
}