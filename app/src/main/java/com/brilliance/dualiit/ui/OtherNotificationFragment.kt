package com.brilliance.dualiit.ui

import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.FragmentOtherNotificationsBinding
import com.brilliance.dualiit.ui.home.adapters.InterestsLoadStateAdapter
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.brilliance.dualiit.ui.notifications.NotificationAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class OtherNotificationFragment : Fragment(R.layout.fragment_other_notifications) {

    private var _binding: FragmentOtherNotificationsBinding? = null
    private val binding get() = _binding!!

    private lateinit var notificationAdapter: NotificationAdapter
    private val viewModel: HomeViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentOtherNotificationsBinding.bind(view)

        setupRecyclerView()


        viewModel.notificationsList.observe(viewLifecycleOwner) {
            notificationAdapter.submitData(viewLifecycleOwner.lifecycle, it)
        }


        notificationAdapter.addLoadStateListener { loadState ->
            binding.apply {
                progressBar.isVisible = loadState.source.refresh is LoadState.Loading
                rvNotifications.isVisible = loadState.source.refresh is LoadState.NotLoading
                buttonRetry.isVisible = loadState.source.refresh is LoadState.Error
                textViewError.isVisible = loadState.source.refresh is LoadState.Error

                if (loadState.source.refresh is LoadState.NotLoading &&
                    loadState.append.endOfPaginationReached &&
                    notificationAdapter.itemCount < 1
                ) {
                    rvNotifications.isVisible = false
                    tvEmpty.isVisible = true
                } else {
                    tvEmpty.isVisible = false
                }
            }
        }

        binding.buttonRetry.setOnClickListener {
            notificationAdapter.retry()
        }
    }

    private fun setupRecyclerView() {
        notificationAdapter = NotificationAdapter()
        binding.rvNotifications.apply {
            layoutManager = LinearLayoutManager(requireContext())
            itemAnimator = null
            val attrs = intArrayOf(android.R.attr.listDivider)
            val a = context.obtainStyledAttributes(attrs)
            val divider = a.getDrawable(0)
            val inset = resources.getDimensionPixelSize(R.dimen._16sdp)
            val insetDivider = InsetDrawable(divider, inset, 0, inset, 0)
            a.recycle()
            val itemDecoration =
                DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
            itemDecoration.setDrawable(insetDivider)
            addItemDecoration(itemDecoration)

            adapter = notificationAdapter.withLoadStateFooter(
                footer = InterestsLoadStateAdapter {
                    notificationAdapter.retry()
                }
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}