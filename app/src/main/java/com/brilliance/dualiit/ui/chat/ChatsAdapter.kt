package com.brilliance.dualiit.ui.chat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ItemChatBinding
import com.brilliance.dualiit.utils.extensions.makeInvisible
import com.brilliance.dualiit.utils.extensions.show
import com.bumptech.glide.Glide
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ChatsAdapter : PagingDataAdapter<RecentChat, ChatsAdapter.ChatsViewHolder>(DiffCallback()) {

    inner class ChatsViewHolder(private val binding: ItemChatBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                onItemClickListener?.let { click ->
                    val chat = getItem(bindingAdapterPosition)
                    if (chat != null) {
                        click(chat)
                    }
                }
            }
        }

        fun bind(recentChat: RecentChat) {
            binding.apply {
                if (recentChat.user?.avatar == "") {
                    ivProfile.setBackgroundResource(R.drawable.ic_user_avatar)
                } else {
                    Glide.with(itemView)
                        .load(recentChat.user?.avatar)
                        .into(ivProfile)
                }

                if (recentChat.user?.onlineStatus == true) {
                    statusOnline.isVisible = true
                    statusOnline.setBackgroundResource(R.drawable.button_green_circle)
                } else {
                    statusOnline.isVisible = false
                }

                tvUserName.text = recentChat.user?.fullName
                tvMessage.text = recentChat.lastMessage?.body
                recentChat.lastMessage?.dateTime?.let {
                    tvLastMsgTime.text = formattedDate(recentChat.lastMessage.dateTime.toString())
                }

                recentChat.unread?.let {
                    if (it > 0) {
                        unreadCountBg.show()
                        tvUnreadMsgCount.show()
                        tvUnreadMsgCount.text = it.toString()
                    } else {
                        unreadCountBg.makeInvisible()
                        tvUnreadMsgCount.makeInvisible()
                    }
                }
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatsViewHolder {
        val binding = ItemChatBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ChatsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ChatsViewHolder, position: Int) {
        val chatItem = getItem(position)
        if (chatItem != null) {
            holder.bind(chatItem)
        }

    }

    private var onItemClickListener: ((RecentChat) -> Unit)? = null

    fun setOnItemClickListener(listener: (RecentChat) -> Unit) {
        onItemClickListener = listener
    }

    class DiffCallback : DiffUtil.ItemCallback<RecentChat>() {
        override fun areItemsTheSame(oldItem: RecentChat, newItem: RecentChat): Boolean {
            return oldItem.lastMessage?._id == newItem.lastMessage?._id
        }

        override fun areContentsTheSame(oldItem: RecentChat, newItem: RecentChat): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private fun formattedDate(t: String): String {
        var time: String? = null
        try {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
            simpleDateFormat.timeZone = TimeZone.getTimeZone("GMT")
            val date = simpleDateFormat.parse(t)
            val simpleDateFormat2 = SimpleDateFormat("MM/dd/yy hh:mm aa", Locale.getDefault())
            simpleDateFormat2.timeZone = TimeZone.getDefault()
            time = simpleDateFormat2.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return time!!
    }

    private fun getCountry(): String {
        val locale = Locale.getDefault()
        val country = locale.country
        return country.lowercase()
    }
}