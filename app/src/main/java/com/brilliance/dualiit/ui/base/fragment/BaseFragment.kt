package com.brilliance.dualiit.ui.base.fragment

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.theartofdev.edmodo.cropper.CropImage
import com.brilliance.dualiit.BuildConfig
import com.brilliance.dualiit.R
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.extensions.launch
import com.brilliance.dualiit.utils.extensions.showDialog
import org.koin.android.ext.android.inject
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

abstract class BaseFragment : Fragment() {
    abstract fun baseViewModel(): BaseViewModel?
    abstract fun setObserver()
    val prefs: AppPreferencesHelper by inject()
    var isGranted = false

    protected abstract fun createDataBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): View

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setBaseObserver()
        return createDataBinding(inflater, container)
    }

    fun setBaseObserver() {
        baseViewModel()?.let { viewModel ->

            viewModel.redirect.observe(this, Observer {
                it?.let {
                    launch(it.mClass, arguments = it.arguments, isTopFinish = it.isTopFinish)
                }
            })
            viewModel.showDialog.observe(this, Observer {
                it?.let {
                    requireContext().showDialog(
                        title = it.title,
                        message = it.message,
                        onPressOk = View.OnClickListener { v ->
                            it.mClass?.let { mClass ->
                                launch(
                                    mClass,
                                    arguments = it.arguments,
                                    isTopFinish = it.isTopFinish
                                )
                            }
                        }
                    )
                }
            })
        }
    }

    private fun askListOfPermissions(
        permissions: Array<String?>,
        requestCode: Int
    ): Boolean {
        var isOk = true
        val perNeed = StringBuilder()
        for (per in permissions) {
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    per!!
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (isOk) isOk = false
                perNeed.append(per)
                perNeed.append(",")
            }
        }
        if (isOk) {
            return true
        }
        val reqPermissions =
            if (perNeed.length > 1) perNeed.substring(0, perNeed.length - 1).toString() else ""
        if (reqPermissions.isNotEmpty()) {
            val arrPer =
                reqPermissions.split(",".toRegex()).toTypedArray()
            requestPermissions(permissions, requestCode)
        }
        return false
    }

    var captureMediaFile: Uri? = null

    fun dialogImagePicker() { //TO pick image from gallery and camera
        val imagePicker =
            AlertDialog.Builder(requireContext(), R.style.AppTheme_AlertDialog)
        imagePicker.setItems(
            arrayOf(
                getString(R.string.gallery),
                getString(R.string.camera)
            )
        ) { dialog, which ->
            val intent = Intent()
            when (which) {
                0 -> pickImageFromGallery()
                1 -> pickImageFromCamera()
                else -> {
                }
            }
        }.setCancelable(true).setTitle(getString(R.string.select_image)).show()
    }

    private fun pickImageFromGallery() {
        if (!askListOfPermissions(
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ),
                REQUEST_GALLERY
            )
        ) return
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        //        intent.setType("image*//**//*");
        intent.type = "image/*"
        startActivityForResult(
            Intent.createChooser(intent, getString(R.string.select_image)),
            REQUEST_GALLERY
        )
    }

    private fun pickImageFromCamera() {
        pickImageFromCamera(
            getTempMediaDirectory(
                requireContext()
            )
        )
    }

    var tempFile: File? = null

    //Pick Image From Camera
    private fun pickImageFromCamera(dir: String?): File? {
        if (!askListOfPermissions(
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ),
                REQUEST_CAPTURE
            )
        ) return null
        val intent = Intent()
        intent.action = MediaStore.ACTION_IMAGE_CAPTURE
        try {
            tempFile = File.createTempFile("image", ".png", File(dir))
            captureMediaFile = FileProvider.getUriForFile(
                requireContext(),
                BuildConfig.APPLICATION_ID + ".provider",
                tempFile!!
            )
            intent.putExtra(MediaStore.EXTRA_OUTPUT, captureMediaFile)
            intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        } catch (e: IOException) {
            e.printStackTrace()
        }
        startActivityForResult(
            intent,
            REQUEST_CAPTURE
        )
        return tempFile
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        isGranted = true
        for (grantResult in grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                isGranted = false

                // System.out.println("prefered location===" + getApplicationClass().getPreferedLocation());
                break
            }
        }
        if (isGranted) {
            if (requestCode == REQUEST_CAPTURE) {
                pickImageFromCamera()
            } else if (requestCode == REQUEST_GALLERY) {
                pickImageFromGallery()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CAPTURE) {
            if (resultCode == Activity.RESULT_OK) {
                if (resultCode == Activity.RESULT_OK) {
                    if (captureMediaFile != null) {
                        cropImage(captureMediaFile)
                    } else if (data != null && data.extras != null) {
                        var out: FileOutputStream? = null
                        try {
                            val extras = data.extras
                            val imageBitmap = extras!!["data"] as Bitmap?
                            val mediaPath = File.createTempFile(
                                "image",
                                ".png",
                                File(
                                    getTempMediaDirectory(
                                        requireContext()
                                    )
                                )
                            ).absolutePath
                            out = FileOutputStream(mediaPath)
                            imageBitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, out)
                            cropImage(Uri.fromFile(File(mediaPath)))
                        } catch (e: Exception) {
                            e.printStackTrace()
                        } finally {
                            try {
                                out?.close()
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }
                        }
                    } else {
                        Log.e("Invalid", "Invalid data")
                    }
                }
            }
        } else if (requestCode == REQUEST_GALLERY && data != null) {
            cropImage(data.data)
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                val resultUri = result.uri
                getCroppedImage(resultUri)
            }
        } else {
            //    show_alert();
        }
    }

    private fun getCroppedImage(resultUri: Uri?) {
        baseViewModel()?.croppedImage?.postValue(resultUri)
    }

    private fun cropImage(sourceFileUri: Uri?) {
        CropImage.activity(sourceFileUri).setOutputCompressQuality(30)
            .setInitialCropWindowPaddingRatio(0f)
            .setAspectRatio(1, 1)
            .start(requireContext(), this)
    }

    companion object {
        const val REQUEST_CAPTURE = 118
        const val REQUEST_GALLERY = 119
        const val REQUEST_SAVE_IMAGE_IN_GALLERY = 120
        fun getTempMediaDirectory(context: Context): String? {
            val state = Environment.getExternalStorageState()
            var dir: File? = null
            dir = if (Environment.MEDIA_MOUNTED == state) {
                context.externalCacheDir
            } else {
                context.cacheDir
            }
            if (dir != null && !dir.exists()) {
                dir.mkdirs()
            }
            return if (dir!!.exists() && dir.isDirectory) {
                dir.absolutePath
            } else null
        }
    }

}