package com.brilliance.dualiit.ui.auth.helpers

sealed class OTPEvent {
    class NumberAlreadyExist(val isExist: Boolean, val message: String) : OTPEvent()
    object OTPSuccess : OTPEvent()
    class OTPFailed(val message: String) : OTPEvent()
}

sealed class ChangePasswordEvent {
    object Loading : ChangePasswordEvent()
    object Success : ChangePasswordEvent()
    class Failed(val message: String) : ChangePasswordEvent()
}

sealed class DeleteAccountEvent {
    object Loading : DeleteAccountEvent()
    object Success : DeleteAccountEvent()
    class Failed(val message: String) : DeleteAccountEvent()
}



