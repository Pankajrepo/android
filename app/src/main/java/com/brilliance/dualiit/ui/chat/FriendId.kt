package com.brilliance.dualiit.ui.chat

import androidx.annotation.Keep

@Keep
data class FriendId(
    val _id: String? = "",
    val accountType: String? = "",
    val avatar: String? = "",
    val email: String? = "",
    val fullName: String? = "",
    val lastOnlineTime: String? = "",
    val onlineStatus: Boolean? = false,
    val phone: String? = ""
)