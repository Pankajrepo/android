package com.brilliance.dualiit.ui.auth.activities

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.brilliance.dualiit.databinding.ActivityChangePasswordBinding
import com.brilliance.dualiit.ui.auth.helpers.ChangePasswordEvent
import com.brilliance.dualiit.ui.auth.viewmodel.AuthViewModel
import com.brilliance.dualiit.ui.base.activity.BaseActivity
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.ui.fragment.SettingsActivity
import com.brilliance.dualiit.utils.StringUtility
import com.brilliance.dualiit.utils.extensions.hide
import com.brilliance.dualiit.utils.extensions.launchActivity
import com.brilliance.dualiit.utils.extensions.show
import com.google.android.material.textfield.TextInputLayout
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class ChangePassword : BaseActivity() {
    private lateinit var binding: ActivityChangePasswordBinding
    private val viewModel: AuthViewModel by viewModel()
    override fun baseViewModel(): BaseViewModel = viewModel
    override fun setObserver() {
        super.setBaseObserver()

//        viewModel.passwordChanged.observe(this, Observer {
//            if (it == true) {
//                val intent = Intent(this, SettingsActivity::class.java)
//                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
//                startActivity(intent)
//                finish()
//            }
//        })

        lifecycleScope.launchWhenStarted {
            viewModel.changePasswordEvent.collect { event ->
                when (event) {
                    ChangePasswordEvent.Loading -> {
                        binding.progressBar.show()
                    }
                    is ChangePasswordEvent.Failed -> {
                        showToast(event.message)
                        binding.progressBar.hide()
                    }
                    ChangePasswordEvent.Success -> {
                        binding.progressBar.hide()
                        showToast("Password Changed Successfully")
                        val intent = Intent(this@ChangePassword, SettingsActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivity(intent)
                        finish()
                    }
                }
            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBar(this)
        binding = ActivityChangePasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setObserver()

        binding.btnChangePassword.setOnClickListener {

            if (!validatePassword(binding.tilOldPassword) || !validatePassword(binding.tilNewPassword)
                || !validateConfirmPassword()
            ) {
                return@setOnClickListener
            }

            viewModel.changePassword(
                binding.etOldPassword.text.toString().trim(),
                binding.etNewPassword.text.toString().trim()
            )
        }

        binding.ivNavigationBack.setOnClickListener {
            finish()
        }
    }

    private fun validatePassword(til: TextInputLayout): Boolean {
        val password = til.editText?.text.toString().trim()
        return when {
            password.isEmpty() -> {
                til.error = "Password can not be empty"
                false
            }
            password.length < 6 -> {
                til.error = "Password must be 6 characters long"
                false
            }
            else -> {
                til.error = null
                til.isErrorEnabled = false
                true
            }
        }
    }

    private fun validateConfirmPassword(): Boolean {
        val password = binding.tilConfirmPassword.editText?.text.toString().trim()
        return when {
            password.isEmpty() -> {
                binding.tilConfirmPassword.error = "Password can not be empty"
                false
            }
            password.length < 6 -> {
                binding.tilConfirmPassword.error = "Password must be 6 characters long"
                false
            }
            password != binding.tilNewPassword.editText?.text.toString() -> {
                binding.tilConfirmPassword.error = "Password and Confirm Password do not match"
                false
            }
            else -> {
                binding.tilConfirmPassword.error = null
                binding.tilConfirmPassword.isErrorEnabled = false
                true
            }
        }
    }
}