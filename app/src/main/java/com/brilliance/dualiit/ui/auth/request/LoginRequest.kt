package com.brilliance.dualiit.ui.auth.request

import androidx.annotation.Keep


@Keep
data class LoginRequest(
    var phone: String?= "",
    var password: String?= "",
    var deviceType: String?= "",
    var deviceToken: String?= "",
    var appVersion: String= ""
)