package com.brilliance.dualiit.ui.chat.models.current

import androidx.annotation.Keep

@Keep
data class From(
    val _id: String? = "",
    val avatar: String? = "",
    val fullName: String? = ""
)