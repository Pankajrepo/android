package com.brilliance.dualiit.ui.auth.request

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.Keep

@Keep
data class SignupRequest(
    var fullName: String? = "",
    var phone: String? = "",
    var code: String? = "",
    var email: String? = "",
    var password: String? = "",
    var deviceType: String? = "",
    var deviceToken: String? = "",
    var appVersion: String? = ""
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(fullName)
        writeString(phone)
        writeString(code)
        writeString(email)
        writeString(password)
        writeString(deviceType)
        writeString(deviceToken)
        writeString(appVersion)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<SignupRequest> =
            object : Parcelable.Creator<SignupRequest> {
                override fun createFromParcel(source: Parcel): SignupRequest = SignupRequest(source)
                override fun newArray(size: Int): Array<SignupRequest?> = arrayOfNulls(size)
            }
    }
}