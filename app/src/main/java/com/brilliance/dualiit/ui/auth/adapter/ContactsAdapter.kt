package com.brilliance.dualiit.ui.auth.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ItemContactBinding
import com.bumptech.glide.Glide
import com.brilliance.dualiit.model.Contact

class ContactsAdapter(
    private var contactsList: List<Contact>
) : RecyclerView.Adapter<ContactsAdapter.ContactsViewHolder>() {

    inner class ContactsViewHolder(private val binding: ItemContactBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(contact: Contact) {
            binding.apply {

                if (contact.userPhoto != null) {
                    Glide.with(itemView).load(contact.userPhoto).into(ivContactImage)
                } else {
                    ivContactImage.setImageResource(R.drawable.ic_user_avatar)
                }

                tvContactName.text = contact.name
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactsViewHolder {
        val binding = ItemContactBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ContactsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ContactsViewHolder, position: Int) {
        val contact = contactsList[position]
        holder.bind(contact)
    }

    override fun getItemCount(): Int {
        return contactsList.size
    }
}