package com.brilliance.dualiit.ui.home.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ItemNearmeBinding
import com.brilliance.dualiit.model.SearchDoc

class SearchAdapter : RecyclerView.Adapter<SearchAdapter.SearchViewHolder>() {


    inner class SearchViewHolder(private val binding: ItemNearmeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                onItemClickListener?.let { click ->
                    val user = searchedUsersList[bindingAdapterPosition]
                    click(user)
                }
            }
        }

        fun bind(userModel: SearchDoc) {
            binding.apply {

                if (userModel.avatar == "") {
                    ivProfile.setBackgroundResource(R.drawable.ic_user_avatar)
                } else {
                    Glide.with(itemView)
                        .load(userModel.avatar)
                        .into(ivProfile)
                }

                if (userModel.onlineStatus == true) {
                    statusOnline.setBackgroundResource(R.drawable.button_green_circle)
                } else {
                    statusOnline.setBackgroundResource(R.drawable.button_red_circle)
                }

                tvName.text = userModel.fullName
                tvPhone.text = userModel.phone

            }
        }
    }

    private val differCallback = object : DiffUtil.ItemCallback<SearchDoc>() {
        override fun areItemsTheSame(oldItem: SearchDoc, newItem: SearchDoc): Boolean {
            return oldItem._id == newItem._id
        }

        override fun areContentsTheSame(oldItem: SearchDoc, newItem: SearchDoc): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val differ = AsyncListDiffer(this, differCallback)

    var searchedUsersList: List<SearchDoc>
        get() = differ.currentList
        set(value) = differ.submitList(value)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val binding = ItemNearmeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SearchViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        val user = searchedUsersList[position]
        holder.bind(user)
    }

    override fun getItemCount(): Int {
        return searchedUsersList.size
    }

    private var onItemClickListener: ((SearchDoc) -> Unit)? = null

    fun setOnItemClickListener(listener: (SearchDoc) -> Unit) {
        onItemClickListener = listener
    }
}