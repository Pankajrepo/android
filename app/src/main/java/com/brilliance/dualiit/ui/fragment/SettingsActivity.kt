package com.brilliance.dualiit.ui.fragment


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.brilliance.dualiit.databinding.ActivitySettingsBinding
import com.brilliance.dualiit.ui.AccountsActivity
import com.brilliance.dualiit.ui.BlockedActivity
import com.brilliance.dualiit.ui.NotificationSettingsActivity
import com.brilliance.dualiit.ui.auth.activities.ChangePassword
import com.brilliance.dualiit.ui.auth.activities.ChooseLoginSignup
import com.brilliance.dualiit.ui.auth.activities.ContactUsActivity
import com.brilliance.dualiit.ui.auth.viewmodel.AuthViewModel
import com.brilliance.dualiit.ui.base.activity.BaseActivity
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.utils.AppConstant.PRIVACY_POLICY_URL
import com.brilliance.dualiit.utils.AppConstant.TERMS_OF_USE_URL
import dagger.hilt.android.AndroidEntryPoint
import io.socket.client.Socket
import org.json.JSONException
import org.json.JSONObject
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class SettingsActivity : BaseActivity() {
    lateinit var binding: ActivitySettingsBinding
    private val viewModel: AuthViewModel by viewModel()
    override fun baseViewModel(): BaseViewModel = viewModel

    @Inject
    lateinit var socket: Socket

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setObserver()

        binding.changePassword.setOnClickListener {
            startActivity(Intent(this, ChangePassword::class.java))
        }
        binding.tvAccount.setOnClickListener {
            startActivity(Intent(this, AccountsActivity::class.java))
        }

        binding.tvLogout.setOnClickListener {
            sendOfflineEvent()
            prefs.logout()
            startActivity(
                Intent(
                    this,
                    ChooseLoginSignup::class.java
                ).setFlags(
                    Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                )
            )
            finish()

        }

        binding.termOfUse.setOnClickListener {
//            startActivity(Intent(this, TermsOfUse::class.java))
            openInBrowser(TERMS_OF_USE_URL)
        }

        binding.privacyPolicy.setOnClickListener {
//            startActivity(Intent(this, PrivacyActivity::class.java))
            openInBrowser(PRIVACY_POLICY_URL)
        }

//        binding.aboutUs.setOnClickListener {
//            startActivity(Intent(this, TermsOfUse::class.java))
//        }

        binding.contactUs.setOnClickListener {
            startActivity(Intent(this, ContactUsActivity::class.java))
        }

        binding.notificationLayout.setOnClickListener {
            startActivity(Intent(this, NotificationSettingsActivity::class.java))
        }

        binding.blockedLayout.setOnClickListener {
            startActivity(Intent(this, BlockedActivity::class.java))
        }

        binding.privacyLayout.setOnClickListener {
            startActivity(Intent(this, PrivacySettingsActivity::class.java))
        }

        binding.settingsToolbar.toolbar.setNavigationOnClickListener { onBackPressed() }
    }

    private fun openInBrowser(url: String) {
        val openURL = Intent(Intent.ACTION_VIEW)
        openURL.data = Uri.parse(url)
        startActivity(openURL)
    }

    private fun sendOfflineEvent() {
        val jsonObject = JSONObject()
        try {
            jsonObject.put("_id", prefs.userModel?.id)
            jsonObject.put("fullName", prefs.userModel?.fullName)
            jsonObject.put("avatar", prefs.userModel?.avatar)
            jsonObject.put("phone", prefs.userModel?.phone)
            jsonObject.put("email", prefs.userModel?.email)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Timber.d("Sending Offline Event: $jsonObject")

        socket.emit("idle", jsonObject)

    }

    override fun setObserver() {
        super.setBaseObserver()
    }
}