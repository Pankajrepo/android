package com.brilliance.dualiit.ui.auth.request

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.Keep

@Keep
data class ResetRequest(
    var phone: String? = "",
    var password: String? = "",
    var code: String? = "",
    var deviceType: String? = "",
    var deviceToken: String? = "",
    var appVersion: String? = ""
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(phone)
        writeString(password)
        writeString(code)
        writeString(deviceType)
        writeString(deviceToken)
        writeString(appVersion)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ResetRequest> = object : Parcelable.Creator<ResetRequest> {
            override fun createFromParcel(source: Parcel): ResetRequest = ResetRequest(source)
            override fun newArray(size: Int): Array<ResetRequest?> = arrayOfNulls(size)
        }
    }
}
