package com.brilliance.dualiit.ui.speeddating

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.brilliance.dualiit.databinding.ActivitySpeedDatingWaitingBinding
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.brilliance.dualiit.ui.speeddating.models.SessionResponse
import com.brilliance.dualiit.ui.speeddating.models.SpeedDatingResponse
import com.brilliance.dualiit.ui.speeddating.models.userdetails.UserDetailsResponse
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.extensions.showToast
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import io.socket.client.Socket
import kotlinx.coroutines.flow.collect
import org.json.JSONException
import org.json.JSONObject
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject

@AndroidEntryPoint
class SpeedDatingWaitingActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySpeedDatingWaitingBinding
    private val viewModel: HomeViewModel by viewModel()
    val prefs: AppPreferencesHelper by inject()
    private var userDetailsResponse: UserDetailsResponse? = null
    private var speedDatingResponse: SpeedDatingResponse? = null
    private var meetingTime = "2"
    private var meetingType = "audio"
    private var sessionResponse: SessionResponse? = null
    val gson: Gson = Gson()

    @Inject
    lateinit var socket: Socket

    companion object {
        private const val TAG = "SpeedWaitingActivity"
    }

    @SuppressLint("LogNotTimber")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySpeedDatingWaitingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        meetingTime = intent?.getStringExtra("MEETING_TIME").toString()
        meetingType = intent?.getStringExtra("MEETING_TYPE").toString()

        sendOnlineEvent()
        initiateSocketConnection()

        lifecycleScope.launchWhenStarted {
            viewModel.sessionEvent.collect { event ->
                when (event) {
                    is HomeViewModel.SessionEvent.OnSessionResponse -> {
                        Log.d(TAG, "SessionResponse: ${event.sessionResponse}")
                        sessionResponse = event.sessionResponse
                        sendCallStatusEvent(event.sessionResponse)
                    }
                    is HomeViewModel.SessionEvent.OnGenerateTokenResponse -> {
                        Log.d(TAG, "Generate Token: ${event.sessionResponse}")
                        sessionResponse = event.sessionResponse
                        if (sessionResponse != null) {
                            startActivity(
                                Intent(
                                    this@SpeedDatingWaitingActivity,
                                    CallingActivity::class.java
                                ).apply {
                                    putExtra("API_KEY", sessionResponse?.apiKey)
                                    putExtra("SESSION_ID", sessionResponse?.sessionId)
                                    putExtra("TOKEN", sessionResponse?.token)
                                    putExtra("ID", speedDatingResponse?.user?._id)
                                    putExtra("NAME", speedDatingResponse?.user?.fullName)
                                    putExtra("MEETING_TYPE", meetingType)
                                    putExtra("MEETING_TIME", meetingTime.toInt())
                                }
                            )

                        }
                    }
                }
            }
        }




        binding.fabCancel.setOnClickListener {
            finish()
        }

    }

    private fun sendCallStatusEvent(response: SessionResponse) {
        val fromUserObject = JSONObject()
        try {
            fromUserObject.put("_id", userDetailsResponse?.from?._id)
            fromUserObject.put("name", userDetailsResponse?.from?.fullName)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val toUserObject = JSONObject()
        try {
            toUserObject.put("_id", prefs.userModel?.id)
            toUserObject.put("name", prefs.userModel?.fullName)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val jsonObject = JSONObject()
        try {
            jsonObject.put("from", fromUserObject)
            jsonObject.put("to", toUserObject)
            jsonObject.put("status", "accepted")
            jsonObject.put("type", meetingType)
            jsonObject.put("shareScreen", false)
            jsonObject.put("speedDating", meetingTime.toInt())
            jsonObject.put("sessionId", response.sessionId)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Log.d(TAG, "Sending Call Status Event: $jsonObject")

        socket.emit("callStatus", jsonObject)

        startActivity(Intent(this@SpeedDatingWaitingActivity, CallingActivity::class.java).apply {
            putExtra("API_KEY", sessionResponse?.apiKey)
            putExtra("SESSION_ID", sessionResponse?.sessionId)
            putExtra("TOKEN", sessionResponse?.token)
            putExtra("ID", userDetailsResponse?.from?._id)
            putExtra("NAME", userDetailsResponse?.from?.fullName)
            putExtra("MEETING_TYPE", meetingType)
            putExtra("MEETING_TIME", meetingTime.toInt())
        })


    }

    @SuppressLint("LogNotTimber")
    private fun sendOnlineEvent() {
        val userJsonObject = JSONObject()
        try {
            userJsonObject.put("_id", prefs.userModel?.id)
            userJsonObject.put("phone", prefs.userModel?.phone)
            userJsonObject.put("email", prefs.userModel?.email)
            userJsonObject.put("lastOnlineTime", prefs.userModel?.lastOnlineTime)
            userJsonObject.put("onlineStatus", prefs.userModel?.onlineStatus)
            userJsonObject.put("fullName", prefs.userModel?.fullName)
            userJsonObject.put("avatar", prefs.userModel?.avatar)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val jsonObject = JSONObject()
        try {
            jsonObject.put("speedDating", 2)
            jsonObject.put("type", "audio")
            jsonObject.put("user", userJsonObject)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Log.d(TAG, "Sending Online Event: $jsonObject")

        socket.emit("speedDatingIn", jsonObject)
    }

    private fun sendOfflineEvent() {
        val userJsonObject = JSONObject()
        try {
            userJsonObject.put("_id", prefs.userModel?.id)
            userJsonObject.put("phone", prefs.userModel?.phone)
            userJsonObject.put("email", prefs.userModel?.email)
            userJsonObject.put("lastOnlineTime", prefs.userModel?.lastOnlineTime)
            userJsonObject.put("onlineStatus", prefs.userModel?.onlineStatus)
            userJsonObject.put("fullName", prefs.userModel?.fullName)
            userJsonObject.put("avatar", prefs.userModel?.avatar)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val jsonObject = JSONObject()
        try {
            jsonObject.put("speedDating", 2)
            jsonObject.put("type", "audio")
            jsonObject.put("user", userJsonObject)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Log.d(TAG, "Sending Offline Event: $jsonObject")

        socket.emit("speedDatingOut", jsonObject)
    }

    @SuppressLint("LogNotTimber")
    private fun initiateSocketConnection() {
        socket.on(Socket.EVENT_CONNECT) {
            Log.d(TAG, "Success: Connection successful")
        }

        socket.on(Socket.EVENT_CONNECT_ERROR) {
            Log.d(TAG, "CONNECTION ERROR: ${it[0]}")
        }

        socket.on(Socket.EVENT_DISCONNECT) {
            Log.d(TAG, "DISCONNECT: ${it[0]}")
        }

        socket.on("error") {
            Log.d(TAG, "Error: ${it[0]}")
        }

        socket.on("connect_timeout") {
            Log.d(TAG, "Connect timeout: ${it[0]}")
        }

        socket.on("reconnecting") {
            Log.d(TAG, "Reconnecting: ${it[0]}")
        }

        socket.on("reconnect_error") {
            Log.d(TAG, "Reconnect error: ${it[0]}")
        }

        socket.on("reconnect") {
            Log.d(TAG, "Reconnect: ${it[0]}")
        }

        socket.on("speedDating") { args ->
            Log.d(TAG, "Speed Dating Event: ${args[0]}")
            speedDatingResponse = gson.fromJson(
                args[0].toString(),
                SpeedDatingResponse::class.java
            )
            sendOfflineEvent()
            Log.d(TAG, "SpeedDatingResponse: $speedDatingResponse")
            sendCallEvent(speedDatingResponse)
        }

        socket.on("callReceive") { args ->
            Log.d(TAG, "Call Receive Event: ${args[0]}")
            userDetailsResponse = gson.fromJson(
                args[0].toString(),
                UserDetailsResponse::class.java
            )
            Log.d(TAG, "UserDetails: $userDetailsResponse")
            viewModel.createSessionToken()
        }

        socket.on("callStatus") { args ->
            Log.d(TAG, "Call Status Event: ${args[0]}")
            val userDetailsResponse = gson.fromJson(
                args[0].toString(),
                UserDetailsResponse::class.java
            )
            viewModel.generateTokenWithSession(userDetailsResponse.sessionId.toString())
        }

    }

    private fun sendCallEvent(speedDatingResponse: SpeedDatingResponse?) {
        val fromUserObject = JSONObject()
        try {
            fromUserObject.put("_id", prefs.userModel?.id)
            fromUserObject.put("name", prefs.userModel?.fullName)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val toUserObject = JSONObject()
        try {
            toUserObject.put("_id", speedDatingResponse?.user?._id)
            toUserObject.put("name", speedDatingResponse?.user?.fullName)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val jsonObject = JSONObject()
        try {
            jsonObject.put("from", fromUserObject)
            jsonObject.put("to", toUserObject)
            jsonObject.put("status", "calling")
            jsonObject.put("type", meetingType)
            jsonObject.put("shareScreen", false)
            jsonObject.put("speedDating", meetingTime.toInt())
            jsonObject.put("sessionId", "")
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Log.d(TAG, "Sending Calling Event: $jsonObject")

        socket.emit("callSend", jsonObject)

    }

    override fun onDestroy() {
        super.onDestroy()
        sendOfflineEvent()
    }
}