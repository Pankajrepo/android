package com.brilliance.dualiit.ui.fragment

import android.app.Dialog
import android.content.res.Resources
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.brilliance.dualiit.databinding.FragmentInterestBottomSheetBinding
import com.brilliance.dualiit.model.InterestDoc
import com.brilliance.dualiit.ui.home.adapters.InterestsAdapter
import com.brilliance.dualiit.ui.home.adapters.InterestsLoadStateAdapter
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.brilliance.dualiit.utils.extensions.showToast
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import org.koin.androidx.viewmodel.ext.android.getViewModel
import timber.log.Timber

class InterestBottomSheetFragment : BottomSheetDialogFragment(),
    InterestsAdapter.OnItemClickListener {

    private var _binding: FragmentInterestBottomSheetBinding? = null
    private val binding get() = _binding!!
    private val homeViewModel: HomeViewModel by lazy {
        requireActivity().getViewModel()
    }
    private var selectedInterests: List<InterestDoc> = ArrayList()
    private lateinit var interestsAdapter: InterestsAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme)
    }

    fun newInstance(): InterestBottomSheetFragment {
        return InterestBottomSheetFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentInterestBottomSheetBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        interestsAdapter = InterestsAdapter(this)

        homeViewModel.interestsList.observe(viewLifecycleOwner, Observer {
            Timber.d(it.toString())
            interestsAdapter.submitData(viewLifecycleOwner.lifecycle, it)
        })

        binding.rvInterests.layoutManager =
            LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        binding.rvInterests.itemAnimator = null
        binding.rvInterests.adapter = interestsAdapter.withLoadStateFooter(
            footer = InterestsLoadStateAdapter {
                interestsAdapter.retry()
            }
        )

        interestsAdapter.addLoadStateListener { loadState ->
            binding.apply {
                progressBar.isVisible = loadState.source.refresh is LoadState.Loading
                rvInterests.isVisible = loadState.source.refresh is LoadState.NotLoading
                buttonRetry.isVisible = loadState.source.refresh is LoadState.Error
                textViewError.isVisible = loadState.source.refresh is LoadState.Error

                if (loadState.source.refresh is LoadState.NotLoading &&
                    loadState.append.endOfPaginationReached &&
                    interestsAdapter.itemCount < 1
                ) {
                    rvInterests.isVisible = false
                    textViewEmpty.isVisible = true
                } else {
                    textViewEmpty.isVisible = false
                }
            }
        }

//        binding.etSearchInterest.addTextChangedListener(object : TextWatcher{
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
//
//            override fun onTextChanged(searchKeyword: CharSequence?, start: Int, before: Int, count: Int) {}
//
//            override fun afterTextChanged(s: Editable?) {
//                homeViewModel.searchInterests(s.toString())
//            }
//        })

        binding.etSearchInterest.setOnEditorActionListener(object :
            TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    homeViewModel.searchInterests(v?.text.toString())
                    return true
                }
                return false
            }
        })

        binding.btnAddInterest.setOnClickListener {
            selectedInterests = interestsAdapter.getSelectedInterests()
            if (selectedInterests.size > 5) {
                requireActivity().showToast("You can select only up to 5 interests")
                return@setOnClickListener
            }
            homeViewModel.submitSelectedInterestList(selectedInterests)
            dismiss()
//            val interestsNames = StringBuilder()
//            for (i in selectedInterests.indices) {
//                if (i == 0) {
//                    interestsNames.append(selectedInterests[i].name)
//                } else {
//                    interestsNames.append("\n").append(selectedInterests[i].name)
//                }
//            }
//            activity?.showToast(interestsNames.toString())
        }

        binding.interestToolbar.toolbar.setNavigationOnClickListener { dismiss() }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog

        dialog.setOnShowListener { dialogInterface ->
            val d = dialogInterface as BottomSheetDialog
            val bottomSheet =
                d.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet!!)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED


            bottomSheetBehavior.peekHeight = BottomSheetBehavior.PEEK_HEIGHT_AUTO
            bottomSheetBehavior.skipCollapsed = true

            //setting max height of bottom sheet
            bottomSheet.minimumHeight =
                (Resources.getSystem().displayMetrics.heightPixels)

            bottomSheetBehavior.addBottomSheetCallback(object :
                BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(bottomSheet: View, slideOffset: Float) {
                    Timber.d("Slided")
                }

                override fun onStateChanged(bottomSheet: View, newState: Int) {
//                    if (BottomSheetBehavior.STATE_EXPANDED == newState) {
//                        showView(appBarLayout, getActionBarSize())
//                        hideAppBar(bi.profileLayout);
//
//                    }
                    if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                        dismiss()
                    }

                    if (BottomSheetBehavior.STATE_HIDDEN == newState) {
                        dismiss()
                    }
//                    if (BottomSheetBehavior.STATE_DRAGGING == newState) {
////                        dismiss()
//                    }
                }

            })

        }

        return dialog
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onItemClick(isSelected: Boolean) {
        if (isSelected) {
            binding.btnAddInterest.visibility = View.VISIBLE
        } else {
            binding.btnAddInterest.visibility = View.GONE
        }
    }
}