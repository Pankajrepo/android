package com.brilliance.dualiit.ui.chat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ItemChatReceiverBinding
import com.brilliance.dualiit.databinding.ItemChatSenderBinding
import com.brilliance.dualiit.ui.chat.models.Message
import com.bumptech.glide.Glide
import org.ocpsoft.prettytime.PrettyTime
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class UserMessagesAdapter(private var senderId: String) :
    ListAdapter<Message, RecyclerView.ViewHolder>(DiffCallback()) {


    companion object {
        private const val VIEW_TYPE_SENDER = 1
        private const val VIEW_TYPE_RECEIVER = 2
    }

    inner class SenderMessageViewHolder(private val binding: ItemChatSenderBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
//                onItemClickListener?.let { click ->
//                    val chat = chatsList[bindingAdapterPosition]
//                    click(chat)
            }
        }

        fun bind(chat: Message) {
            binding.apply {
                if (chat.from?.avatar == "") {
                    ivUserProfile.setBackgroundResource(R.drawable.ic_user_avatar)
                } else {
                    Glide.with(itemView)
                        .load(chat.from?.avatar)
                        .into(ivUserProfile)
                }
                tvMessage.text = chat.body
                tvMsgTime.text = formattedDate(chat.dateTime.toString())
            }
        }
    }


    inner class ReceiverMessageViewHolder(private val binding: ItemChatReceiverBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
//                onItemClickListener?.let { click ->
//                    val chat = chatsList[bindingAdapterPosition]
//                    click(chat)

            }
        }

        fun bind(chat: Message) {
            binding.apply {
                tvMessage.text = chat.body
                tvMsgTime.text = formattedDate(chat.dateTime.toString())
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_SENDER) {
            val senderBinding =
                ItemChatSenderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            SenderMessageViewHolder(senderBinding)
        } else {
            val receiverBinding =
                ItemChatReceiverBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            ReceiverMessageViewHolder(receiverBinding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == VIEW_TYPE_SENDER) {
            getItem(position)?.let { (holder as SenderMessageViewHolder).bind(it) }
        } else {
            getItem(position)?.let { (holder as ReceiverMessageViewHolder).bind(it) }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position)?.from?._id == senderId) {
            VIEW_TYPE_SENDER
        } else {
            VIEW_TYPE_RECEIVER
        }
    }

    class DiffCallback : DiffUtil.ItemCallback<Message>() {
        override fun areItemsTheSame(oldItem: Message, newItem: Message): Boolean {
            return oldItem._id == newItem._id
        }

        override fun areContentsTheSame(oldItem: Message, newItem: Message): Boolean {
            return oldItem == newItem
        }

    }

    private fun formattedDate(t: String): String {
        val prettyTime = PrettyTime(Locale(getCountry()))
        var time: String? = null
        try {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
            simpleDateFormat.timeZone = TimeZone.getTimeZone("GMT")
            val date = simpleDateFormat.parse(t)
            val simpleDateFormat2 = SimpleDateFormat("hh:mm a", Locale.getDefault())
            simpleDateFormat2.timeZone = TimeZone.getDefault()
//            time = prettyTime.format(date)
            time = simpleDateFormat2.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return time!!
    }

    private fun getCountry(): String {
        val locale = Locale.getDefault()
        val country = locale.country
        return country.toLowerCase()
    }

}