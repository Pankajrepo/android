package com.brilliance.dualiit.ui.fragment

import android.content.Context
import android.content.Intent
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.FragmentMeetingBinding
import com.brilliance.dualiit.ui.auth.viewmodel.AuthViewModel
import com.brilliance.dualiit.ui.base.fragment.BaseFragment
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.ui.speeddating.SpeedDatingWaitingActivity
import com.brilliance.dualiit.utils.exhaustive
import com.brilliance.dualiit.utils.extensions.showToast
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.DecimalFormat

class MeetingFragment : BaseFragment() {
    lateinit var binding: FragmentMeetingBinding
    private val viewModel: AuthViewModel by viewModel()
    override fun baseViewModel(): BaseViewModel = viewModel
    private var meetingTime = "2"
    private var meetingType = "audio"

    override fun createDataBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentMeetingBinding.inflate(inflater, container, false)

        binding.radioGroupMinutes.setOnCheckedChangeListener { _, checkedId ->
            meetingTime = when (checkedId) {
                R.id.radioBtn2Min -> "2"
                R.id.radioBtn5Min -> "5"
                R.id.radioBtn10Min -> "10"
                else -> "2"
            }
        }

        binding.radioGroupType.setOnCheckedChangeListener { _, checkedId ->
            meetingType = when (checkedId) {
                R.id.radioBtnAudio -> "audio"
                R.id.radioBtnVideo -> "video"
                else -> "audio"
            }
        }

        binding.btnStartMeeting.setOnClickListener {
            Log.d("MeetingFragment", "Meeting Details - $meetingTime & $meetingType")
            startActivity(Intent(requireActivity(), SpeedDatingWaitingActivity::class.java).apply {
                putExtra("MEETING_TIME", meetingTime)
                putExtra("MEETING_TYPE", meetingType)
            })
        }

        return binding.root
    }

    private val timer = object : CountDownTimer(120000, 1000) {
        override fun onTick(millisUntilFinished: Long) {
            val formatter = DecimalFormat("00");

            val minute = (millisUntilFinished / 60000) % 60
            val second = (millisUntilFinished / 1000) % 60

//            binding.tvTimer.text =
//                "Timer:- ${formatter.format(minute)}:${formatter.format(second)}"
        }

        override fun onFinish() {
            requireActivity().showToast("Timer finished")
        }

    }

    override fun setObserver() {
    }


}