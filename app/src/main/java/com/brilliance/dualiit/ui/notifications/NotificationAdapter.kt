package com.brilliance.dualiit.ui.notifications

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ItemNotificationBinding
import com.brilliance.dualiit.model.notification.NotificationModel
import com.bumptech.glide.Glide

class NotificationAdapter :
    PagingDataAdapter<NotificationModel, NotificationAdapter.NotificationViewHolder>(DiffCallback()) {


    inner class NotificationViewHolder(private val binding: ItemNotificationBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(notification: NotificationModel) {
            binding.apply {
                if (notification.from?.avatar == "") {
                    ivUserProfile.setBackgroundResource(R.drawable.ic_user_avatar)
                } else {
                    Glide.with(itemView)
                        .load(notification.from?.avatar)
                        .into(ivUserProfile)
                }

                tvTitle.text = notification.title
                tvDescription.text = notification.message
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val binding =
            ItemNotificationBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NotificationViewHolder(binding)
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        val notification = getItem(position)
        if (notification != null) {
            holder.bind(notification)
        }
    }


    class DiffCallback : DiffUtil.ItemCallback<NotificationModel>() {
        override fun areItemsTheSame(
            oldItem: NotificationModel,
            newItem: NotificationModel
        ): Boolean {
            return oldItem._id == newItem._id
        }

        override fun areContentsTheSame(
            oldItem: NotificationModel,
            newItem: NotificationModel
        ): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }

    }
}
















