package com.brilliance.dualiit.ui.game

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.airbnb.lottie.LottieAnimationView
import com.brilliance.dualiit.BuildConfig
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ActivityGameBinding
import com.brilliance.dualiit.utils.game.GamePlayUtility
import com.brilliance.dualiit.utils.game.GetPosition
import kotlin.properties.Delegates
import com.brilliance.dualiit.utils.game.Result as GameResult

class GamePlayActivity : AppCompatActivity() {

    private lateinit var binding: ActivityGameBinding

    var x = 0
    private var turn: Int = 0
    private var first: Int = 0
    private var pl: Int = 0
    var fl: Int = 0
    private var gameState = arrayOf(2, 2, 2, 2, 2, 2, 2, 2, 2)
    private lateinit var p1: String
    private lateinit var p2: String
    private var player by Delegates.notNull<Int>()
    private var vsWhom by Delegates.notNull<Int>()
    private var weapon by Delegates.notNull<Int>()
    private var jarvis by Delegates.notNull<Int>()
    private var whichFirst by Delegates.notNull<Int>()
    private var whichLevel by Delegates.notNull<Int>()
    private var done = 0
    private var getP = GetPosition()
    private var flag = false
    private var played = 0
    private var mFlag = false

    // 0 = O      1 = X     2 = blank
    private val obj = GamePlayUtility()
    var list = listOf(0, 1, 2, 3, 4, 5, 6, 7, 8).toMutableList()
    private var isclicked = 0

    @SuppressLint("SetTextI18n")
    fun playerClick(view: View) {
        val img = view as ImageView
        val tappedImage = img.tag.toString().toInt()
        if (vsWhom == 0) {
            if (gameState[tappedImage] == 2 && obj.result != GameResult.TIE && obj.result != GameResult.WON) {
                gameState[tappedImage] = turn
                if (turn == 0) {
                    binding.tvTurn.text = "$p2's Turn"
                    img.setImageResource(R.drawable.ic_circle_secondary)
                    val animFadeIn =
                        AnimationUtils.loadAnimation(applicationContext, R.anim.fade_in)
                    img.startAnimation(animFadeIn)
                } else if (turn == 1) {
                    binding.tvTurn.text = "$p1's Turn"
                    img.setImageResource(R.drawable.ic_cross_yellow)
                    val animFadeIn =
                        AnimationUtils.loadAnimation(applicationContext, R.anim.fade_in)
                    img.startAnimation(animFadeIn)
                }
                obj.isWin(
                    gameState,
                    vsWhom,
                    turn,
                    binding.p1Winning,
                    binding.p2Winning,
                    -1,
                    binding.player1Trophy,
                    binding.player2Trophy
                )
                if (obj.result == GameResult.WON && turn == 1) {
                    openDialogBox(view, p2)
                } else if (obj.result == GameResult.WON && turn == 0) {
                    openDialogBox(view, p1)
                } else if (obj.result == GameResult.TIE) {
                    openDialogBox(view, "That was a Tie!")
                }
                if (obj.result != GameResult.TIE && obj.result != GameResult.WON) {
                    turn++
                    turn %= 2
                }
            }
        } else if (vsWhom == 1 && done % 2 == 1) {
            if (gameState[tappedImage] == 2 && obj.result != GameResult.TIE && obj.result != GameResult.WON && obj.result != GameResult.LOST && turn == weapon) {
                gameState[tappedImage] = turn
                list.remove(tappedImage)
                if (turn == weapon) {
                    binding.tvTurn.text = "Jarvis's Turn"
                } else {
                    binding.tvTurn.text = "Your Turn"
                }
                if (turn == 0) {
                    img.setImageResource(R.drawable.ic_circle_secondary)
                    val animFadeIn =
                        AnimationUtils.loadAnimation(applicationContext, R.anim.fade_in)
                    img.startAnimation(animFadeIn)
                } else if (turn == 1) {
                    img.setImageResource(R.drawable.ic_cross_yellow)
                    val animFadeIn =
                        AnimationUtils.loadAnimation(applicationContext, R.anim.fade_in)
                    img.startAnimation(animFadeIn)
                }
                done++
                obj.isWin(
                    gameState,
                    vsWhom,
                    turn,
                    binding.p1Winning,
                    binding.p2Winning,
                    weapon,
                    binding.player1Trophy,
                    binding.player2Trophy
                )
                if (obj.result != GameResult.TIE && obj.result != GameResult.WON) {
                    turn++
                    turn %= 2
                    putNew(getRandom())
                }
                if (obj.result == GameResult.WON) {
                    openDialogBox(view, "YOU")
                } else if (obj.result == GameResult.TIE) {
                    if (!flag) {
                        openDialogBox(view, "TIE")
                    }
                }
            }
        }
    }

    private fun getRandom(): ImageView {
        val r = getP.getPos(list, whichLevel, gameState, jarvis, weapon)
        isclicked = r
        val q = listOf(
            binding.btn1,
            binding.btn2,
            binding.btn3,
            binding.btn4,
            binding.btn5,
            binding.btn6,
            binding.btn7,
            binding.btn8,
            binding.btn9
        )
        for (i: ImageView in q) {
            if (gameState[isclicked] == 2) {
                return q[isclicked]
            }
        }
        return getRandom()
    }

    @SuppressLint("SetTextI18n")
    private fun putNew(o: ImageView) {
        if (turn == 0) {
            Handler().postDelayed({
                binding.tvTurn.text = "Your Turn"
                o.setImageResource(R.drawable.ic_circle_secondary)
                val animFadeIn = AnimationUtils.loadAnimation(applicationContext, R.anim.fade_in)
                o.startAnimation(animFadeIn)
                done++
            }, 400)
        } else if (turn == 1) {
            Handler().postDelayed({
                binding.tvTurn.text = "Your Turn"
                o.setImageResource(R.drawable.ic_cross_yellow)
                val animFadeIn = AnimationUtils.loadAnimation(applicationContext, R.anim.fade_in)
                o.startAnimation(animFadeIn)
                done++
            }, 400)
        }
        gameState[isclicked] = turn
        list.remove(isclicked)
        obj.isWin(
            gameState,
            vsWhom,
            turn,
            binding.p1Winning,
            binding.p2Winning,
            weapon,
            binding.player1Trophy,
            binding.player2Trophy
        )
        if (obj.result == GameResult.LOST) {
            openDialogBox(o, "JARVIS")
        } else if (obj.result == GameResult.TIE) {
            flag = true
            openDialogBox(o, "TIE")
        }
        if (obj.result != GameResult.TIE && obj.result != GameResult.WON) {
            turn++
            turn %= 2
        }
    }

    private fun openDialogBox(v: View, playerName: String) {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this, R.style.CustomAlertDialog)
        val viewGroup = findViewById<ViewGroup>(android.R.id.content)
        val dialogView: View =
            LayoutInflater.from(v.context).inflate(R.layout.result_dialog, viewGroup, false)
        builder.setView(dialogView)
        val alertDialog: AlertDialog = builder.create()
        if (vsWhom == 0) {
            if (obj.result == GameResult.WON) {
                mFlag = true
                dialogView.findViewById<ImageView>(R.id.resultTrophy)
                    .setImageResource(R.drawable.ic_trophy_won)
                dialogView.findViewById<TextView>(R.id.result).text = "Yepii.. $playerName Won!"
                dialogView.findViewById<LottieAnimationView>(R.id.animation_view).visibility =
                    View.VISIBLE
                music(mFlag)
            } else if (obj.result == GameResult.TIE) {
                mFlag = true
                dialogView.findViewById<ImageView>(R.id.resultTrophy)
                    .setImageResource(R.drawable.ic_trophy_tie)
                dialogView.findViewById<TextView>(R.id.result).text = playerName
                dialogView.findViewById<LottieAnimationView>(R.id.animation_view).visibility =
                    View.VISIBLE
                music(mFlag)
            }
        } else if (vsWhom == 1) {
            if (obj.result != GameResult.TIE) {
                if (obj.playerWon) {
                    mFlag = true
                    dialogView.findViewById<ImageView>(R.id.resultTrophy)
                        .setImageResource(R.drawable.ic_trophy_won)
                    dialogView.findViewById<TextView>(R.id.result).text = "Yeppii.. You Won!"
                    dialogView.findViewById<LottieAnimationView>(R.id.animation_view).visibility =
                        View.VISIBLE
                    music(mFlag)
                } else if (obj.result == GameResult.LOST) {
                    mFlag = true
                    dialogView.findViewById<ImageView>(R.id.resultTrophy)
                        .setImageResource(R.drawable.ic_trophy_lost)
                    dialogView.findViewById<TextView>(R.id.result).text = "Ohh... You Lost!"
                    music(mFlag)
                }
            } else {
                mFlag = true
                dialogView.findViewById<ImageView>(R.id.resultTrophy)
                    .setImageResource(R.drawable.ic_trophy_tie)
                dialogView.findViewById<TextView>(R.id.result).text = "That was a tie!"
                dialogView.findViewById<LottieAnimationView>(R.id.animation_view).visibility =
                    View.VISIBLE
                music(mFlag)
            }
        }
        alertDialog.setCancelable(false)

        val animFadeIn = AnimationUtils.loadAnimation(applicationContext, R.anim.result_fade_in)
        Handler().postDelayed({
            dialogView.startAnimation(animFadeIn)
            alertDialog.show()
        }, 500)

        dialogView.findViewById<TextView>(R.id.btnRematch).setOnClickListener {
            val animFadeOut =
                AnimationUtils.loadAnimation(applicationContext, R.anim.result_fade_out)
            dialogView.startAnimation(animFadeOut)
            Handler().postDelayed({
                reset()
                alertDialog.dismiss()
            }, 500)
        }

        dialogView.findViewById<TextView>(R.id.btnQuit).setOnClickListener {
            val animFadeOut =
                AnimationUtils.loadAnimation(applicationContext, R.anim.result_fade_out)
            dialogView.startAnimation(animFadeOut)
            Handler().postDelayed({
                finish()
            }, 400)
        }
    }

    private fun music(musicFlag: Boolean) {
        val winMusic = MediaPlayer.create(
            this@GamePlayActivity,
            R.raw.game_sound_single_short_generic_click_pop
        )
        val lostMusic = MediaPlayer.create(this@GamePlayActivity, R.raw.cartoon_pop_mouth_004)
        val tieMusic = MediaPlayer.create(
            this@GamePlayActivity,
            R.raw.game_sound_double_short_generic_click_pop_002
        )
        if (musicFlag) {
            when (obj.result) {
                GameResult.WON -> {
                    mFlag = false
                    Handler().postDelayed({
                        winMusic.start()
                    }, 500)
                }
                GameResult.LOST -> {
                    mFlag = false
                    Handler().postDelayed({
                        lostMusic.start()
                    }, 500)
                }
                else -> {
                    mFlag = false
                    Handler().postDelayed({
                        tieMusic.start()
                    }, 500)
                }
            }
        } else {
            winMusic.stop()
            lostMusic.stop()
            tieMusic.stop()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun reset() {
        flag = false
        for (i in 0..8) {
            gameState[i] = 2
        }
        val q = listOf(
            binding.btn1,
            binding.btn2,
            binding.btn3,
            binding.btn4,
            binding.btn5,
            binding.btn6,
            binding.btn7,
            binding.btn8,
            binding.btn9
        )
        for (i: ImageView in q) {
            i.setImageResource(0)
        }
        player = pl
        if (x == 0) {
            turn = pl
            first = fl
        }
        obj.playerWon = false
        list = listOf(0, 1, 2, 3, 4, 5, 6, 7, 8).toMutableList()
        obj.result = null
        if (vsWhom == 1) {
            if (turn != first && played == 1) {
                first = turn
                x = 1
            }
            if (turn == first && played == 0) {
                x = 0
                first++
                first %= 2
                played = 1
            }
            if (first == turn) {
                played = 0
            }
            if (turn != first) {
                done = 0
                turn = if (turn == 0) {
                    1
                } else {
                    0
                }
                putNew(getRandom())
            } else {
                done = 1
            }
            if (turn == weapon) {
                binding.tvTurn.text = "Your Turn"
            } else {
                binding.tvTurn.text = "Jarvis's Turn"
            }
        } else if (vsWhom == 0) {
            pl = if (pl == 0) {
                1
            } else {
                0
            }
            if (player == 0) {
                binding.tvTurn.text = "$p1's Turn"
            } else if (player == 1) {
                binding.tvTurn.text = "$p2's Turn"
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGameBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val versionName = BuildConfig.VERSION_NAME
//        binding.appBottomLine.text = "Designed @ bebetterprogrammer.com | v$versionName"
        p1 = intent.getStringExtra("Player1") ?: "Player1"
        p2 = intent.getStringExtra("Player2") ?: "Player2"
        player = intent.getIntExtra("Player", 0)
        weapon = intent.getIntExtra("Flag1", 0)
        whichFirst = intent.getIntExtra("Flag2", 0)
        whichLevel = intent.getIntExtra("Flag", 0)
        vsWhom = intent.getIntExtra("vsWhom", 2)
        if (vsWhom == 0) { // Vs Friend
            binding.Player1.text = p1 // First Player Name
            binding.Player2.text = p2 // Second Player Name
            if (player == 0) {
                binding.tvTurn.text = "$p1's Turn"
                turn = 0
                pl = 1
            } else if (player == 1) {
                binding.tvTurn.text = "$p2's Turn"
                turn = 1
                pl = 0
            }
        } else if (vsWhom == 1) { // Vs Jarvis
            played++
            binding.Player1.text = "YOU"
            binding.Player2.text = "JARVIS"
            if (whichLevel == 0 || whichLevel == 1 || whichLevel == 2) {
                if (whichFirst == 0) {
                    first = 0 // O
                    fl = 0
                } else if (whichFirst == 1) {
                    first = 1 // X
                    fl = 1
                }
                if ((whichFirst == 0 && weapon == 0) || (whichFirst == 1 && weapon == 1)) {
                    binding.tvTurn.text = "Your Turn"
                } else {
                    binding.tvTurn.text = "Jarvis's Turn"
                }
                if (weapon == 0) {
                    turn = 0 // your O
                    pl = 0
                    jarvis = 1
                    done = 1
                } else if (weapon == 1) {
                    turn = 1 // your X
                    pl = 1
                    jarvis = 0
                    done = 1
                }
                if (turn != first) {
                    played = 1
                    done = 0
                    turn = if (turn == 0) {
                        1
                    } else {
                        0
                    }
                    putNew(getRandom())
                } else {
                    played = 0
                }
            }
        }
        binding.btnQuit.setOnClickListener {
            finish()
        }
    }
}