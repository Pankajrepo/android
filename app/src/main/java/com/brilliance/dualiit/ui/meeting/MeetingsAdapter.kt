package com.brilliance.dualiit.ui.meeting

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ItemMeetingOtherBinding
import com.brilliance.dualiit.databinding.ItemMeetingYouBinding
import com.brilliance.dualiit.ui.meeting.models.Meeting
import com.bumptech.glide.Glide
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class MeetingsAdapter(private var senderId: String) :
    PagingDataAdapter<Meeting, RecyclerView.ViewHolder>(DiffCallback()) {


    companion object {
        private const val VIEW_TYPE_SENDER = 1
        private const val VIEW_TYPE_RECEIVER = 2
    }

    inner class SenderMeetingViewHolder(private val binding: ItemMeetingYouBinding) :
        RecyclerView.ViewHolder(binding.root) {

        val bgView = binding.bgLayout
        val fgView = binding.fgLayout

        init {
            binding.root.setOnClickListener {
                onItemClickListener?.let { click ->
                    val meeting = getItem(bindingAdapterPosition)
                    if (meeting != null) {
                        click(meeting)
                    }
                }
            }

            binding.btnReschedule.setOnClickListener {
                onRescheduleClickListener?.let { click ->
                    val meeting = getItem(bindingAdapterPosition)
                    if (meeting != null) {
                        click(meeting)
                    }
                }
            }
        }

        fun bind(meeting: Meeting) {
            binding.apply {
                if (meeting.from?.avatar == "") {
                    userImage.setBackgroundResource(R.drawable.ic_user_avatar)
                } else {
                    Glide.with(itemView)
                        .load(meeting.from?.avatar)
                        .into(userImage)
                }

                if (meeting.to?.avatar == "") {
                    otherUserImage.setBackgroundResource(R.drawable.ic_user_avatar)
                } else {
                    Glide.with(itemView)
                        .load(meeting.to?.avatar)
                        .into(otherUserImage)
                }

                val timeFormatted =
                    "${formattedDate(meeting.date.toString())}, ${formattedTime(meeting.time.toString())}"

                tvMeetingText.text =
                    "You Scheduled Meeting on $timeFormatted with ${meeting.to?.fullName}"

                when (meeting.status) {
                    1 -> {
                        btnReschedule.isVisible = false
                        tvStatus.isVisible = false
                    }
                    2 -> {
                        tvStatus.isVisible = true
                        tvStatus.text =
                            "${meeting.to?.fullName} Accepted Meeting"
                        btnReschedule.isVisible = true
                    }
                    3 -> {
                        tvStatus.isVisible = true
                        tvStatus.text =
                            "${meeting.to?.fullName} Rejected Meeting"
                        btnReschedule.isVisible = true
                    }
                }
            }
        }
    }

    inner class ReceiverMeetingViewHolder(private val binding: ItemMeetingOtherBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                onItemClickListener?.let { click ->
                    val meeting = getItem(bindingAdapterPosition)
                    if (meeting != null) {
                        click(meeting)
                    }
                }
            }

            binding.btnAccept.setOnClickListener {
                onAcceptClickListener?.let { click ->
                    val meeting = getItem(bindingAdapterPosition)
                    if (meeting != null) {
                        click(meeting)
                    }
                }
            }

            binding.btnReject.setOnClickListener {
                onAcceptClickListener?.let { click ->
                    val meeting = getItem(bindingAdapterPosition)
                    if (meeting != null) {
                        click(meeting)
                    }
                }
            }

            binding.btnReschedule.setOnClickListener {
                onRescheduleClickListener?.let { click ->
                    val meeting = getItem(bindingAdapterPosition)
                    if (meeting != null) {
                        click(meeting)
                    }
                }
            }
        }

        fun bind(meeting: Meeting) {
            binding.apply {
                if (meeting.from?.avatar == "") {
                    otherUserImage.setBackgroundResource(R.drawable.ic_user_avatar)
                } else {
                    Glide.with(itemView)
                        .load(meeting.from?.avatar)
                        .into(otherUserImage)
                }

                if (meeting.to?.avatar == "") {
                    userImage.setBackgroundResource(R.drawable.ic_user_avatar)
                } else {
                    Glide.with(itemView)
                        .load(meeting.to?.avatar)
                        .into(userImage)
                }

                val timeFormatted =
                    "${formattedDate(meeting.date.toString())}, ${formattedTime(meeting.time.toString())}"

                tvMeetingText.text =
                    "${meeting.from?.fullName} Scheduled Meeting on $timeFormatted with you"

                when (meeting.status) {
                    1 -> {
                        btnReschedule.isVisible = false
                        btnAccept.isVisible = true
                        btnReject.isVisible = true
                        tvStatus.isVisible = false
                    }
                    2 -> {
                        tvStatus.isVisible = true
                        tvStatus.text =
                            "You Accepted Meeting Request"
                        btnAccept.isVisible = false
                        btnReject.isVisible = false
                        btnReschedule.isVisible = true
                    }
                    3 -> {
                        tvStatus.isVisible = true
                        tvStatus.text =
                            "You Rejected Meeting Request"
                        btnAccept.isVisible = false
                        btnReject.isVisible = false
                        btnReschedule.isVisible = true
                    }
                }
            }
        }
    }


    class DiffCallback : DiffUtil.ItemCallback<Meeting>() {
        override fun areItemsTheSame(oldItem: Meeting, newItem: Meeting): Boolean {
            return oldItem._id == newItem._id
        }

        override fun areContentsTheSame(oldItem: Meeting, newItem: Meeting): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_SENDER) {
            val senderBinding =
                ItemMeetingYouBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            SenderMeetingViewHolder(senderBinding)
        } else {
            val receiverBinding =
                ItemMeetingOtherBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            ReceiverMeetingViewHolder(receiverBinding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == VIEW_TYPE_SENDER) {
            getItem(position)?.let { (holder as SenderMeetingViewHolder).bind(it) }
        } else {
            getItem(position)?.let { (holder as ReceiverMeetingViewHolder).bind(it) }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position)?.from?._id == senderId) {
            VIEW_TYPE_SENDER
        } else {
            VIEW_TYPE_RECEIVER
        }
    }

    private var onAcceptClickListener: ((Meeting) -> Unit)? = null

    fun setOnAcceptClickListener(listener: (Meeting) -> Unit) {
        onAcceptClickListener = listener
    }

    private var onRejectClickListener: ((Meeting) -> Unit)? = null

    fun setOnRejectClickListener(listener: (Meeting) -> Unit) {
        onRejectClickListener = listener
    }

    private var onRescheduleClickListener: ((Meeting) -> Unit)? = null

    fun setOnRescheduleClickListener(listener: (Meeting) -> Unit) {
        onRescheduleClickListener = listener
    }

    private fun formattedDate(t: String): String {
        var time: String? = null
        try {
            val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
            val date = simpleDateFormat.parse(t)
            val simpleDateFormat2 = SimpleDateFormat("d MMM", Locale.ENGLISH)
//            time = prettyTime.format(date)
            time = simpleDateFormat2.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return time!!
    }

    private fun formattedTime(t: String): String {
        var time: String? = null
        try {
            val simpleDateFormat = SimpleDateFormat("HH:mm", Locale.ENGLISH)
            val date = simpleDateFormat.parse(t)
            val simpleDateFormat2 = SimpleDateFormat("hh:mm a", Locale.ENGLISH)
//            time = prettyTime.format(date)
            time = simpleDateFormat2.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return time!!
    }

    private var onItemClickListener: ((Meeting) -> Unit)? = null

    fun setOnItemClickListener(listener: (Meeting) -> Unit) {
        onItemClickListener = listener
    }

    fun removeMeeting(position: Int) {
        snapshot().items.toMutableList().removeAt(position)
    }
}