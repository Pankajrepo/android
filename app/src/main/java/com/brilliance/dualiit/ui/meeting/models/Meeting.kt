package com.brilliance.dualiit.ui.meeting.models

import androidx.annotation.Keep

@Keep
data class Meeting(
    val _id: String? = "",
    val createdAt: String? = "",
    val date: String? = "",
    val from: From? = null,
    val status: Int? = 0,
    val time: String? = "",
    val to: To? = null,
    val updatedAt: String? = ""
)