package com.brilliance.dualiit.ui.chat

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ActivityChatBinding
import com.brilliance.dualiit.model.UserModel
import com.brilliance.dualiit.ui.home.activities.UserProfileActivity
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.extensions.hide
import com.brilliance.dualiit.utils.extensions.show
import com.brilliance.dualiit.utils.extensions.showToast
import com.bumptech.glide.Glide
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import io.socket.client.Socket
import kotlinx.coroutines.flow.collect
import okhttp3.*
import org.json.JSONException
import org.json.JSONObject
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.ocpsoft.prettytime.PrettyTime
import timber.log.Timber
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import com.brilliance.dualiit.ui.chat.models.Message as NewMessage

@AndroidEntryPoint
class ChatActivity : AppCompatActivity() {
    private lateinit var binding: ActivityChatBinding
    private val viewModel: HomeViewModel by viewModel()
    private lateinit var userMessagesAdapter: UserMessagesAdapter
    private var chatsList: MutableList<NewMessage> =
        mutableListOf()
    private var senderId: String? = ""
    val gson: Gson = Gson()
    private var isLoading: Boolean = true
    private var isFirstTime: Boolean? = null
    private var key: String? = ""

    val prefs: AppPreferencesHelper by inject()

    @Inject
    lateinit var socket: Socket

    companion object {
        private const val TAG = "ChatActivity"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChatBinding.inflate(layoutInflater)
        setContentView(binding.root)

        senderId = intent?.getStringExtra("SENDER_ID")
        key = intent?.getStringExtra("KEY")
        isFirstTime = intent?.getBooleanExtra("ISNEW", false)

        Log.d(TAG, "KEY: $key")
        Log.d(TAG, "SenderID: $senderId")

        viewModel.getUserMessages(key!!)

        viewModel.getOtherUserProfile(senderId!!)

        userMessagesAdapter = UserMessagesAdapter(senderId!!)
        setupRecyclerView()


        viewModel.otherUserProfile.observe(this) {
            loadUserData(it)
        }

        viewModel.userChatsList.observe(this) {
            if (it.isEmpty()) {
//                showToast("No data available")
                binding.progressBar.isVisible = false
            } else {
                binding.progressBar.isVisible = false
                binding.paginationProgress.isVisible = false
                binding.rvChats.isVisible = true
                chatsList = it.toMutableList()
                userMessagesAdapter.submitList(chatsList)
                isLoading = false
                if (userMessagesAdapter.itemCount < 10) {
                    if (!isLoading) {
                        Log.d(TAG, "onScroll: Scrolling")
                        if (viewModel.currentPage.value != 0) {
                            viewModel.currentPage.value = viewModel.currentPage.value?.minus(1)
                            viewModel.getPaginatedUserMessages(key.toString())
                            isLoading = false
                        } else {
                            Log.d(TAG, "Already the last page")
                        }
                    }
                }
            }
        }

        viewModel.chatsList.observe(this) {
            chatsList.addAll(0, it)
            userMessagesAdapter.notifyItemRangeInserted(0, it.size)
            binding.paginationProgress.isVisible = false
        }

        lifecycleScope.launchWhenStarted {
            viewModel.deleteMessageEvent.collect { event ->
                when (event) {
                    is HomeViewModel.DeleteConversationEvent.Loading -> {
                        binding.progressBar.show()
                    }
                    is HomeViewModel.DeleteConversationEvent.Success -> {
                        binding.progressBar.hide()
                        showToast("Conversation deleted")
                        chatsList.clear()
                        finish()
                    }
                    is HomeViewModel.DeleteConversationEvent.Error -> {
                        binding.progressBar.hide()
                        showToast(event.message)
                    }
                }
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.deleteChatEvent.collect { event ->
                when (event) {
                    is HomeViewModel.DeleteChatEvent.Loading -> {
                        binding.progressBar.show()
                    }
                    is HomeViewModel.DeleteChatEvent.Success -> {
                        binding.progressBar.hide()
                        showToast("Chat History Cleared")
                        chatsList.clear()
//                        userMessagesAdapter.notifyDataSetChanged()
                    }
                    is HomeViewModel.DeleteChatEvent.Error -> {
                        binding.progressBar.hide()
                        showToast(event.message)
                    }
                }
            }
        }

        binding.chatLayout.setOnClickListener {
            val userDetailsIntent = Intent(this, UserProfileActivity::class.java)
            userDetailsIntent.putExtra("USER_ID", senderId)
            startActivity(userDetailsIntent)
        }

        binding.rvChats.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(-1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (!isLoading) {
                        Log.d(TAG, "onScroll: Scrolling")
                        if (viewModel.currentPage.value != 0) {
                            binding.paginationProgress.isVisible = true
                            viewModel.currentPage.value = viewModel.currentPage.value?.minus(1)
                            viewModel.getPaginatedUserMessages(key.toString())
                            isLoading = false
                        } else {
                            Log.d(TAG, "Already the last page")
                            binding.paginationProgress.isVisible = false
                        }

                    }
                }
            }
        })

        binding.ivMoreOptions.setOnClickListener {
            showPopupMenu(it)
        }

        binding.ivNavigationBack.setOnClickListener {
            finish()
        }

        binding.btnSendMsg.setOnClickListener {
            val messageBody = binding.etMsg.text.toString().trim()
            if (messageBody.isNotEmpty()) {
                sendNewMessage(messageBody)
            } else {
                showToast("Type Message to send")
            }
        }

    }


    private fun sendNewMessage(messageBody: String) {
        val messageObject = JSONObject()
        try {
            messageObject.put("type", "text")
            messageObject.put("dataOfType", "Mixed")
            messageObject.put("body", messageBody)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val jsonObject = JSONObject()
        try {
            jsonObject.put("from", prefs.userModel?.id)
            jsonObject.put("to", senderId)
            jsonObject.put("message", messageObject)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Timber.d("Sending: $jsonObject")

        if (socket.connected()) {
            socket.emit("touser", jsonObject)
            binding.etMsg.setText("")
        } else {
            showToast("Not connected")
        }
    }


    @SuppressLint("SetTextI18n")
    private fun initiateSocketConnection() {
        socket.on(Socket.EVENT_CONNECT) {
            Timber.d("CHAT:: Success: Connection successful")
//            sendOnlineEvent()
        }
        socket.on(Socket.EVENT_CONNECT_ERROR) {
            Timber.d("CONNECTION ERROR: ${it[0]}")
        }

        socket.on(Socket.EVENT_DISCONNECT) {
            Timber.d("DISCONNECT: ${it[0]}")
        }

        socket.on("error") {
            Timber.d("Error: ${it[0]}")
        }

        socket.on("connect_timeout") {
            Timber.d("Connect timeout: ${it[0]}")
        }

        socket.on("reconnecting") {
            Timber.d("Reconnecting: ${it[0]}")
        }

        socket.on("reconnect_error") {
            Timber.d("Reconnect error: ${it[0]}")
        }

        socket.on("reconnect") {
            Timber.d("Reconnect: ${it[0]}")
        }

        socket.on("touser") { args ->
            Timber.d("onMessageReceive: ${args[0]}")
            val message = gson.fromJson(
                args[0].toString(),
                NewMessage::class.java
            )
            message._id = System.currentTimeMillis().toString()
            message.read = 0
            message.deleted = 0
            Timber.d("Message Response: $message")
            addItemToRecyclerView(message)
        }

        socket.on("online-user") { args ->
            Timber.d("Online Event: ${args[0]}")
            runOnUiThread {
                binding.tvLastSeen.text = "Online"
            }
        }

        socket.on("offline-user") { args ->
            Timber.d("Offline Event: ${args[0]}")
            runOnUiThread {
                binding.tvLastSeen.text = "Offline"
            }
        }

        if (socket.connected()) {
            Timber.d("initiateSocketConnection: Socket already connected")
        }

    }

    @SuppressLint("SetTextI18n")
    private fun loadUserData(userModel: UserModel) {
        Timber.d("loadUserData: $userModel")
        binding.apply {
            if (userModel.avatar == "") {
                ivUserProfile.setBackgroundResource(R.drawable.ic_user_avatar)
            } else {
                Glide.with(this@ChatActivity)
                    .load(userModel.avatar)
                    .into(ivUserProfile)
            }
            tvUserName.text = userModel.fullName
            if (userModel.onlineStatus == true) {
                tvLastSeen.text = "Online"
            } else {
                tvLastSeen.text = "Offline"
            }
        }
    }

    private fun setupRecyclerView() {
        binding.rvChats.apply {
            val linearLayoutManager = LinearLayoutManager(this@ChatActivity)
            linearLayoutManager.stackFromEnd = true
            layoutManager = linearLayoutManager
            adapter = userMessagesAdapter
        }
    }

    private fun addItemToRecyclerView(message: NewMessage) {
        runOnUiThread {
            if (chatsList.size >= 1) {
                chatsList.add(message)
                userMessagesAdapter.notifyItemInserted(chatsList.size)
                binding.rvChats.scrollToPosition(userMessagesAdapter.currentList.size - 1)
            } else {
                chatsList.add(message)
                userMessagesAdapter.submitList(chatsList)
                viewModel.getUserMessages(key.toString())
                binding.rvChats.adapter = userMessagesAdapter
            }

        }
    }

    private fun sendOnlineEvent() {
        val jsonObject = JSONObject()
        try {
            jsonObject.put("_id", prefs.userModel?.id)
            jsonObject.put("fullName", prefs.userModel?.fullName)
            jsonObject.put("avatar", prefs.userModel?.avatar)
            jsonObject.put("phone", prefs.userModel?.phone)
            jsonObject.put("email", prefs.userModel?.email)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Timber.d("Sending Online Event: $jsonObject")

        socket.emit("active", jsonObject)

    }

    private fun sendOfflineEvent() {
        val jsonObject = JSONObject()
        try {
            jsonObject.put("_id", prefs.userModel?.id)
            jsonObject.put("fullName", prefs.userModel?.fullName)
            jsonObject.put("avatar", prefs.userModel?.avatar)
            jsonObject.put("phone", prefs.userModel?.phone)
            jsonObject.put("email", prefs.userModel?.email)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Timber.d("Sending Offline Event: $jsonObject")

        socket.emit("idle", jsonObject)

    }

    private fun formattedDate(t: String): String {
        val prettyTime = PrettyTime(Locale(getCountry()))
        var time: String? = null
        try {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:", Locale.ENGLISH)
            val date = simpleDateFormat.parse(t)
            val simpleDateFormat2 = SimpleDateFormat("HH:mm aa", Locale.ENGLISH)
            time = prettyTime.format(date)
//            time = simpleDateFormat2.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return time!!
    }

    private fun getCountry(): String {
        val locale = Locale.getDefault()
        val country = locale.country
        return country.lowercase(Locale.getDefault())
    }

    private fun showPopupMenu(view: View) {
        val popupMenu = PopupMenu(this, view)
        popupMenu.inflate(R.menu.popup_chats)
        popupMenu.setOnMenuItemClickListener { item ->
            when (item?.itemId) {
                R.id.action_block -> {
                    senderId?.let { viewModel.blockOrUnblockFriend(4, it) }
                    showToast("Blocked")
                    finish()
                }
                R.id.action_clear_chat -> {
                    viewModel.deleteChat(key.toString())
                }
                R.id.action_delete -> {
                    viewModel.deleteConversationHistory(key.toString())
                }
            }
            false
        }
        popupMenu.show()
    }

    override fun onResume() {
        super.onResume()
        initiateSocketConnection()
    }
}