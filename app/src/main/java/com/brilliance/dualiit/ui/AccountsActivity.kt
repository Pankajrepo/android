package com.brilliance.dualiit.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ActivityAccountsBinding
import com.brilliance.dualiit.ui.auth.activities.ChooseLoginSignup
import com.brilliance.dualiit.ui.auth.helpers.DeleteAccountEvent
import com.brilliance.dualiit.ui.auth.viewmodel.AuthViewModel
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.extensions.hide
import com.brilliance.dualiit.utils.extensions.show
import com.brilliance.dualiit.utils.extensions.showToast
import kotlinx.coroutines.flow.collect
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class AccountsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAccountsBinding
    private val viewModel: AuthViewModel by viewModel()
    val prefs: AppPreferencesHelper by inject()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccountsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupObservers()

        binding.btnDeleteAccount.setOnClickListener {
            showConfirmationDialog()
        }

        binding.accountsToolbar.toolbar.setNavigationOnClickListener { onBackPressed() }

    }

    private fun setupObservers() {
        lifecycleScope.launchWhenStarted {
            viewModel.deleteAccountEvent.collect { event ->
                when (event) {
                    is DeleteAccountEvent.Failed -> {
                        binding.progressBar.hide()
                        showToast(event.message)
                    }
                    is DeleteAccountEvent.Loading -> {
                        binding.progressBar.show()
                    }
                    is DeleteAccountEvent.Success -> {
                        binding.progressBar.hide()
                        showToast("Account deleted, logging out...")
                        prefs.logout()
                        startActivity(
                            Intent(
                                this@AccountsActivity,
                                ChooseLoginSignup::class.java
                            ).setFlags(
                                Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                            )
                        )
                        finish()
                    }
                }
            }
        }
    }

    private fun showConfirmationDialog() {
        val builder = AlertDialog.Builder(this)
        with(builder)
        {
            setTitle("Confirm to delete your account")
            setMessage("You can not recover your account once you permanently delete")
            setPositiveButton("Delete") { dialog, _ ->
                viewModel.deleteMyAccount()
                dialog.dismiss()
            }
            setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }
            show()
        }.getButton(AlertDialog.BUTTON_POSITIVE)
            .setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
    }

}