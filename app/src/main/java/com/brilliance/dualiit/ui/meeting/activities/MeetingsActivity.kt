package com.brilliance.dualiit.ui.meeting.activities

import android.content.Intent
import android.graphics.Canvas
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ActivityMeetingsBinding
import com.brilliance.dualiit.ui.home.adapters.InterestsLoadStateAdapter
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.brilliance.dualiit.ui.meeting.MeetingsAdapter
import com.brilliance.dualiit.ui.meeting.dialogs.CallingDialogFragment
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.extensions.showToast
import dagger.hilt.android.AndroidEntryPoint
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator
import kotlinx.coroutines.flow.collect
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class MeetingsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMeetingsBinding
    private val viewModel: HomeViewModel by viewModel()
    private lateinit var meetingsAdapter: MeetingsAdapter
    val prefs: AppPreferencesHelper by inject()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMeetingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupRecyclerView()

        viewModel.meetingsList.observe(this) {
            meetingsAdapter.submitData(lifecycle, it)
        }

        lifecycleScope.launchWhenStarted {
            viewModel.deleteMeetingEvent.collect { event ->
                when (event) {
                    is HomeViewModel.DeleteMeetingEvent.Error -> {
                        showToast(event.message)
                    }
                    is HomeViewModel.DeleteMeetingEvent.Success -> {
                        showToast("Meeting Deleted Successfully")
                    }
                }
            }
        }

        meetingsAdapter.setOnItemClickListener { meeting ->
            try {

                val meetingDateTime = "${meeting.date} ${meeting.time}"
                val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault())
                val meetingDate = simpleDateFormat.parse(meetingDateTime)

                val newMeetingTime = Date(meetingDate?.time!!.plus(1800000))

                val currentDate = Date()

                val userName = if (meeting.from?._id == prefs.userModel?.id.toString()) {
                    meeting.to?.fullName
                } else {
                    meeting.from?.fullName
                }
                val userPhoto = if (meeting.from?._id == prefs.userModel?.id.toString()) {
                    meeting.to?.avatar
                } else {
                    meeting.from?.avatar
                }
                val userId = if (meeting.from?._id == prefs.userModel?.id.toString()) {
                    meeting.to?._id
                } else {
                    meeting.from?._id
                }

                if (currentDate > meetingDate && currentDate < newMeetingTime) {

                    val callingDialogFragment = CallingDialogFragment()
                    val bundle = Bundle().apply {
                        putString("MEETING_ID", meeting._id)
                        putString("USER_NAME", userName)
                        putString("USER_PHOTO", userPhoto)
                        putString("USER_ID", userId)
                        putBoolean("FROM_ME", true)
                    }
                    callingDialogFragment.arguments = bundle
                    callingDialogFragment.show(supportFragmentManager, "CallingDialog")
                } else {
                    showToast("Can not connect now")
                    Log.d("MeetingActivity", "Status: ${meeting._id}")
//                    val callingDialogFragment = CallingDialogFragment()
//                    val bundle = Bundle().apply {
//                        putString("MEETING_ID", meeting._id)
//                        putString("USER_NAME", userName)
//                        putString("USER_PHOTO", userPhoto)
//                        putString("USER_ID", userId)
//                        putBoolean("FROM_ME", true)
//                    }
//                    callingDialogFragment.arguments = bundle
//                    callingDialogFragment.show(supportFragmentManager, "CallingDialog")
                }

            } catch (e: ParseException) {
                e.printStackTrace()
            }
        }

        meetingsAdapter.setOnAcceptClickListener { meeting ->
            viewModel.acceptMeeting(meeting._id.toString(), 2)

        }

        meetingsAdapter.setOnRejectClickListener { meeting ->
            viewModel.rejectMeeting(meeting._id.toString(), 3)
        }

        meetingsAdapter.setOnRescheduleClickListener { meeting ->
            val userName = if (meeting.from?._id == prefs.userModel?.id.toString()) {
                meeting.to?.fullName
            } else {
                meeting.from?.fullName
            }
            val userPhoto = if (meeting.from?._id == prefs.userModel?.id.toString()) {
                meeting.to?.avatar
            } else {
                meeting.from?.avatar
            }
            val updateMeetingIntent = Intent(this, CreateMeetingActivity::class.java).apply {
                putExtra("MEETING_ID", meeting._id)
                putExtra("MEETING_TIME", meeting.time)
                putExtra("MEETING_DATE", meeting.date)
                putExtra("USER_NAME", userName)
                putExtra("USER_PHOTO", userPhoto)
            }
            startActivity(updateMeetingIntent)
        }

        lifecycleScope.launchWhenStarted {
            viewModel.invitationEvent.collect { event ->
                when (event) {
                    is HomeViewModel.InvitationEvent.InvitationAccepted -> {
                        showToast("Meeting Accepted")
                        meetingsAdapter.refresh()
                    }

                    is HomeViewModel.InvitationEvent.InvitationRejected -> {
                        showToast("Meeting Rejected")
                    }
                }
            }
        }

        meetingsAdapter.addLoadStateListener { loadState ->
            binding.apply {
                progressBar.isVisible = loadState.source.refresh is LoadState.Loading
                rvMeetings.isVisible = loadState.source.refresh is LoadState.NotLoading
                buttonRetry.isVisible = loadState.source.refresh is LoadState.Error
                textViewError.isVisible = loadState.source.refresh is LoadState.Error

                if (loadState.source.refresh is LoadState.NotLoading &&
                    loadState.append.endOfPaginationReached &&
                    meetingsAdapter.itemCount < 1
                ) {
                    rvMeetings.isVisible = false
                    tvEmpty.isVisible = true
                } else {
                    tvEmpty.isVisible = false
                }
            }
        }

        binding.buttonRetry.setOnClickListener {
            meetingsAdapter.retry()
        }

        binding.meetingsToolbar.toolbar.setNavigationOnClickListener { onBackPressed() }

    }

    private fun setupRecyclerView() {
        meetingsAdapter = MeetingsAdapter(prefs.userModel?.id.toString())
        binding.rvMeetings.apply {
            layoutManager = LinearLayoutManager(this@MeetingsActivity)
            itemAnimator = null
            val attrs = intArrayOf(android.R.attr.listDivider)
            val a = context.obtainStyledAttributes(attrs)
            val divider = a.getDrawable(0)
            val inset = resources.getDimensionPixelSize(R.dimen._16sdp)
            val insetDivider = InsetDrawable(divider, inset, 0, inset, 0)
            a.recycle()
            val itemDecoration =
                DividerItemDecoration(this@MeetingsActivity, DividerItemDecoration.VERTICAL)
            itemDecoration.setDrawable(insetDivider)
            addItemDecoration(itemDecoration)

            adapter = meetingsAdapter.withLoadStateFooter(
                footer = InterestsLoadStateAdapter {
                    meetingsAdapter.retry()
                }
            )
            val itemTouchHelper = ItemTouchHelper(simpleCallBack)
            itemTouchHelper.attachToRecyclerView(binding.rvMeetings)
        }
    }

    private val simpleCallBack = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

            when (direction) {
                ItemTouchHelper.LEFT -> {
                    val position = viewHolder.bindingAdapterPosition
                    val meeting = meetingsAdapter.snapshot().items[position]
                    viewModel.deleteMeeting(meeting._id.toString())
                    meetingsAdapter.removeMeeting(position)
                    meetingsAdapter.notifyItemRemoved(position)
                }
            }
        }

        override fun onChildDraw(
            c: Canvas,
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            dX: Float,
            dY: Float,
            actionState: Int,
            isCurrentlyActive: Boolean
        ) {
            RecyclerViewSwipeDecorator.Builder(
                c,
                recyclerView,
                viewHolder,
                dX,
                dY,
                actionState,
                isCurrentlyActive
            )
                .addSwipeLeftBackgroundColor(
                    ContextCompat.getColor(
                        this@MeetingsActivity,
                        R.color.colorRed
                    )
                )
                .addSwipeLeftActionIcon(R.drawable.ic_delete)
                .create()
                .decorate()

            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        }


    }

}