package com.brilliance.dualiit.ui.auth.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.brilliance.dualiit.databinding.ActivityValidationOtpBinding
import com.brilliance.dualiit.ui.auth.request.ResetRequest
import com.brilliance.dualiit.ui.auth.request.SignupRequest
import com.brilliance.dualiit.ui.auth.viewmodel.AuthViewModel
import com.brilliance.dualiit.ui.base.activity.BaseActivity
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.utils.AppConstant
import com.brilliance.dualiit.utils.AppConstant.TERMS_OF_USE_URL
import com.brilliance.dualiit.utils.Resource
import com.brilliance.dualiit.utils.extensions.hide
import com.brilliance.dualiit.utils.extensions.launchActivity
import com.brilliance.dualiit.utils.extensions.show
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class ValidateOTP : BaseActivity() {
    private lateinit var binding: ActivityValidationOtpBinding
    private val viewModel: AuthViewModel by viewModel()
    override fun baseViewModel(): BaseViewModel = viewModel
    private var signRequest: SignupRequest? = null
    private var resetRequest: ResetRequest? = null
    private var emailToVerify: String? = null
    private var isEmailVerified: Boolean = false

    override fun setObserver() {
        viewModel.isEmailValid.observe(this, {
            if (it == true) {
                Timber.d("EMAIL VERIFIED SUCCESSFULLY")
                isEmailVerified = true
                finish()
            } else {
                Timber.d("EMAIL NOT VERIFIED")
                isEmailVerified = false
            }
        })

        lifecycleScope.launchWhenStarted {
            viewModel.signUpState.collect {
                when (it) {
                    is Resource.Empty -> {
                    }
                    is Resource.Error -> {
                        binding.progressBar.hide()
                        showToast("OTP validation failed, Please enter a valid OTP")
                    }
                    is Resource.Loading -> {
                        binding.progressBar.show()
                    }
                    is Resource.Success -> {
                        binding.progressBar.hide()
                        showToast("Account created successfully")
                        launchActivity(
                            aClass = CompleteProfile::class.java,
                            isTopFinish = true,
                            flagIsTopClearTask = true
                        )
                    }
                }
            }

        }

        super.setBaseObserver()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBar(this)
        binding = ActivityValidationOtpBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setObserver()

        intent.let {
            if (intent.hasExtra(AppConstant.REQ_RESET)) {
                viewModel.requestReset.value = true
                resetRequest =
                    intent.getParcelableExtra<ResetRequest>(AppConstant.REQ_EXTRA) as ResetRequest

            } else if (intent.hasExtra(AppConstant.REQ_VERIFY_EMAIL)) {
                viewModel.requestEmailVerification.value = true
                emailToVerify = intent.getStringExtra("EMAIL")
            } else {
                viewModel.requestReset.value = false
                signRequest =
                    intent.getParcelableExtra<SignupRequest>(AppConstant.REQ_EXTRA) as SignupRequest
            }
        }

        binding.pinview.isFocusableInTouchMode = true
        binding.pinview.requestFocus()

        binding.btnSendCode.setOnClickListener {

            if (binding.pinview.text.toString().isEmpty()) {
                showToast("Please enter otp")
            } else {

                if (emailToVerify == null) {
                    if (viewModel.requestReset.value!!) {
                        resetRequest?.code = binding.pinview.text.toString()
                        startActivity(
                            Intent(this, NewPasswordActivity::class.java).putExtra(
                                AppConstant.REQ_RESET,
                                true
                            ).putExtra(AppConstant.REQ_EXTRA, resetRequest)
                        )
                    } else {
                        signRequest?.code = binding.pinview.text.toString()
                        viewModel.signUp(signRequest!!)
                    }
                } else {
                    if (viewModel.requestEmailVerification.value!!) {
                        viewModel.verifyEmail(binding.pinview.text.toString(), emailToVerify!!)
                    }
                }

            }
        }

        binding.pinview.setOtpCompletionListener { otp ->
            if (emailToVerify == null) {
                if (viewModel.requestReset.value!!) {
                    resetRequest?.code = otp
                    startActivity(
                        Intent(this, NewPasswordActivity::class.java).putExtra(
                            AppConstant.REQ_RESET,
                            true
                        ).putExtra(AppConstant.REQ_EXTRA, resetRequest)
                    )
                } else {
                    signRequest?.code = otp
                    viewModel.signUp(signRequest!!)
                }
            } else {
                if (viewModel.requestEmailVerification.value!!) {
                    viewModel.verifyEmail(otp, emailToVerify!!)
                }
            }
        }

        binding.termOfUse.setOnClickListener {
            openInBrowser()
        }

        binding.tvResendCode.setOnClickListener {
            showToast("OTP sent")
        }

        binding.validationToolbar.toolbar.setNavigationOnClickListener { onBackPressed() }

    }

    private fun openInBrowser() {
        val openURL = Intent(Intent.ACTION_VIEW)
        openURL.data = Uri.parse(TERMS_OF_USE_URL)
        startActivity(openURL)
    }

    override fun onBackPressed() {
        viewModel.receiveEmailOTP.postValue(false)
        super.onBackPressed()
    }
}