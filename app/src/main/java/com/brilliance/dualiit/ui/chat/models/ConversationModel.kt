package com.brilliance.dualiit.ui.chat.models

import androidx.annotation.Keep

@Keep
data class ConversationModel(
    val _id: String? = "",
    val count: Int? = 0,
    val createdAt: String? = "",
    val key: String? = "",
    val messages: List<Message>? = null,
    val page: Int? = 0,
    val roomKey: String? = "",
    val type: String? = "",
    val updatedAt: String? = "",
    val users: List<String>? = null
)