package com.brilliance.dualiit.ui.meeting.models.callevents

data class Location(
    val coordinates: List<Double>? = null,
    val type: String? = ""
)