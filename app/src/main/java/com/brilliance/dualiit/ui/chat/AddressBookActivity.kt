package com.brilliance.dualiit.ui.chat

import android.content.Intent
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.FragmentAddressBookBinding
import com.brilliance.dualiit.ui.home.adapters.InterestsLoadStateAdapter
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class AddressBookActivity : AppCompatActivity() {
    private val viewModel: HomeViewModel by viewModel()
    private lateinit var binding: FragmentAddressBookBinding
    private lateinit var addressBookAdapter: AddressBookAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = FragmentAddressBookBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupRecyclerView()

        viewModel.addressBookList.observe(this) {
            addressBookAdapter.submitData(lifecycle, it)
        }

        addressBookAdapter.setOnItemClickListener {
            Timber.d("${it.friendId?._id}_${it.userId?._id}")
            Timber.d("User ID: ${it.friendId?._id}")
            val intent = Intent(this, ChatActivity::class.java)
            intent.putExtra("SENDER_ID", it.friendId?._id)
            intent.putExtra("KEY", "${it.friendId?._id}_${it.userId?._id}")
            intent.putExtra("ISNEW", true)
            startActivity(intent)
        }

        addressBookAdapter.addLoadStateListener { loadState ->
            binding.apply {
                progressBar.isVisible = loadState.source.refresh is LoadState.Loading
                rvFriendsList.isVisible = loadState.source.refresh is LoadState.NotLoading
                buttonRetry.isVisible = loadState.source.refresh is LoadState.Error
                textViewError.isVisible = loadState.source.refresh is LoadState.Error

                if (loadState.source.refresh is LoadState.NotLoading &&
                    loadState.append.endOfPaginationReached &&
                    addressBookAdapter.itemCount < 1
                ) {
                    rvFriendsList.isVisible = false
                    tvEmpty.isVisible = true
                } else {
                    tvEmpty.isVisible = false
                }
            }
        }
        binding.buttonRetry.setOnClickListener {
            addressBookAdapter.retry()
        }

        binding.etSearchAddressBookFriends.addTextChangedListener { querySting ->
            if (querySting != null) {
                binding.rvFriendsList.scrollToPosition(0)
                viewModel.searchFriendsList(querySting.toString())
            }
        }

        binding.addressToolbar.toolbar.setNavigationOnClickListener { onBackPressed() }
    }


    private fun setupRecyclerView() {
        addressBookAdapter = AddressBookAdapter()
        binding.rvFriendsList.apply {
            layoutManager = LinearLayoutManager(this@AddressBookActivity)
            itemAnimator = null
            val attrs = intArrayOf(android.R.attr.listDivider)
            val a = context.obtainStyledAttributes(attrs)
            val divider = a.getDrawable(0)
            val inset = resources.getDimensionPixelSize(R.dimen._16sdp)
            val insetDivider = InsetDrawable(divider, inset, 0, inset, 0)
            a.recycle()
            val itemDecoration =
                DividerItemDecoration(this@AddressBookActivity, DividerItemDecoration.VERTICAL)
            itemDecoration.setDrawable(insetDivider)
            addItemDecoration(itemDecoration)

            adapter = addressBookAdapter.withLoadStateFooter(
                footer = InterestsLoadStateAdapter {
                    addressBookAdapter.retry()
                }
            )
            setHasFixedSize(true)
        }
    }

    override fun onResume() {
        super.onResume()
        addressBookAdapter.refresh()
    }

}