package com.brilliance.dualiit.ui.speeddating.models.userdetails

import androidx.annotation.Keep

@Keep
data class UserDetailsResponse(
    val from: From? = null,
    val sessionId: String? = "",
    val speedDating: Int? = 0,
    val status: String? = "",
    val time: String? = "",
    val to: To? = null,
    val type: String? = ""
)