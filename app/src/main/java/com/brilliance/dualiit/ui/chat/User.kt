package com.brilliance.dualiit.ui.chat

import androidx.annotation.Keep

@Keep
data class User(
    val _id: String? = "",
    val avatar: String? = "",
    val fullName: String? = "",
    val lastOnlineTime: String? = "",
    val onlineStatus: Boolean? = false
)