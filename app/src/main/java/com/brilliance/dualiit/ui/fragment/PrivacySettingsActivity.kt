package com.brilliance.dualiit.ui.fragment

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.brilliance.dualiit.databinding.ActivityPrivacySettingsBinding
import com.brilliance.dualiit.model.Privacy
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.brilliance.dualiit.utils.AppPreferencesHelper
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class PrivacySettingsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPrivacySettingsBinding
    private val viewModel: HomeViewModel by viewModel()
    val prefs: AppPreferencesHelper by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPrivacySettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setNotificationValues()

        binding.switchHidePhoneNumber.setOnCheckedChangeListener { _, isChecked ->
            if (prefs.userModel?.privacy != null) {
                val privacy = prefs.userModel?.privacy?.copy(
                    phone = if (isChecked) 1 else 0
                )
                Timber.d("Privacy not null: $privacy")
                privacy?.let {
                    changeNotificationToggle(privacy = it)
                }
            } else {
                val privacy = Privacy(phone = if (isChecked) 1 else 0)
                Timber.d("Privacy null: $privacy")
                changeNotificationToggle(privacy = privacy)
            }
        }

        binding.switchHideBio.setOnCheckedChangeListener { _, isChecked ->
            if (prefs.userModel?.privacy != null) {
                val privacy = prefs.userModel?.privacy?.copy(
                    bio = if (isChecked) 1 else 0
                )
                Timber.d("Privacy not null: $privacy")
                privacy?.let {
                    changeNotificationToggle(privacy = it)
                }
            } else {
                val privacy = Privacy(bio = if (isChecked) 1 else 0)
                Timber.d("Privacy null: $privacy")
                changeNotificationToggle(privacy = privacy)
            }
        }

        binding.switchHideWeekendPlan.setOnCheckedChangeListener { _, isChecked ->

            if (prefs.userModel?.privacy != null) {
                val privacy = prefs.userModel?.privacy?.copy(
                    weekendplan = if (isChecked) 1 else 0
                )
                Timber.d("Privacy not null: $privacy")
                privacy?.let {
                    changeNotificationToggle(privacy = it)
                }
            } else {
                val privacy = Privacy(weekendplan = if (isChecked) 1 else 0)
                Timber.d("Privacy null: $privacy")
                changeNotificationToggle(privacy = privacy)
            }

        }

        binding.privacyToolbar.toolbar.setNavigationOnClickListener { onBackPressed() }

    }


    private fun changeNotificationToggle(privacy: Privacy) {
        val userModel = prefs.userModel?.copy(
            privacy = privacy,
            accessToken = null,
            refreshToken = null
        )
        userModel?.let {
            viewModel.updateNotificationStatus(it)
        }
    }

    private fun setNotificationValues() {
        binding.switchHidePhoneNumber.isChecked = prefs.userModel?.privacy?.phone == 1
        binding.switchHideBio.isChecked = prefs.userModel?.privacy?.bio == 1
        binding.switchHideWeekendPlan.isChecked =
            prefs.userModel?.privacy?.weekendplan == 1
    }
}