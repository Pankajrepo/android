package com.brilliance.dualiit.ui.invitation.models

import androidx.annotation.Keep

@Keep
data class InvitationModel(
    val _id: String? = "",
    val createdAt: String? = "",
    val friendId: FriendId? = null,
    val status: Int? = 0,
    val type: String? = "",
    val updatedAt: String? = ""
)