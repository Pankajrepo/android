package com.brilliance.dualiit.ui.base.activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.util.Log
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import com.brilliance.dualiit.interfaces.AddressCallback

abstract class BaseLocationActivity : BaseActivity(),
    AddressCallback {
    private var mLastUpdateTime: String? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mSettingsClient: SettingsClient? = null
    private var mLocationRequest: LocationRequest? = null
    private var mLocationCallback: LocationCallback? = null
    private var mCurrentLocation: Location? = null
    private var mRequestingLocationUpdates = false
    private val REQUEST_CHECK_SETTINGS = 100
    private val mTAG = "BaseLocationActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        restoreValuesFromBundle(savedInstanceState)
        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                locationResult.locations.forEach { location ->
                    // Update UI with location data
                    mCurrentLocation = location
                    updateLocationUI()
                }
            }
        }
    }

    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("is_requesting_updates", mRequestingLocationUpdates!!)
        outState.putParcelable("last_known_location", mCurrentLocation)
        outState.putString("last_updated_on", mLastUpdateTime)
    }

    private fun restoreValuesFromBundle(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("is_requesting_updates")) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean("is_requesting_updates")
            }

            if (savedInstanceState.containsKey("last_known_location")) {
                mCurrentLocation = savedInstanceState.getParcelable("last_known_location")
            }

            if (savedInstanceState.containsKey("last_updated_on")) {
                mLastUpdateTime = savedInstanceState.getString("last_updated_on")
            }
        }
        updateLocationUI()
    }

    fun showPermissionNeededDialog(permissions: Array<String>) {
        Permissions.check(
            this,
            permissions,
            null,
            null,
            permissionHandler
        )
    }

    private fun initLocation() {
        Log.d(mTAG, "initLocation")

        mLocationRequest = LocationRequest.create().apply {
            interval = 1000
            fastestInterval = 3000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest!!)
        mSettingsClient = LocationServices.getSettingsClient(this)

        val task = mSettingsClient?.checkLocationSettings(builder.build())
        task?.addOnSuccessListener { locationSettingsResponse ->
            startLocationUpdates()
        }

        task?.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    exception.startResolutionForResult(
                        this@BaseLocationActivity,
                        REQUEST_CHECK_SETTINGS
                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
    }

    private fun updateLocationUI() {
        Log.d(mTAG, "updateLocationUI")

        if (mCurrentLocation != null) {
            onGetCountry("", "", mCurrentLocation)
            stopLocationUpdates()
        }
    }

    private fun startLocationUpdates() {
        Log.d(mTAG, "startLocationUpdates")

        mRequestingLocationUpdates = true
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            isPermissionAllowed(false)
            return
        }
        mFusedLocationClient!!.requestLocationUpdates(mLocationRequest!!,
            mLocationCallback!!,
            Looper.getMainLooper())
    }

    private fun stopLocationUpdates() {
        Log.d(mTAG, "stopLocationUpdates")

        mFusedLocationClient
            ?.removeLocationUpdates(mLocationCallback)
            ?.addOnCompleteListener(this) { task ->
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    startLocationUpdates()
                }
                Activity.RESULT_CANCELED -> {
                    mRequestingLocationUpdates = false
                    isPermissionAllowed(false)
                }
            }
        }
    }

    public override fun onResume() {
        super.onResume()
        if(mRequestingLocationUpdates){
            startLocationUpdates()
        }
    }

    override fun onPause() {
        super.onPause()
        if (mRequestingLocationUpdates) {
            stopLocationUpdates()
        }
    }

    open fun isPermissionAllowed(flag: Boolean) {}

    private val permissionHandler = object : PermissionHandler() {
        override fun onGranted() {
            Log.d(mTAG, "permissionHandler onGranted")

            initLocation()
        }

        override fun onDenied(context: Context?, deniedPermissions: java.util.ArrayList<String>?) {
            isPermissionAllowed(false)
        }

        override fun onJustBlocked(
            context: Context?,
            justBlockedList: java.util.ArrayList<String>?,
            deniedPermissions: java.util.ArrayList<String>?
        ) {
            isPermissionAllowed(false)
        }
    }

    override fun onGetCountry(country: String, state: String, location: Location?) {
        prefs.latitude = location?.latitude?.toFloat() ?: 0.0F
        prefs.longitude = location?.longitude?.toFloat() ?: 0.0F
    }
}
