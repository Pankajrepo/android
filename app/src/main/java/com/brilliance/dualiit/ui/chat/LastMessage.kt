package com.brilliance.dualiit.ui.chat

import androidx.annotation.Keep

@Keep
data class LastMessage(
    val _id: String? = "",
    val body: String? = "",
    val dataOfType: String? = "",
    val dateTime: String? = "",
    val deleted: Int? = 0,
    val from: From? = null,
    val read: Int? = 0,
    val to: To? = null,
    val type: String? = ""
)