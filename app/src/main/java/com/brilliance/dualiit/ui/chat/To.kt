package com.brilliance.dualiit.ui.chat

import androidx.annotation.Keep

@Keep
data class To(
    val _id: String? = "",
    val avatar: String? = ""
)