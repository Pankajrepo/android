package com.brilliance.dualiit.ui.speeddating.models

import androidx.annotation.Keep

@Keep
data class SessionResponse(
    val apiKey: String? = "",
    val sessionId: String? = "",
    val token: String? = ""
)