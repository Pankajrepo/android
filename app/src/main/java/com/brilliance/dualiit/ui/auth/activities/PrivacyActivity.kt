package com.brilliance.dualiit.ui.auth.activities

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ActivityPrivacyBinding

class PrivacyActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPrivacyBinding

    private var url: String =
        "https://brilliance.co.in/dualiit-policy-privacy/"

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPrivacyBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.privacyWebview.loadUrl(url)

        binding.privacyWebview.settings.javaScriptEnabled = true

        binding.privacyWebview.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                binding.privacyProgress.visibility = View.VISIBLE
                binding.privacyProgress.progress = newProgress

                if (newProgress == 100) {
                    binding.privacyProgress.visibility = View.GONE
                }

                super.onProgressChanged(view, newProgress)
            }
        }

        binding.ivNavigationBack.setOnClickListener {
            finish()
        }
    }
}