package com.brilliance.dualiit.ui.speeddating.models.userdetails

import androidx.annotation.Keep

@Keep
data class LocationX(
    val coordinates: List<Double>? = null,
    val type: String? = ""
)