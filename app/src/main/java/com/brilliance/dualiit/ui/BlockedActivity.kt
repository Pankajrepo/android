package com.brilliance.dualiit.ui

import android.content.Intent
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ActivityBlockedBinding
import com.brilliance.dualiit.ui.home.activities.UserProfileActivity
import com.brilliance.dualiit.ui.home.adapters.InterestsLoadStateAdapter
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.brilliance.dualiit.utils.extensions.showToast
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class BlockedActivity : AppCompatActivity() {

    private val viewModel: HomeViewModel by viewModel()
    private lateinit var binding: ActivityBlockedBinding
    private lateinit var blockedAdapter: BlockedAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBlockedBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupRecyclerView()

        viewModel.blockedList.observe(this) {
            blockedAdapter.submitData(lifecycle, it)
        }

        blockedAdapter.setOnItemClickListener {
            viewModel.blockOrUnblockFriend(1, it.friendId?._id.toString())
        }

        blockedAdapter.setOnProfileClickListener { userId ->
            val userDetailsIntent = Intent(this, UserProfileActivity::class.java)
            userDetailsIntent.putExtra("USER_ID", userId)
            startActivity(userDetailsIntent)
        }

        lifecycleScope.launchWhenStarted {
            viewModel.invitationEvent.collect { event ->
                when (event) {
                    is HomeViewModel.InvitationEvent.InvitationStatus -> {
                        showToast(event.message)
                        blockedAdapter.refresh()
                    }
                }
            }
        }

        blockedAdapter.addLoadStateListener { loadState ->
            binding.apply {
                progressBar.isVisible = loadState.source.refresh is LoadState.Loading
                rvBlockedUsers.isVisible = loadState.source.refresh is LoadState.NotLoading
                buttonRetry.isVisible = loadState.source.refresh is LoadState.Error
                textViewError.isVisible = loadState.source.refresh is LoadState.Error

                if (loadState.source.refresh is LoadState.NotLoading &&
                    loadState.append.endOfPaginationReached &&
                    blockedAdapter.itemCount < 1
                ) {
                    rvBlockedUsers.isVisible = false
                    tvEmpty.isVisible = true
                } else {
                    tvEmpty.isVisible = false
                }
            }
        }

        binding.etSearchBlockedUsers.addTextChangedListener { querySting ->
            if (querySting != null) {
                binding.rvBlockedUsers.scrollToPosition(0)
                viewModel.searchBlockedList(querySting.toString())
            }
        }

        binding.buttonRetry.setOnClickListener {
            blockedAdapter.retry()
        }

        binding.blockedFriendsToolbar.toolbar.setNavigationOnClickListener { onBackPressed() }

    }

    private fun setupRecyclerView() {
        blockedAdapter = BlockedAdapter()
        binding.rvBlockedUsers.apply {
            layoutManager = LinearLayoutManager(this@BlockedActivity)
            itemAnimator = null
            val attrs = intArrayOf(android.R.attr.listDivider)
            val a = context.obtainStyledAttributes(attrs)
            val divider = a.getDrawable(0)
            val insetLeft = resources.getDimensionPixelSize(R.dimen._40sdp)
            val insetRight = resources.getDimensionPixelSize(R.dimen._16sdp)
            val insetDivider = InsetDrawable(divider, insetLeft, 0, insetRight, 0)
            a.recycle()
            val itemDecoration =
                DividerItemDecoration(
                    this@BlockedActivity,
                    DividerItemDecoration.VERTICAL
                )
            itemDecoration.setDrawable(insetDivider)
            addItemDecoration(itemDecoration)

            adapter = blockedAdapter.withLoadStateFooter(
                footer = InterestsLoadStateAdapter {
                    blockedAdapter.retry()
                }
            )
            setHasFixedSize(true)
        }
    }
}