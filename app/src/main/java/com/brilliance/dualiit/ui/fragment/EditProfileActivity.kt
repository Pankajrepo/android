package com.brilliance.dualiit.ui.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import android.text.InputType
import android.util.Log
import android.util.TypedValue
import android.widget.DatePicker
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ActivityEditProfileBinding
import com.brilliance.dualiit.model.InterestDoc
import com.brilliance.dualiit.services.FetchAddressIntentService
import com.brilliance.dualiit.ui.auth.viewmodel.AuthViewModel
import com.brilliance.dualiit.ui.base.activity.BaseActivity
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.ui.home.activities.MainActivity
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.brilliance.dualiit.utils.AppConstant
import com.brilliance.dualiit.utils.extensions.hide
import com.brilliance.dualiit.utils.extensions.show
import com.bumptech.glide.Glide
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.material.chip.Chip
import com.google.gson.Gson
import kotlinx.coroutines.flow.collect
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File
import java.util.*


class EditProfileActivity : BaseActivity(), DatePickerDialog.OnDateSetListener {

    private lateinit var binding: ActivityEditProfileBinding
    private val viewModel: HomeViewModel by viewModel()
    private val authViewModel: AuthViewModel by viewModel()
    override fun baseViewModel(): BaseViewModel = authViewModel

    private var resultReceiver: ResultReceiver? = null

    private var locationPermissions = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )

    private var addInterestBtn: Chip? = null

    private var uri: Uri? = null
    private var userAvatar: String? = null
    private var currentDay = 0
    private var currentMonth = 0
    private var currentYear = 0
    private var dateOfBirth: String? = ""
    private var ageMin: Int? = null
    private var ageMax: Int? = null
    private var userGender: String? = ""
    private var userPreference: String? = ""
    private var userStatus: String? = ""
    private var userOrientation: String? = ""
    private var interestsList: List<String> = ArrayList()

    private var area: String? = null
    private var country: String? = null
    private var region: String? = null


    companion object {
        private const val TAG = "EditProfileActivity"
        private const val REQUEST_CHECK_SETTING = 168
        private const val LOCATION_REQUEST_CODE = 147
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setObserver()

        resultReceiver = AddressResultReceiver(Handler())

        loadUserDetails()
        binding.etBirthday.inputType = InputType.TYPE_NULL
        binding.etUserPhone.inputType = InputType.TYPE_NULL


        binding.userImg.setOnClickListener {
            dialogImagePicker()
        }

        binding.etBirthday.setOnClickListener {
            pickBirthdayDate()
        }

        binding.tvLocation.setOnClickListener {
            requestLocationSettings()
        }

        binding.etUserPhone.setOnClickListener {
            showToast("Phone Number can not be changed")
        }


        binding.sliderAgePreference.addOnChangeListener { slider, _, _ ->
            val currentValues = slider.values
            val minAge = currentValues[0].toInt()
            val maxAge = currentValues[1].toInt()
            ageMin = minAge
            ageMax = maxAge
            binding.tvMinMaxAge.text = "$minAge-$maxAge"
        }

        binding.chipGroupUserGender.setOnCheckedChangeListener { group, checkedId ->
            val selectedChip = group.findViewById<Chip>(checkedId)
            if (selectedChip != null) {
                userGender = selectedChip.text.toString().lowercase(Locale.ROOT)
            }
        }

        binding.chipGroupStatus.setOnCheckedChangeListener { group, checkedId ->
            val selectedChip = group.findViewById<Chip>(checkedId)
            if (selectedChip != null) {
                userStatus = selectedChip.text.toString().lowercase(Locale.ROOT)
            }
        }

        binding.chipGroupPreference.setOnCheckedChangeListener { group, checkedId ->
            val selectedChip = group.findViewById<Chip>(checkedId)

            if (selectedChip != null) {
                userPreference = if (selectedChip.text.toString() == "Both") {
                    "other"
                } else {
                    selectedChip.text.toString().lowercase(Locale.ROOT)
                }
            }
        }

        binding.chipGroupOrientation.setOnCheckedChangeListener { group, checkedId ->
            val selectedChip = group.findViewById<Chip>(checkedId)
            if (selectedChip != null) {
                userOrientation = selectedChip.text.toString().lowercase(Locale.ROOT)
            }
        }

        binding.btnUpdateProfile.setOnClickListener {

            if (!validateName() || !validateGender() || !validatePreference()
                || !validateStatus() || !validateOrientation()
            ) {
                return@setOnClickListener
            }

            val userMap = HashMap<String, Any>()

            userAvatar?.let {
                userMap["avatar"] = it
            }

            if (binding.etIceBreaker.text.toString().isNotEmpty()) {
                userMap["icebreaker"] = binding.etIceBreaker.text.toString()
            }

            if (binding.etUserName.text.toString().isNotEmpty()) {
                userMap["fullName"] = binding.etUserName.text.toString()
            }

            if (binding.etUserEmail.text.toString().isNotEmpty()) {
                userMap["email"] = binding.etUserEmail.text.toString()
            }

//            if (binding.etWeekendPlan.text.toString().isNotEmpty()) {
//                userMap["weekendplan"] = binding.etWeekendPlan.text.toString()
//            }

            if (binding.etBio.text.toString().isNotEmpty()) {
                userMap["bio"] = binding.etBio.text.toString()
            }

            if (!dateOfBirth.isNullOrEmpty()) {
                userMap["birthDay"] = dateOfBirth!!
            }

            if (interestsList.isNotEmpty()) {
                val interestToJson = Gson().toJsonTree(interestsList)
                userMap["interest"] = interestToJson
            }

            if (userPreference != null) {
                userMap["interested"] = userPreference!!
            }

            if (userGender != null) {
                userMap["gender"] = userGender!!
            }

            if (userStatus != null) {
                userMap["relationshipStatus"] = userStatus!!
            }

            if (userOrientation != null) {
                userMap["orientation"] = userOrientation!!
            }

            if (ageMin != null && ageMax != null) {
                userMap["minAge"] = ageMin!!
                userMap["maxAge"] = ageMax!!
            }

            if (area != null) {
                userMap["area"] = area!!
            }
            if (country != null) {
                userMap["country"] = country!!
            }
            if (region != null) {
                userMap["region"] = region!!
            }

            Log.d(TAG, "User Entries: ${userMap.entries}")

            authViewModel.updateCompleteProfile(userMap)
        }

        binding.btnNavigationBack.setOnClickListener {
            finish()
        }


    }

    private fun getDateTimeCalender() {
        val cal = Calendar.getInstance()
        currentDay = cal.get(Calendar.DAY_OF_MONTH)
        currentMonth = cal.get(Calendar.MONTH)
        currentYear = cal.get(Calendar.YEAR)
    }

    private fun pickBirthdayDate() {
        getDateTimeCalender()
        val datePickerDialog =
            DatePickerDialog(this, this, currentYear - 18, currentMonth, currentDay)
        val calender = Calendar.getInstance().apply {
            set(currentYear - 18, currentMonth, currentDay)
        }
        datePickerDialog.datePicker.maxDate = calender.timeInMillis
        datePickerDialog.show()
    }

    private fun requestLocationSettings() {
        val locationRequest = LocationRequest.create().apply {
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            interval = 5000L
            fastestInterval = 2000L
        }
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        builder.setAlwaysShow(true)

        val result = LocationServices.getSettingsClient(applicationContext)
            .checkLocationSettings(builder.build())

        result.addOnCompleteListener { task ->
            try {
                task.getResult(ApiException::class.java)
//                showToast("GPS is ON")
                getLastLocation()
            } catch (e: ApiException) {
                when (e.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                        val resolvableApiException = e as ResolvableApiException

                        try {
                            resolvableApiException.startResolutionForResult(
                                this,
                                REQUEST_CHECK_SETTING
                            )

                        } catch (ex: IntentSender.SendIntentException) {
                            ex.printStackTrace()
                        } catch (ex: ClassCastException) {
                            ex.printStackTrace()
                        }
                    }

                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {

                    }

                }
            }
        }
    }

    private fun hasPermissions(vararg permissions: String): Boolean = permissions.all {
        ActivityCompat.checkSelfPermission(
            this,
            it
        ) == PackageManager.PERMISSION_GRANTED
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!hasPermissions(*locationPermissions)) {
                requestPermissions(locationPermissions, LOCATION_REQUEST_CODE)
            } else {
                getUserLocation()
            }
        } else {
            getUserLocation()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getUserLocation() {
        val fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(this)
        fusedLocationProviderClient.lastLocation.addOnSuccessListener { location ->

            if (location != null) {
                val latitude = location.latitude
                val longitude = location.longitude

                Log.d(TAG, "getLastLocation: $latitude and $longitude")

                location.latitude = latitude
                location.longitude = longitude
//                    val coordinates = listOf(latitude, longitude)
//                    val jsonObject = JsonObject()
//                    val toJson = Gson().toJsonTree(coordinates)
//                    jsonObject.addProperty("type", "Point")
//                    jsonObject.add("coordinates", toJson)
//                    Timber.d(jsonObject.toString())
//                    userLocation = com.brilliance.dualiit.model.Location("Point", coordinates)
                fetchLocationFromLatLong(location)
//                    val map = HashMap<String, Any>()
//                    map["location"] = userLocation!!

            }

        }.addOnFailureListener {
            Log.d(TAG, "getLastLocation: ${it.message}")
        }
    }

    private fun fetchLocationFromLatLong(location: Location) {
        val intent = Intent(this, FetchAddressIntentService::class.java)
        intent.putExtra(AppConstant.RECEIVER, resultReceiver)
        intent.putExtra(AppConstant.LOCATION_DATA_EXTRA, location)
        startService(intent)
    }


    @SuppressLint("SetTextI18n", "ClickableViewAccessibility")
    private fun loadUserDetails() {
        binding.apply {
            prefs.userModel?.let { user ->
                if (user.avatar.isNullOrEmpty()) {
                    ivUserProfile.setBackgroundResource(R.drawable.ic_user_avatar)
                } else {
                    Glide.with(this@EditProfileActivity)
                        .load(user.avatar)
                        .into(ivUserProfile)
                }

                etUserName.setText(user.fullName)

                tvLocation.text = if (user.area.isNullOrEmpty()) {
                    "Add Location"
                } else {
                    "${user.area}, ${user.country}, ${user.region}"
                }

                etUserEmail.setText(if (user.email.isNullOrEmpty()) null else user.email)

                etUserPhone.setText(if (user.phone.isNullOrEmpty()) null else user.phone)

                etBirthday.setText(if (user.birthDay.isNullOrEmpty()) null else user.birthDay)

                etIceBreaker.setText(if (user.icebreaker.isNullOrEmpty()) null else user.icebreaker)

//                etWeekendPlan.setText(if (user.weekendplan.isNullOrEmpty()) null else user.weekendplan)

                etBio.setText(if (user.bio.isNullOrEmpty()) null else user.bio)

                when (user.gender) {
                    "male" -> chipUserMale.isChecked = true
                    "female" -> chipUserFemale.isChecked = true
                }

                when (user.interested) {
                    "male" -> chipPreferenceMale.isChecked = true
                    "female" -> chipPreferenceFemale.isChecked = true
                    "other" -> chipPreferenceBoth.isChecked = true
                }

                when (user.relationshipStatus) {
                    "single" -> chipSingle.isChecked = true
                    "married" -> chipMarried.isChecked = true
                    "divorced" -> chipDivorced.isChecked = true
                }

                when (user.orientation) {
                    "straight" -> chipStraight.isChecked = true
                    "bisexual" -> chipBisexual.isChecked = true
                    "heterosexual" -> chipHeterosexual.isChecked = true
                    "homosexual" -> chipHomosexual.isChecked = true
                }

                userGender = user.gender
                userPreference = user.interested
                userStatus = user.relationshipStatus
                userOrientation = user.orientation

                if (user.interest.isNullOrEmpty()) {
                    tvAddIntersts.show()
                    chipGroupInterests.hide()
                } else {
                    tvAddIntersts.hide()
                    for (i in user.interest!!.indices) {
                        val interestName = user.interest!![i]
                        val chip = Chip(this@EditProfileActivity)
                        val paddingDp = TypedValue.applyDimension(
                            TypedValue.COMPLEX_UNIT_DIP, 10F,
                            resources.displayMetrics
                        ).toInt()
                        chip.setPadding(paddingDp, paddingDp, paddingDp, paddingDp)
                        chip.chipBackgroundColor = ColorStateList.valueOf(
                            ContextCompat.getColor(
                                this@EditProfileActivity,
                                R.color.com_facebook_button_background_color_disabled
                            )
                        )
                        chip.text = interestName
                        chip.isCloseIconVisible = false
                        chip.id = ViewCompat.generateViewId();
                        chipGroupInterests.addView(chip)

                    }
                    binding.chipGroupInterests.addView(createInterestBtn())
                }

                if (user.interest.isNullOrEmpty()) {
                    tvAddIntersts.setOnClickListener {
                        val interestBottomSheet = InterestBottomSheetFragment().newInstance()
                        interestBottomSheet.show(supportFragmentManager, "Interests")
                    }
                } else {
                    addInterestBtn?.setOnClickListener {
                        if (interestsList.size >= 5) {
                            showToast("You can only add up to 5 interests")
                        } else {
                            val interestBottomSheet = InterestBottomSheetFragment().newInstance()
                            interestBottomSheet.show(supportFragmentManager, "Interests")
                        }
                    }
                }

                if (user.minAge == null || user.maxAge == null ||
                    user.minAge == 0 || user.maxAge == 0
                ) {
                    sliderAgePreference.setValues(18f, 60f)
                    tvMinMaxAge.text = "18-60"
                } else {
                    val minAge = user.minAge?.toFloat()
                    val maxAge = user.maxAge?.toFloat()
                    sliderAgePreference.setValues(minAge, maxAge)
                    tvMinMaxAge.text = "${user.minAge}-${user.maxAge}"
                }


            }
        }
    }

    private fun createInterestBtn(): Chip {
        val paddingDp = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 10F,
            resources.displayMetrics
        ).toInt()
        addInterestBtn = Chip(this).apply {
            setPadding(paddingDp, paddingDp, paddingDp, paddingDp)
            chipBackgroundColor = ColorStateList.valueOf(
                ContextCompat.getColor(
                    this@EditProfileActivity,
                    R.color.com_facebook_button_background_color_disabled
                )
            )
            text = "+ Add"
            isCloseIconVisible = false
        }
        return addInterestBtn!!
    }

    override fun setObserver() {
        authViewModel.userAvatar.observe(this) {
            userAvatar = it
        }

        authViewModel.uploadProgress.observe(this) {
            binding.progressBar.isVisible = it == true
        }


        authViewModel.croppedImage.observe(this) {
            uri = it
            binding.ivUserProfile.show()
            binding.ivUserProfile.setImageURI(uri)
            if (uri != null) {
                val file = File(uri!!.path!!)

                val filePart = MultipartBody.Part.createFormData(
                    "images",
                    file.name,
                    file.asRequestBody("image/*".toMediaTypeOrNull())
                )
                authViewModel.uploadAvatar(filePart)
            }

        }

        authViewModel.completeProfileProgress.observe(this) {
            if (it == true) {
                binding.updateProgressBar.isVisible = true
                binding.btnUpdateProfile.isVisible = false
                showToast("Profile Updated")
                val intent = Intent(this, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("IS_PROFILE", true)
                startActivity(intent)
                finish()
            } else {
                binding.updateProgressBar.isVisible = false
                binding.btnUpdateProfile.isVisible = true

            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.profileEvent.collect { event ->
                when (event) {
                    is HomeViewModel.ProfileEvent.SelectedInterests -> {
                        if (event.interestsList.isEmpty()) {
                            binding.tvAddIntersts.isVisible = true
                        } else {
                            binding.tvAddIntersts.isVisible = false
                            val myInterestsList = event.interestsList.toSet().toList()
                            binding.chipGroupInterests.removeAllViews()
                            setInterestChips(myInterestsList)

                            (interestsList as ArrayList).clear()

                            for (i in myInterestsList.indices) {
                                val interestName = myInterestsList[i].name
                                (interestsList as ArrayList).add(interestName)
                            }
                        }
                    }
                }
            }
        }

        super.setBaseObserver()
    }

    private fun setInterestChips(list: List<InterestDoc>) {
        for (i in list.indices) {
            val interestName = list[i].name
            val chip = Chip(this)
            val paddingDp = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 10F,
                resources.displayMetrics
            ).toInt()
            chip.setPadding(paddingDp, paddingDp, paddingDp, paddingDp)
            chip.chipBackgroundColor = ColorStateList.valueOf(
                ContextCompat.getColor(
                    this,
                    R.color.com_facebook_button_background_color_disabled
                )
            )
            chip.text = interestName
            chip.isCloseIconVisible = false
            binding.chipGroupInterests.addView(chip)
        }
        if (addInterestBtn != null) {
            binding.chipGroupInterests.removeView(addInterestBtn)
        }
        binding.chipGroupInterests.addView(createInterestBtn())
        addInterestBtn?.setOnClickListener {
            if (interestsList.size >= 5) {
                showToast("You can only add up to 5 interests")
            } else {
                val interestBottomSheet = InterestBottomSheetFragment()
                interestBottomSheet.show(supportFragmentManager, "Interests")
            }
        }
    }

    private fun validateGender(): Boolean {
        return if (binding.chipGroupUserGender.checkedChipId == -1) {
            showToast("Please select Gender!")
            false
        } else {
            true
        }
    }

    private fun validateStatus(): Boolean {
        return if (binding.chipGroupStatus.checkedChipId == -1) {
            showToast("Please select Your Relationship status!")
            false
        } else {
            true
        }
    }

    private fun validatePreference(): Boolean {
        return if (binding.chipGroupPreference.checkedChipId == -1) {
            showToast("Please select Preference!")
            false
        } else {
            true
        }
    }

    private fun validateOrientation(): Boolean {
        return if (binding.chipGroupOrientation.checkedChipId == -1) {
            showToast("Please select Orientation!")
            false
        } else {
            true
        }
    }

    private fun validateName(): Boolean {
        val bio = binding.tilUserName.editText?.text.toString().trim()
        return if (bio.isEmpty()) {
            binding.tilUserName.error = "Please enter your name"
            false
        } else {
            binding.tilUserName.error = null
            binding.tilUserName.isErrorEnabled = false
            true
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CHECK_SETTING) {
            when (resultCode) {
                RESULT_OK -> {
                    showToast("GPS is turned on")
                    getLastLocation()
                }
                RESULT_CANCELED -> {
                    showToast("GPS is required to be turned on")
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCATION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() || grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation()
            } else {
                showToast("Location permission is required to get current location")
            }
        }
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val savedMonth = month + 1

        var monthString = savedMonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        }

        dateOfBirth = "${dayOfMonth}-${monthString}-${year}"
        binding.etBirthday.setText(dateOfBirth)
    }

    inner class AddressResultReceiver(handler: Handler) : ResultReceiver(handler) {

        override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
            super.onReceiveResult(resultCode, resultData)
            if (resultCode == AppConstant.SUCCESS_RESULT) {
                binding.tvLocation.text = resultData?.getString(AppConstant.RESULT_DATA_KEY)
                val address =
                    resultData?.getString(AppConstant.RESULT_DATA_KEY)?.split(",")?.toTypedArray()
                area = address?.get(0) ?: ""
                country = address?.get(1)
                region = address?.get(2)
                Log.d(TAG, "Address: $area, $country, $region")
            } else {
                showToast(resultData?.getString(AppConstant.RESULT_DATA_KEY))
            }
        }

    }
}