package com.brilliance.dualiit.ui.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.brilliance.dualiit.databinding.FragmentNotificationBinding
import com.brilliance.dualiit.ui.NotificationPagerAdapter
import com.brilliance.dualiit.ui.base.fragment.BaseFragment
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.google.android.material.tabs.TabLayoutMediator
import org.koin.androidx.viewmodel.ext.android.viewModel

class NotificationFragment : BaseFragment() {
    private var _binding: FragmentNotificationBinding? = null
    private val binding get() = _binding!!
    private val viewModel: HomeViewModel by viewModel()
    override fun baseViewModel(): BaseViewModel = viewModel
    private var tabLayoutMediator: TabLayoutMediator? = null
    private lateinit var notificationPagerAdapter: NotificationPagerAdapter


    override fun createDataBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        _binding = FragmentNotificationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        notificationPagerAdapter =
            NotificationPagerAdapter(childFragmentManager, viewLifecycleOwner.lifecycle)

        binding.viewPagerNotification.adapter = notificationPagerAdapter

        tabLayoutMediator = TabLayoutMediator(
            binding.tabLayout,
            binding.viewPagerNotification
        ) { tab, position ->
            if (position == 0) {
                tab.text = "Invitations"
            } else {
                tab.text = "Notifications"
            }
        }
        tabLayoutMediator?.attach()
    }

    override fun setObserver() {
        super.setBaseObserver()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}