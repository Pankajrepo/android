package com.brilliance.dualiit.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ItemBlockedBinding
import com.brilliance.dualiit.ui.chat.AddressBook
import com.bumptech.glide.Glide
import org.ocpsoft.prettytime.PrettyTime
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class BlockedAdapter :
    PagingDataAdapter<AddressBook, BlockedAdapter.BlockedViewHolder>(DiffCallback()) {

    inner class BlockedViewHolder(private val binding: ItemBlockedBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.btnUnblock.setOnClickListener {
                onItemClickListener?.let { click ->
                    val blockedContact = getItem(bindingAdapterPosition)
                    if (blockedContact != null) {
                        click(blockedContact)
                    }
                }
            }

            binding.root.setOnClickListener {
                onProfileClickListener?.let { click ->
                    val userId = getItem(bindingAdapterPosition)?.friendId?._id
                    if (userId != null) {
                        click(userId)
                    }
                }
            }
        }

        fun bind(blockModel: AddressBook) {
            binding.apply {
                if (blockModel.friendId?.avatar == "") {
                    ivProfile.setBackgroundResource(R.drawable.ic_user_avatar)
                } else {
                    Glide.with(itemView)
                        .load(blockModel.friendId?.avatar)
                        .into(ivProfile)
                }
                tvUserName.text = blockModel.friendId?.fullName
                tvTime.text = formattedDate(blockModel.updatedAt.toString())

                if (blockModel.status == 4) {
                    btnUnblock.text = "unblock"
                } else {
                    btnUnblock.text = "block"
                }
            }
        }
    }


    class DiffCallback : DiffUtil.ItemCallback<AddressBook>() {
        override fun areItemsTheSame(oldItem: AddressBook, newItem: AddressBook): Boolean {
            return oldItem._id == newItem._id
        }

        override fun areContentsTheSame(oldItem: AddressBook, newItem: AddressBook): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BlockedViewHolder {
        val binding =
            ItemBlockedBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BlockedViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BlockedViewHolder, position: Int) {
        val blockModel = getItem(position)
        if (blockModel != null) {
            holder.bind(blockModel)
        }
    }

    private var onItemClickListener: ((AddressBook) -> Unit)? = null

    fun setOnItemClickListener(listener: (AddressBook) -> Unit) {
        onItemClickListener = listener
    }

    private var onProfileClickListener: ((String) -> Unit)? = null

    fun setOnProfileClickListener(listener: (String) -> Unit) {
        onProfileClickListener = listener
    }

    private fun formattedDate(t: String): String {
        val prettyTime = PrettyTime(Locale.getDefault())
        var time: String? = null
        try {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
            simpleDateFormat.timeZone = TimeZone.getTimeZone("GMT")
            val date = simpleDateFormat.parse(t)
//            val simpleDateFormat2 = SimpleDateFormat("HH:mm aa", Locale.ENGLISH)
            time = prettyTime.format(date)
//            time = simpleDateFormat2.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return time!!
    }

    private fun getCountry(): String {
        val locale = Locale.getDefault()
        val country = locale.country
        return country.toLowerCase()
    }
}