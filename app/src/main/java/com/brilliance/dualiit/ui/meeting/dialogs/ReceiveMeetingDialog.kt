package com.brilliance.dualiit.ui.meeting.dialogs

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.lifecycle.lifecycleScope
import com.brilliance.dualiit.R
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.brilliance.dualiit.ui.speeddating.CallingActivity
import com.brilliance.dualiit.ui.speeddating.models.SessionResponse
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import de.hdodenhof.circleimageview.CircleImageView
import io.socket.client.Socket
import kotlinx.coroutines.flow.collect
import org.json.JSONException
import org.json.JSONObject
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject

@AndroidEntryPoint
@SuppressLint("LogNotTimber")
class ReceiveMeetingDialog : AppCompatActivity() {

    private var callingDialog: Dialog? = null
    private var userName: String? = ""
    private var userPhoto: String? = ""
    private var userId: String? = ""
    private var meetingId: String? = ""
    private var sessionResponse: SessionResponse? = null
    private val viewModel: HomeViewModel by viewModel()

    private val TAG = "ReceiveMeetingDialog"

    @Inject
    lateinit var socket: Socket

    val prefs: AppPreferencesHelper by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        callingDialog = Dialog(this)
        callingDialog?.setContentView(R.layout.dialog_calling)


        val window: Window? = callingDialog?.window
        val size = Point()
        val display: Display? = window?.windowManager?.defaultDisplay
        display?.getSize(size)
        val width: Int = size.x

        window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )

        callingDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        val userNameView = callingDialog?.findViewById<TextView>(R.id.tvUserName)
        val userPhotoView = callingDialog?.findViewById<CircleImageView>(R.id.ivUserProfile)
        val cancelButton = callingDialog?.findViewById<AppCompatButton>(R.id.btnCancel)
        val acceptButton = callingDialog?.findViewById<AppCompatButton>(R.id.btnCallOrAccept)

        intent?.let {
            userName = it.getStringExtra("USER_NAME")
            userId = it.getStringExtra("USER_ID")
            userPhoto = it.getStringExtra("USER_PHOTO")
            meetingId = it.getStringExtra("MEETING_ID")

        }

        initializeSocket()

        if (userPhoto == "") {
            userPhotoView?.setBackgroundResource(R.drawable.ic_user_avatar)
        } else {
            Glide.with(this)
                .load(userPhoto)
                .into(userPhotoView!!)
        }
        userNameView?.text = userName

        acceptButton?.text = "Accept"

        cancelButton?.setOnClickListener {
            sendRejectEvent()
            callingDialog?.dismiss()
            finish()
        }

        acceptButton?.setOnClickListener {
            sendAcceptEvent()
        }

        lifecycleScope.launchWhenStarted {
            viewModel.sessionEvent.collect { event ->
                when (event) {
                    is HomeViewModel.SessionEvent.OnSessionResponse -> {
                        Log.d(
                            TAG,
                            "SessionResponse: ${event.sessionResponse}"
                        )
                        sessionResponse = event.sessionResponse
                        sendCallStatusEvent(event.sessionResponse)
                    }
//                    is HomeViewModel.SessionEvent.OnGenerateTokenResponse -> {
//                        Log.d(
//                            TAG,
//                            "Generate Token: ${event.sessionResponse}"
//                        )
//                        sessionResponse = event.sessionResponse
//                        if (sessionResponse != null) {
////                            showToast("Starting now")
//                            startActivity(
//                                Intent(
//                                    this@ReceiveMeetingDialog,
//                                    CallingActivity::class.java
//                                ).apply {
//                                    putExtra("API_KEY", sessionResponse?.apiKey)
//                                    putExtra("SESSION_ID", sessionResponse?.sessionId)
//                                    putExtra("TOKEN", sessionResponse?.token)
//                                    putExtra("ID", userId)
//                                    putExtra("NAME", userName)
//                                    putExtra("MEETING_TYPE", "Meeting")
//                                    putExtra("MEETING_TIME", 0)
//                                }
//                            )
//
//                        }
//                    }
                }
            }
        }

        window?.setGravity(Gravity.CENTER)
        callingDialog?.show()
    }

    private fun sendRejectEvent() {
        val fromUserObject = JSONObject()
        try {
            fromUserObject.put("_id", prefs.userModel?.id)
            fromUserObject.put("name", prefs.userModel?.fullName)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val toUserObject = JSONObject()
        try {
            toUserObject.put("_id", userId)
            toUserObject.put("name", userName)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val jsonObject = JSONObject()
        try {
            jsonObject.put("from", fromUserObject)
            jsonObject.put("to", toUserObject)
            jsonObject.put("status", "rejected")
            jsonObject.put("type", "Meeting")
            jsonObject.put("meetingId", meetingId)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Log.d(TAG, "Sending Rejected Event: $jsonObject")

        socket.emit("scheduledMeeting", jsonObject)


    }

    private fun sendAcceptEvent() {
        val fromUserObject = JSONObject()
        try {
            fromUserObject.put("_id", prefs.userModel?.id)
            fromUserObject.put("name", prefs.userModel?.fullName)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val toUserObject = JSONObject()
        try {
            toUserObject.put("_id", userId)
            toUserObject.put("name", userName)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val jsonObject = JSONObject()
        try {
            jsonObject.put("from", fromUserObject)
            jsonObject.put("to", toUserObject)
            jsonObject.put("status", "accepted")
            jsonObject.put("type", "Meeting")
            jsonObject.put("meetingId", meetingId)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Log.d(TAG, "Sending Accepted Event: $jsonObject")

        socket.emit("scheduledMeeting", jsonObject)

        viewModel.createSessionToken()

    }

    private fun sendCallStatusEvent(response: SessionResponse) {
        val fromUserObject = JSONObject()
        try {
            fromUserObject.put("_id", userId)
            fromUserObject.put("name", userName)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val toUserObject = JSONObject()
        try {
            toUserObject.put("_id", prefs.userModel?.id)
            toUserObject.put("name", prefs.userModel?.fullName)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val jsonObject = JSONObject()
        try {
            jsonObject.put("from", fromUserObject)
            jsonObject.put("to", toUserObject)
            jsonObject.put("status", "accepted")
            jsonObject.put("type", "Meeting")
            jsonObject.put("shareScreen", false)
            jsonObject.put("speedDating", 0)
            jsonObject.put("sessionId", response.sessionId)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Log.d(TAG, "Sending Call Status Event: $jsonObject")

        socket.emit("callStatus", jsonObject)

//        showToast("Starting now")
//        callingDialog?.dismiss()
        finish()
        startActivity(Intent(this, CallingActivity::class.java)
            .apply {
                putExtra("API_KEY", sessionResponse?.apiKey)
                putExtra("SESSION_ID", sessionResponse?.sessionId)
                putExtra("TOKEN", sessionResponse?.token)
                putExtra("ID", userId)
                putExtra("NAME", userName)
                putExtra("MEETING_TYPE", "Meeting")
                putExtra("MEETING_TIME", 2)
            })


    }

    private fun initializeSocket() {
        socket.on(Socket.EVENT_CONNECT_ERROR) {
            Log.d(TAG, "CONNECTION ERROR: ${it[0]}")
        }

        socket.on(Socket.EVENT_DISCONNECT) {
            Log.d(TAG, "DISCONNECT: ${it[0]}")
        }

        socket.on("error") {
            Log.d(TAG, "Error: ${it[0]}")
        }

        socket.on("connect_timeout") {
            Log.d(TAG, "Connect timeout: ${it[0]}")
        }

        socket.on("reconnecting") {
            Log.d(TAG, "Reconnecting: ${it[0]}")
        }

        socket.on("reconnect_error") {
            Log.d(TAG, "Reconnect error: ${it[0]}")
        }

        socket.on("reconnect") {
            Log.d(TAG, "Reconnect: ${it[0]}")
        }

        socket.on("scheduledMeeting") {
            Log.d(TAG, "Receiving Event: ${it[0]}")
        }


    }

    override fun onDestroy() {
        callingDialog?.dismiss()
        super.onDestroy()
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        callingDialog?.dismiss()
        finish()
        return true
    }
}