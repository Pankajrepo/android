package com.brilliance.dualiit.ui.chat

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName


@Keep
data class AddressBookModel(
    @SerializedName("docs")
    val addressBookList: List<AddressBook>? = null,
    val `false`: Boolean? = false,
    val limit: Int? = 0,
    val nextPage: Any? = null,
    val page: Int? = 0,
    val prevPage: Any? = null,
    val totalDocs: Int? = 0,
    val totalPages: Int? = 0
)