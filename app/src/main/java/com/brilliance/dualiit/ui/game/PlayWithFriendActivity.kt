package com.brilliance.dualiit.ui.game

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.ActivityPlayWithFriendBinding

class PlayWithFriendActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPlayWithFriendBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlayWithFriendBinding.inflate(layoutInflater)
        setContentView(binding.root)


        var flag = 0

        binding.circle.setImageResource(R.drawable.ic_circle_secondary)

        binding.circle.setOnClickListener {
            binding.cross.setImageResource(R.drawable.ic_cross_white)
            binding.circle.setImageResource(R.drawable.ic_circle_secondary)
            flag = 0
        }

        binding.cross.setOnClickListener {
            binding.circle.setImageResource(R.drawable.ic_circle_white)
            binding.cross.setImageResource(R.drawable.ic_cross_secondary)
            flag = 1
        }

        binding.btnPlay.setOnClickListener {
            if (TextUtils.isEmpty(binding.playerOne.text) || TextUtils.isEmpty(binding.playerTwo.text)) {
                if (TextUtils.isEmpty(binding.playerTwo.text) && TextUtils.isEmpty(binding.playerTwo.text)) {
                    Toast.makeText(this, "Enter Player Name", Toast.LENGTH_SHORT).show()
                } else if (TextUtils.isEmpty(binding.playerOne.text)) {
                    Toast.makeText(this, "Enter Player_1 Name", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Enter Player_2 Name", Toast.LENGTH_SHORT).show()
                }
            } else {
                val intent = Intent(this, GamePlayActivity::class.java)
                intent.putExtra("Player1", binding.playerOne.text.toString())
                intent.putExtra("Player2", binding.playerTwo.text.toString())
                intent.putExtra("vsWhom", 0) // Vs Friend
                if (flag == 0) {
                    intent.putExtra("Player", 0) // O will move first
                } else if (flag == 1) {
                    intent.putExtra("Player", 1) // X will move first
                }
                startActivity(intent)
                finish()
            }
        }
        binding.btnQuit.setOnClickListener {
            finish()
        }
    }

}