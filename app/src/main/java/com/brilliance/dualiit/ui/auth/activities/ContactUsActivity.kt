package com.brilliance.dualiit.ui.auth.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.brilliance.dualiit.databinding.ActivityContactUsBinding
import com.brilliance.dualiit.utils.extensions.showToast
import com.google.android.material.textfield.TextInputLayout

class ContactUsActivity : AppCompatActivity() {

//    private val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

    private lateinit var binding: ActivityContactUsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityContactUsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.ivNavigationBack.setOnClickListener {
            finish()
        }

        binding.btnSendMessage.setOnClickListener {
            if (!validateField(binding.tilUserName) || !validateField(binding.tilSubject)
                || !validateField(binding.tilMessage)
            ) {
                return@setOnClickListener
            }
            val name = binding.etUserName.text.toString()
            val subject = binding.etSubject.text.toString()
            val messageBody = "From ${name},\n ${binding.etMessage.text}"
            sendEmail(subject, messageBody)
        }

    }

    @SuppressLint("QueryPermissionsNeeded")
    private fun sendEmail(subject: String, messageBody: String) {
        val emailIntent = Intent(Intent.ACTION_SENDTO).apply {
            putExtra(Intent.EXTRA_EMAIL, arrayOf("dualiitapp@gmail.com"))
            putExtra(Intent.EXTRA_SUBJECT, subject)
            putExtra(Intent.EXTRA_TEXT, messageBody)
            data = Uri.parse("mailto:")
        }
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(emailIntent)
        } else {
            showToast("There is no email app to send this message")
        }
    }

    private fun validateField(til: TextInputLayout): Boolean {
        val name = til.editText?.text.toString().trim()
        return if (name.isEmpty()) {
            til.error = "Field can not be empty"
            false
        } else {
            til.error = null
            til.isErrorEnabled = false
            true
        }
    }

//    private fun validateEmail(): Boolean {
//        val email = binding.tilUserEmail.editText?.text.toString().trim()
//        return when {
//            email.isEmpty() -> {
//                binding.tilUserEmail.error = "Email can not be empty"
//                false
//            }
//            !email.matches(emailPattern.toRegex()) -> {
//                binding.tilUserEmail.error = "Invalid email address"
//                false
//            }
//            else -> {
//                binding.tilUserEmail.error = null
//                binding.tilUserEmail.isErrorEnabled = false
//                true
//            }
//        }
//    }
}