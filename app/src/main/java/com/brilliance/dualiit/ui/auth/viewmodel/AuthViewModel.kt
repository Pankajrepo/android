package com.brilliance.dualiit.ui.auth.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.brilliance.dualiit.model.BaseResponse
import com.brilliance.dualiit.model.DialogSuccessModel
import com.brilliance.dualiit.model.RedirectModel
import com.brilliance.dualiit.model.UserModel
import com.brilliance.dualiit.ui.auth.AuthRepository
import com.brilliance.dualiit.ui.auth.activities.CompleteProfile
import com.brilliance.dualiit.ui.auth.activities.Login
import com.brilliance.dualiit.ui.auth.helpers.ChangePasswordEvent
import com.brilliance.dualiit.ui.auth.helpers.DeleteAccountEvent
import com.brilliance.dualiit.ui.auth.helpers.OTPEvent
import com.brilliance.dualiit.ui.auth.request.LoginRequest
import com.brilliance.dualiit.ui.auth.request.ResetRequest
import com.brilliance.dualiit.ui.auth.request.SignupRequest
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.ui.home.activities.MainActivity
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.Resource
import com.brilliance.dualiit.utils.networkRequest.ApiService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import retrofit2.HttpException
import timber.log.Timber


class AuthViewModel(
    private val api: ApiService,
    private val prefs: AppPreferencesHelper,
    private val repository: AuthRepository,
) : BaseViewModel(prefs) {
    var phone = MutableLiveData<String>()
    var fullName = MutableLiveData<String>()
    var code = MutableLiveData<String>()
    var requestReset = MutableLiveData<Boolean>()
    var requestEmailVerification = MutableLiveData<Boolean>()
    var email = MutableLiveData<String>()
    var receiveOTP = MutableLiveData<Boolean>()
    var receiveEmailOTP = MutableLiveData<Boolean>()
    var numberExist = MutableLiveData<Boolean>()
    var password = MutableLiveData<String>()
    var userAvatar = MutableLiveData<String>()
    var passwordChanged = MutableLiveData<Boolean>()
    var deviceToken = MutableLiveData<String>()
    var uploadProgress = MutableLiveData<Boolean>()
    var updateProgress = MutableLiveData<Boolean>()
    var completeProfileProgress = MutableLiveData<Boolean>()
    var canUpdateFragment = MutableLiveData<Boolean>()
    var isEmailValid = MutableLiveData<Boolean>()

    //    Auth Channels
    private val _otpEventChannel = Channel<OTPEvent>()
    val otpEvent = _otpEventChannel.receiveAsFlow()

    private val _changePasswordEventChannel = Channel<ChangePasswordEvent>()
    val changePasswordEvent = _changePasswordEventChannel.receiveAsFlow()

    private val _deleteAccountEventChannel = Channel<DeleteAccountEvent>()
    val deleteAccountEvent = _deleteAccountEventChannel.receiveAsFlow()

    private val _signUpStateFlow: MutableStateFlow<Resource<UserModel>> =
        MutableStateFlow(Resource.Empty())

    val signUpState: StateFlow<Resource<UserModel>> = _signUpStateFlow

    private val _loginStateFlow: MutableStateFlow<Resource<UserModel>> =
        MutableStateFlow(Resource.Empty())

    val loginState: StateFlow<Resource<UserModel>> = _loginStateFlow

    private val _resetPasswordStateFlow: MutableStateFlow<Resource<UserModel>> =
        MutableStateFlow(Resource.Empty())

    val resetPasswordState: StateFlow<Resource<UserModel>> = _resetPasswordStateFlow

//    fun checkExist() {
//        requestData(api.checkExist(
//            phone.value ?: "", email.value ?: ""
//        ),
//            {
//                showSnakBar.postValue(it.message)
//                numberExist.postValue(true)
//
//            }, { msg, _ ->
//                msg?.let {
//                    showDialog.postValue(DialogSuccessModel(title = "Verify OTP", message = it))
//                }
//            })
//    }

    fun checkUserAlreadyExist() {
        viewModelScope.launch {
            repository.checkUserExist(phone.value ?: "", email.value ?: "")
                .catch { e ->
                    Log.d("AuthViewModel", "Api Error: ${e.message}")
                    sendOTPEvent(
                        OTPEvent.NumberAlreadyExist(
                            true,
                            "Phone Number is Already registered"
                        )
                    )
                }
                .collect { data ->
                    if (data.success) {
                        sendOTPEvent(
                            OTPEvent.NumberAlreadyExist(
                                false,
                                "OTP Sent, Check Your device"
                            )
                        )
                    } else {
                        sendOTPEvent(OTPEvent.NumberAlreadyExist(true, data.message))
                    }
                }
        }

    }

    private fun sendOTPEvent(event: OTPEvent) =
        viewModelScope.launch(Dispatchers.IO) {
            _otpEventChannel.send(event)
        }

    private fun sendChangePasswordEvent(event: ChangePasswordEvent) =
        viewModelScope.launch(Dispatchers.IO) {
            _changePasswordEventChannel.send(event)
        }

    fun sendSMSCode() {
        viewModelScope.launch {
            repository.sendSmsCode(phone.value ?: "")
                .catch { e ->
                    Log.d("AuthViewModel", "OTP Error: ${e.message}")
                    sendOTPEvent(OTPEvent.OTPFailed(e.message.toString()))
                }
                .collect { data ->
                    if (data.success) {
                        sendOTPEvent(OTPEvent.OTPSuccess)
                    } else {
                        sendOTPEvent(OTPEvent.OTPFailed(data.message))
                    }
                }
        }
    }

    fun signUp(signupRequest: SignupRequest) {
        viewModelScope.launch {
            repository.signUP(signupRequest)
                .onStart {
                    _signUpStateFlow.value = Resource.Loading()
                }
                .catch { e ->
                    Log.d("AuthViewModel", "SignUP Error: ${e.message}")
                    _signUpStateFlow.value = Resource.Error(e.message.toString())
                }
                .collect { data ->
                    prefs.userModel = data.data
                    prefs.authToken = data.data?.accessToken!!
                    _signUpStateFlow.value = Resource.Success(data.data)
                }
        }
    }

    fun loginUser(loginRequest: LoginRequest) {
        viewModelScope.launch {
            repository.loginUser(loginRequest)
                .onStart {
                    _loginStateFlow.value = Resource.Loading()
                }
                .catch { e ->
                    Log.d("AuthViewModel", "Login Error: ${e.message}")
                    _loginStateFlow.value = Resource.Error("Incorrect Email or password")
                }
                .collect { data ->
                    prefs.userModel = data.data
                    prefs.authToken = data.data?.accessToken!!
                    _loginStateFlow.value = Resource.Success(data.data)
                }
        }
    }

    fun resetPassword(resetRequest: ResetRequest) {
        viewModelScope.launch {
            repository.resetPassword(resetRequest)
                .onStart {
                    _resetPasswordStateFlow.value = Resource.Loading()
                }
                .catch { e ->
                    Log.d("AuthViewModel", "Reset Password Error: ${e.message}")
                    _resetPasswordStateFlow.value = Resource.Error(e.message.toString())
                }
                .collect { data ->
                    _resetPasswordStateFlow.value = Resource.Success(data.data!!)
                }
        }
    }

    fun changePassword(oldPassword: String, newPassword: String) {
        viewModelScope.launch {
            repository.changePassword(oldPassword, newPassword)
                .onStart {
                    sendChangePasswordEvent(ChangePasswordEvent.Loading)
                }
                .catch { e ->
                    Log.d("AuthViewModel", "Change Password Error: ${e.message}")
                    if (e is HttpException) {
                        if (e.code() == 400) {
                            sendChangePasswordEvent(ChangePasswordEvent.Failed("Wrong old password"))
                        }
                    }
                }
                .collect { data ->
                    if (data.success) {
                        sendChangePasswordEvent(ChangePasswordEvent.Success)
                    } else {
                        sendChangePasswordEvent(ChangePasswordEvent.Failed(data.message))
                    }
                }
        }
    }

    fun deleteMyAccount() {
        viewModelScope.launch {
            repository.deleteMyAccount()
                .onStart {
                    _deleteAccountEventChannel.send(DeleteAccountEvent.Loading)
                }
                .catch { e ->
                    Log.d("AuthViewModel", "Delete Account Error: ${e.message}")
                    _deleteAccountEventChannel.send(DeleteAccountEvent.Failed(e.message.toString()))
                }
                .collect { data ->
                    if (data.success) {
                        _deleteAccountEventChannel.send(DeleteAccountEvent.Success)
                    } else {
                        _deleteAccountEventChannel.send(DeleteAccountEvent.Failed(data.message))
                    }
                }
        }
    }

//        fun changePassword(oldPassword: String, newPassWord: String) {
//        val map = HashMap<String, String>()
//        map["Authorization"] = prefs.authToken
//        requestData(api.changePassword(map, oldPassword, newPassWord), {
//            showSnakBar.postValue(it.message)
//            passwordChanged.postValue(it.success)
//            if (it.serverCode == 401) {
//                prefs.logout()
//            }
////            if (it.success) {
////                prefs.userModel = it.data
////                prefs.authToken = it.data!!.refreshToken!!
////                redirect.postValue(
////                    RedirectModel(
////                        mClass = SettingsFragment::class.java,
////                        isTopFinish = true
////                    )
////                )
////            }
//        }, { msg, _ ->
//            msg?.let {
//                Log.d("ChangePassword", "$msg")
//            }
//        })
//    }

    //    fun resetPwd(resetRequest: ResetRequest) {
//        requestData(api.resetPwd(
//            resetRequest
//        ),
//            {
//                redirect.postValue(
//                    RedirectModel(
//                        mClass = Login::class.java,
//                        isTopFinish = true,
//                        flagIsTopClearTask = true
//                    )
//                )
//            }, { msg, _ ->
//                msg?.let {
//                    showDialog.postValue(DialogSuccessModel(title = "Verify OTP", message = it))
//                }
//            })
//    }


//    fun login(loginRequest: LoginRequest) {
//        requestData(api.login(
//            loginRequest
//        ),
//            {
//                prefs.userModel = it.data
//                prefs.authToken = it.data!!.accessToken!!
//                redirect.postValue(
//                    RedirectModel(
//                        mClass = MainActivity::class.java,
//                        isTopFinish = true,
//                        flagIsTopClearTask = true
//                    )
//                )
//
//            }, { msg, _ ->
//                msg?.let {
//                    showDialog.postValue(DialogSuccessModel(title = "Verify OTP", message = it))
//                }
//            })
//    }

//    fun signUp(signup: SignupRequest) {
//        requestData(api.signUp(
//            signup
//        ),
//            {
//
//                prefs.userModel = it.data
//                prefs.authToken = it.data!!.accessToken!!
//                redirect.postValue(
//                    RedirectModel(
//                        mClass = CompleteProfile::class.java,
//                        isTopFinish = true,
//                        flagIsTopClearTask = true
//                    )
//                )
//
//            }, { msg, _ ->
//                msg?.let {
//                    showDialog.postValue(DialogSuccessModel(title = "Verify OTP", message = it))
//                }
//            })
//    }


//    fun sendSMSCode() {
//        requestData(api.sendSMSCode(
//            phone.value ?: ""
//        ),
//            {
//                showSnakBar.postValue(it.message)
//                receiveOTP.postValue(true)
//                /* redirect.postValue(
//                     RedirectModel(
//                         mClass = HomeActivity::class.java,
//                         isTopFinish = true,
//                         flagIsTopClearTask = true
//                     )
//                 )*/
//            }, { msg, _ ->
//                msg?.let {
//                    showDialog.postValue(DialogSuccessModel(title = "Verify OTP", message = it))
//                }
//            })
//    }

    fun sendEmailVerifyCode() {
        requestData(api.sendEmailVerifyCode(
            email.value ?: ""
        ), {
            showSnakBar.postValue(it.message)
            if (it.success) {
                receiveEmailOTP.postValue(true)
            }
        }, { msg, _ ->
            msg?.let {
                showDialog.postValue(DialogSuccessModel(title = "Verify OTP", message = it))
            }
        })
    }

    fun verifyEmail(code: String, email: String) {
        requestData(api.verifyEmail(
            code, email
        ), {
            showSnakBar.postValue(it.message)
            if (it.success) {
                isEmailValid.postValue(true)

            } else {
                isEmailValid.postValue(false)
//                redirect.postValue(
//                    RedirectModel(
//                        mClass = MainActivity::class.java,
//                        isTopFinish = true,
//                        flagIsTopClearTask = true
//                    )
//                )
            }
        }, { msg, _ ->
            msg?.let {
                showDialog.postValue(DialogSuccessModel(title = "Verify OTP", message = it))
            }
        })
    }


    fun uploadAvatar(filePart: MultipartBody.Part) {
        uploadProgress.postValue(true)
        requestData(api.uploadUserAvatar(
            filePart
        ),
            {
                userAvatar.postValue(it.data?.avatar!!)
                uploadProgress.postValue(false)
            }, { msg, _ ->
                msg?.let {
                    showDialog.postValue(DialogSuccessModel(title = "Verify OTP", message = it))
                }
            })
    }

    fun uploadAvatarFirstTime(filePart: MultipartBody.Part) {
        uploadProgress.postValue(true)
        requestData(api.uploadUserAvatar(
            filePart
        ),
            {
                userAvatar.postValue(it.data?.avatar!!)
                val userMap = HashMap<String, Any>()
                userMap["avatar"] = it.data.avatar
                updateProfile(userMap)
                uploadProgress.postValue(false)
//                redirect.postValue(
//                    RedirectModel(
//                        mClass = ContactChoose::class.java,
//                        isTopFinish = true,
//                        flagIsTopClearTask = true
//                    )
//                )
            }, { msg, _ ->
                msg?.let {
                    uploadProgress.postValue(false)
                    showDialog.postValue(DialogSuccessModel(title = "Verify OTP", message = it))
                }
            })
    }


    fun updateProfile(hashMap: HashMap<String, Any>) = CoroutineScope(Dispatchers.IO).launch {
        updateProgress.postValue(true)
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        val response = api.updateProfile(map, hashMap)
        if (response.serverCode == 401) {
            prefs.logout()
        }
        prefs.userModel = response.data
//        prefs.userModel?.accessToken = response.data?.accessToken
        Timber.d("UPDATE: $response")
        Timber.d("Successfully updated profile")
        updateProgress.postValue(false)
        canUpdateFragment.postValue(true)
    }

    fun updateCompleteProfile(hashMap: HashMap<String, Any>) =
        CoroutineScope(Dispatchers.IO).launch {
            completeProfileProgress.postValue(true)
            val map = HashMap<String, String>()
            map["Authorization"] = prefs.authToken
            val response = api.updateProfile(map, hashMap)
            if (response.serverCode == 401) {
                prefs.logout()
            }
            prefs.userModel = response.data
//        prefs.userModel?.accessToken = response.data?.accessToken
            Timber.d("UPDATE: $response")
            Timber.d("Successfully updated profile")
            completeProfileProgress.postValue(false)
        }

    fun updateProfileWithLocation(hashMap: HashMap<String, Any>) =
        CoroutineScope(Dispatchers.IO).launch {
//        updateProgress.postValue(true)
            val map = HashMap<String, String>()
            map["Authorization"] = prefs.authToken
            val response = api.updateProfileWithLocation(map, hashMap)
            if (response.serverCode == 401) {
                prefs.logout()
            }
//        prefs.userModel = response.data
//        prefs.userModel?.accessToken = response.data?.accessToken
            Timber.d("UPDATE: $response")
            Timber.d("Successfully updated location")
//        updateProgress.postValue(false)
//        canUpdateFragment.postValue(true)
        }

    fun getUser() {
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        requestData(api.getProfile(map),
            {
                prefs.userModel = it.data
//                prefs.authToken = it.data!!.accessToken!!
            },
            { msg, _ ->
                msg?.let {
                    showDialog.postValue(DialogSuccessModel(title = "Verify OTP", message = it))
                }
            })
    }
}