package com.brilliance.dualiit.ui.base.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class BaseViewPagerAdapter(
    fm: FragmentManager,
    private val mFrag: ArrayList<Fragment>,
    private val mNames: ArrayList<String>
) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return mFrag[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mNames[position]
    }

    override fun getCount(): Int {
        return mFrag.size
    }
}