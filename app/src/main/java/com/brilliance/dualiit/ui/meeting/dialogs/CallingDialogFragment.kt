package com.brilliance.dualiit.ui.meeting.dialogs

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.DialogCallingBinding
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.brilliance.dualiit.ui.meeting.models.callevents.CallingResponse
import com.brilliance.dualiit.ui.speeddating.CallingActivity
import com.brilliance.dualiit.ui.speeddating.models.SessionResponse
import com.brilliance.dualiit.ui.speeddating.models.userdetails.UserDetailsResponse
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.extensions.showToast
import com.bumptech.glide.Glide
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import io.socket.client.Socket
import kotlinx.coroutines.flow.collect
import org.json.JSONException
import org.json.JSONObject
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject

@AndroidEntryPoint
@SuppressLint("LogNotTimber")
class CallingDialogFragment : DialogFragment() {

    private var _binding: DialogCallingBinding? = null
    private val binding get() = _binding!!
    val prefs: AppPreferencesHelper by inject()

    private val TAG = "CallingDialogFragment"

    @Inject
    lateinit var socket: Socket

    private val viewModel: HomeViewModel by viewModel()
    private val gson: Gson = Gson()
    private var sessionResponse: SessionResponse? = null


    private var userPhoto: String? = null
    private var userName: String? = null
    private var userId: String? = null
    private var meetingId: String? = null
    private var fromMe: Boolean? = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initializeSocket()

        if (arguments != null) {
            userPhoto = arguments?.getString("USER_PHOTO")
            userName = arguments?.getString("USER_NAME")
            userId = arguments?.getString("USER_ID")
            meetingId = arguments?.getString("MEETING_ID")
            fromMe = arguments?.getBoolean("FROM_ME", false)
        }
    }


    override fun onResume() {
        super.onResume()

        val window: Window? = dialog!!.window
        val size = Point()
        val display: Display? = window?.windowManager?.defaultDisplay
        display?.getSize(size)
        val width: Int = size.x

        window?.setLayout((width * 0.9).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)
        window?.setGravity(Gravity.CENTER)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DialogCallingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            if (userPhoto == "") {
                ivUserProfile.setBackgroundResource(R.drawable.ic_user_avatar)
            } else {
                Glide.with(this@CallingDialogFragment)
                    .load(userPhoto)
                    .into(ivUserProfile)
            }

            tvUserName.text = userName
        }

        binding.btnCancel.setOnClickListener {
            dismiss()
        }

        binding.btnCallOrAccept.setOnClickListener {
            if (fromMe == true) {
                sendCallEvent()
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.sessionEvent.collect { event ->
                when (event) {
//                    is HomeViewModel.SessionEvent.OnSessionResponse -> {
//                        Log.d(
//                            TAG,
//                            "SessionResponse: ${event.sessionResponse}"
//                        )
//                        sessionResponse = event.sessionResponse
//                        sendCallStatusEvent(event.sessionResponse)
//                    }
                    is HomeViewModel.SessionEvent.OnGenerateTokenResponse -> {
                        Log.d(
                            TAG,
                            "Generate Token: ${event.sessionResponse}"
                        )
                        sessionResponse = event.sessionResponse
                        if (sessionResponse != null) {
//                            activity?.showToast("Starting now")
                            dismiss()
                            startActivity(
                                Intent(
                                    requireActivity(),
                                    CallingActivity::class.java
                                ).apply {
                                    putExtra("API_KEY", sessionResponse?.apiKey)
                                    putExtra("SESSION_ID", sessionResponse?.sessionId)
                                    putExtra("TOKEN", sessionResponse?.token)
                                    putExtra("ID", userId)
                                    putExtra("NAME", userName)
                                    putExtra("MEETING_TYPE", "Meeting")
                                    putExtra("MEETING_TIME", 2)
                                }
                            )

                        }
                    }
                }
            }
        }
    }

    private fun sendCallEvent() {
        val fromUserObject = JSONObject()
        try {
            fromUserObject.put("_id", prefs.userModel?.id)
            fromUserObject.put("name", prefs.userModel?.fullName)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val toUserObject = JSONObject()
        try {
            toUserObject.put("_id", userId)
            toUserObject.put("name", userName)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val jsonObject = JSONObject()
        try {
            jsonObject.put("from", fromUserObject)
            jsonObject.put("to", toUserObject)
            jsonObject.put("status", "calling")
            jsonObject.put("type", "Meeting")
            jsonObject.put("meetingId", meetingId)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Log.d(TAG, "Sending Calling Event: $jsonObject")

        socket.emit("scheduledMeeting", jsonObject)

        binding.tvCallingStatus.isVisible = true
        binding.tvCallingStatus.text = "Calling"

    }

    private fun initializeSocket() {
        socket.on(Socket.EVENT_CONNECT_ERROR) {
            Log.d(TAG, "CONNECTION ERROR: ${it[0]}")
        }

        socket.on(Socket.EVENT_DISCONNECT) {
            Log.d(TAG, "DISCONNECT: ${it[0]}")
        }

        socket.on("error") {
            Log.d(TAG, "Error: ${it[0]}")
        }

        socket.on("connect_timeout") {
            Log.d(TAG, "Connect timeout: ${it[0]}")
        }

        socket.on("reconnecting") {
            Log.d(TAG, "Reconnecting: ${it[0]}")
        }

        socket.on("reconnect_error") {
            Log.d(TAG, "Reconnect error: ${it[0]}")
        }

        socket.on("reconnect") {
            Log.d(TAG, "Reconnect: ${it[0]}")
        }

        socket.on("scheduledMeeting") { args ->
            Log.d(TAG, "Receiving Event: ${args[0]}")

            val callingResponse = gson.fromJson<CallingResponse>(
                args[0].toString(),
                CallingResponse::class.java
            )

            if (callingResponse.to?._id == prefs.userModel?.id) {
                if (callingResponse.status == "accepted") {
                    activity?.runOnUiThread {
                        activity?.showToast("Accepted by user")
                    }

                } else if (callingResponse.status == "rejected") {
                    activity?.runOnUiThread {
                        binding.tvCallingStatus.isVisible = true
                        binding.tvCallingStatus.text = "Meeting call rejected by user, try again"
                    }
                }
            }
        }

        socket.on("callStatus") { args ->
            Log.d(TAG, "Call Status Event: ${args[0]}")
            val userDetailsResponse = gson.fromJson<UserDetailsResponse>(
                args[0].toString(),
                UserDetailsResponse::class.java
            )
            viewModel.generateTokenWithSession(userDetailsResponse.sessionId.toString())
        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}