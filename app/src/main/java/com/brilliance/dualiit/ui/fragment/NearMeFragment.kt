package com.brilliance.dualiit.ui.fragment


import android.content.Intent
import android.graphics.drawable.InsetDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.brilliance.dualiit.R
import com.brilliance.dualiit.databinding.FragmentNearmeUserBinding
import com.brilliance.dualiit.ui.auth.activities.ChooseLoginSignup
import com.brilliance.dualiit.ui.base.fragment.BaseFragment
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.ui.game.PlayWithFriendActivity
import com.brilliance.dualiit.ui.home.activities.UserProfileActivity
import com.brilliance.dualiit.ui.home.adapters.NearMeSuggestAdapter
import com.brilliance.dualiit.ui.home.adapters.SearchAdapter
import com.brilliance.dualiit.ui.home.viewmodel.HomeViewModel
import com.brilliance.dualiit.ui.meeting.activities.MeetingsActivity
import com.brilliance.dualiit.utils.ConnectionLiveData
import com.brilliance.dualiit.utils.Resource
import com.brilliance.dualiit.utils.extensions.hide
import com.brilliance.dualiit.utils.extensions.launchActivity
import com.brilliance.dualiit.utils.extensions.show
import com.brilliance.dualiit.utils.extensions.showToast
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class NearMeFragment : BaseFragment() {
    private lateinit var binding: FragmentNearmeUserBinding
    private val viewModel: HomeViewModel by viewModel()
    private lateinit var suggestAdapter: NearMeSuggestAdapter
    private lateinit var searchUsersAdapter: SearchAdapter
    override fun baseViewModel(): BaseViewModel = viewModel
    private lateinit var connectionLiveData: ConnectionLiveData

    override fun createDataBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentNearmeUserBinding.inflate(inflater, container, false)
        connectionLiveData = ConnectionLiveData(requireContext())
        connectionLiveData.observe(viewLifecycleOwner) { isNetworkAvailable ->
            if (isNetworkAvailable) {
                setObserver()
                setupRecyclerView()
                binding.tvNoInternet.hide()

                lifecycleScope.launchWhenStarted {
                    viewModel.usersListState.collect {
                        when (it) {
                            is Resource.Empty -> {
                            }
                            is Resource.Error -> {
                                requireActivity().showToast(it.message)
                                binding.progressBar.hide()
                                if (it.errorCode == 401) {
                                    requireActivity().launchActivity(
                                        aClass = ChooseLoginSignup::class.java,
                                        isTopFinish = true,
                                        flagIsTopClearTask = true
                                    )
                                }
                            }
                            is Resource.Loading -> {
                                binding.progressBar.show()
                            }
                            is Resource.Success -> {
                                binding.progressBar.hide()
                                binding.recyclerMeeting.show()
                                suggestAdapter.nearMeList = it.data!!
                            }
                        }
                    }
                }

                viewModel.usersSearchList.observe(viewLifecycleOwner, {
                    if (it.isEmpty()) {
//                activity?.showToast("No data available")
                        binding.progressBar.isVisible = false
                    } else {
                        binding.progressBar.isVisible = false
                        binding.rvSearchUsers.isVisible = true
                        searchUsersAdapter.searchedUsersList = it
                    }
                })

                suggestAdapter.setOnItemClickListener {
                    Timber.d(it.id!!)
                    val userDetailsIntent = Intent(activity, UserProfileActivity::class.java)
                    userDetailsIntent.putExtra("USER_ID", it.id)
                    startActivity(userDetailsIntent)
                }

                searchUsersAdapter.setOnItemClickListener {
                    Timber.d(it._id.toString())
                    val userDetailsIntent = Intent(activity, UserProfileActivity::class.java)
                    userDetailsIntent.putExtra("USER_ID", it._id)
                    startActivity(userDetailsIntent)
                    viewModel.clearSearchList()
                    binding.etSearchUsers.setText("")
                    binding.rvSearchUsers.isVisible = false
                    binding.recyclerMeeting.isVisible = true
                }

                binding.btnMeetings.setOnClickListener {
                    startActivity(Intent(requireActivity(), MeetingsActivity::class.java))
                }

                var job: Job? = null
                binding.etSearchUsers.addTextChangedListener { editable ->
                    job?.cancel()
                    job = MainScope().launch {
                        delay(1000L)
                        editable?.let {
                            if (editable.toString().isNotEmpty()) {
                                binding.recyclerMeeting.isVisible = false
                                binding.rvSearchUsers.isVisible = true
                                viewModel.getSearchedUsers(editable.toString())
                            } else {
                                binding.recyclerMeeting.isVisible = true
                                binding.rvSearchUsers.isVisible = false
                            }
                        }
                    }
                }

            } else {
                binding.tvNoInternet.show()
                binding.recyclerMeeting.hide()
                binding.rvSearchUsers.hide()
            }
        }

        binding.btnGame.setOnClickListener {
            startActivity(Intent(requireActivity(), PlayWithFriendActivity::class.java))
        }

        return binding.root
    }

    private fun setupRecyclerView() {
        suggestAdapter = NearMeSuggestAdapter()
        searchUsersAdapter = SearchAdapter()
        binding.recyclerMeeting.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = suggestAdapter
            itemAnimator = null
            val attrs = intArrayOf(android.R.attr.listDivider)
            val a = context.obtainStyledAttributes(attrs)
            val divider = a.getDrawable(0)
            val insetLeft = resources.getDimensionPixelSize(R.dimen._40sdp)
            val insetRight = resources.getDimensionPixelSize(R.dimen._16sdp)
            val insetDivider = InsetDrawable(divider, insetLeft, 0, insetRight, 0)
            a.recycle()
            val itemDecoration =
                DividerItemDecoration(
                    this@NearMeFragment.requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            itemDecoration.setDrawable(insetDivider)
            addItemDecoration(itemDecoration)
            setHasFixedSize(true)
        }
        binding.rvSearchUsers.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = searchUsersAdapter
        }
    }


    override fun setObserver() {
        viewModel.getSuggestedUsersList()
        super.setBaseObserver()
    }


}