package com.brilliance.dualiit.ui.home.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.brilliance.dualiit.databinding.ItemInterestBinding
import com.brilliance.dualiit.model.InterestDoc

class InterestsAdapter(
    private val listener: OnItemClickListener
) : PagingDataAdapter<InterestDoc, InterestsAdapter.InterestsViewHolder>(
    INTERESTS_COMPARATOR
) {

    inner class InterestsViewHolder(private val binding: ItemInterestBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val item = getItem(position)
                    if (item != null) {
                        if (item.isSelected) {
                            binding.ivCancel.visibility = View.VISIBLE
                            binding.ivDone.visibility = View.INVISIBLE
                            item.isSelected = false
                            if (getSelectedInterests().isEmpty()) {
                                listener.onItemClick(false)
                            }
                        } else {
                            binding.ivCancel.visibility = View.INVISIBLE
                            binding.ivDone.visibility = View.VISIBLE
                            item.isSelected = true
                            listener.onItemClick(true)
                        }
                    }
                }
            }
        }

        fun bind(interest: InterestDoc) {
            binding.apply {
                tvInterestTitle.text = interest.name

                if (interest.isSelected) {
                    ivCancel.visibility = View.INVISIBLE
                    ivDone.visibility = View.VISIBLE
                } else {
                    ivCancel.visibility = View.VISIBLE
                    ivDone.visibility = View.INVISIBLE
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InterestsViewHolder {
        val binding =
            ItemInterestBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return InterestsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: InterestsViewHolder, position: Int) {
        val currentItem = getItem(position)
        if (currentItem != null) {
            holder.bind(currentItem)
        }
    }

    fun getSelectedInterests(): List<InterestDoc> {
        val selectedInterests = ArrayList<InterestDoc>()
        for (interest in snapshot().items) {
            if (interest.isSelected) {
                selectedInterests.add(interest)
            }
        }
        return selectedInterests
    }


    companion object {
        private val INTERESTS_COMPARATOR = object : DiffUtil.ItemCallback<InterestDoc>() {
            override fun areItemsTheSame(oldItem: InterestDoc, newItem: InterestDoc): Boolean {
                return oldItem._id == newItem._id
            }

            override fun areContentsTheSame(oldItem: InterestDoc, newItem: InterestDoc): Boolean {
                return oldItem == newItem
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(isSelected: Boolean)
    }


}