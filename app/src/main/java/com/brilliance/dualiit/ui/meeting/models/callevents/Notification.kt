package com.brilliance.dualiit.ui.meeting.models.callevents

data class Notification(
    val enabled: Int? = 0,
    val invitation: Int? = 0,
    val meetingReminder: Int? = 0,
    val previewOnScreen: Int? = 0,
    val sound: Int? = 0,
    val vibration: Int? = 0
)