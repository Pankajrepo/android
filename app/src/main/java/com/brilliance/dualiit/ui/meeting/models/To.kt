package com.brilliance.dualiit.ui.meeting.models

import androidx.annotation.Keep


@Keep
data class To(
    val _id: String? = "",
    val avatar: String? = "",
    val email: String? = "",
    val fullName: String? = "",
    val lastOnlineTime: String? = "",
    val onlineStatus: Boolean? = false,
    val phone: String? = ""
)