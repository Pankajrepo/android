package com.brilliance.dualiit.ui.auth.activities

import android.content.Intent
import android.os.Bundle
import com.brilliance.dualiit.databinding.ActivityStartAppBinding
import com.brilliance.dualiit.ui.auth.viewmodel.AuthViewModel
import com.brilliance.dualiit.ui.base.activity.BaseActivity
import com.brilliance.dualiit.ui.base.viewModel.BaseViewModel
import com.brilliance.dualiit.ui.home.activities.MainActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class ChooseLoginSignup : BaseActivity() {
    private lateinit var mBinding: ActivityStartAppBinding
    private val viewModel: AuthViewModel by viewModel()
    override fun baseViewModel(): BaseViewModel = viewModel
    override fun setObserver() {
        super.setBaseObserver()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBar(this)
        mBinding = ActivityStartAppBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        setObserver()

        mBinding.btnLogin.setOnClickListener {
            startActivity(Intent(this, Login::class.java))
        }

        mBinding.btnSignup.setOnClickListener {
            startActivity(Intent(this, SignUp::class.java))
        }

    }
}