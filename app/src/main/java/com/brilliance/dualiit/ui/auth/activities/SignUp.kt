package com.brilliance.dualiit.ui.auth.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.brilliance.dualiit.BuildConfig
import com.brilliance.dualiit.databinding.ActivitySignupBinding
import com.brilliance.dualiit.ui.auth.helpers.OTPEvent
import com.brilliance.dualiit.ui.auth.request.SignupRequest
import com.brilliance.dualiit.ui.auth.viewmodel.AuthViewModel
import com.brilliance.dualiit.utils.AppConstant
import com.brilliance.dualiit.utils.extensions.showToast
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class SignUp : AppCompatActivity() {
    private lateinit var binding: ActivitySignupBinding
    private val viewModel: AuthViewModel by viewModel()
    private var deviceToken: String? = ""

    private fun setObserver() {

        lifecycleScope.launchWhenStarted {
            viewModel.otpEvent.collect { event ->
                when (event) {
                    is OTPEvent.NumberAlreadyExist -> {
                        if (event.isExist) {
                            showToast(event.message)
                        } else {
//                            showToast(event.message)
                            viewModel.sendSMSCode()
                        }
                    }
                    is OTPEvent.OTPFailed -> {
                        showToast(event.message)
                    }
                    is OTPEvent.OTPSuccess -> {
                        showToast("OTP Sent Successfully")
                        val phoneNumber =
                            "${binding.countryCodePicker.selectedCountryCodeWithPlus}${
                                binding.etPhone.text.toString().trim()
                            }"
                        val signupRequest = SignupRequest()
                        signupRequest.appVersion = BuildConfig.VERSION_NAME
                        signupRequest.deviceType = BuildConfig.DEVICE_TYPE
                        signupRequest.deviceToken = deviceToken
                        signupRequest.fullName = binding.etName.text.toString()
                        signupRequest.phone = phoneNumber
                        signupRequest.password = binding.etPassword.text.toString()
                        startActivity(
                            Intent(this@SignUp, ValidateOTP::class.java).putExtra(
                                AppConstant.REQ_EXTRA,
                                signupRequest
                            )
                        )
                    }
                }
            }
        }


//        viewModel.receiveOTP.observe(this, Observer {
//            val phoneNumber =
//                "${binding.countryCodePicker.selectedCountryCodeWithPlus}${
//                    binding.etPhone.text.toString().trim()
//                }"
//            val signupRequest = SignupRequest()
//            signupRequest.appVersion = BuildConfig.VERSION_NAME
//            signupRequest.deviceType = BuildConfig.DEVICE_TYPE
//            signupRequest.deviceToken = deviceToken
//            signupRequest.fullName = binding.etName.text.toString()
//            signupRequest.phone = phoneNumber
//            signupRequest.password = binding.etPassword.text.toString()
//            startActivity(
//                Intent(this, ValidateOTP::class.java).putExtra(
//                    AppConstant.REQ_EXTRA,
//                    signupRequest
//                )
//            )
//
//        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignupBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setObserver()

        FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.d("FIREBASE_TOKEN", "Fetching FCM registration token failed", task.exception)
                return@addOnCompleteListener
            }

            deviceToken = task.result
            Log.d("FIREBASE_TOKEN", "TOKEN $deviceToken")
        }

        binding.btnSendCode.setOnClickListener {

            if (!validateName() || !validatePhoneNumber() || !validatePassword() || !validateConfirmPassword()) {
                return@setOnClickListener
            }
            val phoneNumber =
                "${binding.countryCodePicker.selectedCountryCodeWithPlus}${
                    binding.etPhone.text.toString().trim()
                }"
            viewModel.phone.value = phoneNumber
            Log.d("SignUpRequest", "PHONE NUMBER: $phoneNumber")
            viewModel.checkUserAlreadyExist()
        }

        binding.termOfUse.setOnClickListener {
//            startActivity(Intent(this, TermsOfUse::class.java))
            openInBrowser()
        }

        binding.ivNavigationBack.setOnClickListener {
            onBackPressed()
        }

    }

    private fun openInBrowser() {
        val openURL = Intent(Intent.ACTION_VIEW)
        openURL.data = Uri.parse(AppConstant.TERMS_OF_USE_URL)
        startActivity(openURL)
    }


    private fun validateName(): Boolean {
        val name = binding.tiName.editText?.text.toString().trim()
        return if (name.isEmpty()) {
            binding.tiName.error = "Name can not be empty"
            false
        } else {
            binding.tiName.error = null
            binding.tiName.isErrorEnabled = false
            true
        }
    }

    private fun validatePhoneNumber(): Boolean {
        val phoneNumber = binding.tiPhone.editText?.text.toString().trim()
        return when {
            phoneNumber.isEmpty() -> {
                binding.tiPhone.error = "Phone Number can not be empty"
//                binding.tiPhone.requestFocus()
                false
            }
            phoneNumber.length < 10 -> {
                binding.tiPhone.error = "Invalid phone number"
//                binding.tiPhone.requestFocus()
                false
            }
//            !phoneNumber.contains("+") -> {
//                binding.tiPhone.error = "Please enter phone number with country code"
////                binding.tiPhone.requestFocus()
//                false
//            }
            else -> {
                binding.tiPhone.error = null
                binding.tiPhone.isErrorEnabled = false
                true
            }
        }
    }

    private fun validatePassword(): Boolean {
        val password = binding.tiPassword.editText?.text.toString().trim()
        return when {
            password.isEmpty() -> {
                binding.tiPassword.error = "Password can not be empty"
//                binding.tiPassword.requestFocus()
                false
            }
            password.length < 6 -> {
                binding.tiPassword.error = "Password must be 6 characters long"
//                binding.tiPassword.requestFocus()
                false
            }
            else -> {
                binding.tiPassword.error = null
                binding.tiPassword.isErrorEnabled = false
                true
            }
        }
    }

    private fun validateConfirmPassword(): Boolean {
        val password = binding.tiConfirmPassword.editText?.text.toString().trim()
        return when {
            password.isEmpty() -> {
                binding.tiConfirmPassword.error = "Confirm Password can not be empty"
//                binding.tiPassword.requestFocus()
                false
            }
            password != binding.tiPassword.editText?.text.toString() -> {
                binding.tiConfirmPassword.error = "Password not matched"
//                binding.tiPassword.requestFocus()
                false
            }
            else -> {
                binding.tiConfirmPassword.error = null
                binding.tiConfirmPassword.isErrorEnabled = false
                true
            }
        }
    }

}