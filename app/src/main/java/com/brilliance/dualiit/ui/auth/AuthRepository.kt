package com.brilliance.dualiit.ui.auth

import com.brilliance.dualiit.model.BaseResponse
import com.brilliance.dualiit.model.UserModel
import com.brilliance.dualiit.ui.auth.request.LoginRequest
import com.brilliance.dualiit.ui.auth.request.ResetRequest
import com.brilliance.dualiit.ui.auth.request.SignupRequest
import com.brilliance.dualiit.utils.AppPreferencesHelper
import com.brilliance.dualiit.utils.networkRequest.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class AuthRepository(
    private val apiService: ApiService,
    private val prefs: AppPreferencesHelper
) {

    fun checkUserExist(phone: String, email: String): Flow<BaseResponse<Any>> = flow {
        emit(apiService.checkExist(phone, email))
    }.flowOn(Dispatchers.IO)


    fun sendSmsCode(phone: String): Flow<BaseResponse<Any>> = flow {
        emit(apiService.sendSMSCode(phone))
    }.flowOn(Dispatchers.IO)


    fun signUP(signupRequest: SignupRequest): Flow<BaseResponse<UserModel>> = flow {
        emit(apiService.signUp(signupRequest))
    }.flowOn(Dispatchers.IO)

    fun loginUser(loginRequest: LoginRequest): Flow<BaseResponse<UserModel>> = flow {
        emit(apiService.login(loginRequest))
    }.flowOn(Dispatchers.IO)

    fun resetPassword(resetRequest: ResetRequest): Flow<BaseResponse<UserModel>> = flow {
        emit(apiService.resetPwd(resetRequest))
    }.flowOn(Dispatchers.IO)

    fun changePassword(oldPassword: String, newPassword: String): Flow<BaseResponse<Any>> = flow {
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        emit(apiService.changePassword(map, oldPassword, newPassword))
    }.flowOn(Dispatchers.IO)

    fun deleteMyAccount(): Flow<BaseResponse<Any>> = flow {
        val map = HashMap<String, String>()
        map["Authorization"] = prefs.authToken
        emit(apiService.deleteMyAccount(map))
    }.flowOn(Dispatchers.IO)
}